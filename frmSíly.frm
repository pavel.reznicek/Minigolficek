VERSION 5.00
Begin VB.Form frmS�ly 
   AutoRedraw      =   -1  'True
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "S�ly p�sob�c� na m��ek"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   405
   ClientWidth     =   4680
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Label lblLegenda 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sm�r"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Index           =   3
      Left            =   0
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label lblLegenda 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "pamSp�d"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF00FF&
      Height          =   195
      Index           =   2
      Left            =   0
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.Label lblLegenda 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Okam�it� sm�r"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   195
      Index           =   1
      Left            =   0
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblLegenda 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vektor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.Shape shpM��ek 
      Height          =   615
      Left            =   0
      Shape           =   3  'Circle
      Top             =   0
      Width           =   615
   End
   Begin VB.Menu nabKP 
      Caption         =   "<kontroln� pole>"
      Enabled         =   0   'False
      Visible         =   0   'False
   End
End
Attribute VB_Name = "frmS�ly"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim D�le As Boolean

Private Sub Form_Click()
'    Dim Legenda As Label
'    For Each Legenda In lblLegenda
'        Legenda.Visible = Not Legenda.Visible
'    Next
End Sub

Private Sub Form_Load()
    DefTPP
    Width = Width - ScaleWidth + 150 * TPPX
    Height = Height - ScaleHeight + 150 * TPPY
    Move frmMinigolf��ek.Left - Width, frmMinigolf��ek.Top + frmMinigolf��ek.Height - Height
    shpM��ek.Move ScaleLeft, ScaleTop, ScaleWidth, ScaleHeight
    Me.AutoRedraw = True
    Me.PaintPicture frmMinigolf��ek.obrVzorOkraje, 0, 0, ScaleWidth, ScaleHeight
    Me.Picture = Me.Image
    Me.AutoRedraw = frmMinigolf��ek.nabZpomalit
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Hide
        frmPr��ez.Hide
        frmMinigolf��ek.nabS�ly.Checked = False
        Cancel = True
    End If
End Sub

Private Sub lblLegenda_Click(Index As Integer)
    Dim Legenda As Label
    For Each Legenda In lblLegenda
        Legenda.Visible = Not Legenda.Visible
    Next
End Sub
