VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmEditor 
   Caption         =   "Editor reli�fu"
   ClientHeight    =   4620
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   308
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   305
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CD 
      Left            =   1200
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".rel"
      Filter          =   "Reli�fy (*.rel)|*.rel"
   End
   Begin VB.PictureBox obrNorm�l 
      AutoRedraw      =   -1  'True
      Height          =   495
      Left            =   2160
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   3
      Top             =   0
      Width           =   495
   End
   Begin VB.PictureBox obrZv�t�en� 
      AutoRedraw      =   -1  'True
      Height          =   495
      Left            =   0
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   2
      Top             =   0
      Width           =   495
   End
   Begin Golf5.Stupn� opStupn� 
      Height          =   4050
      Left            =   2880
      TabIndex        =   1
      Top             =   0
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   7144
      Nejni���        =   32768
   End
   Begin Golf5.���kaV��ka op���kaV��ka 
      Height          =   540
      Left            =   2880
      TabIndex        =   0
      Top             =   4080
      Width           =   1695
      _ExtentX        =   2778
      _ExtentY        =   953
      ���ka           =   32
      V��ka           =   32
   End
   Begin VB.Menu nabSoubor 
      Caption         =   "&Soubor"
      Begin VB.Menu nabOtev��t 
         Caption         =   "&Otev��t"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabUlo�it 
         Caption         =   "&Ulo�it"
         Shortcut        =   ^U
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabNejvy��� 
         Caption         =   "Barva nej&vy��� polohy �"
      End
      Begin VB.Menu nabNejni��� 
         Caption         =   "Barva nej&ni��� polohy �"
      End
   End
End
Attribute VB_Name = "frmEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Mapa() As Long
Private Sub Form_Load()
    With obrZv�t�en�
    .ScaleWidth = .ScaleWidth / 4
    .ScaleHeight = .ScaleHeight / 4
    End With
    P�izp�sobRozm�ry
End Sub

Private Sub nabNejni���_Click()
    CD.Color = opStupn�.Nejni���
    CD.Flags = cdlCCRGBInit
    On Error GoTo vypadni
    CD.ShowColor
    On Error GoTo 0
    opStupn�.Nejni��� = CD.Color
vypadni:
    On Error GoTo 0
End Sub

Private Sub nabNejvy���_Click()
    CD.Color = opStupn�.Nejvy���
    CD.Flags = cdlCCRGBInit
    On Error GoTo vypadni
    CD.ShowColor
    On Error GoTo 0
    opStupn�.Nejvy��� = CD.Color
vypadni:
    On Error GoTo 0
End Sub

Private Sub nabOtev��t_Click()
    CD.InitDir = App.Path
    On Error GoTo vypadni
    CD.ShowOpen
    Open CD.filename For Binary As #1
    Get #1, , Mapa
    Close
    Dim X, Y
    For Y = 0 To op���kaV��ka.V��ka
        For X = 0 To op���kaV��ka.���ka
            obrZv�t�en�.Line (X, Y)-(X + 3 / 4, Y + 3 / 4), opStupn�.BarvaPodleV��ky(Mapa(X, Y)), BF
            obrNorm�l.PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
        Next
    Next
vypadni:
End Sub

Private Sub nabUlo�it_Click()
    CD.InitDir = App.Path
    On Error GoTo vypadni
    CD.ShowSave
    Open CD.filename For Binary As #1
    Put #1, , Mapa
    Close
vypadni:
End Sub

Private Sub obrNorm�l_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrNorm�l.ScaleWidth _
    Or Y > obrNorm�l.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub obrNorm�l_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrNorm�l.ScaleWidth _
    Or Y > obrNorm�l.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub obrNorm�l_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrNorm�l.ScaleWidth _
    Or Y > obrNorm�l.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub obrZv�t�en�_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrZv�t�en�.ScaleWidth _
    Or Y > obrZv�t�en�.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub obrZv�t�en�_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrZv�t�en�.ScaleWidth _
    Or Y > obrZv�t�en�.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub obrZv�t�en�_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Or X < 0 Or Y < 0 _
    Or X > obrZv�t�en�.ScaleWidth _
    Or Y > obrZv�t�en�.ScaleHeight Then Exit Sub
    obrZv�t�en�.Line (CLng(X), CLng(Y))-(CLng(X) + 3 / 4, CLng(Y) + 3 / 4), opStupn�.Barva, BF
    obrNorm�l.PSet (CLng(X), CLng(Y)), opStupn�.Barva
    Mapa(CLng(X), CLng(Y)) = opStupn�.V��ka
End Sub

Private Sub op���kaV��ka_Change()
    P�izp�sobRozm�ry
End Sub

Private Sub P�izp�sobRozm�ry()
    With obrZv�t�en�
    .Move 0, 0, _
    op���kaV��ka.���ka * 4 + (.Width - .ScaleWidth * 4), _
    op���kaV��ka.V��ka * 4 + (.Height - .ScaleHeight * 4)
    End With
    With obrNorm�l
    .Move .Left, .Top, _
    op���kaV��ka.���ka + (.Width - .ScaleWidth), _
    op���kaV��ka.V��ka + (.Height - .ScaleHeight)
    End With
    ReDim Mapa(op���kaV��ka.���ka, op���kaV��ka.V��ka) As Long
End Sub

