VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "XYObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim pamX#, pamY#, pamD�lka#, pam�hel#


'Vlastnosti sou�adnicov�
Public Property Let X(Nosi� As Double)
    If Nosi� <> pamX Then
        pamX = Nosi�
        ObnovD�
    End If
End Property

Public Property Get X() As Double
    X = pamX
End Property

Public Property Let Y(Nosi� As Double)
    If Nosi� <> pamY Then
        pamY = Nosi�
        ObnovD�
    End If
End Property

Public Property Get Y() As Double
    Y = pamY
End Property

Public Sub NastavXY(Rozm�rX As Double, Rozm�rY As Double)
Attribute NastavXY.VB_UserMemId = 0
    pamX = Rozm�rX
    pamY = Rozm�rY
	ObnovD�
End Sub


'Vlastnosti sm�rov�
Public Property Let �hel(Nosi� As Double)
    Nosi� = Abs�hel(Nosi�)
    If pam�hel <> Nosi� Then
        pam�hel = Nosi�
        ObnovXY
    End If
End Property

Public Property Get �hel() As Double
    �hel = pam�hel
End Property

Public Property Let D�lka(Nosi� As Double)
    If pamD�lka <> Nosi� Then
        pamD�lka = Nosi�
        ObnovXY
    End If
End Property

Public Property Get D�lka() As Double
    D�lka = pamD�lka
End Property


Private Sub ObnovD�()
    pamD�lka = (pamX ^ 2 + pamY ^ 2) ^ (1 / 2)
    pam�hel = Arcus(CXY(pamX, pamY))
End Sub

Private Sub ObnovXY()
    If pam�hel = -1 Or pamD�lka = 0 Then
        pamX = 0
        pamY = 0
        Exit Sub
    End If
    Select Case pam�hel
    Case 0 To 180
        pamX = Sin(StupNaRad(pam�hel)) * pamD�lka
        pamY = -Cos(StupNaRad(pam�hel)) * pamD�lka
    Case Else
        pamX = Sin(StupNaRad(pam�hel)) * pamD�lka
        pamY = -Cos(StupNaRad(pam�hel)) * pamD�lka
    End Select
End Sub

