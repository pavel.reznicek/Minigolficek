VERSION 5.00
Begin VB.Form frmV�b�r 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Volba prvku dr�hy"
   ClientHeight    =   3360
   ClientLeft      =   3105
   ClientTop       =   870
   ClientWidth     =   5415
   Icon            =   "frmV�b�r.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   224
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   361
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox obr 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   540
      Index           =   2
      Left            =   4170
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   7
      Top             =   1440
      Width           =   540
   End
   Begin VB.PictureBox obr 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   540
      Index           =   1
      Left            =   4170
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   5
      Top             =   240
      Width           =   540
   End
   Begin VB.TextBox txtV�b�r 
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   240
      Width           =   3375
   End
   Begin VB.CommandButton tlZru�it 
      Cancel          =   -1  'True
      Caption         =   "&Zru�it"
      Height          =   375
      Left            =   4560
      TabIndex        =   4
      Top             =   2640
      Width           =   735
   End
   Begin VB.CommandButton tlOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   3600
      TabIndex        =   3
      Top             =   2640
      Width           =   735
   End
   Begin VB.FileListBox FL 
      Height          =   2625
      Left            =   120
      Pattern         =   "*.rel"
      TabIndex        =   2
      Top             =   600
      Width           =   3375
   End
   Begin VB.Label lbl 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Height          =   495
      Index           =   2
      Left            =   3600
      TabIndex        =   8
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label lbl 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Height          =   495
      Index           =   1
      Left            =   3600
      TabIndex        =   6
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label lblDruh 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "&Prvky:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3375
   End
End
Attribute VB_Name = "frmV�b�r"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Nic As Boolean
Dim P�v���kaN�pisu As Single
Dim P�vV��ka As Single
Dim Odsazen� As Single
Public opStupn� As Stupn�
Public Prvek As String
Public Reli�f As String
Public Textura As String
Dim pamDruh As DruhV�b�ru


Private Sub FL_Click()
    If FL.ListIndex = -1 Then Exit Sub
    If Not txtV�b�r.Locked Then
        txtV�b�r = FL
    Else
        txtV�b�r.Locked = False
    End If
    On Error Resume Next
    Select Case Druh
    Case dvPrvek
        Prvek = FL
        Reli�f = Prvek
        Textura = Hol�Jm�no(Prvek) & ".bmp"
        lbl(2) = Reli�f
        lbl(1) = Textura
        NakresliRel 2
        obr(1).Picture = LoadPicture(App.Path & "\Prvky\" & Textura)
        lbl(1).Top = obr(1).Top + obr(1).Height + 7
        obr(2).Top = lbl(1).Top + lbl(1).Height + 7
        lbl(2).Top = obr(2).Top + obr(2).Height + 7
        tlOK.Top = lbl(2).Top + lbl(2).Height + 7
    Case dvReli�f
        Reli�f = FL
        lbl(1) = Reli�f
        NakresliRel 1
        lbl(1).Top = obr(1).Top + obr(1).Height + 7
        tlOK.Top = lbl(1).Top + lbl(1).Height + 7
    Case dvTextura
        Textura = FL
        lbl(1) = Textura
        obr(1).Picture = LoadPicture(App.Path & "\Prvky\" & Textura)
        lbl(1).Top = obr(1).Top + obr(1).Height + 7
        tlOK.Top = lbl(1).Top + lbl(1).Height + 7
    Case Else
        MsgBox "Chybn� druh v�b�ru!", vbExclamation
    End Select
    tlZru�it.Top = tlOK.Top
    Rozm��
End Sub

Private Sub Rozm��()
    With obr(1)
    If .Width > P�v���kaN�pisu Then
        lbl(1).Width = .Width
        .Left = lbl(1).Left
    Else
        lbl(1).Width = P�v���kaN�pisu
        .Left = lbl(1).Left + lbl(1).Width / 2 - .Width / 2
    End If
    End With
    Width = Width - ScaleWidth * TPPX + (lbl(1).Left + lbl(1).Width + Odsazen�) * TPPX
    With obr(2)
    If .Width > P�v���kaN�pisu Then
        lbl(2).Width = .Width
        .Left = lbl(2).Left
    Else
        lbl(2).Width = P�v���kaN�pisu
        .Left = lbl(2).Left + lbl(2).Width / 2 - .Width / 2
    End If
    End With
    If Spodek(tlOK) > P�vV��ka - Odsazen� Then
        Height = Height - ScaleHeight * TPPY + (Spodek(tlOK) + Odsazen�) * TPPY
    Else
        Height = Height + (-ScaleHeight + P�vV��ka) * TPPY
    End If
    FL.Height = ScaleHeight - FL.Top - Odsazen�
    'Width = Width - ScaleWidth * TPPX + (lbl(2).Left + lbl(2).Width + Odsazen�Vpravo) * TPPX
End Sub

Private Sub NakresliRel(Kam As Integer)
    Open App.Path & "\Prvky\" & Reli�f For Binary As #1
    Dim ���ka As Integer, V��ka As Integer
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka <= 0 Or V��ka <= 0 Then Close: Exit Sub
    ReDim Mapa(���ka - 1, V��ka - 1) As Single
    Get #1, , Mapa()
    Close
    Dim X, Y
    For Y = 0 To V��ka - 1
        For X = 0 To ���ka - 1
            If opStupn� Is Nothing Then
                Dim D�l, Zelen�D�l
                D�l = (Mapa(X, Y)) / 128 * 255
                If D�l >= 0 Then
                    Zelen�D�l = D�l
                Else
                    Zelen�D�l = 0
                    D�l = -D�l
                End If
                obr(Kam).PSet (X, Y), RGB(D�l, Zelen�D�l, D�l)
            Else
                obr(Kam).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
            End If
        Next
    Next
    
    With obr(Kam)
    .Move .Left, .Top, _
    .Width - .ScaleWidth + ���ka, _
    .Height - .ScaleHeight + V��ka
    End With

    
End Sub
'
'Private Sub Form_Activate()
'    txtV�b�r.Locked = False
'End Sub
'
'Private Sub Form_Deactivate()
'    txtV�b�r.Locked = True
'End Sub

Private Sub FL_DblClick()
    FL_Click
    tlOK_Click
End Sub

Private Sub FL_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then Ob�erstvi
End Sub

Private Sub Form_Activate()
    Ob�erstvi
End Sub

Private Sub Ob�erstvi()
    Dim Jm�no, I
    Jm�no = FL
    FL.Refresh
    For I = 0 To FL.ListCount - 1
        If FL.List(I) = Jm�no Then
            FL.ListIndex = I
            Exit For
        End If
    Next
    Rozm��
End Sub

Private Sub Form_Click()
    Ob�erstvi
End Sub

Private Sub Form_Load()
    ' M�ry
    DefTPP
    P�v���kaN�pisu = lbl(1).Width
    Odsazen� = ScaleWidth - (lbl(1).Left + lbl(1).Width)
    ' Uspo��d�n�
    With obr(1)
    .Move .Left, .Top, .Width - .ScaleWidth + 32, .Height - .ScaleHeight + 32
    End With
    With obr(2)
    .Move .Left, .Top, .Width - .ScaleWidth + 32, .Height - .ScaleHeight + 32
    End With
    lbl(1).Top = obr(1).Top + obr(1).Height + 7
    obr(2).Top = lbl(1).Top + lbl(1).Height + 7
    lbl(2).Top = obr(2).Top + obr(2).Height + 7
    tlOK.Top = lbl(2).Top + lbl(2).Height + 7
    Me.Height = Me.Height - ScaleHeight * TPPY + (FL.Top + FL.Height + Odsazen�) * TPPY
    P�vV��ka = ScaleHeight
    Rozm��
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
    Case vbFormControlMenu
        Cancel = True
        Nic = True
        Hide
    Case Else
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Nic = True
End Sub

Private Sub tlOK_Click()
    If txtV�b�r <> "" Then
        If Left(Caption, 6) = "P�idat" _
        Or Left(Caption, 7) = "Otev��t" Then
            If txtV�b�r <> FL Then Exit Sub
        End If
        Select Case Druh
        Case dvPrvek
            If Right(txtV�b�r, 4) <> ".rel" Then txtV�b�r = Hol�Jm�no(txtV�b�r) & ".rel"
            Reli�f = txtV�b�r
            If Right(txtV�b�r, 4) <> ".bmp" Then txtV�b�r = Hol�Jm�no(txtV�b�r) & ".bmp"
            Textura = txtV�b�r
        Case dzReli�f
            If Right(txtV�b�r, 4) <> ".rel" Then txtV�b�r = Hol�Jm�no(txtV�b�r) & ".rel"
            Reli�f = txtV�b�r
        Case dzTextura
            If Right(txtV�b�r, 4) <> ".bmp" Then txtV�b�r = Hol�Jm�no(txtV�b�r) & ".bmp"
            Textura = txtV�b�r
        Case Else
            MsgBox "Chybn� �daj o druhu v�b�ru!", vbCritical
        End Select
        Nic = False
        Hide
    End If
End Sub

Private Sub tlZru�it_Click()
    Nic = True
    Hide
End Sub

Private Sub txtV�b�r_Change()
    Dim I, �sp�ch As Boolean
    For I = 0 To FL.ListCount - 1
        If Left(FL.List(I), Len(txtV�b�r)) = txtV�b�r And txtV�b�r <> "" Then
            txtV�b�r.Locked = True
            'If Not �sp�ch Then FL.TopIndex = i
            txtV�b�r.Locked = False
            �sp�ch = True
        End If
        If FL.List(I) = txtV�b�r Then
            FL.ListIndex = I
        End If
        If �sp�ch Then Exit For
    Next
    txtV�b�r.Locked = True
    If Not �sp�ch Then
        FL.ListIndex = -1
    Else
        FL.Selected(I) = True
    End If
    txtV�b�r.Locked = False
End Sub

Public Property Let Druh(Nosi� As DruhV�b�ru)
    pamDruh = Nosi�
    Select Case Nosi�
    Case dvPrvek
        lblDruh = "&Prvky"
        FL.Path = App.Path & "\Prvky\"
        FL.Pattern = "*.rel"
        txtV�b�r.Locked = True
        txtV�b�r = Prvek
        txtV�b�r.Locked = False
        obr(1).Visible = True
        obr(2).Visible = True
        lbl(1).Visible = True
        lbl(2).Visible = True
        tlOK.Top = lbl(2).Top + lbl(2).Height + 7
    Case dzReli�f
        lblDruh = "&Reli�fy"
        FL.Path = App.Path & "\Prvky\"
        FL.Pattern = "*.rel"
        txtV�b�r.Locked = True
        txtV�b�r = Reli�f
        txtV�b�r.Locked = False
        obr(1).Visible = True
        obr(2).Visible = False
        lbl(1).Visible = True
        lbl(2).Visible = False
        tlOK.Top = lbl(1).Top + lbl(1).Height + 7
    Case dzTextura
        lblDruh = "&Textury"
        FL.Path = App.Path & "\Prvky\"
        FL.Pattern = "*.bmp"
        txtV�b�r.Locked = True
        txtV�b�r = Textura
        txtV�b�r.Locked = False
        obr(1).Visible = True
        obr(2).Visible = False
        lbl(1).Visible = True
        lbl(2).Visible = False
        tlOK.Top = lbl(1).Top + lbl(1).Height + 7
    Case Else
        MsgBox "Nespr�vn� typ v�b�ru!", vbExclamation
    End Select
    'FL.Refresh 'Zru�� v�b�r! Ne!
    tlZru�it.Top = tlOK.Top
    Ob�erstvi
End Property

Public Property Get Druh() As DruhV�b�ru
    Druh = pamDruh
End Property

Private Sub txtV�b�r_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDown Then
        txtV�b�r = FL.FileName
    End If
    If KeyCode = vbKeyF5 Then Ob�erstvi
End Sub
