VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmV�sl 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "V�sledky"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5190
   Icon            =   "frmV�sl.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   253
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   346
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox obrO��z 
      AutoRedraw      =   -1  'True
      Height          =   855
      Left            =   3720
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   61
      TabIndex        =   4
      Top             =   600
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSComctlLib.ListView lvV�sl 
      Height          =   2175
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      _Version        =   393217
      Icons           =   "ilV�sl"
      SmallIcons      =   "ilV�sl"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Jm�no"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   1
         Text            =   "Den"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "�as"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "Po�et �der�"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ImageList ilV�sl 
      Left            =   3840
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmV�sl.frx":0442
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmV�sl.frx":0896
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmV�sl.frx":0CEA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton tlOK 
      Cancel          =   -1  'True
      Caption         =   "&OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   3360
      Width           =   495
   End
   Begin VB.CommandButton tlSmazatV�e 
      Caption         =   "&Smazat v�e"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   3120
      Width           =   4815
   End
   Begin VB.Data datV�sl 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\Tvorba\MG_1_1\V�sledky.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   240
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "V�sledky"
      Top             =   3360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton tlSmazatV�slDr 
      Caption         =   "&Smazat v�sledky t�to dr�hy"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   2880
      Width           =   4815
   End
End
Attribute VB_Name = "frmV�sl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Activate()
    lvV�sl.SetFocus
End Sub

Private Sub Form_Load()
'    lvV�sl.Move 0, 0, 324, 343
    Me.Width = frmMinigolf��ek.Width
    Me.Height = frmMinigolf��ek.Height
    lvV�sl.Move 0, 0, ScaleWidth, ScaleHeight - tlSmazatV�slDr.Height - 1
    'tlOK.Left = ScaleWidth / 2 - tlOK.Width / 2
    tlOK.Width = 60
    tlSmazatV�slDr.Move tlOK.Width + 5, Spodek(lvV�sl) + 1, 180
    tlSmazatV�e.Move Pravobok(tlSmazatV�slDr), tlSmazatV�slDr.Top, ScaleWidth - Pravobok(tlSmazatV�slDr)
    tlOK.Move 0, tlSmazatV�slDr.Top, tlOK.Width, tlSmazatV�slDr.Height
    'tlOK.Top = Spodek(tlSmazatV�slDr)
    'Me.Height = Height - ScaleHeight * TPPY + Spodek(tlOK) * TPPY '+ 8 * TPPY
    lvV�sl.ColumnHeaders(1).Width = 1250 / TPPX
    lvV�sl.ColumnHeaders(2).Width = 1000 / TPPX
    lvV�sl.ColumnHeaders(3).Width = 1000 / TPPX
    lvV�sl.ColumnHeaders(4).Width = 1250 / TPPX
    datV�sl.DatabaseName = App.Path & "\V�sledky.mdb"
End Sub

Private Sub lvV�sl_ColumnClick(ByVal ColumnHeader As msComctlLib.ColumnHeader)
    If ColumnHeader.Index = 4 Then P�inuluj
    If lvV�sl.SortKey = ColumnHeader.Index - 1 Then
        lvV�sl.SortOrder = -Not CBool(lvV�sl.SortOrder)
    Else
        lvV�sl.SortKey = ColumnHeader.Index - 1
    End If
    lvV�sl.Sorted = True
    If ColumnHeader.Index = 4 Then Odnuluj
End Sub

Private Sub tlOK_Click()
    Unload Me
End Sub

Public Sub Zapi�V�sledek(Dr�ha As String, Jm�no As String, Po�et�der� As Long)
    With datV�sl.Recordset
    If .RecordCount > 0 Then .MoveFirst
    .AddNew
    !Dr�ha = Dr�ha
    !Jm�no = Jm�no
    !�dery = Po�et�der�
    !datum = Date
    !�as = Time
    .Update
    .Bookmark = .LastModified
    Posledn�Z�pis = .AbsolutePosition
    End With
End Sub

Private Property Let Posledn�Z�pis(Nosi� As Integer)
    Ulo�Nastaven� "Nastaven�", "Posledn� z�pis", Nosi�
End Property

Private Property Get Posledn�Z�pis() As Integer
    Posledn�Z�pis = DejNastaven�("Nastaven�", "Posledn� z�pis", 1)
End Property

Public Sub Uka�V�sledky(Dr�ha As String)
    Unload Me
    DoEvents
    Me.Show
'    frmMinigolf��ek.obrTer�n.Line (0, 0)-(0, frmMinigolf��ek.obrTer�n.ScaleHeight), vbButtonFace
    SrovnejRozm�ry obrO��z, frmMinigolf��ek.obrR�me�ek
    obrO��z.PaintPicture frmMinigolf��ek.obrTer�n.Image, 0, 0, , , 1, 1
    lvV�sl.Picture = obrO��z.Image
    lvV�sl.ListItems.Clear
    Me.Caption = "Listina v�t�z� na dr�ze " & Dr�ha
    With datV�sl.Recordset
    If .RecordCount > 0 Then
        .MoveLast
        .MoveFirst
    End If
    Dim ��t As Long
    Do
        ��t = ��t + 1
        If ��t = 1 Then
            .FindFirst "Dr�ha = '" & Dr�ha & "'"
        Else
            .FindNext "Dr�ha = '" & Dr�ha & "'"
        End If
        If .NoMatch Then Exit Do
        Dim Ikonka As Integer
        Select Case !�dery
        Case Is < 7
            Ikonka = 1
        Case 7
            Ikonka = 2
        Case Is > 7
            Ikonka = 3
        Case Else
        End Select
        lvV�sl.ListItems.Add ��t, , !Jm�no, Ikonka, Ikonka
        lvV�sl.ListItems(��t).SubItems(1) = Format(!datum, "dd.mm.yyyy", vbUseSystemDayOfWeek)
        lvV�sl.ListItems(��t).SubItems(2) = Format(!�as, "hh:mm:ss", vbUseSystemDayOfWeek)
        lvV�sl.ListItems(��t).SubItems(3) = Format(!�dery, "##########")
        If .AbsolutePosition = Posledn�Z�pis Then lvV�sl.SelectedItem = lvV�sl.ListItems(��t)
    Loop
    End With
    Me.Show , frmMinigolf��ek
    P�inuluj
    lvV�sl.SortKey = 3
    lvV�sl.Sorted = True
    Odnuluj
End Sub

Private Sub P�inuluj()
    Dim I As msComctlLib.ListItem
    For Each I In lvV�sl.ListItems
         I.SubItems(3) = Format(I.SubItems(3), "0000000000")
    Next
End Sub

Private Sub Odnuluj()
    Dim I As msComctlLib.ListItem
    For Each I In lvV�sl.ListItems
         I.SubItems(3) = Val(I.SubItems(3))
    Next
End Sub

Public Sub Sma�V�sledky(Dr�ha As String)
    With datV�sl.Recordset
    .MoveFirst
    Dim ��t As Long
    Do
        ��t = ��t + 1
        .FindNext "Dr�ha = '" & Dr�ha & "'"
        If .NoMatch Then Exit Do
        .Delete
        .Bookmark = .LastModified
    Loop
    Posledn�Z�pis = .AbsolutePosition
    End With
    lvV�sl.ListItems.Clear
End Sub

Private Sub tlSmazatV�e_Click()
    With datV�sl.Recordset
    .MoveFirst
    Dim ��t As Long
    Do While Not .EOF
        ��t = ��t + 1
        .Delete
        .MoveNext
    Loop
    Posledn�Z�pis = .AbsolutePosition
    End With
    lvV�sl.ListItems.Clear
End Sub

Private Sub tlSmazatV�slDr_Click()
    Sma�V�sledky frmMinigolf��ek.Jm�noDr�hy
End Sub
