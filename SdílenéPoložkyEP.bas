Attribute VB_Name = "Sd�len�Polo�ky"
Option Explicit

Public Sd�len�MapaPrvku() As Single
Public MapaM��ku(14, 14) As Variant
Public TPPX As Integer, TPPY As Integer
Public Z�sobn�k As New Z�sobn�k

Public Type XY
    x As Double
    y As Double
End Type

Public Type XYLng
    x As Long
    y As Long
End Type

Public Type XYByte
    x As Byte
    y As Byte
End Type

Public Type XYZ
    x As Double
    y As Double
    Z As Double
End Type

Public Type XYLngZVar
    x As Long
    y As Long
    Z As Variant
End Type

Public Enum Slo�kyVektoru
    slX = 0
    slY = 1
    slZ = 2
    slXY = -1
    slXZ = -2
    slYZ = -3
    slXYZ = -4
End Enum

Public Type VD� ' vektor ur�en� d�lkou a �hlem
    D�lka As Double
    �hel As Double
End Type


Public Enum DruhV�b�ru
    dvReli�f = 0
    dvTextura = 1
    dvPrvek = 2
End Enum

Public Enum DruhZobrazen�
    dzReli�f = 0
    dzTextura = 1
End Enum

Public Enum Reli�fn�Operace
    ropVlo�it = 0
    ropP�idat = 1
    ropUbrat = -1
    ropPr�m�r = 2
    ropSjednocen� = 3
    ropPr�nik = 4
End Enum

Public Const MaxInt = 32767
Public Const MaxSng = 3.402823E+38
Public Const MaxLng = 2147483647
Public Const PPM = 3779.5242 ' pixel� na metr
Public Const P� = 3.14159265358979
Public Const G = 9.81
'g_pix_ms = g * PPM * (1000 ^ -2) ' gravita�n� konstanta
            ' v pixelech na milisekundu na druhou
Public Const g_pix_ms = 0.037077132402
Public Const MimoM��ek = -&HFFFFFF
Public Const V��kaC�le = -50

Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long


Sub DefTPP()
    TPPX = Screen.TwipsPerPixelX
    TPPY = Screen.TwipsPerPixelY
End Sub

Sub Srovnej(Co As Control, Podle�eho As Control, Optional TakyRozm�ry As Boolean)
    On Error Resume Next
    Co.Move Podle�eho.Left, Podle�eho.Top
    If TakyRozm�ry Then
        Co.Width = Podle�eho.Width
        Co.Height = Podle�eho.Height
    End If
End Sub

Sub SrovnejRozm�ry(Co As Control, Podle�eho As Control)
    On Error Resume Next
    Co.Width = Podle�eho.Width
    Co.Height = Podle�eho.Height
End Sub

Public Sub Rozm��(Co As Control, Optional r���ka, Optional rV��ka)
    If Not IsMissing(r���ka) Then Co.Width = r���ka
    If Not IsMissing(rV��ka) Then Co.Height = rV��ka
End Sub

Function CXY(Optional ByVal x As Double, Optional ByVal y As Double) As XY
    If IsMissing(x) Then x = 0
    If IsMissing(y) Then y = 0
    CXY.x = x
    CXY.y = y
End Function

Function CXYByte(Optional ByVal x As Byte, Optional ByVal y As Byte) As XYByte
    If IsMissing(x) Then x = 0
    If IsMissing(y) Then y = 0
    CXYByte.x = x
    CXYByte.y = y
End Function

Function CXYLng(Optional ByVal x As Long, Optional ByVal y As Long) As XYLng
    If IsMissing(x) Then x = 0
    If IsMissing(y) Then y = 0
    CXYLng.x = x
    CXYLng.y = y
End Function

Function CXYZ(Optional ByVal x, Optional ByVal y, Optional ByVal Z) As XYLngZVar
    If IsMissing(x) Then x = 0
    If IsMissing(y) Then y = 0
    If IsMissing(Z) Then Z = 0
    CXYZ.x = x
    CXYZ.y = y
    CXYZ.Z = Z
End Function

Function XYLngToXY(Vector As XYLng) As XY
    XYLngToXY.x = Vector.x
    XYLngToXY.y = Vector.y
End Function

Public Function �erven�(ByVal Barva As Long)
    �erven� = Val("&H" & Mid(String(10 - Len(Hex(Barva)), "0") + Hex(Barva) & "&", 9, 2))
End Function

Public Function Zelen�(ByVal Barva As Long)
    Zelen� = Val("&H" & Mid(String(10 - Len(Hex(Barva)), "0") + Hex(Barva) & "&", 7, 2))
End Function

Public Function Modr�(ByVal Barva As Long)
    Modr� = Val("&H" & Mid(String(10 - Len(Hex(Barva)), "0") + Hex(Barva) & "&", 5, 2))
End Function


Function tg(ByVal Stupn� As Double)
    tg = Tan(Stupn� * P� / 180)
End Function

Function cotg(ByVal Stupn� As Double)
    If Stupn� = 0 Then
        cotg = tg(90)
    Else
        cotg = 1 / Tan(Stupn� * P� / 180)
    End If
End Function

Function arctg(ByVal Tangens As Double)
    arctg = Atn(Tangens) * (180 / P�)
End Function

Function arccotg(ByVal Cotangens As Double)
    arccotg = (Atn(Cotangens) + 2 * Atn(1)) * (180 / P�)
End Function

Function Arcus(Vector As XY)
Attribute Arcus.VB_Description = "Vrac� absolutn� �hel zadan�ho vektoru v�sou�. soustav� VB, 0� je naho�e."
    If IsZeroVector(Vector) Then
        Arcus = -1
        Exit Function
    End If
    Dim Tangens#, Surov��hel#
    ' Kdy� je na vodorovn� ose
    If Vector.y = 0 Then
        Select Case Vector.x
        Case 0
            Err.Raise vbObjectError + 1, , "Nulov� vektor - nemohu ur�it �hel!"
            Exit Function
        Case Is > 0
            Surov��hel = 90
        Case Is < 0
            Surov��hel = 270
        Case Else
            'nic: sem nem� �anci se dostat!
        End Select
    ' Kdy� nen� na vodorovn� ose
    Else
        Tangens = Vector.x / -Vector.y
        Surov��hel = arctg(Tangens)
    End If
    Arcus = Surov��hel + IIf(-Vector.y >= 0, 0, 180)
End Function

Function ArcusAbAxe(Vector As XY, VectorAxalis As XY)
    ArcusAbAxe = (2 * 360 + Arcus(Vector) - Arcus(VectorAxalis))
    ArcusAbAxe = ArcusAbAxe - (ArcusAbAxe \ 360) * 360
End Function

Function Abs�hel#(�hel#)
    Abs�hel = �hel - (�hel \ 360) * 360
End Function

Public Function IsZeroVector(Vector As XY) As Boolean
    IsZeroVector = (Vector.x = 0 And Vector.y = 0)
End Function

Public Function XYNaVD�(Vektor As XY) As VD�
    XYNaVD�.D�lka = (Vektor.x ^ 2 + Vektor.y ^ 2) ^ (1 / 2)
    XYNaVD�.�hel = Arcus(Vektor)
End Function

Public Function VD�NaXY(Vektor As VD�) As XY
    Vektor.�hel = Abs�hel(Vektor.�hel)
    If Vektor.�hel = -1 Or Vektor.D�lka = 0 Then
        VD�NaXY = CXY()
        Exit Function
    End If
    Select Case Vektor.�hel
    Case 0 To 180
        VD�NaXY.x = Sin(StupNaRad(Vektor.�hel)) * Vektor.D�lka
        VD�NaXY.y = -Cos(StupNaRad(Vektor.�hel)) * Vektor.D�lka
    Case Else
        VD�NaXY.x = Sin(StupNaRad(Vektor.�hel)) * Vektor.D�lka
        VD�NaXY.y = -Cos(StupNaRad(Vektor.�hel)) * Vektor.D�lka
    End Select
End Function

Public Function StupNaRad#(Stupn�#)
    StupNaRad = Stupn� * (P� / 180)
End Function

Public Function RadNaStup#(Radi�ny#)
    RadNaStup = Radi�ny * (180 / P�)
End Function

Public Function XYRozd�l(XY1 As XY, XY2 As XY) As XY
    XYRozd�l.x = XY1.x - XY2.x
    XYRozd�l.y = XY1.y - XY2.y
End Function

Public Function XYSou�et(XY1 As XY, XY2 As XY) As XY
    XYSou�et.x = XY1.x + XY2.x
    XYSou�et.y = XY1.y + XY2.y
End Function

Public Function XYPosun(XY As XY, Posun As Double) As XY
    XYPosun.x = XY.x + Posun
    XYPosun.y = XY.y + Posun
End Function

Public Function XYNeg(XY As XY) As XY
    XYNeg.x = -XY.x
    XYNeg.y = -XY.y
End Function

Public Function Zaokrouhli(��slo) As Integer
    Select Case Abs(��slo - ��slo \ 1)
    Case Is <= 0.5
        Zaokrouhli = ��slo \ 1
    Case Else
        Zaokrouhli = ��slo \ 1 + Sgn(��slo)
    End Select
End Function

Function Hol�Jm�no(Soub As String) As String
    If Len(Soub) > 4 Then
        If Mid(Soub, Len(Soub) - 3, 1) = "." Then
            Hol�Jm�no = Left(Soub, Len(Soub) - 4)
        Else
            Hol�Jm�no = Soub
        End If
    Else
        Hol�Jm�no = Soub
    End If
End Function

Function Pravobok(�eho As Object) As Single
    Pravobok = �eho.Left + �eho.Width
End Function

Function Spodek(�eho As Object) As Single
    Spodek = �eho.Top + �eho.Height
End Function

Public Function DejNastaven�(Odd�len�$, Kl��$, Optional V�choz�) As Variant
    DejNastaven� = GetSetting(App.Title, Odd�len�, Kl��, V�choz�)
    If DejNastaven� = "True" Or DejNastaven� = "False" Then DejNastaven� = CBool(DejNastaven�)
End Function

Public Sub Ulo�Nastaven�(ByVal Odd�len�$, ByVal Kl��$, Nastaven�)
    SaveSetting App.Title, Odd�len�, Kl��, Nastaven�
End Sub

Public Property Let Stavebn�Povolen�(NaCo As DruhZobrazen�, Nosi� As Boolean)
    Select Case NaCo
    Case dzReli�f
        frmEditorDrah.nabStPRel.Checked = Nosi�
        frmSb�rka.nabStPRel.Checked = Nosi�
    Case dzTextura
        frmEditorDrah.nabStPGra.Checked = Nosi�
        frmSb�rka.nabStPGra.Checked = Nosi�
    Case Else
    End Select
End Property

Public Property Get Stavebn�Povolen�(NaCo As DruhZobrazen�) As Boolean
    Select Case NaCo
    Case dzTextura
        Stavebn�Povolen� = frmEditorDrah.nabStPGra.Checked
    Case dzReli�f
        Stavebn�Povolen� = frmEditorDrah.nabStPRel.Checked
    Case Else
    End Select
End Property

Public Property Let Povolen�Stavebn�hoPovolen�(Nosi� As Boolean)
    frmEditorDrah.nabStPGra.Enabled = Nosi�
    frmEditorDrah.nabStPRel.Enabled = Nosi�
    frmSb�rka.nabStPGra.Enabled = Nosi�
    frmSb�rka.nabStPRel.Enabled = Nosi�
End Property

Public Property Get Povolen�Stavebn�hoPovolen�() As Boolean
    Povolen�Stavebn�hoPovolen� = frmEditorDrah.nabStPGra.Enabled
End Property

