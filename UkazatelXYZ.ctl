VERSION 5.00
Begin VB.UserControl UkazatelXYZ 
   AutoRedraw      =   -1  'True
   ClientHeight    =   1980
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1635
   ScaleHeight     =   132
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   109
   Begin VB.Label lblZ 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label lblY 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblX 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "UkazatelXYZ"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim pamTextX As String, pamTextY As String, pamTextZ As String
Dim pamHodX As String, pamHodY As String, pamHodZ As String


Private Sub UserControl_Initialize()
    NastavXYZ 0, 0, 0
End Sub

Private Sub UserControl_Resize()
    lblX.Move 0, 0, ScaleWidth, ScaleHeight \ 3
    lblY.Move 0, lblX.Height, ScaleWidth, ScaleHeight \ 3
    lblZ.Move 0, lblX.Height + lblY.Height, ScaleWidth, ScaleHeight \ 3
    'NastavXYZ HodnotaX, HodnotaY, HodnotaZ
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
    TextX = .ReadProperty("TextX", "X = ")
    TextY = .ReadProperty("TextY", "Y = ")
    TextZ = .ReadProperty("TextZ", "Z = ")
    HodnotaX = .ReadProperty("HodnotaX", 0)
    HodnotaY = .ReadProperty("HodnotaY", 0)
    HodnotaZ = .ReadProperty("HodnotaZ", 0)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
    .WriteProperty "TextX", TextX, "X = "
    .WriteProperty "TextY", TextY, "Y = "
    .WriteProperty "TextZ", TextZ, "Z = "
    .WriteProperty "HodnotaX", HodnotaX
    .WriteProperty "HodnotaY", HodnotaY
    .WriteProperty "HodnotaZ", HodnotaZ
    End With
End Sub

Public Property Let HodnotaX(Nosi�)
    lblX = TextX & Nosi�
    pamHodX = Nosi�
    PropertyChanged
End Property

Public Property Get HodnotaX()
    HodnotaX = pamHodX
End Property

Public Property Let HodnotaY(Nosi�)
    lblY = TextY & Nosi�
    pamHodY = Nosi�
    PropertyChanged
End Property

Public Property Get HodnotaY()
    HodnotaY = pamHodY
End Property

Public Property Let HodnotaZ(Nosi�)
    lblZ = TextZ & Nosi�
    pamHodZ = Nosi�
    PropertyChanged
End Property

Public Property Get HodnotaZ()
    HodnotaZ = pamHodZ
End Property

Public Property Let TextX(Nosi�)
    pamTextX = Nosi�
    lblX = Nosi� & HodnotaX
    PropertyChanged "TextX"
End Property

Public Property Get TextX()
    TextX = pamTextX
End Property

Public Property Let TextY(Nosi�)
    pamTextY = Nosi�
    lblY = Nosi� & HodnotaY
    PropertyChanged "TextY"
End Property

Public Property Get TextY()
    TextY = pamTextY
End Property

Public Property Let TextZ(Nosi�)
    pamTextZ = Nosi�
    lblZ = Nosi� & HodnotaZ
    PropertyChanged "TextZ"
End Property

Public Property Get TextZ()
    TextZ = pamTextZ
End Property

Public Sub NastavXY(X, Y)
    HodnotaX = X
    HodnotaY = Y
End Sub

Public Sub NastavXYZ(X, Y, Z)
    HodnotaX = X
    HodnotaY = Y
    HodnotaZ = Z
End Sub
