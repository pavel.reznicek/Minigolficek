VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.UserControl Rozm�ry 
   Alignable       =   -1  'True
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   795
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1695
   ScaleHeight     =   53
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   113
   Begin VB.TextBox txtV��ka 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   4
      Text            =   "Rozm�ry.ctx":0000
      Top             =   360
      Width           =   540
   End
   Begin VB.TextBox txt���ka 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   1
      Text            =   "Rozm�ry.ctx":0004
      Top             =   0
      Width           =   540
   End
   Begin ComCtl2.UpDown ud���ka 
      Height          =   240
      Left            =   1140
      TabIndex        =   2
      Top             =   0
      Width           =   195
      _ExtentX        =   344
      _ExtentY        =   423
      _Version        =   327681
      Value           =   1
      AutoBuddy       =   -1  'True
      BuddyControl    =   "txt���ka"
      BuddyDispid     =   196610
      OrigLeft        =   720
      OrigRight       =   915
      OrigBottom      =   255
      Increment       =   32
      Max             =   32
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin ComCtl2.UpDown udV��ka 
      Height          =   240
      Left            =   1140
      TabIndex        =   5
      Top             =   360
      Width           =   195
      _ExtentX        =   344
      _ExtentY        =   423
      _Version        =   327681
      Value           =   1
      AutoBuddy       =   -1  'True
      BuddyControl    =   "txtV��ka"
      BuddyDispid     =   196609
      OrigLeft        =   720
      OrigTop         =   360
      OrigRight       =   915
      OrigBottom      =   615
      Increment       =   32
      Max             =   32
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Label lblV��ka 
      AutoSize        =   -1  'True
      Caption         =   "&D�lka:"
      Height          =   195
      Left            =   0
      TabIndex        =   3
      Top             =   405
      Width           =   465
   End
   Begin VB.Label lbl���ka 
      AutoSize        =   -1  'True
      Caption         =   "���&ka:"
      Height          =   195
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   435
   End
End
Attribute VB_Name = "Rozm�ry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Event Ru�n�Zm�na()
Public Event Nastaven�()
Public Event Zm�naM��ky()
Dim pamTextX As String, pamTextY As String
Dim NoRaise As Boolean
'Public Event Click()

Public Property Let TextX(Nosi�)
    pamTextX = Nosi�
    lbl���ka = Nosi�
    PropertyChanged "TextX"
End Property

Public Property Get TextX()
    TextX = pamTextX
End Property

Public Property Let TextY(Nosi�)
    pamTextY = Nosi�
    lblV��ka = Nosi�
    PropertyChanged "TextY"
End Property

Public Property Get TextY()
    TextY = pamTextY
End Property

Public Property Let M��kaX(Nosi� As Integer)
    ud���ka.Increment = Nosi�
    PropertyChanged "M��kaX"
    RaiseEvent Zm�naM��ky
End Property

Public Property Get M��kaX() As Integer
    M��kaX = ud���ka.Increment
End Property

Public Property Let M��kaY(Nosi� As Integer)
    udV��ka.Increment = Nosi�
    PropertyChanged "M��kaY"
    RaiseEvent Zm�naM��ky
End Property

Public Property Get M��kaY() As Integer
    M��kaY = udV��ka.Increment
End Property

Public Sub NastavM��ku(Optional X, Optional Y)
    If Not IsMissing(X) Then ud���ka.Increment = X
    If Not IsMissing(Y) Then udV��ka.Increment = Y
    If Not IsMissing(X) And Not IsMissing(Y) Then RaiseEvent Zm�naM��ky
End Sub

Public Property Let ���ka(Nosi� As Integer)
    ud���ka = IIf(Nosi� < ud���ka.Min, ud���ka.Min, Nosi�)
    PropertyChanged "���ka"
End Property

Public Property Get ���ka() As Integer
    ���ka = ud���ka
End Property

Public Property Let V��ka(Nosi� As Integer)
    udV��ka = IIf(Nosi� < udV��ka.Min, udV��ka.Min, Nosi�)
    PropertyChanged "V��ka"
End Property

Public Property Get V��ka() As Integer
    V��ka = udV��ka
End Property

Public Sub Nastav(Optional X, Optional Y)
    NoRaise = True
    Dim ChangeFlag As Boolean
    If Not IsMissing(X) Then
        If X <> ud���ka Then
            ChangeFlag = True
            ud���ka = X
        End If
    End If
    If Not IsMissing(Y) Then
        If Y <> udV��ka Then
            ChangeFlag = True
            udV��ka = Y
        End If
    End If
    'If ChangeFlag Then RaiseEvent Change
    RaiseEvent Nastaven�
    NoRaise = False
End Sub

Public Property Let Max���ka(Nosi� As Integer)
    ud���ka.Max = Nosi�
    PropertyChanged "Max���ka"
End Property

Public Property Get Max���ka() As Integer
    Max���ka = ud���ka.Max
End Property

Public Property Let MaxV��ka(Nosi� As Integer)
    udV��ka.Max = Nosi�
    PropertyChanged "MaxV��ka"
End Property

Public Property Get MaxV��ka() As Integer
    MaxV��ka = udV��ka.Max
End Property

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    Dim Ctl As Control
    For Each Ctl In UserControl.Controls
        Ctl.Enabled = Nosi�
    Next
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property
'
'Public Property Let Zm�na(Nosi� As Integer)
'    ud���ka.Increment = Nosi�
'    udV��ka.Increment = Nosi�
'    PropertyChanged "Zm�na"
'End Property
'
'Public Property Get Zm�na() As Integer
'    Zm�na = ud���ka.Increment
'End Property

Private Sub txt���ka_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0") To Asc("9"), 8
        'nech tak
    Case 13
        'potvr� hodnotu
        If Val(txt���ka) < ud���ka.Min Then
            ud���ka = ud���ka.Min
        ElseIf Val(txt���ka) > ud���ka.Max Then
            ud���ka = ud���ka.Max
        Else
            ud���ka = Val(txt���ka)
        End If
        KeyAscii = 0
    Case Else
        'nic nepi�
        KeyAscii = 0
    End Select
End Sub

Private Sub txtV��ka_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0") To Asc("9"), 8
        'nech tak
    Case 13
        'potvr� hodnotu
        If Val(txtV��ka) < udV��ka.Min Then
            udV��ka = udV��ka.Min
        ElseIf Val(txtV��ka) > udV��ka.Max Then
            udV��ka = udV��ka.Max
        Else
            udV��ka = Val(txtV��ka)
        End If
        KeyAscii = 0
    Case Else
        'nic nepi�
        KeyAscii = 0
    End Select
End Sub

Private Sub ud���ka_Change()
    Static MinHod
    If MinHod <> ud���ka And Not NoRaise Then RaiseEvent Ru�n�Zm�na
    MinHod = ud���ka
End Sub

Private Sub udV��ka_Change()
    Static MinHod
    If MinHod <> udV��ka And Not NoRaise Then RaiseEvent Ru�n�Zm�na
    MinHod = udV��ka
End Sub

Private Sub UserControl_Initialize()
    txt���ka = ud���ka
    txtV��ka = udV��ka
    lbl���ka.BackColor = txt���ka.BackColor
    lblV��ka.BackColor = txtV��ka.BackColor
    BackColor = txt���ka.BackColor
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        TextX = .ReadProperty("TextX", "���&ka:")
        TextY = .ReadProperty("TextY", "&D�lka:")
        Max���ka = .ReadProperty("Max���ka", 32)
        MaxV��ka = .ReadProperty("MaxV��ka", 32)
        ���ka = .ReadProperty("���ka", ud���ka)
        V��ka = .ReadProperty("V��ka", udV��ka)
        Enabled = .ReadProperty("Enabled", True)
        M��kaX = .ReadProperty("M��kaX", 32)
        M��kaY = .ReadProperty("M��kaY", 32)
    End With
End Sub

Private Sub UserControl_Resize()
    DefTPP
    txt���ka.Move txt���ka.Left, 0, ScaleWidth - ud���ka.Width - txt���ka.Left
    txtV��ka.Move txtV��ka.Left, txt���ka.Height, ScaleWidth - udV��ka.Width - txtV��ka.Left
    ud���ka.Move ScaleWidth - ud���ka.Width, txt���ka.Top
    udV��ka.Move ScaleWidth - udV��ka.Width, txtV��ka.Top
    lbl���ka.Move 2, txt���ka.Top + txt���ka.Height / 2 - lbl���ka.Height / 2
    lblV��ka.Move 2, txtV��ka.Top + txtV��ka.Height / 2 - lblV��ka.Height / 2
    UserControl.Height = (Height - ScaleHeight * TPPY) + (txtV��ka.Top + txtV��ka.Height) * TPPY
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "TextX", TextX, "���&ka:"
        .WriteProperty "TextY", TextY, "&D�lka:"
        .WriteProperty "Max���ka", Max���ka, 32
        .WriteProperty "MaxV��ka", MaxV��ka, 32
        .WriteProperty "���ka", ���ka, ud���ka
        .WriteProperty "V��ka", V��ka, udV��ka
        .WriteProperty "Enabled", Enabled, True
        .WriteProperty "M��kaX", M��kaX, 32
        .WriteProperty "M��kaY", M��kaY, 32
    End With
End Sub
