VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPředvolby 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Předvolby reliéfu"
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   1875
   ClientWidth     =   7020
   Icon            =   "frmPředvolby.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   419
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   468
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.TreeView tvPředvolby 
      Height          =   6015
      Left            =   120
      TabIndex        =   29
      Top             =   120
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   10610
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   661
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      SingleSel       =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   7
      Left            =   4800
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   25
      Top             =   2760
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   6
      Left            =   4800
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   24
      Top             =   3000
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   5
      Left            =   4800
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   23
      Top             =   3240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   4
      Left            =   5040
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   22
      Top             =   3240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   3
      Left            =   5280
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   21
      Top             =   3240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   2
      Left            =   5280
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   20
      Top             =   3000
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   1
      Left            =   5280
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   19
      Top             =   2760
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CommandButton tlProveď 
      Caption         =   "&Proveď!"
      Height          =   375
      Left            =   4920
      TabIndex        =   18
      Top             =   5760
      Width           =   900
   End
   Begin VB.Frame fraOperace 
      Caption         =   "&Operace"
      Height          =   1815
      Left            =   4920
      TabIndex        =   14
      Top             =   3840
      Width           =   1980
      Begin VB.OptionButton optOperace 
         Caption         =   "Prů&nik"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   28
         ToolTipText     =   "Porovná výšku každého bodu reliéfu s výškou odpovídajícího bodu zvoleného tvaru a vybere nižší (jako log. op. AND)"
         Top             =   1440
         Width           =   1695
      End
      Begin VB.OptionButton optOperace 
         Caption         =   "S&jednocení"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   27
         ToolTipText     =   "Porovná výšku každého bodu reliéfu s výškou odpovídajícího bodu zvoleného tvaru a vybere vyšší (jako log. op. OR)"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.OptionButton optOperace 
         Caption         =   "Prů&měr"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   26
         ToolTipText     =   "Z výšky každého bodu reliéfu a výšky odpovídajícího bodu zvoleného tvaru spočítá průměr"
         Top             =   960
         Width           =   1695
      End
      Begin VB.OptionButton optOperace 
         Caption         =   "&Rozdíl"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   17
         ToolTipText     =   "Od výšky každého bodu reliéfu ubere výšku odpovídajícího bodu zvoleného tvaru"
         Top             =   720
         Width           =   1695
      End
      Begin VB.OptionButton optOperace 
         Caption         =   "&Součet"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   16
         ToolTipText     =   "K výšce každého bodu reliéfu přidá výšku odpovídajícího bodu zvoleného tvaru"
         Top             =   480
         Width           =   1695
      End
      Begin VB.OptionButton optOperace 
         Caption         =   "&Vložit"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   15
         ToolTipText     =   "Vytvoří z vybraného tvaru reliéf"
         Top             =   240
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.CommandButton tlVykreslit 
      Caption         =   "Vykreslit &náhled"
      Height          =   375
      Left            =   4920
      TabIndex        =   13
      ToolTipText     =   "Vykreslí ukázku tvaru"
      Top             =   2280
      Width           =   1980
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   3
      Left            =   6030
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Směr"
      Top             =   3360
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   4
      Left            =   5790
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Směr"
      Top             =   3360
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   2
      Left            =   6030
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Směr"
      Top             =   3120
      Width           =   255
   End
   Begin VB.CheckBox chbSvislýSměr 
      Height          =   255
      Left            =   5790
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   3120
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   5
      Left            =   5550
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Směr"
      Top             =   3360
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   6
      Left            =   5550
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Směr"
      Top             =   3120
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   1
      Left            =   6030
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Směr"
      Top             =   2880
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   8
      Left            =   5790
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Směr"
      Top             =   2880
      Value           =   -1  'True
      Width           =   255
   End
   Begin VB.OptionButton obSměr 
      Height          =   255
      Index           =   7
      Left            =   5550
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Směr"
      Top             =   2880
      Width           =   255
   End
   Begin VB.PictureBox obrTl 
      AutoRedraw      =   -1  'True
      Height          =   255
      Index           =   8
      Left            =   5040
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   11
      Top             =   2760
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CommandButton tlZrušit 
      Cancel          =   -1  'True
      Caption         =   "&Zavřít"
      Height          =   375
      Left            =   6000
      TabIndex        =   9
      Top             =   5760
      Width           =   900
   End
   Begin VB.PictureBox obrReliéf 
      AutoRedraw      =   -1  'True
      Height          =   1980
      Left            =   4920
      ScaleHeight     =   128
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   128
      TabIndex        =   0
      Top             =   120
      Width           =   1980
   End
   Begin VB.CheckBox chbOkraje 
      Height          =   975
      Left            =   5430
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Vodorovné cípy"
      Top             =   2760
      Width           =   975
   End
End
Attribute VB_Name = "frmPředvolby"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim SoukrMapa() As Single
Dim SMDimensována As Boolean


Private Enum CoKreslit
    KreslitTlačítka = 0
    KreslitNáhled = 1
    KreslitMapu = 2
End Enum

Const Sl = "Složka"

Private Sub Form_Activate()
'    GenerálněNamapuj True
End Sub

Private Sub Form_Load()
    chbSvislýSměr = DejNastavení("Předvolby", "Dutý", False)
    chbSvislýSměr.ToolTipText = IIf(CBool(chbSvislýSměr), _
    "Nyní je tvar dutý (přepnout na vypouklý)", "Nyní je tvar vypouklý (přepnout na dutý)")
    chbOkraje = DejNastavení("Předvolby", "Cípy", False)
    obSměr(DejNastavení("Předvolby", "Směr", 8)) = True
    With tvPředvolby.Nodes
        .Add , , "rz", "reliéf ze zásobníku"
        .Add , , "v", "vodorovná plocha"
        .Add , , "p", "pyramida"
            .Add "p", 4, "pj", "jednoduchá"
                .Add "pj", 4, "pj22", "22,5°"
                .Add "pj", 4, "pj45", "45°"
'                Způsobuje absolutní odraz!
'                .Add "pj", 4, "pj77", "77,5°"
            .Add "p", 4, "pr", "rozložená"
                .Add "pr", 4, "prk", "kosá"
                    .Add "prk", 4, "prk22", "22,5°"
                        .Add "prk22", 4, "prk22s", "střed"
                        .Add "prk22", 4, "prk22o", "okraj"
                    .Add "prk", 4, "prk45", "45°"
                .Add "pr", 4, "prl", "lomená"
                    .Add "prl", 4, "prl22", "22,5°"
                        .Add "prl22", 4, "prl22s", "střed"
                        .Add "prl22", 4, "prl22o", "okraj"
                    .Add "prl", 4, "prl45", "45°"
        .Add , , "k", "kužel"
            .Add "k", 4, "kj", "jednoduchý"
                .Add "kj", 4, "kj22", "22,5°"
                .Add "kj", 4, "kj45", "45°"
'                Způsobuje absolutní odraz!
'                .Add "kj", 4, "kj77", "77,5°"
            .Add "k", 4, "kr", "rozložený"
                .Add "kr", 4, "kr22", "22,5°"
                    .Add "kr22", 4, "kr22s", "střed"
                    .Add "kr22", 4, "kr22o", "okraj"
                .Add "kr", 4, "kr45", "45°"
        .Add , , "o", "polokoule"
            .Add "o", 4, "oj", "jednoduchá"
            .Add "o", 4, "or", "rozložená"
    End With
    Dim Styčník As msComctlLib.Node, PoslTvar As String
    PoslTvar = DejNastavení("Předvolby", "Poslední tvar")
    For Each Styčník In tvPředvolby.Nodes
        If Styčník.Children > 0 Then Styčník.Tag = Sl
        If Styčník.Key = PoslTvar Then
            Styčník.Selected = True
            GenerálněNamapuj True
        End If
    Next
End Sub

Private Sub Form_Paint()
    'GenerálněNamapuj True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Cancel = True
        Hide
    End If
End Sub

Private Sub chbOkraje_Click()
    GenerálněNamapuj True
    UložNastavení "Předvolby", "Cípy", chbOkraje.Value
End Sub

Private Sub chbSvislýSměr_Click()
    UložNastavení "Předvolby", "Dutý", chbSvislýSměr.Value
    chbSvislýSměr.ToolTipText = IIf(CBool(chbSvislýSměr), _
    "Nyní je tvar dutý (přepnout na vypouklý)", "Nyní je tvar vypouklý (přepnout na dutý")
    GenerálněNamapuj True
End Sub

Private Sub obSměr_Click(Index As Integer)
    'GenerálněNamapuj  True
    UložNastavení "Předvolby", "Směr", Index
End Sub

Private Sub GenerálněNamapuj(Optional Tlačítka As Boolean, Optional Náhled As Boolean, Optional Mapa As Boolean)
    Dim I
    If Tlačítka Then
        For I = 1 To 8
            Namapuj I, KreslitTlačítka
            DoEvents
        Next
    End If
    If Náhled Then
        Namapuj Směr, KreslitNáhled
    End If
    If Mapa Then
        Namapuj Směr, KreslitMapu
    End If
End Sub

Private Sub Namapuj(ČísloSměru, CoNamapovat As CoKreslit)
    Dim X, Y, Z, pamSměr As XY, SB 'SložkaBarvy
    Dim Gradient As XY, Rozložení, Rx, Barva, Šířka, Výška
    Dim MaxZ, Max, ZvedatDutý As Boolean
    Dim TvarNevypracován As Boolean, Cesta As String
    If tvPředvolby.SelectedItem Is Nothing Then Exit Sub
    MousePointer = vbHourglass
    Cesta = tvPředvolby.SelectedItem.FullPath
    pamSměr = Oktál(ČísloSměru)
    Select Case CoNamapovat
    Case KreslitNáhled
        Šířka = obrReliéf.ScaleWidth
        Výška = obrReliéf.ScaleHeight
    Case KreslitMapu
        Šířka = frmEditorPrvků.opRozměry.Šířka
        Výška = frmEditorPrvků.opRozměry.Výška
        ReDim SdílenáMapaPrvku(Šířka - 1, Výška - 1)
    Case KreslitTlačítka
        Šířka = obrTl(1).ScaleWidth
        Výška = obrTl(1).ScaleHeight
    Case Else
    End Select
    MaxZ = IIf(Šířka <= Výška, Šířka, Výška)
    Max = MaxZ - 1
    Dim čX As Long, čY As Long
    DoEvents
    For čY = 0 To Výška - 1
        For čX = 0 To Šířka - 1
            X = čX + 1
            Y = čY + 1
            Gradient.X = (MaxZ - 1) * Abs(pamSměr.X = -1) + čX * (pamSměr.X)
            Gradient.Y = (MaxZ - 1) * Abs(pamSměr.Y = -1) + čY * (pamSměr.Y)
            With tvPředvolby.SelectedItem
            Select Case Cesta
            Case "reliéf ze zásobníku"
                If SMDimensována Then
                    Dim ŠSMP%, VSMP%
                    ŠSMP = UBound(SoukrMapa, 1)
                    VSMP = UBound(SoukrMapa, 2)
                    Z = SoukrMapa(čX * ŠSMP / (Šířka - 1), čY * VSMP / (Výška - 1))
                    MaxZ = IIf(ŠSMP <= VSMP, ŠSMP, VSMP) + 1
                    MaxZ = MaxZ * (chbSvislýSměr) '+ chbSvislýSměr
                    Z = -Z + MaxZ
                Else
                    Z = 0
                    MaxZ = 0
                End If
            Case "vodorovná plocha"
                Z = 0
            Case "pyramida\jednoduchá\22,5°"
                Rozložení = 0.5
                If Abs(čX - Max / 2) >= Abs(čY - Max / 2) Then
                    Z = Rozložení * Abs(čX - Max / 2) + MaxZ - Max * Rozložení / 2
                Else
                    Z = Rozložení * Abs(čY - Max / 2) + MaxZ - Max * Rozložení / 2
                End If
                Z = Z + (chbSvislýSměr - 1) * Rozložení
            Case "pyramida\jednoduchá\45°"
                Rozložení = 1
                If Abs(čX - Max / 2) >= Abs(čY - Max / 2) Then
                    Z = Rozložení * Abs(čX - Max / 2) + MaxZ - Max * Rozložení / 2
                Else
                    Z = Rozložení * Abs(čY - Max / 2) + MaxZ - Max * Rozložení / 2
                End If
                Z = Z + (chbSvislýSměr - 1) * Rozložení
'            Způsobuje absolutní odraz!
'            Case "pyramida\jednoduchá\77,5°"
'                Rozložení = 2
'                'MaxZ = Max + 1 - Rozložení
'                If Abs(X - MaxZ / 2) >= Abs(Y - MaxZ / 2) Then
'                    Z = Rozložení * Abs(X - MaxZ / 2) + MaxZ * (1 - Rozložení / 2)
'                Else
'                    Z = Rozložení * Abs(Y - MaxZ / 2) + MaxZ * (1 - Rozložení / 2)
'                End If
            Case "pyramida\rozložená\kosá\45°"
                Rozložení = 1
                Z = (Gradient.X * Rozložení + Gradient.Y * Rozložení)
                Z = Z + (chbSvislýSměr) * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
            Case "pyramida\rozložená\kosá\22,5°\střed"
                Rozložení = 0.5
                Z = (Gradient.X * Rozložení + Gradient.Y * Rozložení)
                Z = Z + (chbSvislýSměr) * Rozložení
                If chbOkraje And Z > MaxZ / 2 Then Z = MaxZ / 2
            Case "pyramida\rozložená\kosá\22,5°\okraj"
                Rozložení = 0.5
                Z = ((Gradient.X + MaxZ / 2) * Rozložení + (Gradient.Y + MaxZ / 2) * Rozložení)
                Z = Z + (chbSvislýSměr) * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
            Case "pyramida\rozložená\lomená\45°"
                Rozložení = 1
                If Abs(Gradient.X) >= Abs(Gradient.Y) Then
                    Z = Gradient.X
                Else
                    Z = Gradient.Y
                End If
                Z = Z + (chbSvislýSměr) * Rozložení
            Case "pyramida\rozložená\lomená\22,5°\střed"
                Rozložení = 0.5
                If Abs(Gradient.X) >= Abs(Gradient.Y) Then
                    Z = Gradient.X * Rozložení
                Else
                    Z = Gradient.Y * Rozložení
                End If
                Z = Z + (chbSvislýSměr) * Rozložení
            Case "pyramida\rozložená\lomená\22,5°\okraj"
                Rozložení = 0.5
                If Abs(Gradient.X) >= Abs(Gradient.Y) Then
                    Z = (MaxZ + Gradient.X) * Rozložení
                Else
                    Z = (MaxZ + Gradient.Y) * Rozložení
                End If
                Z = Z + (chbSvislýSměr) * Rozložení
            Case "kužel\jednoduchý\22,5°"
                Rozložení = 0.5
                ZvedatDutý = True
                Z = ((čX - Max / 2) ^ 2 + (čY - Max / 2) ^ 2) ^ (1 / 2) * Rozložení + MaxZ - MaxZ / 2 * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
            Case "kužel\jednoduchý\45°"
                Rozložení = 1
                ZvedatDutý = False
                Max = MaxZ - 1
                Z = ((čX - Max / 2) ^ 2 + (čY - Max / 2) ^ 2) ^ (1 / 2) * Rozložení + MaxZ - MaxZ / 2 * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
'            Způsobuje absolutní odraz!
'            Case "kužel\jednoduchý\77,5°"
'                Rozložení = 2
'                'MaxZ = Max + 1 - Rozložení
'                Z = 2 * ((X - MaxZ / 2) ^ 2 + (Y - MaxZ / 2) ^ 2) ^ (1 / 2)
'                If chbOkraje And Z > MaxZ + Rozložení Then Z = MaxZ + Rozložení
            Case "kužel\rozložený\22,5°\střed"
                Rozložení = 0.5
                Z = ((Gradient.X ^ 2 + Gradient.Y ^ 2) ^ (1 / 2)) * Rozložení + chbSvislýSměr * Rozložení
                If chbOkraje And Z > MaxZ / 2 Then Z = MaxZ / 2
            Case "kužel\rozložený\22,5°\okraj"
                Rozložení = 0.5
                Z = (MaxZ + (Gradient.X ^ 2 + Gradient.Y ^ 2) ^ (1 / 2)) * Rozložení + chbSvislýSměr * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
            Case "kužel\rozložený\45°"
                Rozložení = 1
                Z = (Gradient.X ^ 2 + Gradient.Y ^ 2) ^ (1 / 2) * Rozložení + chbSvislýSměr * Rozložení
                If chbOkraje And Z > MaxZ Then Z = MaxZ
            Case "polokoule\jednoduchá"
                Rozložení = 1
                Max = MaxZ - 1
                If (MaxZ / 2) ^ 2 - ((čX - Max / 2) ^ 2 + (čY - Max / 2) ^ 2) < 0 Then
                    If chbOkraje Then
                        Z = MaxZ
                    Else
                        Z = MaxZ - (-((Max / 2) ^ 2 - ((čX - Max / 2) ^ 2 + (čY - Max / 2) ^ 2))) ^ (1 / 2)
                    End If
                Else
                    Z = MaxZ - ((MaxZ / 2) ^ 2 - ((čX - Max / 2) ^ 2 + (čY - Max / 2) ^ 2)) ^ (1 / 2)
                End If
            Case "polokoule\rozložená"
                Rozložení = 1
                Max = MaxZ - 1
                'Pokud je to venku za poloměrem
                If MaxZ ^ 2 - (Gradient.X ^ 2 + Gradient.Y ^ 2) < 0 Then
                    If chbOkraje Then
                        Z = MaxZ
                    Else
                        Z = MaxZ - (-(MaxZ ^ 2 - (Gradient.X ^ 2 + Gradient.Y ^ 2))) ^ (1 / 2)
                    End If
                'Vevnitř
                Else
                    Z = MaxZ - (MaxZ ^ 2 - (Gradient.X ^ 2 + Gradient.Y ^ 2)) ^ (1 / 2)
                End If
            Case Else
                If .Tag <> Sl Then TvarNevypracován = True
                Exit For
            End Select
            End With
            Dim Dutý As Boolean
            Dutý = CBool(chbSvislýSměr)
            If Dutý Then
                Z = Z '+ Rozložení ' * Abs(ZvedatDutý)
                'Z = Z + Rozložení
            Else
                'Z = (MaxZ + Rozložení) - (Z)
                Z = MaxZ - Z '+ Rozložení
            End If
            'SB = IIf(Z / MaxZ * 255 >= 0, Z / MaxZ * 255, 0)
            'Barva = RGB(SB, SB, SB)
            Dim HloubkaPrvku As Single
            With frmEditorPrvků.opRozměry
            HloubkaPrvku = IIf(.Šířka > .Výška, _
            .Výška, .Šířka)
            End With
            If Cesta = "reliéf ze zásobníku" Then
                Barva = frmEditorPrvků.opStupně.BarvaPodleVýšky(CSng(Z))
            Else
                Barva = frmEditorPrvků.opStupně.BarvaPodleVýšky(CSng(Z / MaxZ * HloubkaPrvku))
            End If
            Select Case CoNamapovat
            Case KreslitTlačítka
                obrTl(ČísloSměru).PSet (čX, čY), Barva
            Case KreslitNáhled
                obrReliéf.PSet (čX, čY), Barva
            Case KreslitMapu
                SdílenáMapaPrvku(čX, čY) = Z
            Case Else
            End Select
        Next
        obrReliéf.Refresh
        'Refresh
    Next
    DoEvents
    If TvarNevypracován Then
'        MsgBox "Tvar " & Cesta _
'        & " ještě není vypracován.", vbInformation, Caption
    End If
    If CoNamapovat = KreslitTlačítka Then
        obSměr(ČísloSměru).DownPicture = obrTl(ČísloSměru).Image
        obSměr(ČísloSměru).Picture = obSměr(ČísloSměru).DownPicture
        obSměr(ČísloSměru).Refresh
    End If
vypadni:
    MousePointer = vbDefault
End Sub

Private Function Směr()
    Dim I
    For I = 1 To 8
        If obSměr(I) Then Směr = I
    Next
End Function

Private Function Oktál(ByVal Index As Integer) As XY
    Select Case Index
    Case 0
        Oktál.X = 0
        Oktál.Y = 0
    Case 1
        Oktál.X = 1
        Oktál.Y = -1
    Case 2
        Oktál.X = 1
        Oktál.Y = 0
    Case 3
        Oktál.X = 1
        Oktál.Y = 1
    Case 4
        Oktál.X = 0
        Oktál.Y = 1
    Case 5
        Oktál.X = -1
        Oktál.Y = 1
    Case 6
        Oktál.X = -1
        Oktál.Y = 0
    Case 7
        Oktál.X = -1
        Oktál.Y = -1
    Case 8
        Oktál.X = 0
        Oktál.Y = -1
    Case Else
        MsgBox "Špatná hodnota - oktál má indexy" _
        & "jen od 0 do 8!", vbCritical
    End Select
End Function

Private Sub tlProveď_Click()
    If tvPředvolby.SelectedItem Is Nothing Then Exit Sub
    If tvPředvolby.SelectedItem.Tag = Sl Then Exit Sub
    UložNastavení "Předvolby", "Poslední tvar", tvPředvolby.SelectedItem.Key
    GenerálněNamapuj , , True
    frmEditorPrvků.UplatniSdílenouMapuPrvku RelOp
End Sub

Private Function RelOp() As ReliéfníOperace
    Dim Ctl As OptionButton
    For Each Ctl In optOperace
        If Ctl.Value = True Then RelOp = Ctl.Index - 1
    Next
End Function

Private Sub tlVykreslit_Click()
    GenerálněNamapuj True, True
End Sub

Private Sub tlZrušit_Click()
    Hide
End Sub

Private Sub tvPředvolby_NodeClick(ByVal Node As msComctlLib.Node)
    'tvPředvolby.ToolTipText = Node.FullPath
    If Node.Key = "rz" Then NačtiReliéf
    If Node.Tag <> Sl Then GenerálněNamapuj True
End Sub

Private Sub NačtiReliéfPoStaru()
    frmVýběr.Druh = dzReliéf
    frmVýběr.Caption = "Vybrat uložený reliéf pro slučovací operace"
    Enabled = False
    DoEvents
    frmVýběr.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmVýběr.Nic
    If Nic Then Exit Sub
    On Error GoTo vypadni
    Open App.Path & "\Prvky\" & frmVýběr.Reliéf For Binary As #1
    Dim Šířka As Integer, Výška As Integer
    Get #1, , Šířka
    Get #1, , Výška
    ReDim SoukrMapa(Šířka - 1, Výška - 1)
    Get #1, , SoukrMapa()
    Close #1
    SMDimensována = True
    Exit Sub
vypadni:
    MsgBox "Chyba při otvírání!", vbCritical
End Sub

Public Sub NačtiReliéf()
    Dim X%, Y%, Š%, v%
    Š = Zásobník.Šířka
    v = Zásobník.Výška
    If Š = 0 Then
        Š = frmEditorPrvků.opRozměry.Šířka
    End If
    If v = 0 Then
        v = frmEditorPrvků.opRozměry.Výška
    End If
    ReDim SoukrMapa(Š - 1, v - 1)
    SMDimensována = True
    For X = 0 To Š - 1
        For Y = 0 To v - 1
            SoukrMapa(X, Y) = Zásobník.Bod(X, Y)
        Next
    Next
End Sub
