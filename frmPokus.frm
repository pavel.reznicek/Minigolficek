VERSION 5.00
Begin VB.Form frmPokus 
   AutoRedraw      =   -1  'True
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox obrVzorOkraje 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   4440
      Picture         =   "frmPokus.frx":0000
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   2
      Top             =   0
      Width           =   225
   End
   Begin VB.PictureBox obrVzorMaska 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   4200
      Picture         =   "frmPokus.frx":0532
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   1
      Top             =   0
      Width           =   225
   End
   Begin VB.PictureBox obrVzor 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   3960
      Picture         =   "frmPokus.frx":0A64
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   0
      Top             =   0
      Width           =   225
   End
End
Attribute VB_Name = "frmPokus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Okraj As Boolean

Private Sub Form_Load()
    Dim X, Y
    For X = 0 To 14
        For Y = 0 To 14
        'If X ^ 2 + Y ^ 2 <= 7 ^ 2 _
        'And obrVzorMaska.Point(X + 7, Y + 7) = vbWhite Then
'        And obrVzor.Point(X + 7, Y + 7) <> vbBlack
        'If ((X - 7) ^ 2 + (Y - 7) ^ 2 <= 7 ^ 2) _
        'Or (obrVzor.Point(X, Y) <> 0 Or obrVzorOkraje.Point(X, Y) = 0) Then
            'If JeNaKraji(CXYZ(X, Y)) Then
            If JeVM��ku(X, Y) Then
                Const Zv = 12
                Line (X * Zv, Y * Zv)-((X + 1) * Zv, (Y + 1) * Zv), , B
            End If
        Next Y
    Next X
End Sub

Private Function JeVM��ku(X, Y) As Boolean
    ' Pokud sou�et �tverc� nad ob�ma odv�snami (X a Y)
    ' je men�� ne� �tverec nad p�eponou (polom�rem)
    ' nebo shodn�,
    ' pak je bod (X, Y) uvnit� kruhu.
    JeVM��ku = ((X - 7) ^ 2 + (Y - 7) ^ 2 <= 7 ^ 2) _
    Or obrVzorOkraje.Point(X, Y) = 0
    '
End Function

Private Function JeNaKraji(Bod As XYZ) As Boolean
    ' M�lo by se to po��tat matematicky,
    ' jestli bod le�� na kru�nici,
    ' ale sou�adnicov� zkreslen� by zp�sobilo,
    ' �e by to v�t�inou bylo t�sn� vedle.
    ' Proto to mus�me zjistit z bitmapy.
    JeNaKraji = (obrVzorOkraje.Point(Bod.X, Bod.Y) = vbBlack)
    'Debug.Assert Not (Nejnov�j��Zdvih - 7 > 0)
    'Debug.Assert Not JeNaKraji
    'Debug.Assert JeVM��ku(Bod.X, Bod.Y)
End Function

