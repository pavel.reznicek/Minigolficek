# Minigolfíček

A miniature golf simulation game written in VB6, the base for the later 
MinigolfeeCheck. All in Czech, including identifiers.
This is a static legacy source code repository.
Although the inner documentation states otherwise, the code has been made 
available to the public under the terms of the GNU General Public Licence, 
version 3.0 (GNU/GPLv3), on September 13th, 2016.

Hra simulující minigolf napsaná ve VB6, základ pro pozdější hru, 
která se anglicky jmenuje MinigolfeeCheck.
Vše v češtině, včetně identifikátorů.
Toto je statické úložiště staršího zdrojového kódu.
Přestože vnitřní dokumentace hovoří jinak, kód byl zpřístupněn veřejnosti
pod podmínkami Všeobecné veřejné licence GNU, verse 3.0 (GNU/GPLv3), 
13. září 2016.
