VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEditorPrvk� 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Editor ter�nn�ch prvk�"
   ClientHeight    =   4905
   ClientLeft      =   7530
   ClientTop       =   615
   ClientWidth     =   4455
   Icon            =   "frmEditorPrvk�.frx":0000
   MaxButton       =   0   'False
   ScaleHeight     =   327
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   297
   Begin Minigolf��ek.Stupn� opStupn� 
      Height          =   3255
      Left            =   2880
      TabIndex        =   5
      Top             =   600
      Width           =   1575
      _extentx        =   2773
      _extenty        =   5736
      n�pis           =   "&Ter�n:"
   End
   Begin VB.PictureBox obrPamObr 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   1560
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   33
      TabIndex        =   8
      Top             =   1320
      Visible         =   0   'False
      Width           =   495
   End
   Begin Minigolf��ek.Rozm�ry opRozm�ry 
      Height          =   540
      Left            =   2880
      TabIndex        =   7
      Top             =   0
      Width           =   1575
      _extentx        =   2773
      _extenty        =   931
      max���ka        =   64
      maxv��ka        =   64
      ���ka           =   32
      v��ka           =   32
   End
   Begin Minigolf��ek.UkazatelXYZ opUkazatelXYZ 
      Height          =   765
      Left            =   2880
      TabIndex        =   6
      Top             =   3870
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1349
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin VB.CommandButton tlBarva 
      Height          =   495
      Left            =   1560
      Picture         =   "frmEditorPrvk�.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Kreslic� barva"
      Top             =   0
      Width           =   495
   End
   Begin VB.PictureBox obrNorm�l 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   495
      Index           =   1
      Left            =   2160
      MouseIcon       =   "frmEditorPrvk�.frx":074C
      MousePointer    =   99  'Custom
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   0
      Top             =   0
      Width           =   495
   End
   Begin VB.PictureBox obrZv�t�en� 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   495
      Index           =   1
      Left            =   0
      MouseIcon       =   "frmEditorPrvk�.frx":0A56
      MousePointer    =   99  'Custom
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   1
      Top             =   0
      Width           =   495
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   840
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".prv"
      Filter          =   "Prvky (*.prv)|*.prv"
   End
   Begin VB.PictureBox obrNorm�l 
      AutoRedraw      =   -1  'True
      Height          =   495
      Index           =   0
      Left            =   2160
      MouseIcon       =   "frmEditorPrvk�.frx":0D60
      MousePointer    =   99  'Custom
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   3
      Top             =   600
      Width           =   495
   End
   Begin VB.PictureBox obrZv�t�en� 
      AutoRedraw      =   -1  'True
      Height          =   495
      Index           =   0
      Left            =   0
      MouseIcon       =   "frmEditorPrvk�.frx":106A
      MousePointer    =   99  'Custom
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   29
      TabIndex        =   2
      Top             =   600
      Width           =   495
   End
   Begin MSComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   9
      Top             =   4635
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   476
      Style           =   1
      SimpleText      =   "[nov� prvek]"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Menu nabSoubor 
      Caption         =   "&Soubor"
      Begin VB.Menu nabNov�Prvek 
         Caption         =   "&Nov� prvek"
         Shortcut        =   ^N
      End
      Begin VB.Menu nabOtev��tPrvek 
         Caption         =   "&Otev��t prvek"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabUlo�itPrvek 
         Caption         =   "&Ulo�it prvek"
         Shortcut        =   ^U
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
      End
      Begin VB.Menu nabNov�Reli�f 
         Caption         =   "Nov� reli�&f"
      End
      Begin VB.Menu nabOtev��tReli�f 
         Caption         =   "Otev��t &reli�f"
      End
      Begin VB.Menu nabUlo�itReli�f 
         Caption         =   "Ulo�it re&li�f"
      End
      Begin VB.Menu nabSep2 
         Caption         =   "-"
      End
      Begin VB.Menu nabNov�Textura 
         Caption         =   "Nov� textur&a"
      End
      Begin VB.Menu nabOtev��tTexturu 
         Caption         =   "Otev��t &texturu"
      End
      Begin VB.Menu nabUlo�itTexturu 
         Caption         =   "Ulo�it te&xturu"
      End
      Begin VB.Menu nabSep6 
         Caption         =   "-"
      End
      Begin VB.Menu nabMinigolf��ek 
         Caption         =   "=> &Minigolf��ek"
         Shortcut        =   {F2}
      End
      Begin VB.Menu nabSb�rka 
         Caption         =   "=> &Sb�rka ter�nn�ch prvk�"
         Shortcut        =   +{F3}
      End
      Begin VB.Menu nabEditorDrah 
         Caption         =   "=> Editor &drah"
         Shortcut        =   {F3}
      End
      Begin VB.Menu nabSep9 
         Caption         =   "-"
      End
      Begin VB.Menu nabZav��t 
         Caption         =   "&Zav��t"
      End
      Begin VB.Menu nabKonec 
         Caption         =   "&Konec"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu nabPrvek 
      Caption         =   "&Prvek"
      Begin VB.Menu nabOtev��t 
         Caption         =   "&Otev��t ze z�sobn�ku"
         Shortcut        =   +{INSERT}
      End
      Begin VB.Menu nabUlo�it 
         Caption         =   "&Ulo�it do z�sobn�ku"
         Shortcut        =   ^{INSERT}
      End
   End
   Begin VB.Menu nab�pravy 
      Caption         =   "�&pravy"
      Begin VB.Menu nabObnovit 
         Caption         =   "P�e&barvit reli�f"
         Shortcut        =   {F5}
      End
      Begin VB.Menu nabSep4 
         Caption         =   "-"
      End
      Begin VB.Menu nabRelTex 
         Caption         =   "&Reli�f => textura"
         Shortcut        =   {F6}
      End
      Begin VB.Menu nabTexRel 
         Caption         =   "&Textura => reli�f"
         Shortcut        =   +{F6}
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
      End
      Begin VB.Menu nabZv��it 
         Caption         =   "&Zv��it reli�f"
         Shortcut        =   {F7}
      End
      Begin VB.Menu nabSn�it 
         Caption         =   "&Sn�it reli�f"
         Shortcut        =   +{F7}
      End
      Begin VB.Menu nabSep7 
         Caption         =   "-"
      End
      Begin VB.Menu nabP�eklopit 
         Caption         =   "Zrcadlov� p�e&klopit"
         Begin VB.Menu nabP�eklopitVodorovn� 
            Caption         =   "&vodorovn�"
            Shortcut        =   {F8}
         End
         Begin VB.Menu nabP�eklopitSvisle 
            Caption         =   "&svisle"
            Shortcut        =   +{F8}
         End
      End
      Begin VB.Menu nabOto�it 
         Caption         =   "&Oto�it"
         Begin VB.Menu nabOto�itPodleRu�i�ek 
            Caption         =   "&ve sm�ru hodinov�ch ru�i�ek"
            Shortcut        =   ^{F8}
         End
         Begin VB.Menu nabOto�itProtiRu�i�k�m 
            Caption         =   "&proti sm�ru hodinov�ch ru�i�ek"
            Shortcut        =   +^{F8}
         End
      End
      Begin VB.Menu nabSep8 
         Caption         =   "-"
      End
      Begin VB.Menu nabP�edvolby 
         Caption         =   "&P�edvolby"
         Shortcut        =   {F9}
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabBarva 
         Caption         =   "&Kreslic� barva �"
      End
      Begin VB.Menu nabSep5 
         Caption         =   "-"
      End
      Begin VB.Menu nabPropojit 
         Caption         =   "&Propojit tex. a rel."
         Shortcut        =   ^P
      End
      Begin VB.Menu nabKrTex 
         Caption         =   "Kreslit &texturu"
         Checked         =   -1  'True
         Shortcut        =   {F11}
      End
      Begin VB.Menu nabKrRel 
         Caption         =   "Kreslit &reli�f"
         Checked         =   -1  'True
         Shortcut        =   {F12}
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmEditorPrvk�"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim Mapa() As Single
Dim Neulo�eno() As Boolean
Dim NejmV��ka As Integer
Dim Neuplat�uj As Boolean

Private Sub Form_Load()
    DefTPP
    opStupn�.V��kaVzorku = opStupn�.Max - opStupn�.Min
    opRozm�ry.Move 0, 0
    opUkazatelXYZ.Move 0, Spodek(opRozm�ry) + 1
    opStupn�.Move 0, Spodek(opUkazatelXYZ) + 1
    NejmV��ka = Spodek(opStupn�)
    Height = Height + (-ScaleHeight + Spodek(opStupn�) + sbrStav.Height) * TPPY
    Dim I As PictureBox
    For Each I In obrZv�t�en�
    With I
    .ScaleWidth = .ScaleWidth / 4
    .ScaleHeight = .ScaleHeight / 4
    End With
    Next I
    obrNorm�l(0).BackColor = opStupn�.Barva
    obrZv�t�en�(0).BackColor = obrNorm�l(0).BackColor
    P�izp�sobRozm�ry
    ReDim Neulo�eno(0 To 2)
    Barva = DejNastaven�("Barvy", "Kreslic�", vbWhite)
    opUkazatelXYZ.NastavXYZ 1, 1, 0
    Load frmV�b�r
    Set frmV�b�r.opStupn� = opStupn�
    Propojit = DejNastaven�("Nastaven�", "Propojit v EP", False)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Erase Mapa
    If Not frmMinigolf��ek.Visible And Not (frmEditorDrah.Visible Or frmSb�rka.Visible) Then
        End
    Else
        Cancel = True
        Me.Hide
        frmP�edvolby.Hide
    End If
'    Unload frmV�b�r
End Sub

Private Sub nabBarva_Click()
    CD.Color = Barva
    CD.Flags = cdlCCRGBInit
    On Error GoTo vypadni
    CD.ShowColor
    Barva = CD.Color
vypadni:
End Sub

Private Sub nabEditorDrah_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Editor drah"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\EditorDrah", vbNormalFocus
    frmEditorDrah.Show
End Sub

Private Sub nabKonec_Click()
    End
End Sub

Private Sub nabKrRel_Click()
    KrRel = Not KrRel
End Sub

Private Property Get KrRel() As Boolean
    KrRel = nabKrRel.Checked
End Property

Private Property Let KrRel(Nosi� As Boolean)
    nabKrRel.Checked = Nosi�
End Property

Private Sub nabKrTex_Click()
    KrTex = Not KrTex
End Sub

Private Property Get KrTex() As Boolean
    KrTex = nabKrTex.Checked
End Property

Private Property Let KrTex(Nosi� As Boolean)
    nabKrTex.Checked = Nosi�
End Property

Private Sub nabMinigolf��ek_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Minigolf��ek"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\Minigolf��ek", vbNormalFocus
    frmMinigolf��ek.Show
End Sub

Private Sub nabOtev��t_Click()
    Otev�iZeZ�sobn�ku
End Sub

Private Sub nabOto�itPodleRu�i�ek_Click()
    Dim X&, Y&, �&, v&, N�&, NV&
    � = opRozm�ry.���ka - 1
    v = opRozm�ry.V��ka - 1
    N� = v
    NV = �
    Dim Nov�Mapa!()
    Dim Nov�Textura&()
    ReDim Nov�Mapa(N�, NV)
    ReDim Nov�Textura(N�, NV)
    For X = 0 To N�
        For Y = 0 To NV
            Nov�Mapa(X, Y) = Mapa(Y, N� - X)
            Nov�Textura(X, Y) = obrNorm�l(dzTextura).Point(Y, N� - X)
        Next
    Next
    Neuplat�uj = True
    opRozm�ry.Nastav N� + 1, NV + 1
    Neuplat�uj = False
    For X = 0 To N�
        For Y = 0 To NV
            NastavBodReli�fu X, Y, Nov�Mapa(X, Y)
            NastavBodTextury X, Y, Nov�Textura(X, Y)
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvTextura) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabOto�itProtiRu�i�k�m_Click()
    Dim X&, Y&, �&, v&, N�&, NV&
    � = opRozm�ry.���ka - 1
    v = opRozm�ry.V��ka - 1
    N� = v
    NV = �
    Dim Nov�Mapa!()
    Dim Nov�Textura&()
    ReDim Nov�Mapa(N�, NV)
    ReDim Nov�Textura(N�, NV)
    For X = 0 To N�
        For Y = 0 To NV
            Nov�Mapa(X, Y) = Mapa(NV - Y, X)
            Nov�Textura(X, Y) = obrNorm�l(dzTextura).Point(NV - Y, X)
        Next
    Next
    Neuplat�uj = True
    opRozm�ry.Nastav N� + 1, NV + 1
    Neuplat�uj = False
    For X = 0 To N�
        For Y = 0 To NV
            NastavBodReli�fu X, Y, Nov�Mapa(X, Y)
            NastavBodTextury X, Y, Nov�Textura(X, Y)
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvTextura) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabPropojit_Click()
    Propojit = Not Propojit
End Sub

Public Property Get Propojit() As Boolean
    Propojit = nabPropojit.Checked
End Property

Public Property Let Propojit(Nosi� As Boolean)
    nabPropojit.Checked = Nosi�
    nabKrRel = Nosi�
    nabKrTex = Nosi�
    Ulo�Nastaven� "Nastaven�", "Propojit v EP", Nosi�
End Property

Private Sub nabP�eklopitSvisle_Click()
    Dim �&, v&, X&, Y&
    � = opRozm�ry.���ka - 1
    v = opRozm�ry.V��ka - 1
    Dim Sloupec!()
    Dim SloupecTextury&()
    ReDim Sloupec!(v)
    ReDim SloupecTextury&(v)
    For X = 0 To �
        For Y = 0 To v
            Sloupec(Y) = Mapa(X, v - Y)
            SloupecTextury(Y) = obrNorm�l(dzTextura).Point(X, v - Y)
        Next
        For Y = 0 To v
            NastavBodReli�fu X, Y, Sloupec(Y)
            NastavBodTextury X, Y, SloupecTextury(Y)
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvTextura) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabP�eklopitVodorovn�_Click()
    Dim �&, v&, X&, Y&
    � = opRozm�ry.���ka - 1
    v = opRozm�ry.V��ka - 1
    Dim ��dek!()
    Dim ��dekTexury&()
    ReDim ��dek!(�)
    ReDim ��dekTexury&(�)
    For Y = 0 To v
        For X = 0 To �
            ��dek(X) = Mapa(� - X, Y)
            ��dekTexury&(X) = obrNorm�l(dzTextura).Point(� - X, Y)
        Next
        For X = 0 To �
            NastavBodReli�fu X, Y, ��dek(X)
            NastavBodTextury X, Y, ��dekTexury&(X)
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvTextura) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabSb�rka_Click()
    frmSb�rka.Show
End Sub

Private Sub nabUlo�it_Click()
    Ulo�DoZ�sobn�ku
End Sub

Private Sub nabZav��t_Click()
    Unload Me
End Sub

Private Sub opRozm�ry_Nastaven�()
    P�izp�sobRozm�ry
End Sub

Private Sub opRozm�ry_Ru�n�Zm�na()
    P�izp�sobRozm�ry
End Sub

Private Sub opStupn�_ColorSelect()
    UplatniMapu
End Sub

Private Sub tlBarva_Click()
    CD.Color = Barva
    CD.Flags = cdlCCRGBInit
    On Error GoTo vypadni
    CD.ShowColor
    Barva = CD.Color
vypadni:
End Sub
Private Sub nabN�pov�da_Click()
'    frmN�pov�da.Show
    On Error GoTo Neni_spu�t�n�_n�pov�da
    AppActivate "N�pov�da�k�Minigolf��ku"
    Exit Sub
Neni_spu�t�n�_n�pov�da:
    Shell "winhelp " & App.Path & "\Minigolficek.hlp"
End Sub

Private Sub nabNov�Prvek_Click()
    If Neulo�eno(dvPrvek) Then
        If MsgBox("Opustit tento prvek a za��t nov�?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    Dim I As PictureBox
    For Each I In obrNorm�l
        I.Picture = Nothing
    Next
    For Each I In obrZv�t�en�
        I.Picture = Nothing
    Next
    ReDim Mapa(opRozm�ry.���ka - 1, opRozm�ry.V��ka - 1)
    ReDim Neulo�eno(0 To 2)
    frmV�b�r.Prvek = ""
    frmV�b�r.Reli�f = ""
    frmV�b�r.Textura = ""
    sbrStav.SimpleText = "[nov� prvek]"
End Sub

Private Sub nabNov�Reli�f_Click()
    If Neulo�eno(dvReli�f) Then
        If MsgBox("Opustit tento reli�f a za��t nov�?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    obrNorm�l(0).Cls
    obrZv�t�en�(0).Cls
    ReDim Mapa(opRozm�ry.���ka - 1, opRozm�ry.V��ka - 1)
    Neulo�eno(dvReli�f) = False
    frmV�b�r.Reli�f = ""
End Sub

Private Sub nabNov�Textura_Click()
    If Neulo�eno(dvTextura) Then
        If MsgBox("Opustit tuto texturu a za��t novou?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    obrNorm�l(1).Cls
    obrZv�t�en�(1).Cls
    Neulo�eno(dvTextura) = False
    frmV�b�r.Textura = ""
End Sub

Private Sub nabObnovit_Click()
    UplatniMapu
End Sub

Private Sub nabOtev��tPrvek_Click()
    If Neulo�eno(dvPrvek) Then
        If MsgBox("Opustit tento prvek a otev��t jin�?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    frmV�b�r.Caption = "Otev��t prvek"
    frmV�b�r.Druh = dvPrvek
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
'    obrNorm�l(1).AutoSize = True
    On Error GoTo vypadni
    obrNorm�l(1).Picture = LoadPicture(App.Path + "\Prvky\" + frmV�b�r.Textura)
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Binary As #1
    On Error GoTo 0
    Dim ���ka As Integer, V��ka As Integer
    Get #1, , ���ka
    Get #1, , V��ka
    ReDim Mapa(���ka - 1, V��ka - 1)
    Get #1, , Mapa()
    Close #1
'    Z�sobn�k.Reli�f = Mapa()
'    Z�sobn�k.Textura = obrNorm�l(1).Image
    opRozm�ry.Nastav ���ka, V��ka
    Dim X, Y
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            'Debug.Assert Mapa(X, Y) = 0
'            Debug.Print Mapa(X, Y)
            ZobrazBodReli�fu X, Y
            Zv�t�iBodTextury X, Y
        Next
'        obrZv�t�en�(1).Refresh
'        obrZv�t�en�(0).Refresh
'        obrNorm�l(0).Refresh
        Refresh
    Next
    ReDim Neulo�eno(0 To 2) As Boolean
    sbrStav.SimpleText = Hol�Jm�no(frmV�b�r.Prvek)
    Exit Sub
vypadni:
    On Error GoTo 0
    MsgBox "Chyba p�i otev�r�n�!", vbCritical
End Sub

Private Sub nabOtev��tReli�f_Click()
    If Neulo�eno(dvReli�f) Then
        If MsgBox("Opustit tento reli�f a otev��t jin�?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    frmV�b�r.Druh = dvReli�f
    frmV�b�r.Caption = "Otev��t reli�f"
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    On Error GoTo vypadni
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Binary As #1
    Dim ���ka As Integer, V��ka As Integer
    Get #1, , ���ka
    Get #1, , V��ka
    ReDim Mapa(���ka - 1, V��ka - 1)
    Get #1, , Mapa()
    Close #1
    opRozm�ry.Nastav ���ka, V��ka
    Dim X, Y
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            ZobrazBodReli�fu X, Y
        Next
        Refresh
    Next
    Neulo�eno(dvReli�f) = False
    
    Exit Sub
vypadni:
    On Error GoTo 0
    MsgBox "Chyba p�i otv�r�n�!", vbCritical
End Sub

Private Sub nabOtev��tTexturu_Click()
    If Neulo�eno(dvTextura) Then
        If MsgBox("Opustit tuto texturu a otev��t jinou?", vbQuestion Or vbYesNo) = vbNo Then Exit Sub
    End If
    frmV�b�r.Caption = "Otev��t texturu"
    frmV�b�r.Druh = dvTextura
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    On Error GoTo vypadni
    obrNorm�l(1).Picture = LoadPicture(App.Path + "\Prvky\" + frmV�b�r.Textura)
    On Error GoTo 0
    Dim X, Y
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Zv�t�iBodTextury X, Y
        Next
        Refresh
    Next
    Neulo�eno(dvTextura) = False
    Exit Sub
vypadni:
    On Error GoTo 0
    MsgBox "Chyba p�i otv�r�n�!", vbCritical
End Sub

Private Sub nabP�edvolby_Click()
    frmP�edvolby.Show , Me
End Sub

Private Sub nabRelTex_Click()
    obrNorm�l(dzTextura).Picture = obrNorm�l(0).Image
    Dim X&, Y&
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Zv�t�iBodTextury X, Y
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvTextura) = True
End Sub

Private Sub nabSn�it_Click()
    Dim X#, Y#
    Static OKolik#
    OKolik = Val(InputBox("Zadejte ��slo vyjad�uj�c� v��ku, o ni� se m� sn�it reli�f", "Sn�en�", OKolik))
    If OKolik = 0 Then Exit Sub
    DoEvents
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Mapa(X, Y) = Mapa(X, Y) - OKolik
            If Mapa(X, Y) < -50 Then Mapa(X, Y) = -50
            If Mapa(X, Y) > 150 Then Mapa(X, Y) = 150
            ZobrazBodReli�fu X, Y
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabTexRel_Click()
    Dim X&, Y&, B&
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            B = obrNorm�l(1).Point(X, Y)
            Mapa(X, Y) = CInt((�erven�(B) + Zelen�(B) + Modr�(B)) / 3 / 255 * 32)
            ZobrazBodReli�fu X, Y
        Next X
        Refresh
    Next Y
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub nabUlo�itPrvek_Click()
    frmV�b�r.Caption = "Ulo�it prvek"
    frmV�b�r.Druh = dvPrvek
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    PojmenujNovou��st
    On Error GoTo chyba
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Output As #1
    Close #1
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Binary As #1
    Put #1, , CInt(opRozm�ry.���ka)
    Put #1, , CInt(opRozm�ry.V��ka)
    Put #1, , Mapa()
    Close
    obrPamObr.Move 0, 0, opRozm�ry.V��ka, opRozm�ry.V��ka
    obrPamObr.Picture = LoadPicture()
    obrPamObr.PaintPicture obrNorm�l(1).Image, 0, 0, opRozm�ry.���ka, opRozm�ry.V��ka
    SavePicture obrPamObr.Image, App.Path & "\Prvky\" & frmV�b�r.Textura
    On Error GoTo 0
    ReDim Neulo�eno(0 To 2) As Boolean
    sbrStav.SimpleText = Hol�Jm�no(frmV�b�r.Prvek)
    Exit Sub
chyba:
    On Error GoTo 0
    MsgBox "Chyba p�i ukl�d�n�!", vbExclamation
End Sub

Private Sub nabUlo�itReli�f_Click()
    frmV�b�r.Caption = "Ulo�it reli�f"
    frmV�b�r.Druh = dvReli�f
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    PojmenujNovou��st
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Binary As #1
    Put #1, , CInt(opRozm�ry.���ka)
    Put #1, , CInt(opRozm�ry.V��ka)
    Put #1, , Mapa()
    Close
    On Error GoTo 0
    ReDim Neulo�eno(0 To 2) As Boolean
    Exit Sub
vypadni:
    On Error GoTo 0
    MsgBox "Chyba p�i ukl�d�n�!", vbExclamation
End Sub

Private Sub nabUlo�itTexturu_Click()
    frmV�b�r.Caption = "Ulo�it reli�f"
    frmV�b�r.Druh = dvTextura
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    PojmenujNovou��st
    obrPamObr.Move 0, 0, opRozm�ry.V��ka, opRozm�ry.V��ka
    obrPamObr.Picture = LoadPicture()
    obrPamObr.PaintPicture obrNorm�l(1).Image, 0, 0, opRozm�ry.���ka, opRozm�ry.V��ka
    SavePicture obrPamObr.Image, App.Path & "\Prvky\" & frmV�b�r.Textura
    On Error GoTo 0
    ReDim Neulo�eno(0 To 2) As Boolean
    Exit Sub
vypadni:
    On Error GoTo 0
    MsgBox "Chyba p�i ukl�d�n�!", vbExclamation
End Sub

Private Sub nabZv��it_Click()
    Dim X#, Y#
    Static OKolik#
    OKolik = Val(InputBox("Zadejte ��slo vyjad�uj�c� v��ku, o ni� se m� zv��it reli�f", "Zv��en�", OKolik))
    If OKolik = 0 Then Exit Sub
    DoEvents
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Mapa(X, Y) = Mapa(X, Y) + OKolik
            If Mapa(X, Y) < -50 Then Mapa(X, Y) = -50
            If Mapa(X, Y) > 150 Then Mapa(X, Y) = 150
            ZobrazBodReli�fu X, Y
        Next
        Refresh
    Next
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvReli�f) = True
End Sub

Private Sub obrNorm�l_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrNorm�l(Index).ScaleWidth _
    Or Y >= obrNorm�l(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub obrNorm�l_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrNorm�l(Index).ScaleWidth _
    Or Y >= obrNorm�l(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub obrNorm�l_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrNorm�l(Index).ScaleWidth _
    Or Y >= obrNorm�l(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub obrZv�t�en�_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrZv�t�en�(Index).ScaleWidth _
    Or Y >= obrZv�t�en�(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub obrZv�t�en�_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrZv�t�en�(Index).ScaleWidth _
    Or Y >= obrZv�t�en�(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub obrZv�t�en�_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If X < 0 Or Y < 0 _
    Or X >= obrZv�t�en�(Index).ScaleWidth _
    Or Y >= obrZv�t�en�(Index).ScaleHeight Then Exit Sub
    NakresliBod Index, X, Y, Button
End Sub

Private Sub P�izp�sobRozm�ry()
    Dim I As PictureBox
    For Each I In obrZv�t�en�
        With I
            .Width = opRozm�ry.���ka * 4 + (.Width - .ScaleWidth * 4)
            .Height = opRozm�ry.V��ka * 4 + (.Height - .ScaleHeight * 4)
            .Left = opRozm�ry.Width + 8
            'DoEvents
            If .Index = 0 Then
                Dim Spodek_tlB%, Vr�ekRel%
                Spodek_tlB = obrNorm�l(1).Height - obrNorm�l(1).ScaleHeight + opRozm�ry.V��ka + 8 + 36
                Vr�ekRel = IIf(.Height >= Spodek_tlB, .Height, Spodek_tlB) + 8
                .Top = Abs(.Index - 1) * Vr�ekRel
            Else
                .Top = 0
            End If
        End With
    Next I
    For Each I In obrNorm�l
        With I
            .Width = opRozm�ry.���ka + (.Width - .ScaleWidth)
            .Height = opRozm�ry.V��ka + (.Height - .ScaleHeight)
            .Top = obrZv�t�en�(I.Index).Top
            .Left = Pravobok(obrZv�t�en�(I.Index)) + 8  '+ (opStupn�.Left - obrZv�t�en�(i.Index).Width - .Width) / 2
        End With
    Next I
    With obrNorm�l(1)
        Dim tlBLevobok%
        tlBLevobok = .Left + IIf(opRozm�ry.���ka >= 32, .Width / 2 - 18, 0)
        tlBarva.Move tlBLevobok, .Top + .Height + 8, 36, 36
    End With
    ' rozm��en� formu
    Dim ���e As Integer, V��e As Integer, opStV��e As Integer
    ���e = IIf(opRozm�ry.���ka >= 32, Pravobok(obrNorm�l(1)), Pravobok(tlBarva)) + 8
    V��e = IIf(NejmV��ka >= Spodek(obrZv�t�en�(0)), NejmV��ka, Spodek(obrZv�t�en�(0))) + sbrStav.Height
    Width = Width + TPPX * (-ScaleWidth + ���e)
    Height = Height + TPPY * (-ScaleHeight + V��e)
    If Left + Width > Screen.Width Then
        Move Screen.Width - Width
        If Left < 0 Then Left = 0
    End If
    If Top + Height > Screen.Height Then
        Move Left, Screen.Height - Height
        If Top < 0 Then Top = 0
    End If
    ' p�izp�soben� v��ky opStupn�
    opStV��e = ScaleHeight - sbrStav.Height - opStupn�.Top
    'If opStV��e > 375 Then opStV��e = 375
    opStupn�.Height = opStV��e
    DoEvents
    Rozm��Mapu
End Sub

Sub Rozm��Mapu()
    Dim �%, v%, X%, Y%
    � = opRozm�ry.���ka - 1
    v = opRozm�ry.V��ka - 1
    On Error GoTo Nedimensov�no
    Mapa(0, 0) = Mapa(0, 0)
    If � = UBound(Mapa, 1) And v = UBound(Mapa, 2) Then
        Exit Sub
    ElseIf � = UBound(Mapa, 1) Then
        ReDim Preserve Mapa(�, v)
        GoTo Ulpatni_a_vypadni
    End If
    Dim Nov�Mapa() As Single
    ReDim Nov�Mapa(�, v) As Single
    On Error Resume Next
    For X = 0 To �
        For Y = 0 To v
            Nov�Mapa(X, Y) = Mapa(X, Y)
        Next
    Next
    ReDim Mapa(�, v)
    For X = 0 To �
        For Y = 0 To v
            Mapa(X, Y) = Nov�Mapa(X, Y)
        Next
    Next
    Erase Nov�Mapa
    On Error GoTo 0
Ulpatni_a_vypadni:
    If Not Neuplat�uj Then UplatniMapu
    Exit Sub
Nedimensov�no:
    ReDim Mapa(�, v)
    Resume
End Sub

Private Sub NakresliBod(Druh_Index As Integer, _
ByVal X As Single, ByVal Y As Single, Button As Integer)
    X = Int(X)
    Y = Int(Y)
    If (Druh_Index = 0 And Not Propojit) Or (Propojit And KrRel) Then
        If Button = vbLeftButton Then
            NastavBodReli�fu X, Y, opStupn�.V��ka
            Neulo�eno(dvPrvek) = True
            Neulo�eno(dvReli�f) = True
        ElseIf Button = vbRightButton Then
            opStupn�.V��ka = Mapa(X, Y)
        End If
    End If
    If (Druh_Index = 1 And Not Propojit) Or (Propojit And KrTex) Then
        If Button = vbLeftButton Then
            NastavBodTextury X, Y, Barva
            Neulo�eno(dvPrvek) = True
            Neulo�eno(dvTextura) = True
        ElseIf Button = vbRightButton Then
            Barva = obrNorm�l(1).Point(X, Y)
        End If
    End If
    On Error Resume Next
    opUkazatelXYZ.NastavXYZ X + 1, Y + 1, Mapa(X, Y)
End Sub

Private Sub NastavBodTextury(ByVal X&, ByVal Y&, ByVal NaBarvu As Long)
    On Error Resume Next
    obrNorm�l(dzTextura).PSet (X, Y), NaBarvu
    obrZv�t�en�(dzTextura).Line (X, Y)-(X + 4 / 5, Y + 4 / 5), NaBarvu, BF
End Sub

Private Sub NastavBodReli�fu(ByVal X&, ByVal Y&, ByVal NaV��ku As Single)
    On Error Resume Next
    Mapa(X, Y) = NaV��ku
    obrNorm�l(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(NaV��ku)
    obrZv�t�en�(dzReli�f).Line (X, Y)-(X + 4 / 5, Y + 4 / 5), obrNorm�l(dzReli�f).Point(X, Y), BF
End Sub

Private Sub ZobrazBodReli�fu(ByVal X&, ByVal Y&)
    On Error Resume Next
    obrNorm�l(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
    obrZv�t�en�(dzReli�f).Line (X, Y)-(X + 4 / 5, Y + 4 / 5), obrNorm�l(dzReli�f).Point(X, Y), BF
End Sub

Private Sub Zv�t�iBodTextury(ByVal X&, ByVal Y&)
    On Error Resume Next
    obrZv�t�en�(dzTextura).Line (X, Y)-(X + 4 / 5, Y + 4 / 5), obrNorm�l(dzTextura).Point(X, Y), BF
End Sub

Private Sub UplatniMapu()
    obrNorm�l(dzReli�f).Picture = LoadPicture()
    obrZv�t�en�(dzReli�f).Picture = LoadPicture()
    Dim X, Y
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            ZobrazBodReli�fu X, Y
        Next X
        Refresh
    Next Y
End Sub

Public Sub UplatniSd�lenouMapuPrvku(Optional P�idat As Reli�fn�Operace)
    Dim X%, Y%
    'ReDim Mapa(opRozm�ry.���ka - 1, opRozm�ry.V��ka - 1)
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Select Case P�idat
            Case ropVlo�it
                Mapa(X, Y) = Sd�len�MapaPrvku(X, Y)
            Case ropPr�m�r
                Mapa(X, Y) = (Mapa(X, Y) + Sd�len�MapaPrvku(X, Y)) / 2
            Case ropSjednocen�
                If Mapa(X, Y) < Sd�len�MapaPrvku(X, Y) Then
                    Mapa(X, Y) = Sd�len�MapaPrvku(X, Y)
                End If
            Case ropPr�nik
                If Mapa(X, Y) > Sd�len�MapaPrvku(X, Y) Then
                    Mapa(X, Y) = Sd�len�MapaPrvku(X, Y)
                End If
            Case Else
                Mapa(X, Y) = Mapa(X, Y) + Sd�len�MapaPrvku(X, Y) * P�idat
            End Select
            ZobrazBodReli�fu X, Y
        Next X
        Refresh
    Next Y
    Neulo�eno(dvPrvek) = True
    Neulo�eno(dvReli�f) = True
End Sub

Public Sub Otev�iZeZ�sobn�ku(Optional P�idat As Reli�fn�Operace)
    Dim X%, Y%
    Neuplat�uj = True
    opRozm�ry.Nastav Z�sobn�k.���ka, Z�sobn�k.V��ka
    Neuplat�uj = False
    'P�izp�sobRozm�ry
    Dim obr As PictureBox
    For Each obr In obrZv�t�en�
        obr.Picture = LoadPicture
    Next
    For Each obr In obrNorm�l
        obr.Picture = LoadPicture
    Next
    obrNorm�l(1).Picture = Z�sobn�k.Textura
    DoEvents
    For Y = 0 To opRozm�ry.V��ka - 1
        For X = 0 To opRozm�ry.���ka - 1
            Select Case P�idat
            Case ropVlo�it
                Mapa(X, Y) = Z�sobn�k(X, Y)
            Case ropPr�m�r
                Mapa(X, Y) = (Mapa(X, Y) + Z�sobn�k(X, Y)) / 2
            Case ropSjednocen�
                If Mapa(X, Y) < Z�sobn�k(X, Y) Then
                    Mapa(X, Y) = Z�sobn�k(X, Y)
                End If
            Case ropPr�nik
                If Mapa(X, Y) > Z�sobn�k(X, Y) Then
                    Mapa(X, Y) = Z�sobn�k(X, Y)
                End If
            Case Else
                Mapa(X, Y) = Mapa(X, Y) + Z�sobn�k(X, Y) * P�idat
            End Select
            ZobrazBodReli�fu X, Y
            Zv�t�iBodTextury X, Y
        Next X
        Refresh
    Next Y
    sbrStav.SimpleText = "[prvek ze z�sobn�ku]"
End Sub

Public Sub Ulo�DoZ�sobn�ku(Optional P�idat As Reli�fn�Operace)
    Dim X%, Y%, �%, v%
    � = opRozm�ry.���ka
    v = opRozm�ry.V��ka
    Z�sobn�k.Rozm�� �, v
    For Y = 0 To v - 1
        For X = 0 To � - 1
            Select Case P�idat
            Case ropVlo�it
                Z�sobn�k(X, Y) = Mapa(X, Y)
            Case ropPr�m�r
                Z�sobn�k(X, Y) = (Mapa(X, Y) + Z�sobn�k(X, Y)) / 2
            Case ropSjednocen�
                If Z�sobn�k(X, Y) < Mapa(X, Y) Then
                    Z�sobn�k(X, Y) = Mapa(X, Y)
                End If
            Case ropPr�nik
                If Z�sobn�k(X, Y) > Mapa(X, Y) Then
                     Z�sobn�k(X, Y) = Mapa(X, Y)
                End If
            Case Else '(ropUbrat a jin�)
                 Z�sobn�k(X, Y) = Z�sobn�k(X, Y) + Mapa(X, Y) * P�idat
            End Select
        Next X
        Refresh
    Next Y
    obrPamObr.Picture = LoadPicture()
    obrPamObr.Move 0, 0, opRozm�ry.���ka, opRozm�ry.V��ka
    obrPamObr.PaintPicture obrNorm�l(1).Image, 0, 0 ', , , 0, 0, opRozm�ry.���ka, opRozm�ry.V��ka
    Z�sobn�k.Textura = obrPamObr.Image
End Sub

Private Property Let Barva(Nosi� As Long)
    tlBarva.BackColor = Nosi�
    Ulo�Nastaven� "Barvy", "Kreslic�", tlBarva.BackColor
End Property

Private Property Get Barva() As Long
    Barva = tlBarva.BackColor
End Property

Sub PojmenujNovou��st()
    If frmV�b�r.Reli�f = "" Then frmV�b�r.Reli�f = Hol�Jm�no(frmV�b�r.Prvek) & ".rel"
    If frmV�b�r.Textura = "" Then frmV�b�r.Textura = Hol�Jm�no(frmV�b�r.Prvek) & ".bmp"
End Sub
