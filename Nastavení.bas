Attribute VB_Name = "modNastaven�"
Option Explicit
Dim pamUkotvit As Boolean
Dim pam���ka�ipky As Single
Dim pamRychlostZobrazen� As Integer
Dim pamM��itPrakem As Boolean
Dim pamBarvaT� As Long
Dim pamBarvaH� As Long
Dim pamBarvaSS As Long
Dim pamBarvaOs As Long
Dim pam�asova� As Boolean
Public TrB As OLE_COLOR 'transparentn� barva

Public Property Let ���ka�ipky(Nosi� As Single)
    pam���ka�ipky = Nosi�
    Ulo�Nastaven� "Nastaven�\�ipka", "���ka", ���ka�ipky
End Property

Public Property Get ���ka�ipky() As Single
    ���ka�ipky = pam���ka�ipky
End Property

Public Property Let RychlostZobrazen�(Nosi� As Integer)
    pamRychlostZobrazen� = Nosi�
    Ulo�Nastaven� "Nastaven�", "Rychlost", Nosi�
End Property

Public Property Get RychlostZobrazen�() As Integer
    RychlostZobrazen� = pamRychlostZobrazen�
End Property

Public Property Let Zvuk(Nosi� As Boolean)
    frmMinigolf��ek.nabZvuk.Checked = Nosi�
    frmNast.chbZvuk = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Zvuk", Nosi�
End Property

Public Property Get Zvuk() As Boolean
    Zvuk = frmMinigolf��ek.nabZvuk.Checked
End Property

Public Property Let Ukotvit(Nosi� As Boolean)
    pamUkotvit = Nosi�
    Ulo�Nastaven� "Nastaven�", "Ukotvit", Nosi�
End Property

Public Property Get Ukotvit() As Boolean
    Ukotvit = pamUkotvit
End Property

Public Property Let M��itPrakem(Nosi� As Boolean)
    pamM��itPrakem = Nosi�
    Ulo�Nastaven� "Nastaven�", "M��it prakem", Nosi�
End Property

Public Property Get M��itPrakem() As Boolean
    M��itPrakem = pamM��itPrakem
End Property

Public Property Let BarvaT�(Nosi� As Long)
    pamBarvaT� = Nosi�
    Ulo�Nastaven� "Nastaven�\�ipka", "Barva t�la", Nosi�
End Property

Public Property Get BarvaT�() As Long
    BarvaT� = pamBarvaT�
End Property

Public Property Let BarvaH�(Nosi� As Long)
    pamBarvaH� = Nosi�
    Ulo�Nastaven� "Nastaven�\�ipka", "Barva hlavi�ky", Nosi�
End Property

Public Property Get BarvaH�() As Long
    BarvaH� = pamBarvaH�
End Property

Public Property Let BarvaSS(Nosi� As Long)
    pamBarvaSS = Nosi�
    Ulo�Nastaven� "Barvy", "Syt� st�ny", Nosi�
End Property

Public Property Get BarvaSS() As Long
    BarvaSS = pamBarvaSS
End Property

Public Property Let BarvaOs(Nosi� As Long)
    pamBarvaOs = Nosi�
    Ulo�Nastaven� "Barvy", "Osv�tlen�", Nosi�
End Property

Public Property Get BarvaOs() As Long
    BarvaOs = pamBarvaOs
End Property

Public Property Let �asova�(Nosi� As Boolean)
    pam�asova� = Nosi�
    Ulo�Nastaven� "Nastaven�", "�asova�", Nosi�
    frmMinigolf��ek.tmr�as�k.Enabled = Nosi�
End Property

Public Property Get �asova�() As Boolean
    �asova� = pam�asova�
End Property

Public Property Let Vyrovn�vat(Nosi� As Boolean)
    frmEditorDrah.nabVyrovn�vat.Checked = Nosi�
    frmNast.chbVyrovn�vat = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Vyrovn�vat", Nosi�
End Property

Public Property Get Vyrovn�vat() As Boolean
    Vyrovn�vat = frmEditorDrah.nabVyrovn�vat.Checked
End Property

Public Property Let Syt�St�ny(Nosi� As Boolean)
    frmEditorDrah.nabSyt�St�ny.Checked = Nosi�
    frmNast.chbSyt�St�ny = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Syt� st�ny", Nosi�
End Property

Public Property Get Syt�St�ny() As Boolean
    Syt�St�ny = frmEditorDrah.nabSyt�St�ny.Checked
End Property

Public Property Let Zv�raz�ovat(Nosi� As Boolean)
    frmEditorDrah.nabZv�raz�ovat.Checked = Nosi�
    frmNast.chbZv�r�ik = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Zv�raz�ovat", Nosi�
End Property

Public Property Get Zv�raz�ovat() As Boolean
    Zv�raz�ovat = frmEditorDrah.nabZv�raz�ovat.Checked
End Property

Public Property Let ZvPodSSt(Nosi� As Boolean)
    frmEditorDrah.nabZvPodSt�ny.Checked = Nosi�
    frmNast.chbZv�rSyt = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Zv�raz�ovat pod st�ny", Nosi�
End Property

Public Property Get ZvPodSSt() As Boolean
    ZvPodSSt = frmEditorDrah.nabZvPodSt�ny.Checked
End Property

Public Property Let TrBGra(Nosi� As Boolean)
    frmEditorDrah.nabTrBTex.Checked = Nosi�
    frmEditorDrah.sbrStav.Panels(4).Bevel = IIf(Nosi�, sbrInset, sbrNoBevel)
    frmSb�rka.sbrStav.Panels(4).Bevel = IIf(Nosi�, sbrInset, sbrNoBevel)
    frmSb�rka.nabTrBTex.Checked = Nosi�
    frmNast.chbTrBGra = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Transparentn� textura", Nosi�
End Property

Public Property Get TrBGra() As Boolean
    TrBGra = frmEditorDrah.nabTrBTex.Checked
End Property

Public Property Let TrBRel(Nosi� As Boolean)
    frmEditorDrah.nabTrBRel.Checked = Nosi�
    frmSb�rka.nabTrBRel.Checked = Nosi�
    frmEditorDrah.sbrStav.Panels(5).Bevel = IIf(Nosi�, sbrInset, sbrNoBevel)
    frmSb�rka.sbrStav.Panels(5).Bevel = IIf(Nosi�, sbrInset, sbrNoBevel)
    frmNast.chbTrBRel = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Transparentn� reli�f", Nosi�
End Property

Public Property Get TrBRel() As Boolean
    TrBRel = frmEditorDrah.nabTrBRel.Checked
End Property

Public Property Let ObtHrany(Nosi� As Boolean)
    frmEditorDrah.nabObtahovatHrany.Checked = Nosi�
    frmNast.chbObtahovat = -Nosi�
    Ulo�Nastaven� "Nastaven�", "Obtahovat hrany", Nosi�
End Property

Public Property Get ObtHrany() As Boolean
    ObtHrany = frmEditorDrah.nabObtahovatHrany.Checked
End Property

Public Sub ObnovHodZRegistru()
    Zvuk = DejNastaven�("Nastaven�", "Zvuk", True)
    Ukotvit = DejNastaven�("Nastaven�", "Ukotvit", True)
    ���ka�ipky = DejNastaven�("Nastaven�\�ipka", "���ka", 1)
    RychlostZobrazen� = DejNastaven�("Nastaven�", "Rychlost", 1)
    M��itPrakem = DejNastaven�("Nastaven�", "M��it prakem", True)
    BarvaH� = DejNastaven�("Nastaven�\�ipka", "Barva hlavi�ky", &HFF8080)
    BarvaT� = DejNastaven�("Nastaven�\�ipka", "Barva t�la", &HFF8080)
    �asova� = DejNastaven�("Nastaven�", "�asova�", False)
    frmEditorDrah.opRozm�ry.NastavM��ku DejNastaven�("Nastaven�", "M��ka X", 32), DejNastaven�("Nastaven�", "M��ka Y", 32)
    frmSb�rka.opRozm�ry.NastavM��ku DejNastaven�("Nastaven�", "M��ka ve sb�rce X", 32), DejNastaven�("Nastaven�", "M��ka ve sb�rce Y", 32)
    frmEditorDrah.St�edit = frmEditorDrah.St�edit
    frmSb�rka.St�edit = frmSb�rka.St�edit
    frmEditorDrah.Zarovn�vat = DejNastaven�("Nastaven�", "Zarovn�vat", True)
    Vyrovn�vat = DejNastaven�("Nastaven�", "Vyrovn�vat", True)
    Syt�St�ny = DejNastaven�("Nastaven�", "Syt� st�ny", False)
    Zv�raz�ovat = DejNastaven�("Nastaven�", "Zv�raz�ovat", True)
    ZvPodSSt = DejNastaven�("Nastaven�", "Zv�raz�ovat pod st�ny", True)
    ObtHrany = DejNastaven�("Nastaven�", "Obtahovat hrany", False)
    TrB = DejNastaven�("Barvy", "Transparentn�", vbWhite)
    TrBGra = DejNastaven�("Nastaven�", "Transparentn� textura", False)
    TrBRel = DejNastaven�("Nastaven�", "Transparentn� reli�f", False)
    BarvaSS = DejNastaven�("Barvy", "Syt� st�ny", RGB(127, 127, 127))
    BarvaOs = DejNastaven�("Barvy", "Osv�tlen�", vbWhite)
End Sub

