VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEditorDrah 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Editor drah - [nov� dr�ha]"
   ClientHeight    =   4890
   ClientLeft      =   3405
   ClientTop       =   2235
   ClientWidth     =   4710
   Icon            =   "frmEditorDrah.frx":0000
   MaxButton       =   0   'False
   ScaleHeight     =   326
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   314
   StartUpPosition =   2  'CenterScreen
   Begin Minigolf��ek.Stupn� opStupn� 
      Height          =   3255
      Left            =   3000
      TabIndex        =   10
      Top             =   0
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   5741
      Zm�na           =   0,01
      N�pis           =   "Z�k&lad:"
      Max             =   1000
   End
   Begin VB.PictureBox obrPamUkl 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   540
      Left            =   2400
      ScaleHeight     =   36
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   36
      TabIndex        =   8
      Top             =   2160
      Visible         =   0   'False
      Width           =   540
   End
   Begin Minigolf��ek.UkazatelXYZ UXYZ 
      Height          =   735
      Left            =   3000
      TabIndex        =   7
      Top             =   3885
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1296
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin VB.PictureBox obrVzorOkraje 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   2160
      Picture         =   "frmEditorDrah.frx":0442
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   225
   End
   Begin Minigolf��ek.Rozm�ry opRozm�ry 
      Height          =   525
      Left            =   3000
      TabIndex        =   5
      ToolTipText     =   "Rozm�ry dr�hy"
      Top             =   3360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   953
      Max���ka        =   600
      MaxV��ka        =   480
      ���ka           =   320
      V��ka           =   320
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   2
      Left            =   120
      MouseIcon       =   "frmEditorDrah.frx":0974
      Negotiate       =   -1  'True
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   4
      Top             =   2040
      Visible         =   0   'False
      Width           =   855
      Begin VB.Image imgM��ek 
         Enabled         =   0   'False
         Height          =   240
         Index           =   2
         Left            =   480
         Picture         =   "frmEditorDrah.frx":0AC6
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image imgStart 
         Enabled         =   0   'False
         Height          =   240
         Index           =   2
         Left            =   480
         Picture         =   "frmEditorDrah.frx":0C10
         Top             =   360
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         Height          =   480
         Index           =   2
         Left            =   0
         Top             =   15
         Width           =   480
      End
   End
   Begin VB.PictureBox obrSt�ny 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox obrPamObr 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   540
      Left            =   2400
      ScaleHeight     =   36
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   36
      TabIndex        =   0
      Top             =   1440
      Visible         =   0   'False
      Width           =   540
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   1920
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".mig"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   1
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   2
      Top             =   1080
      Width           =   855
      Begin VB.Image imgM��ek 
         Enabled         =   0   'False
         Height          =   240
         Index           =   1
         Left            =   480
         Picture         =   "frmEditorDrah.frx":0D5A
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image imgStart 
         Enabled         =   0   'False
         Height          =   240
         Index           =   0
         Left            =   480
         Picture         =   "frmEditorDrah.frx":0EA4
         Top             =   360
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         Height          =   480
         Index           =   1
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      Height          =   855
      Index           =   0
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   1
      Top             =   120
      Width           =   855
      Begin VB.Image imgM��ek 
         Enabled         =   0   'False
         Height          =   240
         Index           =   0
         Left            =   480
         Picture         =   "frmEditorDrah.frx":0FEE
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image imgStart 
         Enabled         =   0   'False
         Height          =   240
         Index           =   1
         Left            =   480
         Picture         =   "frmEditorDrah.frx":1138
         Top             =   360
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         Height          =   480
         Index           =   0
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin MSComDlg.CommonDialog CDObr 
      Left            =   2400
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".bmp"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin MSComDlg.CommonDialog CDSb 
      Left            =   2400
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".mig"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin Minigolf��ek.Rozm�ry opRozmV�b 
      Height          =   540
      Left            =   1080
      TabIndex        =   9
      ToolTipText     =   "Rozm�ry v�b�rov�ho obd�n�ku"
      Top             =   3360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   953
      TextX           =   "�. v�b.:"
      TextY           =   "D. v�b.:"
      Max���ka        =   64
      MaxV��ka        =   64
      ���ka           =   64
      V��ka           =   64
      M��kaX         =   1
      M��kaY         =   1
   End
   Begin MSComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   11
      Top             =   4620
      Width           =   4710
      _ExtentX        =   8308
      _ExtentY        =   476
      SimpleText      =   "P�ipraven"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6245
            MinWidth        =   2117
            Text            =   "P�ipraven"
            TextSave        =   "P�ipraven"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmEditorDrah.frx":1282
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmEditorDrah.frx":17D6
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmEditorDrah.frx":1D2A
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmEditorDrah.frx":227E
         EndProperty
      EndProperty
   End
   Begin VB.Menu nabSoubor 
      Caption         =   "&Soubor"
      Begin VB.Menu nabNov� 
         Caption         =   "&Nov� dr�ha / sb�rka prvk�"
         Shortcut        =   ^D
      End
      Begin VB.Menu nabSep0 
         Caption         =   "-"
      End
      Begin VB.Menu nabOtev��tStarou 
         Caption         =   "Konvertovat starou dr�hu"
         Visible         =   0   'False
      End
      Begin VB.Menu nabOtev��t 
         Caption         =   "&Otev��t dr�hu"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabUlo�it 
         Caption         =   "&Ulo�it jako dr�hu"
         Shortcut        =   ^U
      End
      Begin VB.Menu nabUlo�itJenGrafiku 
         Caption         =   "Ulo�it jen &grafiku"
         Enabled         =   0   'False
         Shortcut        =   ^G
      End
      Begin VB.Menu nabUlo�itJenStart 
         Caption         =   "Ulo�it jen &start a m��ku"
         Enabled         =   0   'False
      End
      Begin VB.Menu nabTest 
         Caption         =   "Ulo�it a o&testovat"
      End
      Begin VB.Menu nabSep12 
         Caption         =   "-"
      End
      Begin VB.Menu nabOtev��tSb�rku 
         Caption         =   "Otev��t s&b�rku prvk�"
         Shortcut        =   ^B
      End
      Begin VB.Menu nabUlo�itJakoSb�rku 
         Caption         =   "Ulo�it jako sb�rku &prvk�"
         Shortcut        =   ^P
      End
      Begin VB.Menu nabSep11 
         Caption         =   "-"
      End
      Begin VB.Menu nabMinigolf��ek 
         Caption         =   "=> &Minigolf��ek"
         Shortcut        =   {F2}
      End
      Begin VB.Menu nabSb�rkaNabSoub 
         Caption         =   "=> Sb�&rka ter�nn�ch prvk�"
         Shortcut        =   +{F3}
      End
      Begin VB.Menu nabEditorPrvk� 
         Caption         =   "=> Editor ter�nn�ch &prvk�"
         Shortcut        =   {F4}
      End
      Begin VB.Menu nabSep9 
         Caption         =   "-"
      End
      Begin VB.Menu nabZav��t 
         Caption         =   "&Zav��t"
      End
      Begin VB.Menu nabKonec 
         Caption         =   "&Konec"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu nab�pravy 
      Caption         =   "�&pravy"
      Begin VB.Menu nabSb�rka 
         Caption         =   "Vybrat &prvek ze sb�rky�"
      End
      Begin VB.Menu nabVybratPrvek 
         Caption         =   "Vybrat prvek ze &souboru�"
         Shortcut        =   {F6}
      End
      Begin VB.Menu nabNas�tPrvek 
         Caption         =   "&Nas�t prvek"
         Shortcut        =   ^{INSERT}
      End
      Begin VB.Menu nabVtisknoutPrvek 
         Caption         =   "&Vtisknout prvek"
         Shortcut        =   +{INSERT}
      End
      Begin VB.Menu nabUpravit 
         Caption         =   "&Upravit obsah z�sobn�ku"
         Shortcut        =   ^{F4}
      End
      Begin VB.Menu nabSep8 
         Caption         =   "-"
      End
      Begin VB.Menu nabObr�zek 
         Caption         =   "P�ikreslit &obr�zek�"
         Shortcut        =   {F7}
      End
      Begin VB.Menu nabBo�it 
         Caption         =   "&Bo�it"
         Shortcut        =   {F8}
      End
      Begin VB.Menu nabSep6 
         Caption         =   "-"
      End
      Begin VB.Menu nabStart 
         Caption         =   "&Start �"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu nabZobrazit 
      Caption         =   "&Zobrazit"
      Begin VB.Menu nabTextura 
         Caption         =   "- &texuru"
         Checked         =   -1  'True
         Shortcut        =   ^X
      End
      Begin VB.Menu nabReli�f 
         Caption         =   "- &reli�f"
         Shortcut        =   ^R
      End
      Begin VB.Menu nabSt�ny 
         Caption         =   "- texturu se &st�ny"
         Shortcut        =   ^S
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
      End
      Begin VB.Menu nabVykreslitSt�ny 
         Caption         =   "&Vykreslit st�ny"
         Shortcut        =   {F9}
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabSouhNast 
         Caption         =   "Souhrn &nastaven�"
         Shortcut        =   ^N
      End
      Begin VB.Menu nabSep10 
         Caption         =   "-"
      End
      Begin VB.Menu nabM��ka 
         Caption         =   "Zarovn�vat ke &m��ce"
         Checked         =   -1  'True
         Shortcut        =   ^Z
      End
      Begin VB.Menu nabSt�edit 
         Caption         =   "St�&edit do m��ky"
         Shortcut        =   ^E
      End
      Begin VB.Menu nabRozte� 
         Caption         =   "&Rozte� &m��ky�"
         Shortcut        =   ^M
         Visible         =   0   'False
      End
      Begin VB.Menu nabSep5 
         Caption         =   "-"
      End
      Begin VB.Menu nabVyrovn�vat 
         Caption         =   "P�i &bo�en� vyrovn�vat se z�kladem"
         Shortcut        =   +{F8}
      End
      Begin VB.Menu nabSep4 
         Caption         =   "-"
      End
      Begin VB.Menu nabStavPovolen� 
         Caption         =   "Sta&vebn� povolen�"
         Begin VB.Menu nabStPGra 
            Caption         =   "na &grafiku"
            Checked         =   -1  'True
            Shortcut        =   {F11}
         End
         Begin VB.Menu nabStPRel 
            Caption         =   "na &reli�f"
            Checked         =   -1  'True
            Shortcut        =   {F12}
         End
      End
      Begin VB.Menu nabSep7 
         Caption         =   "-"
      End
      Begin VB.Menu nabTransparent 
         Caption         =   "&Transparentn� barva �"
         Shortcut        =   ^T
      End
      Begin VB.Menu nabPou��vatTrB 
         Caption         =   "&Pou��vat transparentn� barvu"
         Begin VB.Menu nabTrBTex 
            Caption         =   "pro &grafiku"
            Shortcut        =   +{F11}
         End
         Begin VB.Menu nabTrBRel 
            Caption         =   "pro &reli�f"
            Shortcut        =   +{F12}
         End
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
      End
      Begin VB.Menu nabSyt�St�ny 
         Caption         =   "&Syt� st�ny na �ikm�ch ploch�ch"
      End
      Begin VB.Menu nabZv�raz�ovat 
         Caption         =   "&Zv�raz�ovat �ikm� plochy a hrany"
         Checked         =   -1  'True
      End
      Begin VB.Menu nabZvPodSt�ny 
         Caption         =   "Zv�raz�ovat &pod syt�mi st�ny"
      End
      Begin VB.Menu nabObtahovatHrany 
         Caption         =   "&Obtahovat osv�tlen� hrany"
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmEditorDrah"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim Mapa() As Single
Dim MapaPrvku() As Single
Dim MapaSty�n�chBod�() As XYByte
Dim P�eru�itProces As Boolean

Dim P�vAkce As TypAkce

Const �ed� = 8421504 'RGB(128, 128, 128)
Const P�ipraven = "P�ipraven"

Public Enum TypAkce
    aBo�itRychle = -2
    aBo�it = -1
    aNic = 0
    aPrvek = 1
    aObr�zek = 2
    aStart = 3
    aNas�t = 4
End Enum

Dim pamAkce As Integer
Dim PoslSoub$


Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        KeyAscii = 0
        If MsgBox("P�eru�it prob�haj�c� proces (pokud n�jak� prob�h�)?", vbQuestion Or vbYesNo) = vbYes Then
            P�eru�itProces = True
        End If
    End If
End Sub

Private Sub Form_Load()
    DefTPP
    Set frmSb�rka.opStupn� = opStupn�
    ObnovHodZRegistru
    Z�sobn�k.Rozm�� opRozm�ry.M��kaX, opRozm�ry.M��kaY
    opRozm�ry_Zm�naM��ky
    obrDr�ha(0).Visible = False
    obrDr�ha(0).BackColor = opStupn�.BarvaPodleV��ky(0)
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Width = opRozm�ry.M��kaX
        Obd.Height = opRozm�ry.M��kaY
    Next Obd
    Dim Img As Control
    For Each Img In imgStart
        Img.Move 9, 9
    Next
    For Each Img In imgM��ek
        Srovnej Img, imgStart(Img.Index)
    Next
    Nam��
    ReDim Mapa(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1)
    ReDim MapaPrvku(31, 31)
    Me.KeyPreview = True
    NamapujM��ek
    Load frmEditorPrvk�
    Set frmV�b�r.opStupn� = frmEditorPrvk�.opStupn�
    If InStr(Command, "/EP") > 0 Then
        frmEditorPrvk�.Show
        Me.Show
        frmEditorPrvk�.SetFocus
    End If
    sbrStav.Panels(2).ToolTipText = "stavebn� povolen� na grafiku"
    sbrStav.Panels(3).ToolTipText = "stavebn� povolen� na reli�f"
    sbrStav.Panels(4).ToolTipText = "pou��t transparentn� barvu na grafiku"
    sbrStav.Panels(5).ToolTipText = "pou��t transparentn� barvu na reli�f"
'    frmSb�rka.Show
'    frmEditorPrvk�.Show
End Sub

Private Sub Form_Resize()
    If WindowState <> vbNormal Then Exit Sub
    opStupn�.Left = 0
    opStupn�.Height = ScaleHeight _
    - opRozm�ry.Height - opRozmV�b.Height - UXYZ.Height - sbrStav.Height
    opStupn�.Top = ScaleHeight - opStupn�.Height - sbrStav.Height
    opRozm�ry.Move 0, 0
    opRozmV�b.Move 0, Spodek(opRozm�ry)
    UXYZ.Move 0, Spodek(opRozmV�b)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not frmMinigolf��ek.Visible And Not (frmEditorPrvk�.Visible Or frmSb�rka.Visible) Then
        Zav�i PoslSoub
        End
    Else
        Cancel = True
        Me.Hide
    End If
'    Unload frmSb�rka
'    Unload frmEditorPrvk�
'    Unload frmP�edvolby
'    Unload frmV�b�r
'    End
End Sub

Private Sub nabBo�it_Click()
    Akce = aBo�it
End Sub

Private Sub nabKonec_Click()
    End
End Sub

Private Sub nabMinigolf��ek_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Minigolf��ek"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\Minigolf��ek", vbNormalFocus
    frmMinigolf��ek.Show
End Sub

Private Sub nabEditorPrvk�_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Editor ter�nn�ch prvk�"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\EditorPrvk�", vbNormalFocus
    frmEditorPrvk�.Show
End Sub

Private Sub nabM��ka_Click()
    Zarovn�vat = Not Zarovn�vat
End Sub

Public Property Let Zarovn�vat(Nosi� As Boolean)
    nabM��ka.Checked = Nosi�
    Ulo�Nastaven� "Nastaven�", "Zarovn�vat", Nosi�
End Property

Public Property Get Zarovn�vat() As Boolean
    Zarovn�vat = nabM��ka.Checked
End Property

Private Sub nabN�pov�da_Click()
'    frmN�pov�da.Show
    On Error GoTo Neni_spu�t�n�_n�pov�da
    AppActivate "N�pov�da�k�Minigolf��ku"
    Exit Sub
Neni_spu�t�n�_n�pov�da:
    Shell "winhelp " & App.Path & "\Minigolficek.hlp"
End Sub

Private Sub nabNas�tPrvek_Click()
    Akce = aNas�t
End Sub

Private Sub nabNov�_Click()
    obrDr�ha(0).Picture = LoadPicture()
    obrDr�ha(0).BackColor = opStupn�.BarvaPodleV��ky(0)
    obrDr�ha(1).Picture = LoadPicture()
    obrDr�ha(dzStTex).Picture = LoadPicture()
    obrSt�ny.Picture = LoadPicture()
    ReDim Mapa(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1)
    Dim Img As Control
    For Each Img In imgStart
        Img.Move 9, 9
    Next
    For Each Img In imgM��ek
        Srovnej Img, imgStart(Img.Index)
    Next
    nabUlo�itJenGrafiku.Enabled = False
    nabUlo�itJenStart.Enabled = False
    Caption = "Editor drah - [nov� dr�ha]"
    CD.filename = ""
End Sub

Private Sub nabObr�zek_Click()
    On Error GoTo vypadni
    CDObr.InitDir = App.Path
    CDObr.DialogTitle = "P�ikreslit obr�zek"
    CDObr.Flags = cdlOFNHideReadOnly Or cdlOFNFileMustExist
    CDObr.Filter = "Obr�zky (*.bmp;*.jpg;*.jpeg;*.gif)|*.bmp;*.jpg;*.jpeg;*.gif"
    Enabled = False
    CDObr.ShowOpen
    DoEvents
    Akce = aObr�zek
    obrPamObr.Picture = LoadPicture(CDObr.filename)
    Dim I As Shape
    For Each I In obdPoloha
        I.Width = obrPamObr.ScaleWidth
        I.Height = obrPamObr.ScaleHeight
    Next I
    On Error GoTo 0
vypadni:
    On Error GoTo 0
    Enabled = True
End Sub

Private Sub nabObtahovatHrany_Click()
    ObtHrany = Not ObtHrany
End Sub

Private Sub nabOtev��tSb�rku_Click()
    DoEvents
    Umo�n�no = False
    CDSb.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CDSb.DialogTitle = "Otev��t sb�rku prvk�"
    CDSb.InitDir = App.Path & "\Prvky\" & Hol�Jm�no(CDSb.FileTitle)
    CDSb.Filter = "Sb�rky ter�nn�ch prvk� (*.stp)|*.stp"
    On Error GoTo vypadni
    CDSb.ShowOpen
    DoEvents
    Otev�iSb�rku CDSb.filename
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    Exit Sub
chyba:
    MsgBox "Chyba p�i otev�r�n�!", vbCritical, "Chyba"
    GoTo vypadni
End Sub

Public Sub Otev�iSb�rku(Soubor As String)
    Umo�n�no = False
    DoEvents
    With CDObr
    .Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    .DialogTitle = "Otev��t texturu sb�rky prvk� " & Hol�Jm�no(CDSb.FileTitle)
    .InitDir = App.Path & "\Prvky\" & Hol�Jm�no(Dir(Soubor))
    .Filter = "Textury sb�rek ter�nn�ch prvk� (*.bmp)|*.bmp"
    .filename = Hol�Jm�no(CDSb.FileTitle) & ".bmp"
    End With
    On Error GoTo vypadni
    CDObr.ShowOpen
    DoEvents
    On Error GoTo chyba
    Stav = "Otev�r�m sb�rku �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = CDObr.filename
    Dim ���ka As Integer, V��ka As Integer
    Dim MaxBod� As Integer, Rozte�X As Byte, Rozte�Y As Byte
    Dim Start As XY
    Close
    Open Soubor For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka > 5000 Or V��ka > 5000 _
    Or ���ka < 0 Or V��ka < 0 Then
        Close
        MsgBox "�patn� form�t souboru �i jin� chyba p�i otev�r�n�", vbCritical, "Chyba"
        GoTo vypadni
    End If
    Get #1, , Rozte�X
    Get #1, , Rozte�Y
    If Rozte�Y = 0 Then Rozte�Y = Rozte�X
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    Close
    opRozm�ry.Nastav ���ka, V��ka
    opRozm�ry.NastavM��ku Rozte�X, Rozte�Y
    Dim Img As Control
    For Each Img In imgStart
        Img.Move Start.X, Start.Y
        Srovnej imgM��ek(Img.Index), Img
    Next
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    obrDr�ha(dzStTex).Picture = LoadPicture()
    Caption = "Editor drah - sb�rka prvk� " & Hol�Jm�no(Dir(Soubor))
'    UplatniMapu
    nabUlo�itJenGrafiku.Enabled = False
    nabUlo�itJenStart.Enabled = False
    CDSb.filename = Soubor
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    MousePointer = vbDefault
    If Not Me.Visible Then Me.Show
    Me.SetFocus
    Beep
    Exit Sub
chyba:
    MsgBox "Chyba p�i otev�r�n�!", vbCritical, "Chyba"
    GoTo vypadni
End Sub

Private Sub nabOtev��tStarou_Click()
    DoEvents
    Umo�n�no = False
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Otev��t dr�hu"
    CD.InitDir = App.Path & "\Dr�hy"
    CD.Filter = "Dr�hy minigolfu (*.mig)|*.mig"
    On Error GoTo vypadni
    CD.ShowOpen
    DoEvents
    On Error GoTo chyba
    Caption = "Editor drah - star� dr�ha " & Hol�Jm�no(CD.FileTitle)
    Stav = "Otev�r�m starou dr�hu �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = Hol�Jm�no(CD.filename) & " - z�klad.bmp"
    Jm�noSt�n� = Hol�Jm�no(CD.filename) & ".bmp"
    Dim ���ka As Integer, V��ka As Integer
    Dim MaxBod� As Integer
    Dim Start As XY
    Close
    Open CD.filename For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka > 5000 Or V��ka > 5000 _
    Or ���ka < 0 Or V��ka < 0 Then
        Close
        MsgBox "�patn� form�t souboru �i jin� chyba p�i otev�r�n�!", vbCritical, "Chyba"
        GoTo vypadni
    End If
    Get #1, , MaxBod�
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    ReDim MapaSty�n�chBod�(���ka + 2 - 15, V��ka + 2 - 15, MaxBod�)
    Get #1, , MapaSty�n�chBod�
    If Not Loc(1) >= LOF(1) Then
        Get #1, , Start
    Else
        Start = CXY(8, 8)
    End If
    Close
    Open CD.filename For Output As #1
    Close
    Open CD.filename For Binary As #1
    Put #1, , ���ka
    Put #1, , V��ka
    Put #1, , CInt(32)
    Put #1, , MaxBod�
    Put #1, , Start
    Put #1, , Mapa()
    Put #1, , MapaSty�n�chBod�()
    Close
    opRozm�ry.Nastav ���ka, V��ka
    Dim Img As Control
    For Each Img In imgStart
        Img.Move Start.X, Start.Y
        Srovnej imgM��ek(Img.Index), Img
    Next
    UplatniMapu
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    obrDr�ha(dzStTex).Picture = LoadPicture(Jm�noSt�n�)
    nabUlo�itJenGrafiku.Enabled = True
    nabUlo�itJenStart.Enabled = True
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    Exit Sub
chyba:
    MsgBox "�patn� form�t souboru!", vbCritical, "Chyba"
    GoTo vypadni
End Sub

Private Sub nabOtev��t_Click()
    DoEvents
    Umo�n�no = False
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Otev��t dr�hu"
    CD.InitDir = App.Path & "\Dr�hy"
    CD.Filter = "Stla�en� dr�hy minigolfu (*.dr�ha)|*.dr�ha|Dr�hy minigolfu (*.mig)|*.mig"
    On Error GoTo vypadni
    CD.ShowOpen
    DoEvents
    Otev�i CD.filename
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    Exit Sub
End Sub


Public Sub Otev�i(Soubor As String)
    Stav = "Otev�r�m dr�hu�"
    If Not Hol�Jm�no(PoslSoub) = Hol�Jm�no(Soubor) Then Zav�i PoslSoub
    Set �ekac�Form = Me
    If Right(Soubor, 4) = ".mig" Then
        Compressor Soubor
        Soubor = Hol�Jm�no(Soubor) & ".dr�ha"
    Else
        Decompressor Soubor
    End If
    PoslSoub = Soubor
    Otev�iMIG Hol�Jm�no(Soubor) & ".mig"
    Stav = ""
End Sub


Public Sub Otev�iMIG(Soubor As String)
    If Soubor = "" Then
        Me.Show
        GoTo vypadni
    End If
    If Right(Soubor, 3) = "stp" Then
        Me.Show
        Otev�iSb�rku Soubor
        Exit Sub
    End If
    
    DoEvents
    Umo�n�no = False
    On Error GoTo chyba
    Stav = "Otev�r�m dr�hu �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = Hol�Jm�no(Soubor) & " - z�klad.bmp"
    Jm�noSt�n� = Hol�Jm�no(Soubor) & ".bmp"
    Dim ���ka As Integer, V��ka As Integer
    Dim MaxBod� As Integer, Rozte�X As Byte, Rozte�Y As Byte
    Dim Start As XY
    Close
    Open Soubor For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka > 5000 Or V��ka > 5000 _
    Or ���ka < 0 Or V��ka < 0 Then
        Close
        MsgBox "�patn� form�t souboru!", vbCritical, "Chyba"
        GoTo vypadni
    End If
    Get #1, , Rozte�X
    Get #1, , Rozte�Y
    If Rozte�Y = 0 Then Rozte�Y = Rozte�X
    'If Rozte�X = 0 Then Rozte�X = Rozte�Y
    Get #1, , MaxBod�
    Get #1, , Start
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    ReDim MapaSty�n�chBod�(���ka + 2 - 15, V��ka + 2 - 15, MaxBod�)
    Get #1, , MapaSty�n�chBod�
    Close
    opRozm�ry.Nastav ���ka, V��ka
    opRozm�ry.NastavM��ku Rozte�X, Rozte�Y
    Dim Img As Control
    For Each Img In imgStart
        Img.Move Start.X, Start.Y
        Srovnej imgM��ek(Img.Index), Img
    Next
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    On Error GoTo chyb�_obr�zek
    obrDr�ha(dzStTex).Picture = LoadPicture(Jm�noSt�n�)
    On Error GoTo chyba
    Caption = "Editor drah - " & Hol�Jm�no(Dir(Soubor))
'    UplatniMapu
    nabUlo�itJenGrafiku.Enabled = True
    nabUlo�itJenStart.Enabled = True
    If Existuje(Hol�Jm�no(Soubor) & ".dr�ha") Then
        CD.filename = Hol�Jm�no(Soubor) & ".dr�ha"
    Else
        CD.filename = Soubor
    End If
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    Me.MousePointer = vbDefault
    Uka�Obr�zekPodleNab�dky
    If Not Me.Visible Then Me.Show
    Me.SetFocus
    Beep
    Exit Sub
chyba:
    MsgBox "�patn� form�t souboru �i jin� chyba p�i otev�r�n�! " & Err.Description & ".", vbCritical, "Chyba"
    GoTo vypadni
chyb�_obr�zek:
    MsgBox "Nenalezl jsem v�sledn� st�novan� obr�zek. Dr�ha by se nedala hr�t. Ulo�en�m grafiky jej obnov�te.", vbExclamation, "Chyb� obr�zek"
    Resume Next
End Sub

'Private Sub nabRozte�_Click()
'    Dim Rozte�%, �etRozte�$
'    On Error Resume Next
'    �etRozte� = InputBox("Zadejte rozte� m��ky v bodech:", "Rozte� m��ky", opRozm�ry.M��kaX)
'    If �etRozte� = "" Then Exit Sub
'    Rozte� = Val(�etRozte�)
'    If Rozte� < 1 Then Rozte� = 1:   MsgBox "Rozte� m��ky nem��e b�t men�� ne� 1 bod - zv�t�uji na 1.", vbInformation
'    If Rozte� > 64 Then Rozte� = 64: MsgBox "Rozte� m��ky nem��e b�t v�t�� ne� 64 bod� - zmen�uji na 64.", vbInformation
'    opRozm�ry.NastavM��ku Rozte�, Rozte�
'End Sub

Private Sub nabSb�rka_Click()
    frmSb�rka.Show
    'Akce = aPrvek
End Sub

Private Sub nabSb�rkaNabSoub_Click()
    frmSb�rka.Show
End Sub

Private Sub nabSouhNast_Click()
    frmNast.Karta = 1
    frmNast.Show 0, Me
End Sub

Private Sub nabStart_Click()
    On Error GoTo vypadni
    Akce = aStart
    Dim I As Shape
    For Each I In obdPoloha
        I.Width = imgM��ek(I.Index).Width
        I.Height = imgM��ek(I.Index).Height
    Next
vypadni:
    Enabled = True
End Sub

Private Sub nabStPGra_Click()
    Stavebn�Povolen�(dzTextura) = Not nabStPGra.Checked
End Sub

Private Sub nabStPRel_Click()
    Stavebn�Povolen�(dzReli�f) = Not nabStPRel.Checked
End Sub

Private Sub nabSt�edit_Click()
    St�edit = Not St�edit
End Sub

Private Sub nabSyt�St�ny_Click()
    Syt�St�ny = Not Syt�St�ny
End Sub

Private Sub nabTest_Click()
    nabUlo�it_Click
    If P�eru�itProces Then
        P�eru�itProces = False
        Exit Sub
    End If
    frmMinigolf��ek.Otev�i CD.filename
    frmMinigolf��ek.Show
End Sub

Private Sub nabTransparent_Click()
    frmTransparent.Show 1, Me
End Sub

Private Sub nabTrBRel_Click()
    nabTrBRel.Checked = Not nabTrBRel.Checked
    frmSb�rka.nabTrBRel.Checked = nabTrBRel.Checked
    Ulo�Nastaven� "Nastaven�", "Transparentn� reli�f", nabTrBRel.Checked
End Sub

Private Sub nabTrBTex_Click()
    nabTrBTex.Checked = Not nabTrBTex.Checked
    frmSb�rka.nabTrBTex.Checked = nabTrBTex.Checked
    Ulo�Nastaven� "Nastaven�", "Transparentn� textura", nabTrBTex.Checked
End Sub

Private Sub nabUlo�it_Click()
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Ulo�it dr�hu"
    CD.InitDir = App.Path & "\Dr�hy"
    CD.Filter = "Dr�hy minigolfu (*.mig)|*.mig"
    On Error GoTo vypadni
    CD.ShowSave
    On Error GoTo 0
    P�eru�itProces = False
    Caption = "Editor drah - " & Hol�Jm�no(CD.FileTitle)
    P�idejR�me�ek
    P�eru�itProces = False
    NamapujSty�n�Body
    If P�eru�itProces Then
        GoTo p�eru�eno
    End If
    Open CD.filename For Output As #1
    Close
    Open CD.filename For Binary As #1
    Put #1, , CInt(opRozm�ry.���ka)
    Put #1, , CInt(opRozm�ry.V��ka)
    Put #1, , CByte(opRozm�ry.M��kaX)
    Put #1, , CByte(opRozm�ry.M��kaY)
    Put #1, , CInt(UBound(MapaSty�n�chBod�(), 3))
    Put #1, , CXY(imgStart(dzTextura).Left, imgStart(dzTextura).Top)
    Put #1, , Mapa()
    Put #1, , MapaSty�n�chBod�()
    Close
    Ulo�Obr�zek obrSt�ny.Image, App.Path & "\Sty�n� body.bmp"
    Vyst�nuj
    Osv�tli
    Erase MapaSty�n�chBod�()
    Ulo�Obr�zek obrDr�ha(1).Image, Hol�Jm�no(CD.filename) + " - z�klad.bmp"
    Ulo�Obr�zek obrDr�ha(dzStTex).Image, Hol�Jm�no(CD.filename) + ".bmp"
    Uka�Obr�zekPodleNab�dky
    Beep
    nabUlo�itJenGrafiku.Enabled = True
    nabUlo�itJenStart.Enabled = True
    DoEvents
    Exit Sub
p�eru�eno:
    Uka�Obr�zekPodleNab�dky
    obrDr�ha(dzStTex).MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = P�ipraven
    Umo�n�no = True
    Close
vypadni:
    'On Error GoTo 0
End Sub

Sub Ulo�Obr�zek(Obr�zek As IPictureDisp, Cesta As String)
    obrPamUkl.Picture = LoadPicture()
    obrPamUkl.Move 0, 0, opRozm�ry.���ka + 2, opRozm�ry.V��ka + 2
    obrPamUkl.PaintPicture Obr�zek, 0, 0 ', opRozm�ry.���ka, opRozm�ry.V��ka
    SavePicture obrPamUkl.Image, Cesta
End Sub

Private Sub nabUlo�itJakoSb�rku_Click()
    CDSb.Flags = cdlOFNHideReadOnly
    CDSb.DialogTitle = "Ulo�it sb�rku prvk� (zadejte n�zev slo�ky = n�z. sb�rky)"
    CDSb.InitDir = App.Path & "\Prvky"
    CDSb.Filter = "Sb�rky ter�nn�ch prvk� (*.stp)|*.stp"
    CDSb.DefaultExt = ".stp"
    If CDSb.filename <> "" Then CDSb.filename = Hol�Jm�no(CDSb.FileTitle) ' & CDSb.DefaultExt
    On Error GoTo vypadni
    CDSb.ShowSave
    On Error GoTo 0
    Dim Adres��Sb�rky As String, CestaKeSb�rce As String
    Adres��Sb�rky = App.Path & "\Prvky\" & Hol�Jm�no(CDSb.FileTitle)
    CestaKeSb�rce = Adres��Sb�rky & "\" & CDSb.FileTitle
    On Error Resume Next
    MkDir Adres��Sb�rky
    On Error GoTo 0
    Caption = "Editor drah - sb�rka prvk� " & Hol�Jm�no(CDSb.FileTitle)
    P�idejR�me�ek
    P�eru�itProces = False
    'NamapujSty�n�Body
    If P�eru�itProces Then
        P�eru�itProces = False
        GoTo p�eru�eno
    End If
    Open CestaKeSb�rce For Output As #1
    Close
    Open CestaKeSb�rce For Binary As #1
    Put #1, , CInt(opRozm�ry.���ka)
    Put #1, , CInt(opRozm�ry.V��ka)
    Put #1, , CByte(opRozm�ry.M��kaX)
    Put #1, , CByte(opRozm�ry.M��kaY)
    Put #1, , Mapa()
    Close
    'Vyst�nuj
    'Osv�tli
    Erase MapaSty�n�chBod�()
    CDObr.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CDObr.DialogTitle = "Ulo�it texturu sb�rky prvk� " & Hol�Jm�no(CDSb.FileTitle)
    CDObr.InitDir = Adres��Sb�rky
    CDObr.Filter = "Textury sb�rek ter�nn�ch prvk� (*.bmp)|*.bmp"
    CDObr.DefaultExt = ".bmp"
    On Error GoTo vypadni
    CDObr.ShowSave
    On Error GoTo 0
    
    Ulo�Obr�zek obrDr�ha(1).Image, Hol�Jm�no(CDObr.filename) + ".bmp"
    'Ulo�Obr�zek obrDr�ha(dzStTex).Image, Hol�Jm�no(CDSb.FileName) + ".bmp"
    Uka�Obr�zekPodleNab�dky
    Beep
    nabUlo�itJenGrafiku.Enabled = False
    nabUlo�itJenStart.Enabled = False
    DoEvents
    Exit Sub
p�eru�eno:
    Uka�Obr�zekPodleNab�dky
    obrDr�ha(dzStTex).MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = P�ipraven
    Umo�n�no = True
vypadni:
    'On Error GoTo 0
End Sub

Private Sub nabUlo�itJenStart_Click()
    Open CD.filename For Binary As #1
    Put #1, 5, CByte(opRozm�ry.M��kaX)
    Put #1, 6, CByte(opRozm�ry.M��kaY)
    Put #1, 9, CXY(imgStart(dzTextura).Left, imgStart(dzTextura).Top)
    Close
End Sub

Private Sub nabUpravit_Click()
    frmEditorPrvk�.Otev�iZeZ�sobn�ku
    frmEditorPrvk�.Show
End Sub

Private Sub nabVtisknoutPrvek_Click()
    Akce = aPrvek
End Sub

Private Sub nabVybratPrvek_Click()
    frmV�b�r.Druh = dvPrvek
    frmV�b�r.Caption = "P�idat ter�nn� prvek"
    Enabled = False
    DoEvents
    frmV�b�r.Show 1, Me
    DoEvents
    Enabled = True
    Dim Nic As Boolean
    Nic = frmV�b�r.Nic
    If Nic Then Exit Sub
    Akce = aPrvek
    Open App.Path & "\Prvky\" & frmV�b�r.Reli�f For Binary As #1
    Dim ���ka  As Integer, V��ka As Integer
    Get #1, , ���ka
    Get #1, , V��ka
    ReDim MapaPrvku(���ka - 1, V��ka - 1)
    Get #1, , MapaPrvku
    Close
    Z�sobn�k.Reli�f = MapaPrvku
    Z�sobn�k.Textura = LoadPicture(App.Path & "\Prvky\" & frmV�b�r.Textura)
    Dim I As Shape
    For Each I In obdPoloha
        I.Width = ���ka
        I.Height = V��ka
    Next I
End Sub

Private Sub nabReli�f_Click()
    nabTextura.Checked = False
    nabSt�ny.Checked = False
    nabReli�f.Checked = True
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = True
    obrDr�ha(dzStTex).Visible = False
End Sub

Private Sub nabTextura_Click()
    nabTextura.Checked = True
    nabReli�f.Checked = False
    nabSt�ny.Checked = False
    obrDr�ha(1).Visible = True
    obrDr�ha(0).Visible = False
    obrDr�ha(dzStTex).Visible = False
End Sub

Private Sub nabSt�ny_Click()
    nabSt�ny.Checked = True
    nabTextura.Checked = False
    nabReli�f.Checked = False
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = False
    obrDr�ha(dzStTex).Visible = True
End Sub

Private Sub nabUlo�itJenGrafiku_Click()
    nabVykreslitSt�ny_Click
    If P�eru�itProces Then Exit Sub
    Ulo�Obr�zek obrDr�ha(1).Image, Hol�Jm�no(CD.filename) + " - z�klad.bmp"
    Ulo�Obr�zek obrDr�ha(dzStTex).Image, Hol�Jm�no(CD.filename) + ".bmp"
    Uka�Obr�zekPodleNab�dky
End Sub

Private Sub nabVykreslitSt�ny_Click()
    Umo�n�no = False
    MousePointer = vbHourglass
    P�eru�itProces = False
    DoEvents
    nabTextura.Checked = False
    nabReli�f.Checked = False
    nabSt�ny.Checked = True
    P�idejR�me�ek
    Vyst�nuj
    Osv�tli
    P�idejR�me�ek
    Uka�Obr�zekPodleNab�dky
    Umo�n�no = True
    MousePointer = vbDefault
End Sub

Private Sub nabVyrovn�vat_Click()
    Vyrovn�vat = Not Vyrovn�vat
End Sub

Private Sub nabZav��t_Click()
    Unload Me
End Sub

Private Sub nabZvPodSt�ny_Click()
    ZvPodSSt = Not ZvPodSSt
End Sub

Private Sub nabZv�raz�ovat_Click()
    Zv�raz�ovat = Not Zv�raz�ovat
End Sub

Private Sub obrDr�ha_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    If Akce <> aBo�itRychle Then P�vAkce = Akce
    If Button = vbRightButton Then Akce = aBo�itRychle
    Hejbni X, Y
End Sub

Private Sub obrDr�ha_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    Hejbni X, Y
    If Akce = aStart Then
        Srovnej imgM��ek(Index), obdPoloha(Index)
    End If
    On Error Resume Next
    UXYZ.NastavXYZ X, Y, Mapa(X, Y)
End Sub

Private Sub obrDr�ha_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    If Button = vbRightButton Then Akce = aBo�itRychle
    Stavba Index
    If P�vAkce <> aBo�itRychle Then Akce = P�vAkce Else Debug.Assert False: Akce = aBo�itRychle
    DoEvents
    'Hejbni X, Y
End Sub

Private Sub Hejbni(X As Single, Y As Single)
    Dim Obd As Shape
    For Each Obd In obdPoloha
        If St�edit Then
            Obd.Move _
            X + ((-X + 1) Mod opRozm�ry.M��kaX - (Obd.Width - opRozm�ry.M��kaX) / 2) * -nabM��ka.Checked + Obd.Width / 2 * (Not nabM��ka.Checked), _
            Y + ((-Y + 1) Mod opRozm�ry.M��kaY - (Obd.Height - opRozm�ry.M��kaY) / 2) * -nabM��ka.Checked + Obd.Height / 2 * (Not nabM��ka.Checked)
        Else
            Obd.Move _
            X + ((-X + 1) Mod opRozm�ry.M��kaX) * -nabM��ka.Checked, _
            Y + ((-Y + 1) Mod opRozm�ry.M��kaY) * -nabM��ka.Checked
        End If
        If Akce = aStart Then
            If Obd.Left < 1 Then Obd.Left = 1
            If Obd.Top < 1 Then Obd.Top = 1
            If Pravobok(Obd) > opRozm�ry.���ka + 1 Then Obd.Left = opRozm�ry.���ka - Obd.Width + 1
            If Spodek(Obd) > opRozm�ry.V��ka + 1 Then Obd.Top = opRozm�ry.V��ka - Obd.Height + 1
        End If
    Next
End Sub

Private Property Let Stav(Nosi� As String)
    sbrStav.Panels(1).Text = Nosi�
End Property

Private Property Get Stav() As String
    Stav = sbrStav.Panels(1).Text
End Property

Private Sub Nam��()
    With obrDr�ha(0)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrDr�ha(1)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrSt�ny
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrDr�ha(dzStTex)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    Width = Width - ScaleWidth * TPPX + (obrDr�ha(0).Width + opStupn�.Width) * TPPX
    Dim Minim_Y, Je_pod_min_Y
    Minim_Y = obrDr�ha(0).Height - obrDr�ha(0).ScaleHeight + 320 ' jako minim�ln� vej�ka opStupn�
    Je_pod_min_Y = obrDr�ha(0).Height <= Minim_Y
    Height = Height - ScaleHeight * TPPY + (sbrStav.Height + IIf(Je_pod_min_Y, Minim_Y, obrDr�ha(0).Height)) * TPPY
    DoEvents
    Static Pov�ce As Boolean
    If Not Pov�ce Then
        Pov�ce = Not Pov�ce
        Exit Sub
    End If
End Sub

Private Sub O��zniMapu()
    Dim ���ka As Long, V��ka As Long
    Dim P�v� As Long, P�vV As Long
    On Error Resume Next
    P�v� = UBound(Mapa, 1)
    P�vV = UBound(Mapa, 2)
    On Error GoTo 0
    ���ka = opRozm�ry.���ka + 2
    V��ka = opRozm�ry.V��ka + 2
    Sma�R�me�ek
    Screen.MousePointer = vbHourglass
'    MousePointer = vbHourglass
    ReDim Nov�Mapa(���ka - 1, V��ka - 1)
    Dim X As Long, Y As Long
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            On Error Resume Next
            Nov�Mapa(X, Y) = Mapa(X, Y)
        Next Y
    Next X
    'DoEvents
'    MousePointer = vbHourglass
    ReDim Mapa(���ka - 1, V��ka - 1)
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            'On Error Resume Next
            Mapa(X, Y) = Nov�Mapa(X, Y)
        Next Y
    Next X
    P�idejR�me�ek
    UplatniMapu P�v�, P�vV
    Screen.MousePointer = vbDefault
End Sub

Private Sub UplatniMapu(Optional Za�X As Long, Optional Za�Y As Long)
    P�eru�itProces = False
    obrDr�ha(dzReli�f).Enabled = False
    obrDr�ha(dzReli�f).Visible = True
    obrDr�ha(dzStTex).Visible = False
    obrDr�ha(dzTextura).Visible = False
    Umo�n�no = False
    MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, pamV��ka As Long, pam���ka As Long
    pam���ka = UBound(Mapa, 1)
    pamV��ka = UBound(Mapa, 2)
    For Y = Za�X To pamV��ka
        For X = Za�Y To pam���ka
            ' Stupn� u� to kontrolujou
            SetPixel obrDr�ha(dzReli�f).hdc, X, Y, opStupn�.BarvaPodleV��ky(Mapa(X, Y))
            'obrDr�ha(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
        Next
        obrDr�ha(dzReli�f).Refresh
        Stav = "Kresl�m reli�f: ��dek " & Y & " (Stop: Escape)"
        DoEvents
        If P�eru�itProces Then P�eru�itProces = False: Exit For
    Next
    obrDr�ha(dzReli�f).Enabled = True
    Uka�Obr�zekPodleNab�dky
    Umo�n�no = True
    MousePointer = vbDefault
End Sub

Private Sub Stavba(Index As Integer)
    Dim a As TypAkce
    a = Akce
    If a = aNic Then Exit Sub
    If a <> aStart Then MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, Levobok As Long, Vr�ek As Long
    Dim V��ka As Long, ���ka As Long
    With obdPoloha(Index)
        Levobok = .Left
        Vr�ek = .Top
        ���ka = .Width
        V��ka = .Height
    End With
    Select Case a
    Case TypAkce.aBo�it, TypAkce.aBo�itRychle
        On Error Resume Next
        If Stavebn�Povolen�(dzReli�f) Then
            For Y = Vr�ek To Vr�ek + V��ka - 1
                For X = Levobok To Levobok + ���ka - 1
                    Mapa(X, Y) = 0 + opStupn�.V��ka * -nabVyrovn�vat.Checked
                    ' Stupn� u� to kontrolujou
                    SetPixel obrDr�ha(dzReli�f).hdc, X, Y, opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                    'obrDr�ha(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                Next X
            Next Y
        End If
        If Stavebn�Povolen�(dzTextura) Then
            With obdPoloha(Index)
                obrDr�ha(dzTextura).Line (.Left, .Top)-(.Left + .Width - 1, .Top + .Height - 1), vbWhite, BF
            End With
        End If
    Case TypAkce.aPrvek
        If (Not TrBGra) And Stavebn�Povolen�(dzTextura) Then
            obrDr�ha(dzTextura).PaintPicture obrPamObr.Image, obdPoloha(Index).Left, obdPoloha(Index).Top, obdPoloha(Index).Width, obdPoloha(Index).Height, 0, 0, obdPoloha(Index).Width, obdPoloha(Index).Height
        End If
        On Error Resume Next
        For Y = Vr�ek To Vr�ek + V��ka - 1
            For X = Levobok To Levobok + ���ka - 1
                If Not (nabTrBRel.Checked _
                And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) _
                And Stavebn�Povolen�(dzReli�f) Then
                        ' Pokud je tam jamka,
                        If Z�sobn�k(X - Levobok, Y - Vr�ek) = V��kaC�le Then
                            ' nech tam tu jamku!
                            Mapa(X, Y) = V��kaC�le
                        ' Pokud tam neni jamka,
                        Else
                            ' p�idej vej�ku k z�kladu.
                            Mapa(X, Y) = Z�sobn�k(X - Levobok, Y - Vr�ek) + opStupn�.V��ka
                        End If
                        SetPixel obrDr�ha(dzReli�f).hdc, X, Y, opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                        'obrDr�ha(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                End If
                If Not (nabTrBTex.Checked And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) _
                And Stavebn�Povolen�(dzTextura) Then
                    SetPixel obrDr�ha(dzTextura).hdc, X, Y, GetPixel(obrPamObr.hdc, X - Levobok, Y - Vr�ek)
                    'obrDr�ha(dzTextura).PSet (X, Y), obrPamObr.Point(X - Levobok, Y - Vr�ek)
                End If
            Next X
        Next Y
        On Error GoTo 0
    Case TypAkce.aObr�zek
        If Not nabTrBTex.Checked Then
            obrDr�ha(dzTextura).PaintPicture obrPamObr.Image, obdPoloha(Index).Left, obdPoloha(Index).Top
        Else
            On Error Resume Next
            For Y = Vr�ek To Vr�ek + V��ka - 1
                For X = Levobok To Levobok + ���ka - 1
                    If Not (nabTrBTex.Checked And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) Then
                        SetPixel obrDr�ha(dzTextura).hdc, X, Y, GetPixel(obrPamObr.hdc, X - Levobok, Y - Vr�ek)
                        'obrDr�ha(dzTextura).PSet (X, Y), obrPamObr.Point(X - Levobok, Y - Vr�ek)
                    End If
                Next X
            Next Y
        End If
        On Error GoTo 0
    Case TypAkce.aStart
        Dim Start�k As Control
        For Each Start�k In imgStart
            Srovnej Start�k, obdPoloha(Index)
            Start�k.Visible = True
        Next
    Case TypAkce.aNas�t
        If Stavebn�Povolen�(dzTextura) Then
            With obdPoloha(Index)
                Z�sobn�k.Rozm�� .Width, .Height
                obrPamObr.Move .Left, .Top, .Width, .Height
                obrPamObr.PaintPicture obrDr�ha(dzTextura).Image, 0, 0, , , .Left, .Top
            End With
            Z�sobn�k.Textura = obrPamObr.Image
        End If
        If Stavebn�Povolen�(dzReli�f) Then
            ReDim MapaPrvku(���ka - 1, V��ka - 1)
            On Error Resume Next
            For Y = Vr�ek To Vr�ek + V��ka - 1
                For X = Levobok To Levobok + ���ka - 1
                    MapaPrvku(X - Levobok, Y - Vr�ek) = Mapa(X, Y)
                Next X
            Next Y
            On Error GoTo 0
            Z�sobn�k.Reli�f = MapaPrvku()
        End If
    Case Else
        'nic
    End Select
    If a <> aStart Then P�idejR�me�ek
    MousePointer = vbDefault
End Sub

Private Sub Vyst�nuj()
    If P�eru�itProces Then
        Exit Sub
    End If
    Dim X, Y, St�nX, St�nY, V��kaSt�nu
    obrSt�ny.Cls
    obrSt�ny.BackColor = vbWhite
    obrDr�ha(dzReli�f).Visible = False
    obrDr�ha(dzTextura).Visible = False
    obrDr�ha(dzStTex).Visible = False
    obrSt�ny.Visible = True
    DoEvents
    obrSt�ny.MousePointer = vbHourglass
    For Y = 0 To obrDr�ha(dzStTex).ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 0 To obrDr�ha(dzStTex).ScaleWidth - 2
            If X = 0 Or Y = 0 Then
                V��kaSt�nu = opRozm�ry.M��kaX
            Else
                V��kaSt�nu = Mapa(X, Y) + (Not nabSyt�St�ny.Checked) * 2
            End If
            If X + 2 < UBound(Mapa, 1) Then
                If Mapa(X + 1, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 2, Y + 1) < Mapa(X, Y) Then
                    St�nX = X + 2
                    St�nY = Y + 1
                Else
                    St�nX = X + 1
                    St�nY = Y + 1
                End If
            Else
                St�nX = X + 1
                St�nY = Y + 1
            End If
            Do Until Mapa(Int(St�nX), Int(St�nY)) >= V��kaSt�nu
                'Stop
'                If obrSt�ny.Point(St�nX, St�nY) _
'                <> �ed� Then
'                    Debug.Assert obrSt�ny.Point(St�nX, St�nY) = &HFFFFFF
'                    'obrSt�ny.PSet (St�nX, St�nY), �ed�
'                End If
                obrSt�ny.Line (St�nX, St�nY)-(St�nX + 1, St�nY + 1.5), �ed�
                'obrSt�ny.PSet (St�nX, St�nY), �ed�
                St�nX = St�nX + 1
                St�nY = St�nY + 1 / 2
                V��kaSt�nu = V��kaSt�nu - 2
                'Debug.Assert Not St�nX = 320
                If St�nX >= obrDr�ha(dzStTex).ScaleWidth - 1 _
                Or St�nY >= obrDr�ha(dzStTex).ScaleHeight - 1 _
                Then Exit Do
            Loop
            'To je pomal�!
            'obrSt�ny.Line (X + 1, Y + 1)-(St�nX, St�nY), �ed�
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Kresl�m st�ny � �. " & Y & " (Stop: Escape)"
    Next Y
    For Y = 1 To obrDr�ha(dzTextura).ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzTextura).ScaleWidth - 2
            ' Retu�e p�esahuj�c�ch st�n�
            If Mapa(X, Y - 1) < Mapa(X, Y) - 1 _
            And obrSt�ny.Point(X, Y) <> vbWhite _
            And (obrSt�ny.Point(X, Y + 1) = vbWhite _
             And obrSt�ny.Point(X + 1, Y + 1) = vbWhite _
             Or (obrSt�ny.Point(X - 1, Y - 1) = vbWhite _
              And Mapa(X, Y) = Mapa(X - 1, Y - 1) _
             And Mapa(X, Y) > Mapa(X + 1, Y + 1))) _
            And Y < UBound(Mapa, 2) Then
                'Debug.Assert False
                SetPixel obrSt�ny.hdc, X, Y, vbWhite
                'obrSt�ny.PSet (X, Y), vbWhite
            End If
            ' Retu�e mezer mezi rohy
            If obrSt�ny.Point(X + 1, Y) <> vbWhite _
            And obrSt�ny.Point(X, Y + 1) <> vbWhite Then
                If Mapa(X, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 1, Y) = Mapa(X, Y) Then
                    SetPixel obrSt�ny.hdc, X, Y, �ed�
                    'obrSt�ny.PSet (X, Y), �ed�
                End If
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Retu�uji st�ny � �. " & Y & " (Stop: Escape)"
    Next Y
    ' Vyb�lit r�me�ek
    obrSt�ny.Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    Refresh
    obrDr�ha(dzStTex).Picture = obrDr�ha(dzTextura).Image
    obrSt�ny.Visible = False
    obrDr�ha(dzStTex).Visible = True
    Me.MousePointer = vbHourglass
    Dim Max&, R&, G&, B&
    Max = �erven�(BarvaSS)
    If Zelen�(BarvaSS) > Max Then Max = Zelen�(BarvaSS)
    If Modr�(BarvaSS) > Max Then Max = Modr�(BarvaSS)
    Max = Max + 64
    For Y = 1 To obrDr�ha(dzStTex).ScaleHeight - 1
'        obrDr�ha(dzStTex).MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzStTex).ScaleWidth - 1
            If obrSt�ny.Point(X, Y) <> vbWhite Then
                'obrDr�ha(dzStTex).PSet (X, Y),
                R = �erven�(obrDr�ha(dzStTex).Point(X, Y)) + �erven�(BarvaSS) - Max
                G = Zelen�(obrDr�ha(dzStTex).Point(X, Y)) + Zelen�(BarvaSS) - Max
                B = Modr�(obrDr�ha(dzStTex).Point(X, Y)) + Modr�(BarvaSS) - Max
                If R < 0 Then R = 0
                If G < 0 Then G = 0
                If B < 0 Then B = 0
                SetPixel obrDr�ha(dzStTex).hdc, X, Y, _
                RGB(R, G, B)
            End If
            If ObtHrany Then
                If X < obrDr�ha(dzStTex).ScaleWidth - 2 And Y < obrDr�ha(dzStTex).ScaleHeight - 2 Then
                    If (Mapa(X + 1, Y) > Mapa(X, Y) + 2 Or Mapa(X, Y + 1) > Mapa(X, Y) + 2) Then
                        SetPixel obrDr�ha(dzStTex).hdc, X, Y, BarvaOs
                    End If
                End If
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Aplikuji st�ny � �." & Y & " (Stop: Escape)"
    Next Y
p�eru�eno:
    Stav = P�ipraven
End Sub

Public Sub Osv�tli()
    If Not Zv�raz�ovat Then GoTo p�eru�eno
    If P�eru�itProces Then
'        P�eru�itProces = False
        GoTo p�eru�eno
    End If
    Dim X&, Y&
    obrDr�ha(dzStTex).Visible = True
    obrSt�ny.Visible = False
    For Y = 1 To obrDr�ha(dzStTex).ScaleHeight - 1
        MousePointer = vbHourglass
        obrDr�ha(dzStTex).MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzStTex).ScaleWidth - 1
            If (obrSt�ny.Point(X, Y) = vbWhite _
            Or nabZvPodSt�ny.Checked) _
            And nabZv�raz�ovat.Checked Then
                Dim R As Long, G As Long, B As Long
                Dim P�id�k As Long, PX, PY
                On Error GoTo 0
                If X = 1 Then
                    PX = Mapa(X, Y)
                Else
                    PX = Mapa(X - 1, Y)
                End If
                If Y = 1 Then
                    PY = Mapa(X, Y)
                Else
                    PY = Mapa(X, Y - 1)
                End If
                P�id�k = (Mapa(X, Y) - PX) * 64 _
                + (Mapa(X, Y) - PY) * 32
                Dim MaxP�id As Integer, MaxUbr As Integer
                MaxP�id = 96
                MaxUbr = -96
                If P�id�k > MaxP�id Then P�id�k = MaxP�id
                If P�id�k < MaxUbr Then P�id�k = MaxUbr
                R = �erven�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * �erven�(BarvaOs) / 255 - 32 + �erven�(BarvaOs) / 8
                G = Zelen�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * Zelen�(BarvaOs) / 255 - 32 + Zelen�(BarvaOs) / 8
                B = Modr�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * Modr�(BarvaOs) / 255 - 32 + Modr�(BarvaOs) / 8
                If R < 0 Then R = 0
                If G < 0 Then G = 0
                If B < 0 Then B = 0
                On Error Resume Next
                SetPixel obrDr�ha(dzStTex).hdc, X, Y, RGB(R, G, B)
                'obrDr�ha(dzStTex).PSet (X, Y), RGB(R, G, B)
                'On Error GoTo 0
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "St�nuji �ikm� plochy a hrany � �. " & Y & " (Stop: Escape)"
    Next Y
p�eru�eno:
    obrDr�ha(dzStTex).MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = P�ipraven
    Umo�n�no = True
End Sub

Public Function Zdvih(ByVal Levobok As Long, ByVal Vr�ek As Long) As Variant
                                   'absolutn� v��ka
                                   'z-st�edu m��ku
    Dim X As Long, Y As Long, Z As Variant, I As Long
    Dim PX As Long, PY As Long 'polarisovan�
    ' Aby jakoby zvedal m��ek z hlubin bezedn�ch
    Z = MimoM��ek
    For X = 0 To 7
        For Y = 0 To 7
            'Debug.Assert Zdvih <= 7
            'Debug.Assert Not (X = 7 And Y = 0)
            For I = 1 To 4
                Select Case I
                Case 1
                    PX = -X + 7
                    PY = -Y + 7
                Case 2
                    PX = X + 7
                    PY = -Y + 7
                Case 3
                    PX = X + 7
                    PY = Y + 7
                Case 4
                    PX = -X + 7
                    PY = Y + 7
                Case Else
                End Select
                If Z - MapaM��ku(PX, PY) <= Mapa(Levobok + PX, Vr�ek + PY) Then
                    Z = Mapa(Levobok + PX, Vr�ek + PY) + MapaM��ku(PX, PY)
                End If
            Next I
        Next Y
    Next X
    Zdvih = Z
    'Nejnov�j��Zdvih = Z
End Function

Private Sub SadaSty�n�chBod�(ByVal Levobok As Long, ByVal Vr�ek As Long)
    Dim X As Long, Y As Long, Z As Variant, I As Long
'    Dim PX As Long, PY As Long
    Dim Kolik�t� As Long, ���ka As Long, V��ka As Long
    ���ka = UBound(MapaSty�n�chBod�(), 1)
    V��ka = UBound(MapaSty�n�chBod�(), 2)
    Z = Zdvih(Levobok, Vr�ek)
    For Y = 0 To 14
        For X = 0 To 14
            'PX = X
            'PY = Y
            If MapaM��ku(X, Y) <> MimoM��ek Then
                If Z - MapaM��ku(X, Y) = Mapa(Levobok + X, Vr�ek + Y) Then
                    If UBound(MapaSty�n�chBod�(), 3) < Kolik�t� Then
                        ReDim Preserve MapaSty�n�chBod�( _
                        ���ka, V��ka, Kolik�t�)
                    End If
                    MapaSty�n�chBod�(Levobok, Vr�ek, Kolik�t�).X = X
                    MapaSty�n�chBod�(Levobok, Vr�ek, Kolik�t�).Y = Y
'                    Debug.Print PX, PY
                    Kolik�t� = Kolik�t� + 1
                'Else
                    'Debug.Assert False
                    'Debug.Assert Not (CSng(Z - MapaM��ku(PX, PY)) = CSng(Mapa(Levobok + PX, Vr�ek + PY)))
                    'Debug.Assert MapaM��ku(PX, PY) <> MimoM��ek
                    'Debug.Assert obrVzorOkraje.Point(X, Y) <> vbBlack
                    'Debug.Assert Not Str(CDec(Z - MapaM��ku(X, Y))) = Str(CDec(Mapa(Levobok + X, Vr�ek + Y)))
                    'If Mapa(Levobok + X, Vr�ek + Y) <> 0 Then
                        'Debug.Print "Je v��: " & (Z - MapaM��ku(X, Y) > V��kaTer�nuPodM��kem(X, Y)), "V��ka: " & V��kaTer�nuPodM��kem(X, Y)
                    'End If
                End If
            End If
        Next
    Next
End Sub

Private Sub NamapujSty�n�Body()
    Dim X As Long, Y As Long
    Dim pam���ka As Long, pamV��ka As Long, I As Long
    Umo�n�no = False
    MousePointer = vbHourglass
    obrDr�ha(0).Visible = False
    obrDr�ha(1).Visible = False
    obrDr�ha(dzStTex).Visible = False
    obrSt�ny.Visible = True
    obrSt�ny.MousePointer = vbHourglass
    obrSt�ny.DrawMode = vbCopyPen
    obrSt�ny.Cls
    DoEvents
    ReDim MapaSty�n�chBod�(UBound(Mapa, 1) - 14, UBound(Mapa, 2) - 14, 0)
    MousePointer = vbHourglass
    pam���ka = UBound(Mapa, 1) - 14
    pamV��ka = UBound(Mapa, 2) - 14
    For Y = 0 To pamV��ka
        For X = 0 To pam���ka
            SadaSty�n�chBod� X, Y
            Dim R As Long, G As Long, B As Long, Prost�edn�Bod As Long
            G = MapaSty�n�chBod�(X, Y, 0).X * 15
            B = MapaSty�n�chBod�(X, Y, 0).Y * 15
            On Error Resume Next
            R = (MapaSty�n�chBod�(X, Y, 1).X + MapaSty�n�chBod�(X, Y, 1).Y) * 7.5
            On Error GoTo 0
            SetPixel obrSt�ny.hdc, X + 7, Y + 7, RGB(R, G, B)
            'obrSt�ny.PSet (X + 7, Y + 7), RGB(R, G, B)
            If (X + 1) Mod 64 = 0 Then obrSt�ny.Refresh: DoEvents
            If P�eru�itProces Then
                Exit Sub
            End If
        Next
        Stav = "Po��t�m sty�n� body - �. " & Y & " (Stop: Escape)"
        obrSt�ny.Refresh
        DoEvents
        If P�eru�itProces Then
'            P�eru�itProces = False
            GoTo p�eru�eno
        End If
    Next
    Exit Sub
p�eru�eno:
    obrDr�ha(dzStTex).MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = P�ipraven
    Umo�n�no = True
End Sub


Private Sub Uka�Obr�zekPodleNab�dky()
    obrSt�ny.Visible = False
    obrDr�ha(dzStTex).Visible = nabSt�ny.Checked
    obrDr�ha(0).Visible = nabReli�f.Checked
    obrDr�ha(1).Visible = nabTextura.Checked
End Sub

Public Property Let Akce(Nosi� As TypAkce)
    pamAkce = Nosi�
    Dim I As Control
    Static MinAkce As TypAkce
'    Static pamStPGra As Boolean
'    Static pamStPRel As Boolean
'    Static Pov�ce As Boolean
'    If Not Pov�ce Then
'        pamStPGra = Stavebn�Povolen�(dzTextura)
'        pamStPRel = Stavebn�Povolen�(dzReli�f)
'        Pov�ce = Not Pov�ce
'    End If
    nabBo�it.Checked = (Nosi� = aBo�it)
    nabVtisknoutPrvek.Checked = (Nosi� = aPrvek)
    nabObr�zek.Checked = (Nosi� = aObr�zek)
    nabStart.Checked = (Nosi� = aStart)
    nabNas�tPrvek.Checked = (Nosi� = aNas�t)
    For Each I In imgM��ek
        I.Visible = (Nosi� = aStart)
    Next
    For Each I In imgStart
        I.Visible = (Nosi� = aStart)
    Next
'    If (Nosi� = aStart) Or (Nosi� = aObr�zek) Then
'        Stavebn�Povolen�(dzTextura) = (Nosi� = aObr�zek)
'        Stavebn�Povolen�(dzReli�f) = False
'    Else
'        If (MinAkce = aObr�zek) Or (MinAkce = aStart) Then
'            Stavebn�Povolen�(dzTextura) = pamStPGra
'            Stavebn�Povolen�(dzReli�f) = pamStPRel
'        End If
'    End If
'    If Not (((Nosi� = aStart) Or (Nosi� = aObr�zek))) Or MinAkce = aNic Then
'        pamStPGra = Stavebn�Povolen�(dzTextura)
'        pamStPRel = Stavebn�Povolen�(dzReli�f)
'    End If
'    Povolen�Stavebn�hoPovolen� = (Nosi� <> aStart) And (Nosi� <> aObr�zek)
    MinAkce = Nosi�
    Dim Barva As Long
    Select Case Nosi�
    Case aBo�it
        Barva = vbRed
    Case aBo�itRychle
        Barva = RGB(255, 100, 20)
    Case aObr�zek
        Barva = vbMagenta
    Case aPrvek
        Barva = vbBlue
    Case aStart
        Barva = RGB(0, 200, 100)
        For Each I In obdPoloha
            Rozm�� I, imgM��ek(I.Index).Width, imgM��ek(I.Index).Height
            Srovnej imgM��ek(I.Index), I
        Next
    Case aNas�t
        Barva = vbGreen
    Case Else
        Barva = �ed�
    End Select
    Dim J As Shape
    For Each J In obdPoloha
        J.BorderColor = Barva
        J.Visible = Not (Nosi� = aStart)
        If Nosi� <> aObr�zek And Nosi� <> aStart Then Rozm�� J, opRozmV�b.���ka, opRozmV�b.V��ka
    Next
End Property

Public Property Get Akce() As TypAkce
    If nabBo�it.Checked Then
        Akce = aBo�it
    ElseIf nabObr�zek.Checked Then
        Akce = aObr�zek
    ElseIf nabVtisknoutPrvek.Checked Then
        Akce = aPrvek
    ElseIf nabStart.Checked Then
        Akce = aStart
    ElseIf nabNas�tPrvek.Checked Then
        Akce = aNas�t
    ElseIf pamAkce = aBo�itRychle Then
        Akce = pamAkce
    Else
        Akce = aNic
    End If
End Property

Private Sub P�idejR�me�ek()
    Dim I%
    Const V��kaNebety�n� = 1000 'takov� vej�ka, aby se vod n�
                       'ur�it� vodrazil
    MousePointer = vbHourglass
    Refresh
    For I = 0 To opRozm�ry.���ka + 2 - 1
        Mapa(I, 0) = V��kaNebety�n�
        Mapa(I, opRozm�ry.V��ka + 2 - 1) = V��kaNebety�n�
    Next
    For I = 0 To opRozm�ry.V��ka + 2 - 1
        Mapa(0, I) = V��kaNebety�n�
        Mapa(opRozm�ry.���ka + 2 - 1, I) = V��kaNebety�n�
    Next
    'Vyzna�it okraj na reli�fu
    obrDr�ha(0).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), opStupn�.BarvaPodleV��ky(1000), B
    'Vyzna�it okraj i na textu�e (nevim, jak by to jin�� �lo)
    '                            (kv�li st�n�m a map�)
    obrDr�ha(1).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    DoEvents
    MousePointer = vbDefault
End Sub

Private Sub Sma�R�me�ek()
    Dim I, ���ka, V��ka
    ���ka = UBound(Mapa, 1)
    V��ka = UBound(Mapa, 2)
    MousePointer = vbHourglass
    Refresh
    For I = 0 To ���ka
        Mapa(I, V��ka) = 0
    Next
    For I = 0 To V��ka
        Mapa(���ka, I) = 0
    Next
    obrDr�ha(0).Line (0, 0)-(���ka, V��ka), opStupn�.BarvaPodleV��ky(0), B
    obrDr�ha(1).Line (0, 0)-(���ka, V��ka), vbWhite, B
    DoEvents
    MousePointer = vbDefault
End Sub

Public Sub Na�tiSd�lenouMapuPrvku()
    Dim X%, Y%, ���ka%, V��ka%
    On Error Resume Next
    ���ka = UBound(Sd�len�MapaPrvku, 1)
    V��ka = UBound(Sd�len�MapaPrvku, 2)
    ReDim MapaPrvku(���ka, V��ka)
    For Y = 0 To V��ka
        For X = 0 To ���ka
            MapaPrvku(X, Y) = Sd�len�MapaPrvku(X, Y)
        Next
    Next
    Z�sobn�k.Reli�f = Sd�len�MapaPrvku
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Width = ���ka + 1
        Obd.Height = V��ka + 1
    Next
End Sub

Private Property Let Umo�n�no(Nosi� As Boolean)
    Dim Kontrolka As Control
    For Each Kontrolka In Me.Controls
        Select Case Kontrolka.Name
        Case "CD", "CDSb", "CDObr", "nabUlo�itJenGrafiku", "nabUlo�itJenStart", _
        "nabSep0", "nabSep1", "nabSep2", _
        "nabSep3", "nabSep4", "nabSep5", "nabSep6", "nabSep7", _
        "nabSep8", "nabSep9", "nabSep10", "nabSep11", _
        "obrSt�ny", "UXYZ"
            'nic
        Case "obdPoloha"
            Kontrolka.Visible = Nosi� And Akce <> aStart
        Case "imgM��ek", "imgStart"
            Kontrolka.Visible = Nosi� And Akce = aStart
        Case Else
            If Left(Kontrolka.Name, 6) <> "nabSep" Then Kontrolka.Enabled = Nosi�
        End Select
    Next
End Property

Private Property Get Umo�n�no() As Boolean
    Umo�n�no = nabSoubor.Enabled
End Property

Private Sub opRozm�ry_Ru�n�Zm�na()
    Dim BylP�ipravenej As Boolean
    BylP�ipravenej = (Stav = P�ipraven)
    If BylP�ipravenej Then Stav = "M�n�m rozm�ry dr�hy �"
    Nam��
    O��zniMapu
    'UplatniMapu
    If BylP�ipravenej Then Stav = P�ipraven
End Sub

Private Sub opRozm�ry_Nastaven�()
    Dim BylP�ipravenej As Boolean
    BylP�ipravenej = (Stav = P�ipraven)
    If BylP�ipravenej Then Stav = "M�n�m rozm�ry dr�hy �"
    Nam��
    'O��zniMapu
    UplatniMapu
    If BylP�ipravenej Then Stav = P�ipraven
End Sub

Private Sub opRozm�ry_Zm�naM��ky()
    If Akce <> aPrvek And Akce <> aStart Then
        Dim I As Shape
        For Each I In obdPoloha
            Rozm�� I, opRozm�ry.M��kaX, opRozm�ry.M��kaY
        Next
        opRozmV�b.Nastav opRozm�ry.M��kaX, opRozm�ry.M��kaY
    End If
    Ulo�Nastaven� "Nastaven�", "M��ka X", opRozm�ry.M��kaX
    Ulo�Nastaven� "Nastaven�", "M��ka Y", opRozm�ry.M��kaY
End Sub

Private Sub opRozmV�b_Nastaven�()
    If Akce = aStart Then Exit Sub
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Width = opRozmV�b.���ka
        Obd.Height = opRozmV�b.V��ka
    Next
End Sub

Private Sub opRozmV�b_Ru�n�Zm�na()
    opRozmV�b_Nastaven�
End Sub

Private Sub opStupn�_ColorSelect()
    If MsgBox("Chcete p�ebarvit reli�f?", _
    vbQuestion Or vbYesNoCancel) _
    = vbYes Then
        Stav = "P�ebarvuji reli�f �"
        UplatniMapu
        Stav = P�ipraven
    End If
End Sub

Public Property Let St�edit(Nosi� As Boolean)
    nabSt�edit.Checked = Nosi�
    Ulo�Nastaven� "Nastaven�", "St�edit", Nosi�
End Property

Public Property Get St�edit() As Boolean
    St�edit = DejNastaven�("Nastaven�", "St�edit", True)
End Property

Private Sub sbrStav_PanelClick(ByVal Panel As msComctlLib.Panel)
    Select Case Panel.Index
    Case 2
        If Panel.Enabled Then Stavebn�Povolen�(dzTextura) = Not Stavebn�Povolen�(dzTextura)
    Case 3
        If Panel.Enabled Then Stavebn�Povolen�(dzReli�f) = Not Stavebn�Povolen�(dzReli�f)
    Case 4
        TrBGra = Not TrBGra
    Case 5
        TrBRel = Not TrBRel
    End Select
End Sub

