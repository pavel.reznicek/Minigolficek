VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMinigolf��ek 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Minigolf��ek"
   ClientHeight    =   3780
   ClientLeft      =   156
   ClientTop       =   432
   ClientWidth     =   4692
   Icon            =   "frmMinigolf��ek.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MousePointer    =   99  'Custom
   Palette         =   "frmMinigolf��ek.frx":0442
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   315
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   391
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   276
      Left            =   0
      TabIndex        =   12
      Top             =   3504
      Width           =   4692
      _ExtentX        =   8276
      _ExtentY        =   487
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   3239
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1693
            MinWidth        =   1693
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   3239
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox obrR�me�ek 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      Negotiate       =   -1  'True
      ScaleHeight     =   67
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   67
      TabIndex        =   10
      Top             =   2520
      Width           =   855
      Begin VB.PictureBox obrTer�n 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   3864
         Left            =   120
         MouseIcon       =   "frmMinigolf��ek.frx":30484
         MousePointer    =   99  'Custom
         Negotiate       =   -1  'True
         Picture         =   "frmMinigolf��ek.frx":30D4E
         ScaleHeight     =   322
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   322
         TabIndex        =   11
         Top             =   120
         Width           =   3864
      End
   End
   Begin VB.Timer tmrOdr�e� 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   2880
      Top             =   0
   End
   Begin VB.PictureBox obrVzorOkraje 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   180
      Left            =   720
      Picture         =   "frmMinigolf��ek.frx":7CF20
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   5
      Top             =   0
      Visible         =   0   'False
      Width           =   180
   End
   Begin MCI.MMControl MMC 
      Height          =   495
      Index           =   1
      Left            =   1080
      TabIndex        =   4
      Top             =   480
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   868
      _Version        =   393216
      UpdateInterval  =   50
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   120
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CommandButton tlVy�isti 
      Caption         =   "&Vy�isti"
      Height          =   255
      Left            =   1200
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Timer tmr�as�k 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   2280
      Top             =   0
   End
   Begin VB.PictureBox obrVzor 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   180
      Left            =   0
      Picture         =   "frmMinigolf��ek.frx":7D452
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   2
      Top             =   0
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.PictureBox obrVzorMaska 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   180
      Left            =   240
      Picture         =   "frmMinigolf��ek.frx":7D764
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   15
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.PictureBox obrPozad� 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   480
      ScaleHeight     =   19
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   19
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   225
   End
   Begin MCI.MMControl MMC 
      Height          =   495
      Index           =   2
      Left            =   1080
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   868
      _Version        =   393216
      UpdateInterval  =   50
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin MCI.MMControl MMC 
      Height          =   495
      Index           =   3
      Left            =   1080
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   868
      _Version        =   393216
      UpdateInterval  =   50
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin MCI.MMControl MMC 
      Height          =   495
      Index           =   4
      Left            =   1080
      TabIndex        =   8
      Top             =   2280
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   868
      _Version        =   393216
      UpdateInterval  =   50
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin MCI.MMControl MMC 
      Height          =   495
      Index           =   5
      Left            =   1080
      TabIndex        =   9
      Top             =   2880
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   868
      _Version        =   393216
      UpdateInterval  =   50
      DeviceType      =   ""
      FileName        =   ""


   End
   Begin VB.Menu nabHra 
      Caption         =   "&Hra"
      Begin VB.Menu nabDr�ha 
         Caption         =   "&Vybrat dr�hu �"
         Shortcut        =   ^D
      End
      Begin VB.Menu nabNaStart 
         Caption         =   "N�vrat na &start (Home)"
         Enabled         =   0   'False
      End
      Begin VB.Menu nabSep4 
         Caption         =   "-"
      End
      Begin VB.Menu nabV�sl 
         Caption         =   "V�s&ledky na t�to dr�ze �"
         Enabled         =   0   'False
         Shortcut        =   ^V
      End
      Begin VB.Menu nabSep10 
         Caption         =   "-"
      End
      Begin VB.Menu nabEditorDrah 
         Caption         =   "=> Editor &drah"
         Shortcut        =   {F3}
      End
      Begin VB.Menu nabSb�rka 
         Caption         =   "=> &Sb�rka ter�nn�ch prvk�"
         Shortcut        =   +{F3}
      End
      Begin VB.Menu nabEditorPrvk� 
         Caption         =   "=> Editor ter�nn�ch &prvk�"
         Shortcut        =   {F4}
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
      End
      Begin VB.Menu nabZav��t 
         Caption         =   "&Zav��t"
      End
      Begin VB.Menu nabKonec 
         Caption         =   "&Konec"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabSouhNast 
         Caption         =   "&Souhrn nastaven�"
         Shortcut        =   ^N
      End
      Begin VB.Menu nabSep5 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabSep6 
         Caption         =   "-"
      End
      Begin VB.Menu nabZvuk 
         Caption         =   "&Zvuk"
         Checked         =   -1  'True
         Shortcut        =   ^Z
      End
      Begin VB.Menu nabSep2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabStopa 
         Caption         =   "&Kreslit stopu"
         Visible         =   0   'False
      End
      Begin VB.Menu nabVy�istit 
         Caption         =   "&Vy�istit"
         Visible         =   0   'False
      End
      Begin VB.Menu nabKontrola 
         Caption         =   "&Kontrola"
         Visible         =   0   'False
         Begin VB.Menu nabS�ly 
            Caption         =   "&Zobrazova� sil"
         End
         Begin VB.Menu nabZpomalit 
            Caption         =   "&Zpomalit"
            Visible         =   0   'False
         End
         Begin VB.Menu nabKrokovat 
            Caption         =   "&Krokovat"
            Shortcut        =   {F12}
         End
         Begin VB.Menu nabKontrolaRel 
            Caption         =   "&Kontrola reli�fu"
         End
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
      Begin VB.Menu nabInfo 
         Caption         =   "&Info"
         Shortcut        =   ^I
      End
      Begin VB.Menu nabLogo 
         Caption         =   "&Logo"
      End
   End
End
Attribute VB_Name = "frmMinigolf��ek"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim Poloha As XY
Dim �der As XY
Dim Start As Boolean
Dim StartXY As XY
Dim KreslitSpojku As Boolean
Dim My� As XY
Dim Pohyb As Boolean
Dim pamPo�et�der� As Long

Const �ikm�Zrychlen� = g_pix_ms

Dim Mapa() As Single
Dim MapaSty�n�chBod�() As XYByte
Dim Sty�n�Body() As XYLngZVar
Dim Odraz As Long
Dim Odra�eno As Boolean
Dim D�le As Boolean
Dim Nejnov�j��Zdvih As Variant
Dim AbsZdvih As Variant
Dim StB As XYLng ' surov� sty�n� bod
Dim PStB As XY ' symetricky polarisovan� sty�n� bod


Private Sub Nakresli(ByVal lx As Long, ByVal ly As Long, Optional lw, Optional lh, Optional Za��tek As Boolean)
    Dim lwl As Long, lhl As Long
    Dim lrtn As Long
    Static BackSavedFlag As Boolean
    Static Min_lx As Long, Min_ly As Long
    If Not IsMissing(lw) Then lwl = CLng(lw) Else lwl = obrPozad�.ScaleWidth
    If Not IsMissing(lh) Then lhl = CLng(lh) Else lhl = obrPozad�.ScaleHeight
    ' Pokud za��n�me, pozad� nen� ulo�eno!
    If Za��tek Then BackSavedFlag = False
    ' Pokud jedeme, pozad� je ulo�eno, a tak ho obnov!
    If BackSavedFlag = True Then
        lrtn = BitBlt(obrTer�n.hdc, Min_lx, Min_ly, lwl, lhl, _
            obrPozad�.hdc, 0, 0, vbSrcCopy)
    End If
    ' Kresl� te�ku pro kontrolu dr�hy
    If nabStopa.Checked Then ' Pokud je to dovoleno
        obrTer�n.DrawWidth = 1
        obrTer�n.PSet (lx + 7, ly + 7), RGB(&HFF, &H0, &HFF)
    End If
    ' Ukl�d� pozad� na nov� posici
    lrtn = BitBlt(obrPozad�.hdc, 0, 0, lwl, lhl, _
        obrTer�n.hdc, lx, ly, vbSrcCopy)
    ' Ulo�eno.
    BackSavedFlag = True
    ' Aplikuje masku
    lrtn = BitBlt(obrTer�n.hdc, lx, ly, lwl, lhl, _
        obrVzorMaska.hdc, 0, 0, vbSrcAnd)
    ' Kresl� m��ek
    lrtn = BitBlt(obrTer�n.hdc, lx, ly, lwl, lhl, _
        obrVzor.hdc, 0, 0, vbSrcPaint)
    obrTer�n.Refresh
    ' Zapamatuje si tuto posici pro p��t� �taci
    Min_lx = lx
    Min_ly = ly
End Sub

Private Sub NakresliSpojku()
    If KreslitSpojku Then
        With obrTer�n
         Dim D�lkaZob�ku#
         Dim Spojka As New XYObj
         Dim ��ra(0 To 2) As New XYObj
         Dim KrajC�le As XY
         Dim C�l As New XYObj
         C�l My�.X, My�.Y
         Spojka C�l.X - St�edM��ku.X, C�l.Y - St�edM��ku.Y
         If Not M��itPrakem Then Spojka.�hel = Spojka.�hel + 180
         S�la = Spojka.D�lka
         If Spojka.D�lka > 7 And M��itPrakem Then Spojka.D�lka = Spojka.D�lka - 7
         KrajC�le = CXY(St�edM��ku.X + Spojka.X, St�edM��ku.Y + Spojka.Y)
         D�lkaZob�ku = Spojka.D�lka / 5
         .AutoRedraw = False
         .Refresh
         .DrawWidth = ���ka�ipky
         obrTer�n.Line (St�edM��ku.X, St�edM��ku.Y)-(KrajC�le.X, KrajC�le.Y), BarvaT�
         Dim I%
         For I = 0 To 2 Step 2
            ��ra(I).D�lka = D�lkaZob�ku
            ��ra(I).�hel = Spojka.�hel + (360 - (I - 1) * 30)
            obrTer�n.Line (KrajC�le.X - ��ra(I).X, KrajC�le.Y - ��ra(I).Y)-(KrajC�le.X, KrajC�le.Y), BarvaH�
         Next
         .AutoRedraw = True
         DoEvents
        End With
    End If
End Sub

Private Property Let Po�et�der�(Nosi� As Long)
    sbrStav.Panels(3).Text = "Po�et �der�: " & Nosi�
    pamPo�et�der� = Nosi�
End Property

Private Property Get Po�et�der�() As Long
    Po�et�der� = pamPo�et�der�
End Property

Private Property Let S�la(ByVal Nosi� As Single)
    sbrStav.Panels(1).Text = "S�la: " & Nosi�
End Property

Private Sub Form_Activate()
    If Me.Visible And nabS�ly.Checked Then
        frmS�ly.Show
        frmPr��ez.Show
    End If
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
    Case KeyCodeConstants.vbKeyHome
        If Not nabNaStart.Enabled Then Exit Sub
        Pohyb = False
        tmr�as�k.Enabled = False
        Poloha = StartXY
        Nakresli Poloha.X, Poloha.Y
        KreslitSpojku = True
        Po�et�der� = 0
        If Not Ukotvit Then
            Sje�
        End If
    Case KeyCodeConstants.vbKeyControl
        If obrTer�n.Enabled Then
            If Not Pohyb Then
                Spus�
            Else
                Pohyb = False
                KreslitSpojku = True
            End If
        End If
    Case KeyCodeConstants.vbKeySpace
        D�le = True
    Case Else
    End Select
End Sub

Private Sub Form_Load()
    DefTPP
    'obrTer�n.Move 0, Bottom(obrVzor)
    ObnovHodZRegistru
    obrTer�n.BorderStyle = 0
    obrTer�n.Move -1, -1, 322, 322
    obrR�me�ek.Move 0, 0, _
    obrR�me�ek.Width - obrR�me�ek.ScaleWidth + obrTer�n.Width - 2, _
    obrR�me�ek.Height - obrR�me�ek.ScaleHeight + obrTer�n.Height - 2
    obrTer�n.AutoSize = True
    Me.Width = Me.Width - (Me.ScaleWidth - obrR�me�ek.Width) * Screen.TwipsPerPixelX
    Me.Height = Me.Height - (Me.ScaleHeight - obrR�me�ek.Height - sbrStav.Height) * Screen.TwipsPerPixelY
    NamapujM��ek
'    If nabS�ly.Checked Then
'        nabS�ly.Checked = False
'        nabS�ly_Click
'        'frmS�ly.Show
'        'frmPr��ez.Show
'    End If
    MMC(1).FileName = App.Path & "\Zvuky\odraz.wav"
    MMC(2).FileName = App.Path & "\Zvuky\dopad.wav"
    MMC(3).FileName = App.Path & "\Zvuky\potlesk.wav"
    MMC(4).FileName = App.Path & "\Zvuky\potlesk7.wav"
    MMC(5).FileName = App.Path & "\Zvuky\odpal.wav"
    Dim Zvuka� As MMControl
    For Each Zvuka� In MMC
        Zvuka�.Command = "open"
    Next
    'Nakresl� m��ek doprost�ed dr�hy (na uk�zku)
'    StartXY = CXY(obrTer�n.ScaleWidth / 2 - 7, obrTer�n.ScaleHeight / 2 - 7)
'    Poloha = StartXY
'    Nakresli Poloha.X, Poloha.Y, , , True
    'Vyzna�TG
    If Command <> "" Then
        If Right(Command, 4) = " /ed" Then
            Me.Hide
            frmEditorDrah.Otev�i Left(Command, Len(Command) - 4)
        ElseIf Right(Command, 3) = "/ed" Then
            Me.Hide
            frmEditorDrah.Otev�i Left(Command, Len(Command) - 3)
        ElseIf Right(Command, 3) = "stp" Then
            Me.Hide
            frmSb�rka.Otev�i Command
        Else
            Otev�i Command
        End If
    End If
End Sub

Private Sub Form_Resize()
    If WindowState <> vbNormal Then Exit Sub
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    'obrSm�r.Move ScaleWidth - obrSm�r.Width - 1, ScaleHeight - obrSm�r.Height - 1
End Sub
'
'Private Sub MMC_StatusUpdate()
'    'If MMC.Position > 0 Then DoEvents ': Stop
'    'MMC.Refresh
'End Sub

Private Sub MMC_Done(Index As Integer, NotifyCode As Integer)
    'Stop
    'MMC(Index).From = 0
End Sub

Private Sub nabBarvaT�la�ipky_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = BarvaT�
    On Error GoTo vypadni
    CD.ShowColor
    BarvaT� = CD.Color
    Ulo�Nastaven� "Nastaven�\�ipka", "Barva t�la", CD.Color
    NakresliSpojku
vypadni:
End Sub

Private Sub nabBarvaHlavi�ky�ipky_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = BarvaH�
    On Error GoTo vypadni
    CD.ShowColor
    BarvaH� = CD.Color
    Ulo�Nastaven� "Nastaven�\�ipka", "Barva hlavi�ky", CD.Color
    NakresliSpojku
vypadni:
End Sub

'Private Sub MMC_StatusUpdate(Index As Integer)
'    'If MMC(Index).Position > 0 Then DoEvents
'End Sub

Private Sub nabDr�ha_Click()
    CD.CancelError = True
    CD.InitDir = App.Path & "\Dr�hy"
    CD.DialogTitle = "Vybrat dr�hu"
    CD.Filter = "Dr�hy minigolfu (*.mig)|*.mig"
    CD.DefaultExt = ".mig"
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNExplorer Or cdlOFNLongNames
    Enabled = False
    DoEvents
    On Error GoTo vypadni
    CD.ShowOpen
    DoEvents
    Otev�i CD.FileName
    Exit Sub
vypadni:
    Enabled = True
End Sub

Public Sub Otev�i(Soubor As String)
    MousePointer = vbHourglass
    obrTer�n.Enabled = False
    obrTer�n.MousePointer = vbHourglass
    Pohyb = False
    Dim Jm�noTextury As String
    Jm�noTextury = Hol�Jm�no(Soubor) & ".bmp"
    Dim ���ka As Integer, V��ka As Integer
    Dim MaximumSty�n�chBod� As Integer, Rozte�X As Byte, Rozte�Y As Byte
    On Error GoTo 0
    On Error GoTo chyba
    Close
    Open Soubor For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    Get #1, , Rozte�X
    Get #1, , Rozte�Y
    Get #1, , MaximumSty�n�chBod�
    Get #1, , StartXY
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    ReDim MapaSty�n�chBod�(���ka + 2 - 15, V��ka + 2 - 15, MaximumSty�n�chBod�)
    Get #1, , MapaSty�n�chBod�()
    Close
'    If MapaSty�n�chBod�(0, 0, 0) = CXYByte() Then
'        MsgBox "Neplatn� mapa sty�n�ch bod�!", vbCritical, Caption
'        MousePointer = vbDefault
'        obrTer�n.MousePointer = vbDefault
'        Enabled = True
'        Exit Sub
'    End If
    Pohyb = False
    tmr�as�k.Enabled = False
    Poloha = StartXY
    With obrTer�n
        .Picture = LoadPicture(Jm�noTextury)
        obrR�me�ek.Move 0, 0, _
        obrR�me�ek.Width - obrR�me�ek.ScaleWidth + ���ka, _
        obrR�me�ek.Height - obrR�me�ek.ScaleHeight + V��ka
    End With
    With obrR�me�ek
        Width = Width - ScaleWidth * TPPX + (.Width) * TPPX
        Height = Height - ScaleHeight * TPPY + (.Height + sbrStav.Height) * TPPY
    End With
    Nakresli Poloha.X, Poloha.Y, , , True
    KreslitSpojku = True
    Po�et�der� = 0
    obrTer�n.Enabled = True
    obrTer�n.MousePointer = vbCustom
    MousePointer = vbDefault
    Enabled = True
    Caption = "Minigolf��ek - " & Hol�Jm�no(Dir(Soubor))
    If nabS�ly.Checked Then
        P�i�a�Sty�n�Body
        ZobrazSm�r CXY(), , , , True
        ZobrazSty�n�Body
        ZobrazPr��ez
    End If
    If Not Ukotvit Then
        Sje�
    End If
    nabNaStart.Enabled = True
    nabV�sl.Enabled = True
    CD.FileName = Soubor
    Exit Sub
chyba:
    MsgBox "Chyba p�i otev�r�n�! " & Err.Description & ".", vbCritical
    MousePointer = vbDefault
    obrTer�n.MousePointer = vbCustom
vypadni:
'    obrTer�n.Enabled = True
    Enabled = True
'    MousePointer = vbDefault
'    obrTer�n.MousePointer = vbCustom
End Sub

Private Sub nabEditorDrah_Click()
    frmEditorDrah.Show
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Editor drah"
'    SendKeys "{F3}"
'    Exit Sub
'neni_spu�t�nej:
'    On Error Resume Next
'    Shell App.Path & "\EditorDrah", vbNormalFocus
'    AppActivate "Editor drah"
End Sub

Private Sub nabEditorPrvk�_Click()
    frmEditorPrvk�.Show
'    On Error GoTo neni_spu�t�nej
'    Me.SetFocus
'    AppActivate "Editor drah", True
'    Dim I&, a
'    For I = 1 To 10000
'        a = 2E+20
'    Next
'    AppActivate "Editor drah"
'    SendKeys "{F4}", True
'    Exit Sub
'neni_spu�t�nej:
'    On Error Resume Next
'    Shell App.Path & "\EditorDrah /EP", vbNormalFocus
'    SendKeys "{F4}"
End Sub

Private Sub nabInfo_Click()
    frmInfo.Show 1, Me
End Sub

Private Sub nabKonec_Click()
    End
End Sub

Private Sub nabKontrolaRel_Click()
    Dim X As Long, Y As Long
    On Error Resume Next
    For Y = 0 To obrTer�n.ScaleHeight - 1
        For X = 0 To obrTer�n.ScaleWidth - 1
'            obrTer�n.DrawMode = vbXorPen
'            If Mapa(X, Y) <> 0 Then
'                obrTer�n.PSet (X, Y)
'            Else
'                obrTer�n.PSet (X, Y), vbWhite
'            End If
            obrTer�n.DrawMode = vbXorPen
            If MapaSty�n�chBod�(X, Y, 0).X = 7 _
            And MapaSty�n�chBod�(X, Y, 0).Y = 7 Then
                obrTer�n.PSet (X + 7, Y + 7), vbWhite
            End If
        Next
        Refresh
    Next
vypadni:
    obrTer�n.DrawMode = vbCopyPen
End Sub

Private Sub nabKrokovat_Click()
    nabKrokovat.Checked = Not nabKrokovat.Checked
    D�le = Not nabKrokovat.Checked
End Sub

Private Sub nabM��it_Click(Index As Integer)
    M��itPrakem = Index
End Sub

Private Sub nabLogo_Click()
    frmSplash.Show
End Sub

Private Sub nabN�pov�da_Click()
'    frmN�pov�da.Show
    On Error GoTo Neni_spu�t�n�_n�pov�da
    AppActivate "N�pov�da�k�Minigolf��ku"
    Exit Sub
Neni_spu�t�n�_n�pov�da:
    Shell "winhelp " & App.Path & "\Minigolficek.hlp"
End Sub

Private Sub nabNaStart_Click()
    SendKeys "{home}"
End Sub

Private Sub nabRychlost_Click()
    On Error GoTo chyba
    Dim Box$
    Box = InputBox("Zadejte rychlost zobrazen�:" _
    & Chr(13) & "(1 = kreslit ka�dou polohu, 2 = ka�dou 2. polohu atd.)", _
    "Rychlost zobrazen�", RychlostZobrazen�)
    If Box = "" Then
        Exit Sub
    Else
        RychlostZobrazen� = Val(Box)
    End If
    If RychlostZobrazen� = 0 Then RychlostZobrazen� = 1
    If RychlostZobrazen� > 100 Then
chyba:
        RychlostZobrazen� = 100
    End If
    Ulo�Nastaven� "Nastaven�", "Rychlost", RychlostZobrazen�
    NakresliSpojku
End Sub


Private Sub nabSb�rka_Click()
    frmSb�rka.Show
End Sub

Private Sub nabS�ly_Click()
    nabS�ly.Checked = Not nabS�ly.Checked
    If nabS�ly.Checked Then
        P�i�a�Sty�n�Body
        ZobrazSm�r CXY(), , , , True
        ZobrazPr��ez
        ZobrazSty�n�Body
        frmS�ly.Show
        frmPr��ez.Show
    Else
        frmS�ly.Hide
        frmPr��ez.Hide
    End If
End Sub

Private Sub nabSouhNast_Click()
    frmNast.Karta = 0
    frmNast.Show 0, Me
End Sub

Private Sub nabStopa_Click()
    With nabStopa
    .Checked = Not .Checked
    End With
End Sub

Private Sub nab���ka�ipky_Click()
    On Error GoTo chyba
    Dim Box$
    Box = InputBox("Zadejte ���ku �ipky v bodech:", "���ka �ipky", ���ka�ipky)
    If Box = "" Then
        Exit Sub
    Else
        ���ka�ipky = Val(Box)
    End If
    If ���ka�ipky > 100 Then
chyba:
        ���ka�ipky = 100
    End If
    Ulo�Nastaven� "Nastaven�\�ipka", "���ka", ���ka�ipky
    NakresliSpojku
End Sub

Private Sub nabVy�istit_Click()
    obrTer�n.Cls
    Nakresli Poloha.X, Poloha.Y
End Sub

Private Sub nabV�sl_Click()
    frmV�sl.Uka�V�sledky Jm�noDr�hy
End Sub

Private Sub nabZav��t_Click()
    Unload Me
End Sub

Private Sub nabZpomalit_Click()
    nabZpomalit.Checked = Not nabZpomalit.Checked
    frmS�ly.AutoRedraw = nabZpomalit.Checked
End Sub

Private Sub nabZvuk_Click()
    Zvuk = Not Zvuk
End Sub

Private Sub obrTer�n_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    My� = CXY(X, Y)
    If KreslitSpojku Then
        NakresliSpojku
    End If
End Sub

Private Sub obrTer�n_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Static Pov�ce As Boolean
    If Button = MouseButtonConstants.vbLeftButton Then
        If Not Pov�ce Then
            Pov�ce = Not Pov�ce
'            Poloha.X = HalfX(obrTer�n) - HalfX(obrVzor)
'            Poloha.Y = HalfY(obrTer�n) - HalfY(obrVzor)
            Po�et�der� = Po�et�der� + 1
            �der.X = X - HalfX(obrVzor) - Poloha.X
            �der.Y = Y - HalfY(obrVzor) - Poloha.Y
            Start = True
            Spus�
        Else
            If Pohyb And Shift = vbShiftMask Then
                Pohyb = False
'                �der.X = X - Poloha.X
'                �der.Y = Y - Poloha.Y
                KreslitSpojku = True
                My� = CXY(X, Y)
            ElseIf Not Pohyb Then
'                obrTer�n.Cls
                Po�et�der� = Po�et�der� + 1
                �der.X = X - HalfX(obrVzor) - Poloha.X
                �der.Y = Y - HalfY(obrVzor) - Poloha.Y
                Start = True
                Spus�
            End If
        End If
    ElseIf Button = vbRightButton Then
        Pohyb = False
        KreslitSpojku = True
        My� = CXY(X, Y)
        Poloha.X = My�.X - HalfX(obrVzor)
        Poloha.Y = My�.Y - HalfY(obrVzor)
        If Poloha.X < 0 Then Poloha.X = 1
        If Poloha.Y < 0 Then Poloha.Y = 1
        If Poloha.X > obrTer�n.ScaleWidth - 15 Then Poloha.X = obrTer�n.ScaleWidth - 15
        If Poloha.Y > obrTer�n.ScaleHeight - 15 Then Poloha.Y = obrTer�n.ScaleHeight - 15
        Nakresli Poloha.X, Poloha.Y
        'Nejnov�j��Zdvih = -&HFFFFFF ' aby si nemyslel, �e spadnul
    End If
    If nabS�ly.Checked Then
        P�i�a�Sty�n�Body
        ZobrazPr��ez
        ZobrazSm�r CXY(), , , , True
        ZobrazSty�n�Body
    End If
End Sub

Private Sub Sje�()
    Start = True
    �der = CXY(0, 0)
    frmS�ly.nabKP.Caption = "<kontroln� pole>"
    KreslitSpojku = False
    Pohyb = False
    Je�
    My� = CXY(Poloha.X + 7, Poloha.Y + 7)
End Sub

Private Sub Spus�()
    Start = True
    �der = XYRozd�l(My�, St�edM��ku)
    If Not M��itPrakem Then �der = XYNeg(�der)
    frmS�ly.nabKP.Caption = "<kontroln� pole>"
    KreslitSpojku = False
    Pohyb = False
    Je�
End Sub

Private Sub Je�()
    Dim t As Double, v As Double
    Dim X As Double, Y As Double, Z As Double
    Dim pamSp�d As XY
    Pohyb = True
    tmr�as�k.Enabled = �asova�
    Do While Pohyb
        DoEvents
        
        If Start Then
            If Po�et�der� > 0 And nabZvuk.Checked Then
                Zazvu� 3
            End If
            X = �der.X / 100
            Y = �der.Y / 100
            Start = False
            'Nejnov�j��Zdvih = -&HFFFFFF  ' aby si nemyslel, �e spadnul
        End If
        
        P�i�a�Sty�n�Body 'zjist� z mapy sty�n�ch bod�,
                         'odkud p�sob� s�ly
        If AbsZdvih = V��kaC�le + 7 Then
            'Pohyb = False
            tmr�as�k.Enabled = False
            DoEvents
            Exit Do
        End If
        pamSp�d = Sp�d(CXY(X, Y))
        X = pamSp�d.X - pamSp�d.X / 200
        Y = pamSp�d.Y - pamSp�d.Y / 200
        Static Vektor As XY
        Vektor = CXY(X, Y)
        Static Sm�rPohybu As XY
        Sm�rPohybu = Okam�it�Sm�r(Vektor)
'        Z = V��ka(Okt�l(0)) - V��ka(Sm�rPohybu)
'
'        If (X ^ 2 + Y ^ 2 + Z ^ 2) = 0 Then
'            v = 0
'        Else
'            v = (Abs(((X ^ 2 + Y ^ 2) * Z * g_pix_ms) / (X ^ 2 + Y ^ 2 + Z ^ 2))) ^ (1 / 2)
'        End If
'        If v = 0 Then
'            t = 0
'            'Pohyb = False
'        Else
'            t = ((Sm�rPohybu.X ^ 2 + Sm�rPohybu.Y ^ 2) ^ (1 / 2)) / v
'        End If

        If Not Pohyb Then Exit Do
        Poloha.X = Poloha.X + Sm�rPohybu.X
        Poloha.Y = Poloha.Y + Sm�rPohybu.Y
        
        P�i�a�Sty�n�Body
        
        ' Zdr� ho na p�ek�k�ch
'        Dim i As Double
'        For i = 1 To t
'            Dim a
'            a = DoEvents()
'            If i > 10000 Then
'                Pohyb = False
'            End If
'            If Not Pohyb Then Exit For
'        Next
        'Debug.Print t
        'Debug.Print CSng(X), CSng(Y)
        
        ' Pokud u� se skoro v�bec nehejbe, tak mu utni tipec
        Const Mez = 0.02
        
        If CXYObj(X, Y).D�lka < Mez Then
            If M��eZastavit Then
                Pohyb = False
                tmr�as�k.Enabled = False
                KreslitSpojku = True
                Exit Do
            End If
        End If
'        If (X) >= -Mez And (X) <= Mez _
'        And (Y) >= -Mez And (Y) <= Mez _
'        Then
'            If M��eZastavit Then
'                'Debug.Print CSng(X), CSng(Y)
'                Pohyb = False
'                tmr�as�k.Enabled = False
'                KreslitSpojku = True
'                'MsgBox "Nulov� energie!"
'                Exit Do
'            End If
'        End If
        
        If nabZvuk.Checked Then Zazvu�
        
        If nabS�ly.Checked Then
            P�i�a�Sty�n�Body
            ZobrazSm�r Vektor, 200, 200, &HFFFFFF, True     'b�l�
            ZobrazSm�r Okam�it�Sm�r(Vektor), 1, 1, &HFF0000 'modr�
            ZobrazSm�r pamSp�d, 5, 5, &HFF00FF              'fialov�
            'ZobrazSm�r Sm�r(Vektor), 1, 1, &H0              '�ern�
            ZobrazSty�n�Body
            ZobrazPr��ez
            D�le = Not nabKrokovat.Checked
            Do While Not D�le
                DoEvents
            Loop
        End If
        
        Dim Kolikr�tProb�hl As Long
        Kolikr�tProb�hl = Kolikr�tProb�hl + 1
        If Kolikr�tProb�hl = RychlostZobrazen� Then
            If Not �asova� Then Nakresli Poloha.X, Poloha.Y
            Kolikr�tProb�hl = 0
        End If
    Loop
    Nakresli Poloha.X, Poloha.Y
    'KreslitSpojku = True
    DoEvents
    If Po�et�der� > 0 Then NakresliSpojku
    If Nejnov�j��Zdvih = -50 + 7 Then
        'Pohyb = False
        tmr�as�k.Enabled = False
        DoEvents
        Dim Jm�no As String
        Jm�no = InputBox("Gratuluji, dos�hl(a) jste c�le! Po�et �der�: " & _
          Po�et�der� & "." & Chr(13) & _
          "Zadejte, pros�m, sv� jm�no do listiny v�t�z�!", _
          "V�sledky", _
          JmPoslHr��e)
        JmPoslHr��e = Jm�no
        If Jm�no = "" Then Jm�no = "Nezn�m�"
        frmV�sl.Zapi�V�sledek Jm�noDr�hy, Jm�no, Po�et�der�
        If Po�et�der� <= 7 Then
            If nabZvuk.Checked Then Zazvu� 1
        Else
            If nabZvuk.Checked Then Zazvu� 2
        End If
        frmV�sl.Uka�V�sledky Jm�noDr�hy
    End If

'    If nabS�ly.Checked Then
'        ZobrazSm�r Vektor, 200, 200, &HFFFFFF, True     'b�l�
'        ZobrazSm�r Okam�it�Sm�r(Vektor), 1, 1, &HFF0000 'modr�
'        ZobrazSm�r pamSp�d, 5, 5, &HFF00FF              'fialov�
'        'ZobrazSm�r Sm�r(Vektor), 1, 1, &H0              '�ern�
'        ZobrazSty�n�Body
'        ZobrazPr��ez
'    End If
End Sub

Public Function Jm�noDr�hy() As String
    Jm�noDr�hy = Hol�Jm�no(Dir(CD.FileName))
End Function

Private Property Let JmPoslHr��e(Nosi� As String)
    Ulo�Nastaven� "Nastaven�", "Posledn� hr��", Nosi�
End Property

Private Property Get JmPoslHr��e() As String
    JmPoslHr��e = DejNastaven�("Nastaven�", "Posledn� hr��", "")
End Property

Private Function M��eZastavit() As Boolean
    Dim I%
    For I = 0 To UBound(Sty�n�Body())
        If (Sty�n�Body(I).X = 7 And Sty�n�Body(I).Y = 7) _
        Or UBound(Sty�n�Body()) > 0 _
        Or (VTPM(7, 7) = VTPM(7, 8) And VTPM(7, 6) = VTPM(7, 9) And VTPM(7, 6) > VTPM(7, 7) And Nejnov�j��Zdvih - 7 > VTPM(7, 7)) _
        Or (VTPM(7, 7) = VTPM(8, 7) And VTPM(6, 7) = VTPM(9, 7) And VTPM(6, 7) > VTPM(7, 7) And Nejnov�j��Zdvih - 7 > VTPM(7, 7)) _
        Or JeNaKrajiXYLngZVar(Sty�n�Body(I)) Then
            M��eZastavit = True
        End If
    Next
End Function

Private Function Okam�it�Sm�r(Vektor As XY) As XY
    Dim objVektor As New XYObj
    objVektor Vektor.X, Vektor.Y
    If objVektor.D�lka > 1 Then objVektor.D�lka = 1
    Okam�it�Sm�r = CXY(objVektor.X, objVektor.Y)
    Exit Function
'
'    Dim SPX As Double, SPY As Double ' Sm�ry pohybu
'    Dim Pom�r As Variant
'    SPX = Vektor.X
'    SPY = Vektor.Y
'    If SPX = 0 Then
'        Pom�r = tg(90) * Sgn(SPY)
'    Else
'        Pom�r = SPY / SPX
'    End If
'    Dim �hel
'    �hel = (arctg(Pom�r))
'    Select Case SPX
'    Case Is > 1
'        Select Case �hel
'        Case -90# To -45
'            Okam�it�Sm�r.X = -tg(-90 - �hel)
'            Okam�it�Sm�r.Y = -1
'        Case -45 To 0
'            Okam�it�Sm�r.X = 1
'            Okam�it�Sm�r.Y = tg(�hel)
'        Case 0 To 45
'            Okam�it�Sm�r.X = 1
'            Okam�it�Sm�r.Y = tg(�hel)
'        Case 45 To 90#
'            Okam�it�Sm�r.X = tg(90 - �hel)
'            Okam�it�Sm�r.Y = 1
'        Case Else
'            Stop
'        End Select
'    Case 0 To 1
'        Select Case SPY
'        Case Is < -1, Is > 1
'            Select Case �hel
'            Case -90# To -45
'                Okam�it�Sm�r.X = -tg(-90 - �hel)
'                Okam�it�Sm�r.Y = -1
'            Case -45 To 0
'                Okam�it�Sm�r.X = 1
'                Okam�it�Sm�r.Y = tg(�hel)
'            Case 0 To 45
'                Okam�it�Sm�r.X = 1
'                Okam�it�Sm�r.Y = tg(�hel)
'            Case 45 To 90#
'                Okam�it�Sm�r.X = tg(90 - �hel)
'                Okam�it�Sm�r.Y = 1
'            Case Else
'                MsgBox "? Divnej �hel! �hel: " & �hel & " spx: " & SPX & " spy: " & SPY
'                'Stop
'            End Select
'        Case Else
'            Okam�it�Sm�r.X = SPX
'            Okam�it�Sm�r.Y = SPY
'        End Select
'    Case -1 To 0
'        Select Case SPY
'        Case Is < -1, Is > 1
'            Select Case �hel
'            Case -90# To -45
'                Okam�it�Sm�r.X = tg(-90 - �hel)
'                Okam�it�Sm�r.Y = 1
'            Case -45 To 0
'                Okam�it�Sm�r.X = -1
'                Okam�it�Sm�r.Y = -tg(�hel)
'            Case 0 To 45
'                Okam�it�Sm�r.X = -1
'                Okam�it�Sm�r.Y = -tg(�hel)
'            Case 45 To 90#
'                Okam�it�Sm�r.X = -tg(90 - �hel)
'                Okam�it�Sm�r.Y = -1
'            Case Else
'                MsgBox "? Divnej �hel! �hel: " & �hel & " spx: " & SPX & " spy: " & SPY
'            End Select
'        Case Else
'            Okam�it�Sm�r.X = SPX
'            Okam�it�Sm�r.Y = SPY
'        End Select
'    Case Is < -1
'        Select Case �hel
'        Case -90# To -45
'            Okam�it�Sm�r.X = tg(-90 - �hel)
'            Okam�it�Sm�r.Y = 1
'        Case -45 To 0
'            Okam�it�Sm�r.X = -1
'            Okam�it�Sm�r.Y = -tg(�hel)
'        Case 0 To 45
'            Okam�it�Sm�r.X = -1
'            Okam�it�Sm�r.Y = -tg(�hel)
'        Case 45 To 90#
'            Okam�it�Sm�r.X = -tg(90 - �hel)
'            Okam�it�Sm�r.Y = -1
'        Case Else
'            MsgBox "? Divnej �hel! �hel: " & �hel & " spx: " & SPX & " spy: " & SPY
'            Stop
'        End Select
'    Case Else
'    End Select
'    'Debug.Print Okam�it�Sm�r.X, Okam�it�Sm�r.Y, �hel
End Function

Private Function Sm�r(Vektor As XY) As XY
    'Sm�r.X = Sgn(Sp�d.X)
    'Sm�r.Y = Sgn(Sp�d.Y)
    Sm�r = Okam�it�Sm�r(Sp�d(Vektor))
End Function

Private Function Sp�dZBodu(X, Y) As XY
    Dim Sklon As XY
    Dim Sou�et As XY
    Dim I As Long
    For I = 1 To 8
        Sklon.X = (V��ka(CXY(X - 7, Y - 7)) - V��ka(CXY(X - 7 + Okt�l(I).X, Y - 7 + Okt�l(I).Y))) * Okt�l(I).X
        Sklon.Y = (V��ka(CXY(X - 7, Y - 7)) - V��ka(CXY(X - 7 + Okt�l(I).X, Y - 7 + Okt�l(I).Y))) * Okt�l(I).Y
    
'        Sklon.X = (V��kaTer�nuPodM��kem(X, Y) - V��kaTer�nuPodM��kem(X + Okt�l(i).X, Y + Okt�l(i).Y)) * Okt�l(i).X
'        Sklon.Y = (V��kaTer�nuPodM��kem(X, Y) - V��kaTer�nuPodM��kem(X + Okt�l(i).X, Y + Okt�l(i).Y)) * Okt�l(i).Y
        Sou�et.X = Sou�et.X + Sklon.X
        Sou�et.Y = Sou�et.Y + Sklon.Y
    Next I
    Sp�dZBodu.X = Sou�et.X '/ 8
    Sp�dZBodu.Y = Sou�et.Y '/ 8
End Function

Private Function Sp�d(Vektor As XY) As XY
    Dim Sklon As XY
    Dim Z As Single
    Static MinSp�d As XY
    Static MinZdvih
    Dim I As Long
    Dim Odr�et As Boolean
    Dim YUr�eno As Boolean
    'Odr�et = (Poloha.X Mod 2 = Poloha.Y Mod 2)
    ' Pokud m��ek z n�jak�ch d�vod� nedosedl:
    If Sty�n�Body(0).X = 0 And Sty�n�Body(0).Y = 0 Then
'        Debug.Assert False
'        Pohyb = False
        ' Zachovej sp�d, jak� byl minule.
        ' (M��e se v budoucnu nechat nule,
        ' pokud se poda�� vychytat ty "nedosed�ky".)
        ' (To u� se poda�ilo, ale pro jistotu
        '                       nech�me to tu.)
        Sp�d = Vektor
        Exit Function
    End If
    'Dim Zd
    'Zd = Zdvih
    'PStB = CXYLng()
    'StB = CXYLng()
    Dim Pov�ce As Boolean
    Odraz = 0
    ' Zpr�m�rov�n� sty�n�ch bod� (te� se nepou��v�)
    
    For I = 0 To UBound(Sty�n�Body)
        If Sty�n�Body(I).X = 0 And Sty�n�Body(I).Y = 0 _
        Then Exit For
'        If Not Pov�ce Then
            PStB.X = (Sty�n�Body(I).X - 7)
            PStB.Y = (Sty�n�Body(I).Y - 7)
'            Pov�ce = Not Pov�ce
'        Else
'            On Error Resume Next
'            PStB.X = tg((Arcus(XYLngToXY(PStB)) + Arcus(CXY(Sty�n�Body(i).X - 7, Sty�n�Body(i).Y - 7))) / 2) * 7
'            PStB.Y = -cotg((Arcus(XYLngToXY(PStB)) + Arcus(CXY(Sty�n�Body(i).X - 7, Sty�n�Body(i).Y - 7))) / 2) * 7
'            On Error GoTo 0
'            PStB.X = (PStB.X + Sty�n�Body(i).X - 7) / 2
'            PStB.Y = (PStB.Y + Sty�n�Body(i).Y - 7) / 2
'        End If
'    Next i
'    If PStB.X > 0 Then
'        If PStB.X - Int(PStB.X) > 0 Then PStB.X = Int(PStB.X) + 1
'    Else
'        If PStB.X - Int(PStB.X) < 0 Then PStB.X = Int(PStB.X) - 1
'    End If
'    If PStB.Y > 0 Then
'        If PStB.Y - Int(PStB.Y) > 0 Then PStB.Y = Int(PStB.Y) + 1
'    Else
'        If PStB.Y - Int(PStB.Y) < 0 Then PStB.Y = Int(PStB.Y) - 1
'    End If
    StB = CXYLng(PStB.X + 7, PStB.Y + 7)
    YUr�eno = False

        
    ' Z�le�� na sm�ru a sty�n�m bod� => odraz
    ' V�b�rov� ��zen� na ose X
    Select Case PStB.X * Sgn(Vektor.X)
    Case 0
        ' ��dnej vodraz
        Sklon.X = ZrychliSm�r(Vektor, slX)
        'Odra�eno = False
    Case Is < 0
        If JeNaKrajiXYLng(StB) Then
            ' Nezrychluj - mo�n� to byl odraz
            ' (p�edchoz� pad�n� u� t� zrychlilo)
            Sklon.X = Vektor.X
            'Sklon.X = ZrychliSm�r(Vektor, slX)
            If Odraz <> 1 Then Odraz = 2
        Else
            ' Zrychlen� na �ikm� plo�e
            Sklon.X = ZrychliSm�r(Vektor, slX)
        End If
    Case Is > 0
        If JeNaKrajiXYLng(StB) Then
            ' Absolutn� odraz
            Odraz = 1
            If PStB.Y = 0 Then
                Sklon.X = -1 * Vektor.X
            Else
                '(X) (odraz bude zm�n�n podle pot�eby)
                Sklon = Odra�Sm�r(Vektor)
                YUr�eno = True
            End If
        Else
            ' Zpomalen�
            Sklon.X = ZrychliSm�r(Vektor, slX)
        End If
    Case Else
    End Select
    
    ' V�b�rov� ��zen� na ose Y
    Select Case PStB.Y * Sgn(Vektor.Y)
    Case 0
        ' ��dnej vodraz
        If Not YUr�eno Then Sklon.Y = ZrychliSm�r(Vektor, slY)
        'Odra�eno = False
    Case Is < 0
        If JeNaKrajiXYLng(StB) Then
            ' Mo�n� to byl odraz - nezrychluj
            ' (p�edchoz� pad�n� u� t� zrychlilo)
            If Not YUr�eno Then
                Sklon.Y = Vektor.Y
                'Sklon.Y = ZrychliSm�r(Vektor, slY)
            End If
            If Odraz <> 1 Then Odraz = 2
        Else
            ' Zrychlen�
            Sklon.Y = ZrychliSm�r(Vektor, slY)
        End If
    Case Is > 0
        If JeNaKrajiXYLng(StB) Then
            ' Absolutn� odraz
            Odraz = 1
            If PStB.X = 0 Then
                Sklon.Y = -1 * Vektor.Y
            Else
                '(Y) (odraz bude zm�n�n podle pot�eby)
                Sklon = Odra�Sm�r(Vektor)
            End If
        Else
            ' Zpomalen�
            Sklon.Y = ZrychliSm�r(Vektor, slY)
        End If
    Case Else
    End Select
    
'Debug.Print PStB.X * Sgn(Vektor.X), PStB.Y * Sgn(Vektor.Y)
    
dosko�i�t�:
    
        Sp�d.X = Sp�d.X + Sklon.X
        Sp�d.Y = Sp�d.Y + Sklon.Y
        Vektor = Sklon
    Next I
    
    Sp�d = Vektor
    'Sp�d.X = Sp�d.X / i
    'Sp�d.Y = Sp�d.Y / i
        
'    If Nejnov�j��Zdvih < MinZdvih - 3 _
'    Then Odraz = 2: Stop
    'Debug.Assert Not CBool(Odraz)
    MinZdvih = Nejnov�j��Zdvih
    MinSp�d = Sp�d
End Function

Private Function Odra�Sm�r(Vektor As XY) As XY
    Dim ArcusCadendi As Double
    Dim VectorAxalis As XY
    VectorAxalis = (PStB)
    ArcusCadendi = ArcusAbAxe(Vektor, VectorAxalis)
    Select Case ArcusCadendi
    Case 90# To 270#
            Odra�Sm�r = Vektor
            If Odraz <> 1 Then Odraz = 2
    Case Else
        If Sgn(PStB.X) = Sgn(PStB.Y) _
        Then
            Odra�Sm�r.X = -Vektor.Y
            Odra�Sm�r.Y = -Vektor.X
        Else
            Odra�Sm�r.X = Vektor.Y
            Odra�Sm�r.Y = Vektor.X
        End If
        Odraz = 1
    End Select
End Function

Private Function ZrychliSm�r(Vektor As XY, KterouSlo�ku As Slo�kyVektoru)
    Select Case KterouSlo�ku
    Case slX
        ZrychliSm�r = Vektor.X - �ikm�Zrychlen� * PStB.X / 7
    Case slY
        ZrychliSm�r = Vektor.Y - �ikm�Zrychlen� * PStB.Y / 7
    Case Else
    End Select
End Function

Private Function V��ka(Kde As XY) As Double
    Dim St�ed As Double
    St�ed = HalfX(obrPozad�)
'    V��ka = (obrPozad�.Point(St�ed + Kter�mSm�rem.X, _
'    St�ed + Kter�mSm�rem.Y) / &HFFFFFF) * 15
    On Error Resume Next
    V��ka = Mapa(Abs(Poloha.X) + St�ed + Kde.X, _
    Abs(Poloha.Y) + St�ed + Kde.Y) '  * 16
'    Debug.Print V��ka
End Function

Private Function Okt�l(ByVal Index As Integer) As XY
    Select Case Index
    Case 0
        Okt�l.X = 0
        Okt�l.Y = 0
    Case 1
        Okt�l.X = 1
        Okt�l.Y = -1
    Case 2
        Okt�l.X = 1
        Okt�l.Y = 0
    Case 3
        Okt�l.X = 1
        Okt�l.Y = 1
    Case 4
        Okt�l.X = 0
        Okt�l.Y = 1
    Case 5
        Okt�l.X = -1
        Okt�l.Y = 1
    Case 6
        Okt�l.X = -1
        Okt�l.Y = 0
    Case 7
        Okt�l.X = -1
        Okt�l.Y = -1
    Case 8
        Okt�l.X = 0
        Okt�l.Y = -1
    Case Else
        MsgBox "�patn� hodnota - okt�l m� indexy" _
        & "jen od 0 do 8!", vbCritical
    End Select
End Function

Private Sub ZobrazSm�r(Vektor As XY, Optional MaxX, Optional MaxY, Optional Barva As OLE_COLOR, Optional Prvn� As Boolean)
    On Error Resume Next
    With frmS�ly
    If Prvn� Then .Cls
    If Not IsMissing(MaxX) Then
        .ScaleWidth = MaxX * 2
        .ScaleLeft = -MaxX
    End If
    If Not IsMissing(MaxY) Then
        .ScaleHeight = MaxY * 2
        .ScaleTop = -MaxY
    End If
    End With
    If Vektor.X > MaxInt Then
        Vektor.X = MaxInt - 1
        frmS�ly.nabKP.Caption = "X = raketa!"
        'stop
    End If
    If Vektor.Y > MaxInt Then
        Vektor.Y = MaxInt - 1
        frmS�ly.nabKP.Caption = "Y = raketa!"
        ' Stop
    End If
    If Vektor.X < -MaxInt + 1 Then
        Vektor.X = -MaxInt + 2
        frmS�ly.nabKP.Caption = "X = meteorit!"
        ' Stop
    End If
    If Vektor.Y < -MaxInt + 1 Then
        Vektor.Y = -MaxInt + 2
        frmS�ly.nabKP.Caption = "Y = meteorit!"
        ' Stop
    End If
    frmS�ly.Line (0, 0)-(Vektor.X, Vektor.Y), Barva
End Sub

'
'Private Function Right(Object As Object)
'    Right = Object.Left + Object.Width
'End Function

Private Function Bottom(Object As Object)
    Bottom = Object.Top + Object.Height
End Function

Private Function MiddleX(Object As Object)
    MiddleX = Object.Left + Object.ScaleWidth \ 2
End Function

Private Function MiddleY(Object As Object)
    MiddleY = Object.Top + Object.ScaleHeight \ 2
End Function

Private Function HalfX(Object As Object)
    HalfX = Object.ScaleWidth \ 2
End Function

Private Function HalfY(Object As Object)
    HalfY = Object.ScaleHeight \ 2
End Function

Private Sub Vyzna�C�l(X As Single, Y As Single)
    Dim AR As Boolean
    AR = obrTer�n.AutoRedraw
    obrTer�n.AutoRedraw = True
    obrTer�n.Cls
    obrTer�n.PaintPicture obrTer�n.MouseIcon, X - HalfX(obrVzor) + 2, Y - HalfY(obrVzor) - 1
    obrTer�n.AutoRedraw = AR
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not (frmSb�rka.Visible Or frmEditorDrah.Visible Or frmEditorPrvk�.Visible) Then
        End
    Else
        Cancel = True
        Me.Hide
        frmS�ly.Hide
        frmPr��ez.Hide
    End If
End Sub

Private Sub Vyzna�TG()
    Dim I As Single
    With frmS�ly
    .ScaleHeight = 10
    .ScaleWidth = 720
    .ScaleTop = -5
    .ScaleLeft = -360
    For I = -360 To 360
        Dim tg1 As Single, tg2 As Single
        tg1 = tg(I - 1)
        tg2 = tg(I)
        If tg1 > MaxInt - 1 Then tg1 = MaxInt
        If tg1 < -MaxInt Then tg1 = -MaxInt
        If tg2 > MaxInt Then tg2 = MaxInt
        If tg2 < -MaxInt Then tg2 = -MaxInt
        frmS�ly.Line (I - 1, tg2)-(I, tg1), 0
    Next
    .ScaleWidth = 60
    .ScaleHeight = 60
    .ScaleLeft = -30
    .ScaleTop = -30
    End With
End Sub

Private Sub tlVy�isti_Click()
    obrTer�n.Cls
    Nakresli Poloha.X, Poloha.Y
End Sub

Private Sub tmr�as�k_Timer()
    If Pohyb Then Nakresli Poloha.X, Poloha.Y
End Sub

Private Function St�edM��ku() As XY
    St�edM��ku = XYPosun(Poloha, 7)
End Function

Private Function JeNaKraji(ByVal X As Long, ByVal Y As Long) As Boolean
    ' M�lo by se to po��tat matematicky,
    ' jestli bod le�� na kru�nici,
    ' ale sou�adnicov� zkreslen� by zp�sobilo,
    ' �e by to v�t�inou bylo t�sn� vedle.
    ' Proto to mus�me zjistit z bitmapy.
    JeNaKraji = (obrVzorOkraje.Point(X, Y) = vbBlack)
End Function

Private Function JeNaKrajiXYLng(Bod As XYLng) As Boolean
    JeNaKrajiXYLng = (obrVzorOkraje.Point(Bod.X, Bod.Y) = vbBlack)
End Function

Private Function JeNaKrajiXYLngZVar(Bod As XYLngZVar) As Boolean
    JeNaKrajiXYLngZVar = (obrVzorOkraje.Point(Bod.X, Bod.Y) = vbBlack)
End Function

Private Function V��kaTer�nuPodM��kem(ByVal X As Long, ByVal Y As Long)
    If JeVM��ku(X, Y) Then
        V��kaTer�nuPodM��kem = Mapa(Poloha.X + X, Poloha.Y + Y)
    Else
        V��kaTer�nuPodM��kem = MimoM��ek
    End If
'    V��kaTer�nuPodM��kem = Mapa(Poloha.X + X, Poloha.Y + Y)
End Function

Private Function VTPM(ByVal X As Long, ByVal Y As Long)
    On Error Resume Next
    If JeVM��ku(X, Y) Then
        VTPM = Mapa(Poloha.X + X, Poloha.Y + Y)
    Else
        VTPM = MimoM��ek
    End If
'    V��kaTer�nuPodM��kem = Mapa(Poloha.X + X, Poloha.Y + Y)
End Function

Public Function M�Spadnout() As Boolean
    M�Spadnout = (V��kaTer�nuPodM��kem(7, 7) < V��kaTer�nuPodM��kem(StB.X, StB.Y))
End Function

Private Sub P�i�a�Sty�n�Body()
    Dim I As Long
    Dim IPX As Integer, IPY As Integer
    IPX = Zaokrouhli(Poloha.X)
    IPY = Zaokrouhli(Poloha.Y)
    Dim Kolik�t�
    Static MinZ
    Static BylNaKraji As Boolean
    On Error GoTo chyba
    ReDim Sty�n�Body(0)
    For I = 0 To UBound(MapaSty�n�chBod�(), 3)
        If MapaSty�n�chBod�(IPX, IPY, I).X = 0 _
        And MapaSty�n�chBod�(IPX, IPY, I).Y = 0 _
        Then
            Debug.Assert Not I = 0
            Exit For
        End If
        ReDim Preserve Sty�n�Body(I)
        Sty�n�Body(I).X = MapaSty�n�chBod�(IPX, IPY, I).X
        Sty�n�Body(I).Y = MapaSty�n�chBod�(IPX, IPY, I).Y
        Sty�n�Body(I).Z = Mapa(IPX + Sty�n�Body(I).X, IPY + Sty�n�Body(I).Y) + MapaM��ku(Sty�n�Body(I).X, Sty�n�Body(I).Y)
        If Not JeNaKrajiXYLngZVar(Sty�n�Body(I)) Then
            'If BylNaKraji Then
                'Nejnov�j��Zdvih = MinZ
                'BylNaKraji = False
            'Else
                Nejnov�j��Zdvih = Sty�n�Body(I).Z
                'BylNaKraji = False
            'End If
            MinZ = Nejnov�j��Zdvih
            AbsZdvih = Nejnov�j��Zdvih
        Else
            ' Nem�nit nejnov�j�� zdvih
            AbsZdvih = Sty�n�Body(I).Z ' + MapaM��ku(Sty�n�Body(I).X, Sty�n�Body(I).Y)
'            If Nejnov�j��Zdvih < Sty�n�Body(i).Z Then
'                Nejnov�j��Zdvih = Mapa(IPX + 7, IPY + 7) + 7
'            Else
'                Nejnov�j��Zdvih = Sty�n�Body(i).Z
'            End If
            
            'Nejnov�j��Zdvih = MinZ + 7 - MapaM��ku(Sty�n�Body(i).X, Sty�n�Body(i).Y)
            'BylNaKraji = True
            'Debug.Assert Not (Sty�n�Body(i).Z = MimoM��ek - 7)
            'Debug.Assert JeVM��ku(Sty�n�Body(i).X, Sty�n�Body(i).Y)
        End If
    Next
    Exit Sub
chyba:
'    Debug.Assert False
'    Pohyb = False
    'MsgBox "Neplatn� mapa sty�n�ch bod�!", vbCritical, Caption
End Sub
'
'Private Sub NajdiSty�n�Body() 'star� verse
'    Dim X As Long, Y As Long, Z
'    Dim Kolik�t�
'    ReDim Sty�n�Body(15 * 15)
'    Z = Zdvih
'    For X = 0 To 14
'        For Y = 0 To 14
'            If JeVM��ku(X, Y) Then
'                If CDec(Z - MapaM��ku(X, Y)) = CDec(V��kaTer�nuPodM��kem(X, Y)) Then
'                    Sty�n�Body(Kolik�t�) = CXYZ(X, Y, V��kaTer�nuPodM��kem(X, Y))
'                    Kolik�t� = Kolik�t� + 1
'                Else
'                    If V��kaTer�nuPodM��kem(X, Y) <> 0 Then
'                        'Stop
'                        'Debug.Print "Je v��: " & (Z - MapaM��ku(X, Y) > V��kaTer�nuPodM��kem(X, Y)), "V��ka: " & V��kaTer�nuPodM��kem(X, Y)
'                        'Debug.Assert Not Str(CDec(Z - MapaM��ku(X, Y))) = Str(CDec(V��kaTer�nuPodM��kem(X, Y)))
'                    End If
'                End If
'            End If
'        Next
'    Next
'    'Debug.Print Kolik�t�
'End Sub

Private Sub ZobrazSty�n�Body()
    Dim I
    With frmS�ly
    .ScaleMode = vbPixels
    .DrawWidth = .ScaleHeight / 15
    frmS�ly.Scale (0, 0)-(15, 15)
    For I = 0 To UBound(Sty�n�Body())
        If Sty�n�Body(I).X = 0 And Sty�n�Body(I).Y = 0 Then Exit For
        frmS�ly.PSet (Sty�n�Body(I).X + 0.5, Sty�n�Body(I).Y + 0.5), RGB(&HFF, &HFF, &H0)
    Next I
'    frmS�ly.PSet (StB.X + 0.5, StB.Y + 0.5), RGB(&HFF, 0, &HFF)
    .DrawWidth = 1
    End With
End Sub

Private Sub ZobrazPr��ez()
    With frmPr��ez
    .Cls
    Dim X As Long, Z As Long
    On Error Resume Next
    For X = .ScaleLeft To .ScaleWidth + .ScaleLeft
        Dim MaxZ
        MaxZ = 0
        MaxZ = Mapa(Zaokrouhli(Poloha.X) + 7 + X, Zaokrouhli(Poloha.Y) + 7)
        For Z = 0 To MaxZ
            frmPr��ez.PSet (X, Z)
        Next Z
    Next X
    frmPr��ez.Circle (0, IIf(IsEmpty(Nejnov�j��Zdvih), 7, Nejnov�j��Zdvih)), 7
    '.shpM��ek.Move -7, Nejnov�j��Zdvih + 7, 15, 15
    End With
End Sub

Private Sub Zazvu�(Optional Jin�Druh As Integer)
    Dim Zvuka� As MMControl
    Static MinOdraz As Integer
    On Error Resume Next
    Select Case Jin�Druh
    Case 0
        Select Case Odraz
        Case 0
            ' nic
        Case Else
            'For Each Zvuka� In MMC
                'If Zvuka�.Index <> Odraz Then
                    'Zvuka�.Command = "stop"
                'End If
            'Next
            DoEvents
            If Odraz <> MinOdraz Then
                MMC(Odraz).From = 0
                MMC(Odraz).Command = "play"
                'MMC(Odraz).Command = "sound"
            End If
        End Select
    Case 1
        For Each Zvuka� In MMC
            Zvuka�.Command = "stop"
        Next
        DoEvents
        MMC(3).From = 0
        MMC(3).Command = "play"
    Case 2
        For Each Zvuka� In MMC
            Zvuka�.Command = "stop"
        Next
        DoEvents
        MMC(4).From = 0
        MMC(4).Command = "sound"
    Case 3
        For Each Zvuka� In MMC
            Zvuka�.Command = "stop"
        Next
        DoEvents
        MMC(5).From = 0
        MMC(5).Command = "sound"
    Case Else
    End Select
    MinOdraz = Odraz
    If Odraz Then DoEvents
End Sub

Private Sub tmrOdr�e�_Timer()
    Odra�eno = False
    tmrOdr�e�.Enabled = False
End Sub
