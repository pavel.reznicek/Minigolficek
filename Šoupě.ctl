VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.UserControl �oup� 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   240
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   Begin VB.TextBox TXT 
      Alignment       =   1  'Right Justify
      Height          =   300
      Left            =   720
      MultiLine       =   -1  'True
      TabIndex        =   0
      Text            =   "�oup�.ctx":0000
      Top             =   360
      Width           =   540
   End
   Begin ComCtl2.UpDown UD 
      Height          =   300
      Left            =   1275
      TabIndex        =   1
      Top             =   360
      Width           =   195
      _ExtentX        =   344
      _ExtentY        =   529
      _Version        =   327681
      Value           =   1
      AutoBuddy       =   -1  'True
      BuddyControl    =   "TXT"
      BuddyDispid     =   196609
      OrigLeft        =   96
      OrigTop         =   24
      OrigRight       =   109
      OrigBottom      =   40
      Max             =   32
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Label lblN�pis 
      Caption         =   "Hodnota:"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "�oup�"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event Zm�na()

Private Sub UserControl_Initialize()
    UserControl.Refresh
End Sub

Private Sub UserControl_InitProperties()
    UserControl.Refresh
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Hodnota = .ReadProperty("Hodnota", 1)
        Min = .ReadProperty("Min", 1)
        Max = .ReadProperty("Max", 10)
        N�pis = .ReadProperty("N�pis", lblN�pis.Caption)
        ���kaTxt = .ReadProperty("���kaTxt", 36)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Hodnota", Hodnota, 1
        .WriteProperty "Min", Min, 1
        .WriteProperty "Max", Max, 10
        .WriteProperty "���kaTxt", ���kaTxt, 50
        .WriteProperty "N�pis", lblN�pis.Caption, "Hodnota:"
    End With
End Sub

Private Sub UserControl_Resize()
    DefTPP
    TXT.Move ScaleWidth - UD.Width - TXT.Width, 0, TXT.Width, ScaleHeight
    UserControl.Height = TXT.Height * TPPY
    lblN�pis.Move 0, 3, TXT.Left, ScaleHeight
    UD.Height = ScaleHeight
    UD.Move ScaleWidth - UD.Width, 0
'Debug.Print UD.Width
'    UD.Width = ScaleWidth - UD.Left
'    UD.Move ScaleWidth - UD.Width, 0
'    Width = Width - ScaleWidth * TPPX + Pravobok(UD) * TPPX
'    UserControl.Refresh
End Sub

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    Dim Ctl As Control
    For Each Ctl In UserControl.Controls
        Ctl.Enabled = Nosi�
    Next
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Public Property Let Max(Nosi� As Integer)
    UD.Max = Nosi�
    PropertyChanged "Max"
End Property

Public Property Get Max() As Integer
    Max = UD.Max
End Property

Public Property Let Min(Nosi� As Integer)
    UD.Min = Nosi�
    PropertyChanged "Min"
End Property

Public Property Get Min() As Integer
    Min = UD.Min
End Property

Public Property Let Hodnota(Nosi� As Integer)
    UD = IIf(Nosi� < UD.Min, UD.Min, Nosi�)
    PropertyChanged "Hodnota"
End Property

Public Property Get Hodnota() As Integer
Attribute Hodnota.VB_UserMemId = 0
    If Val(TXT) < UD.Min Then
        Hodnota = UD.Min
    ElseIf Val(TXT) > UD.Max Then
        Hodnota = UD.Max
    Else
        Hodnota = Val(TXT)
    End If
    UD = Hodnota
End Property

Public Property Let ���kaTxt(Nosi� As Integer)
    TXT.Width = Nosi�
    TXT.Left = ScaleWidth - UD.Width - Nosi�
    PropertyChanged "���kaTxt"
End Property

Public Property Get ���kaTxt() As Integer
    ���kaTxt = TXT.Width
End Property

Public Property Let N�pis(Nosi� As String)
    lblN�pis = Nosi�
    PropertyChanged "N�pis"
End Property

Public Property Get N�pis() As String
    N�pis = lblN�pis
End Property

Private Sub TXT_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0") To Asc("9"), 8
        'nech tak
    Case 13
        'potvr� hodnotu
        If Val(TXT) < UD.Min Then
            UD = UD.Min
        ElseIf Val(TXT) > UD.Max Then
            UD = UD.Max
        Else
            UD = Val(TXT)
        End If
        KeyAscii = 0
    Case Else
        'nic nepi�
        KeyAscii = 0
    End Select
End Sub

Private Sub UD_Change()
    Static MinHod
    If MinHod <> UD Then RaiseEvent Zm�na
    MinHod = UD
End Sub

