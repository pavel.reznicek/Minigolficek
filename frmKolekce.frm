VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmKolekce 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sb�rka ter�nn�ch prvk�"
   ClientHeight    =   4890
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   4710
   Icon            =   "frmKolekce.frx":0000
   MaxButton       =   0   'False
   ScaleHeight     =   326
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   314
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   7
      Top             =   4620
      Width           =   4710
      _ExtentX        =   8308
      _ExtentY        =   476
      Style           =   1
      SimpleText      =   "P�ipraven"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin EditorDrah.UkazatelXYZ UXYZ 
      Height          =   735
      Left            =   3000
      TabIndex        =   6
      Top             =   3885
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1296
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin EditorDrah.Rozm�ry opRozm�ry 
      Height          =   525
      Left            =   3000
      TabIndex        =   5
      Top             =   3360
      Width           =   1695
      _ExtentX        =   2778
      _ExtentY        =   953
      Max���ka        =   600
      MaxV��ka        =   480
      ���ka           =   320
      V��ka           =   320
      M��ka          =   32
   End
   Begin VB.PictureBox obrTer�n 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   855
      Left            =   120
      MouseIcon       =   "frmKolekce.frx":0442
      MousePointer    =   99  'Custom
      Negotiate       =   -1  'True
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   4
      Top             =   3120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox obrSt�ny 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   3
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox obrPamObr 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   540
      Left            =   2400
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   0
      Top             =   1560
      Visible         =   0   'False
      Width           =   540
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   2400
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".glf"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   1
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   2
      Top             =   1080
      Width           =   855
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         Height          =   480
         Index           =   1
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      Height          =   855
      Index           =   0
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   1
      Top             =   120
      Width           =   855
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         Height          =   480
         Index           =   0
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin MSComDlg.CommonDialog CDObr 
      Left            =   2400
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".glf"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin VB.Menu nabSoubor 
      Caption         =   "&Soubor"
      Begin VB.Menu nabOtev��tStarou 
         Caption         =   "Otev��t starou dr�hu"
         Visible         =   0   'False
      End
      Begin VB.Menu nabOtev��t 
         Caption         =   "&Otev��t dr�hu"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabSep0 
         Caption         =   "-"
      End
      Begin VB.Menu nabSep8 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabMinigolf��ek 
         Caption         =   "=> &Minigolf��ek"
         Shortcut        =   {F2}
      End
      Begin VB.Menu nabEditorPrvk� 
         Caption         =   "=> Editor ter�nn�ch &prvk�"
         Shortcut        =   {F3}
      End
   End
   Begin VB.Menu nab�pravy 
      Caption         =   "�&pravy"
      Visible         =   0   'False
      Begin VB.Menu nabSep7 
         Caption         =   "-"
      End
      Begin VB.Menu nabSep6 
         Caption         =   "-"
      End
   End
   Begin VB.Menu nabZobrazit 
      Caption         =   "&Zobrazit"
      Begin VB.Menu nabTextura 
         Caption         =   "- &texuru"
         Checked         =   -1  'True
         Shortcut        =   ^X
      End
      Begin VB.Menu nabReli�f 
         Caption         =   "- &reli�f"
         Shortcut        =   ^R
      End
      Begin VB.Menu nabSt�ny 
         Caption         =   "- texturu se &st�ny"
         Shortcut        =   ^S
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabM��ka 
         Caption         =   "Zarovn�vat ke &m��ce"
         Checked         =   -1  'True
         Shortcut        =   ^M
      End
      Begin VB.Menu nabSep5 
         Caption         =   "-"
      End
      Begin VB.Menu nabSep4 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabTransparent 
         Caption         =   "&Transparentn� barva �"
         Shortcut        =   ^T
      End
      Begin VB.Menu nabPou��vatTrB 
         Caption         =   "&Pou��vat transparentn� barvu"
         Begin VB.Menu nabTrBTex 
            Caption         =   "pro &grafiku"
            Shortcut        =   {F11}
         End
         Begin VB.Menu nabTrBRel 
            Caption         =   "pro &reli�f"
            Shortcut        =   {F12}
         End
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmKolekce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim TPPX, TPPY
Dim Mapa() As Single
Dim MapaPrvku() As Single
Dim MapaSty�n�chBod�() As XYByte
Dim P�eru�itProces As Boolean
Public opStupn� As Control
Public TrB As OLE_COLOR 'transparentn� barva

Dim P�vAkce As TypAkce

Const �ed� = 8421504 'RGB(128, 128, 128)
Const P�ipraven = "P�ipraven"

Private Enum TypAkce
    aBo�it = -1
    aNic = 0
    aPrvek = 1
    aObr�zek = 2
    aStart = 3
    aNas�t = 4
End Enum

Private Enum DruhZobrazen�
    drReli�f = 0
    drTexura = 1
End Enum


Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        'KeyAscii = 0
        If MsgBox("P�eru�it prob�haj�c� proces (pokud n�jak� prob�h�)?", vbQuestion Or vbYesNo) = vbYes Then
            P�eru�itProces = True
        End If
    End If
End Sub

Private Sub Form_Load()
    TPPX = Screen.TwipsPerPixelX
    TPPY = Screen.TwipsPerPixelY
    Set opStupn� = frmEditorDrah.opStupn�
    TrB = GetSetting("Minigolf��ek", App.Title & "\Barvy", "Transparentn�", vbWhite)
    nabTrBTex.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Transparentn� textura", False)
    nabTrBRel.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Transparentn� reli�f", False)
    nabM��ka.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Zarovn�vat na kolekci", True)
    nabVyrovn�vat.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Vyrovn�vat", True)
    nabSyt�St�ny.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Syt� st�ny", False)
    nabZv�raz�ovat.Checked = GetSetting("Minigolf��ek", App.Title & "\Nastaven�", "Zv�raz�ovat", True)
    obrDr�ha(0).Visible = False
    obrDr�ha(0).BackColor = opStupn�.BarvaPodleV��ky(0)
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Height = opRozm�ry.M��ka
        Obd.Width = opRozm�ry.M��ka
    Next Obd
    Dim Img As Control
    For Each Img In imgStart
        Img.Move 9, 9
    Next
    For Each Img In imgM��ek
        Srovnej Img, imgStart(Img.Index)
    Next
    Nam��
    ReDim Mapa(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1)
    ReDim MapaPrvku(31, 31)
    Me.KeyPreview = True
    NamapujM��ek
    Set frmV�b�r.opStupn� = opStupn�
End Sub

Private Sub Form_Resize()
    If WindowState <> vbNormal Then Exit Sub
    opStupn�.Left = 0
    opStupn�.Height = ScaleHeight _
    - opRozm�ry.Height - UXYZ.Height - sbrStav.Height
    opStupn�.Top = ScaleHeight - opStupn�.Height - sbrStav.Height
    opRozm�ry.Move 0, 0
    UXYZ.Move 0, opRozm�ry.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub nabMinigolf��ek_Click()
    On Error GoTo neni_spu�t�nej
    AppActivate "Minigolf��ek"
    Exit Sub
neni_spu�t�nej:
    Shell App.Path & "\Minigolf��ek", vbNormalFocus
End Sub

Private Sub nabEditorPrvk�_Click()
    On Error GoTo neni_spu�t�nej
    AppActivate "Editor ter�nn�ch prvk�"
    Exit Sub
neni_spu�t�nej:
    Shell App.Path & "\EditorPrvk�", vbNormalFocus
End Sub

Private Sub nabM��ka_Click()
    nabM��ka.Checked = Not nabM��ka.Checked
    SaveSetting "Minigolf��ek", App.Title & "\Nastaven�", "Zarovn�vat na kolekci", nabM��ka.Checked
End Sub

Private Sub nabN�pov�da_Click()
    frmN�pov�da.Show
End Sub

Private Sub nabNas�tPrvek_Click()
    Akce = aNas�t
End Sub

Private Sub nabOtev��tStarou_Click()
    DoEvents
    Umo�n�no = False
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Otev��t dr�hu"
    CD.InitDir = App.Path & "\Dr�hy"
    CD.Filter = "Dr�hy minigolfu (*.glf)|*.glf"
    On Error GoTo Vypadni
    CD.ShowOpen
    DoEvents
    On Error GoTo 0
    Stav = "Otev�r�m starou dr�hu �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = Hol�Jm�no(CD.FileName) & " - z�klad.bmp"
    Jm�noSt�n� = Hol�Jm�no(CD.FileName) & ".bmp"
    Dim ���ka As Integer, V��ka As Integer
    Dim MaxBod� As Integer
    Dim Start As XY
    Close
    Open CD.FileName For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    Get #1, , MaxBod�
    ReDim Mapa(���ka - 1, V��ka - 1)
    Get #1, , Mapa()
    ReDim MapaSty�n�chBod�(���ka - 15, V��ka - 15, MaxBod�)
    Get #1, , MapaSty�n�chBod�
    If Not Loc(1) >= LOF(1) Then
        Get #1, , Start
        Start.X = Start.X + 1
        Start.Y = Start.Y + 1
    Else
        Start = CXY(9, 9)
    End If
    Close
    opRozm�ry.���ka = ���ka
    opRozm�ry.V��ka = V��ka
    opRozm�ry_Change
    Dim Img As Control
    For Each Img In imgStart
        Img.Move Start.X, Start.Y
        Srovnej imgM��ek(Img.Index), Img
    Next
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    obrTer�n.Picture = LoadPicture(Jm�noSt�n�)
    Dim X As Long, Y As Long
    For Y = UBound(Mapa, 2) To 1 Step -1
        For X = UBound(Mapa, 1) To 1 Step -1
            If X = 1 And Y = 1 Then
                'nech to tam!
            ElseIf X = 1 Then
                Mapa(X, Y) = Mapa(X, Y - 1)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X, Y - 1)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X, Y - 1)
            ElseIf Y = 1 Then
                Mapa(X, Y) = Mapa(X - 1, Y)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X - 1, Y)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X - 1, Y)
            Else
                Mapa(X, Y) = Mapa(X - 1, Y - 1)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X - 1, Y - 1)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X - 1, Y - 1)
            End If
            
        Next
        DoEvents
    Next
    For Y = UBound(Mapa, 2) To 1 Step -1
        For X = UBound(Mapa, 1) To 1 Step -1
            If X = UBound(Mapa, 1) - 1 And Y = UBound(Mapa, 2) - 1 Then
                Mapa(X, Y) = Mapa(X - 1, Y - 1)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X - 1, Y - 1)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X - 1, Y - 1)
            ElseIf X = UBound(Mapa, 1) - 1 Then
                Mapa(X, Y) = Mapa(X - 1, Y)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X - 1, Y)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X - 1, Y)
            ElseIf Y = UBound(Mapa, 2) - 1 Then
                Mapa(X, Y) = Mapa(X, Y - 1)
                obrDr�ha(1).PSet (X, Y), obrDr�ha(1).Point(X, Y - 1)
                obrTer�n.PSet (X, Y), obrTer�n.Point(X, Y - 1)
            End If
        Next
    Next
    P�idejR�me�ek
    UplatniMapu
    nabUlo�itJenGrafiku.Enabled = True
    Caption = "Editor drah - " & Hol�Jm�no(CD.FileTitle)
Vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = "P�ipraven"
End Sub

Private Sub nabOtev��t_Click()
    DoEvents
    Umo�n�no = False
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Otev��t dr�hu"
    CD.InitDir = App.Path & "\Dr�hy"
    CD.Filter = "Dr�hy minigolfu (*.glf)|*.glf"
    On Error GoTo Vypadni
    CD.ShowOpen
    DoEvents
    On Error GoTo Chyba
    Caption = "Editor drah - " & Hol�Jm�no(CD.FileTitle)
    Stav = "Otev�r�m dr�hu �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = Hol�Jm�no(CD.FileName) & " - z�klad.bmp"
    Jm�noSt�n� = Hol�Jm�no(CD.FileName) & ".bmp"
    Dim ���ka As Integer, V��ka As Integer
    Dim MaxBod� As Integer
    Dim Start As XY
    Close
    Open CD.FileName For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka > 5000 Or V��ka > 5000 _
    Or ���ka < 0 Or V��ka < 0 Then
        Close
        MsgBox "�patn� form�t souboru!", vbCritical, "Chyba"
        GoTo Vypadni
    End If
    Get #1, , MaxBod�
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    ReDim MapaSty�n�chBod�(���ka + 2 - 15, V��ka + 2 - 15, MaxBod�)
    Get #1, , MapaSty�n�chBod�
    If Not Loc(1) >= LOF(1) Then
        Get #1, , Start
    Else
        Start = CXY(8, 8)
    End If
    Close
    opRozm�ry.���ka = ���ka
    opRozm�ry.V��ka = V��ka
    Dim Img As Control
    For Each Img In imgStart
        Img.Move Start.X, Start.Y
        Srovnej imgM��ek(Img.Index), Img
    Next
    UplatniMapu
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    obrTer�n.Picture = LoadPicture(Jm�noSt�n�)
    nabUlo�itJenGrafiku.Enabled = True
Vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = "P�ipraven"
    Exit Sub
Chyba:
    MsgBox "�patn� form�t souboru!", vbCritical, "Chyba"
    GoTo Vypadni
End Sub

Private Sub nabTransparent_Click()
    frmTransparent.Show 1, Me
End Sub

Private Sub nabTrBRel_Click()
    nabTrBRel.Checked = Not nabTrBRel.Checked
    SaveSetting "Minigolf��ek", App.Title & "\Nastaven�", "Transparentn� reli�f", nabTrBRel.Checked
End Sub

Private Sub nabTrBTex_Click()
    nabTrBTex.Checked = Not nabTrBTex.Checked
    SaveSetting "Minigolf��ek", App.Title & "\Nastaven�", "Transparentn� textura", nabTrBTex.Checked
End Sub

Private Sub nabReli�f_Click()
    nabTextura.Checked = False
    nabSt�ny.Checked = False
    nabReli�f.Checked = True
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = True
    obrTer�n.Visible = False
End Sub

Private Sub nabTextura_Click()
    nabTextura.Checked = True
    nabReli�f.Checked = False
    nabSt�ny.Checked = False
    obrDr�ha(1).Visible = True
    obrDr�ha(0).Visible = False
    obrTer�n.Visible = False
End Sub

Private Sub nabSt�ny_Click()
    nabSt�ny.Checked = True
    nabTextura.Checked = False
    nabReli�f.Checked = False
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = False
    obrTer�n.Visible = True
End Sub

Private Sub obrDr�ha_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    P�vAkce = Akce
    If Button = vbRightButton Then Akce = aBo�it
End Sub

Private Sub obrDr�ha_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    obdPoloha(Index).Move _
    X + ((-X + 1) Mod opRozm�ry.M��ka - (obdPoloha(Index).Width) / 2 + opRozm�ry.M��ka / 2) * -nabM��ka.Checked + obdPoloha(Index).Width / 2 * (Not nabM��ka.Checked), _
    Y + ((-Y + 1) Mod opRozm�ry.M��ka - (obdPoloha(Index).Height) / 2 + opRozm�ry.M��ka / 2) * -nabM��ka.Checked + obdPoloha(Index).Height / 2 * (Not nabM��ka.Checked)
    If Akce = aStart Then
        Srovnej imgM��ek(Index), obdPoloha(Index)
    End If
    UXYZ.NastavXYZ X, Y, Mapa(X, Y)
End Sub

Private Sub obrDr�ha_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    If Button = vbRightButton Then Akce = aBo�it
    Stavba Index
    DoEvents
    Akce = P�vAkce
End Sub

Private Sub opRozm�ry_Change()
    Dim BylP�ipravenej As Boolean
    BylP�ipravenej = (Stav = "P�ipraven")
    If BylP�ipravenej Then Stav = "M�n�m rozm�ry dr�hy �"
    Nam��
    O��zniMapu
    UplatniMapu
    If BylP�ipravenej Then Stav = "P�ipraven"
End Sub

Private Property Let Stav(Nosi� As String)
    sbrStav.SimpleText = Nosi�
End Property

Private Property Get Stav() As String
    Stav = sbrStav.SimpleText
End Property

Private Sub Nam��()
    With obrDr�ha(0)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrDr�ha(1)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrSt�ny
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrTer�n
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    Width = Width - ScaleWidth * TPPX + (obrDr�ha(0).Width + opStupn�.Width) * TPPX
    Dim Minim_Y, Je_pod_min_Y
    Minim_Y = obrDr�ha(0).Height - obrDr�ha(0).ScaleHeight + 320 ' jako minim�ln� vej�ka opStupn�
    Je_pod_min_Y = obrDr�ha(0).Height <= Minim_Y
    Height = Height - ScaleHeight * TPPY + (sbrStav.Height + IIf(Je_pod_min_Y, Minim_Y, obrDr�ha(0).Height)) * TPPY
    DoEvents
    Static Pov�ce As Boolean
    If Not Pov�ce Then
        Pov�ce = Not Pov�ce
        Exit Sub
    End If
End Sub

Private Sub O��zniMapu()
    Dim ���ka As Long, V��ka As Long
    ���ka = opRozm�ry.���ka + 2
    V��ka = opRozm�ry.V��ka + 2
    Sma�R�me�ek
    Screen.MousePointer = vbHourglass
'    MousePointer = vbHourglass
    ReDim Nov�Mapa(���ka - 1, V��ka - 1)
    Dim X As Long, Y As Long
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            On Error Resume Next
            Nov�Mapa(X, Y) = Mapa(X, Y)
        Next Y
    Next X
    'DoEvents
'    MousePointer = vbHourglass
    ReDim Mapa(���ka - 1, V��ka - 1)
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            'On Error Resume Next
            Mapa(X, Y) = Nov�Mapa(X, Y)
        Next Y
    Next X
    P�idejR�me�ek
    UplatniMapu
    Screen.MousePointer = vbDefault
End Sub

Public Sub UplatniMapu()
    MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, pamV��ka As Long, pam���ka As Long
    pam���ka = UBound(Mapa, 1)
    pamV��ka = UBound(Mapa, 2)
    For Y = 0 To pamV��ka
        For X = 0 To pam���ka
            ' Stupn� u� to kontrolujou
'            If Mapa(X, Y) = -50 Then
'                obrDr�ha(drReli�f).PSet (X, Y), opStupn�.BarvaC�le
'            Else
                obrDr�ha(drReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                
'            End If
            'If X = 0 Then Refresh
        Next
    Next
    MousePointer = vbDefault
End Sub

Private Sub Stavba(Index As Integer)
    Dim A As TypAkce
    A = Akce
    If A = aNic Then Exit Sub
    If A <> aStart Then MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, Levobok As Long, Vr�ek As Long
    Dim V��ka As Long, ���ka As Long
    With obdPoloha(Index)
        Levobok = .Left
        Vr�ek = .Top
        ���ka = .Width
        V��ka = .Height
    End With
    Select Case A
    Case TypAkce.aBo�it
        On Error Resume Next
        For Y = Vr�ek To Vr�ek + V��ka - 1
            For X = Levobok To Levobok + ���ka - 1
                Mapa(X, Y) = 0 + opStupn�.V��ka * -nabVyrovn�vat.Checked
                ' Stupn� u� to kontrolujou
                obrDr�ha(0).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
            Next X
        Next Y
        With obdPoloha(Index)
            obrDr�ha(1).Line (.Left, .Top)-(.Left + .Width - 1, .Top + .Height - 1), vbWhite, BF
        End With
    Case TypAkce.aPrvek
        If Not nabTrBTex.Checked Then
            obrDr�ha(1).PaintPicture obrPamObr.Image, obdPoloha(Index).Left, obdPoloha(Index).Top
        End If
        On Error Resume Next
        For Y = Vr�ek To Vr�ek + V��ka - 1
            For X = Levobok To Levobok + ���ka - 1
                If Not (nabTrBRel.Checked And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) Then
                    ' Pokud je tam jamka,
                    If MapaPrvku(X - Levobok, Y - Vr�ek) = -50 Then
                        ' nech tam tu jamku!
                        Mapa(X, Y) = -50
                        obrDr�ha(0).PSet (X, Y), opStupn�.BarvaC�le
                    ' Pokud tam neni jamka,
                    Else
                        ' p�idej vej�ku k z�kladu.
                        Mapa(X, Y) = MapaPrvku(X - Levobok, Y - Vr�ek) + opStupn�.V��ka
                        obrDr�ha(0).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
                    End If
                End If
                If Not (nabTrBTex.Checked And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) Then
                    obrDr�ha(1).PSet (X, Y), obrPamObr.Point(X - Levobok, Y - Vr�ek)
                End If
            Next X
        Next Y
        On Error GoTo 0
    Case TypAkce.aObr�zek
        If Not nabTrBTex.Checked Then
            obrDr�ha(1).PaintPicture obrPamObr.Image, obdPoloha(Index).Left, obdPoloha(Index).Top
        Else
            On Error Resume Next
            For Y = Vr�ek To Vr�ek + V��ka - 1
                For X = Levobok To Levobok + ���ka - 1
                    If Not (nabTrBTex.Checked And obrPamObr.Point(X - Levobok, Y - Vr�ek) = TrB) Then
                        obrDr�ha(1).PSet (X, Y), obrPamObr.Point(X - Levobok, Y - Vr�ek)
                    End If
                Next X
            Next Y
        End If
        On Error GoTo 0
    Case TypAkce.aStart
        Dim Start�k As Control
        For Each Start�k In imgStart
            Srovnej Start�k, obdPoloha(Index)
            Start�k.Visible = True
        Next
    Case TypAkce.aNas�t
        With obdPoloha(Index)
            obrPamObr.Move .Left, .Top, .Width, .Height
            obrPamObr.PaintPicture obrDr�ha(drTexura).Image, 0, 0, , , .Left, .Top
        End With
        On Error Resume Next
        For Y = Vr�ek To Vr�ek + V��ka - 1
            For X = Levobok To Levobok + ���ka - 1
                MapaPrvku(X - Levobok, Y - Vr�ek) = Mapa(X, Y)
            Next X
        Next Y
        On Error GoTo 0
    Case Else
        'nic
    End Select
    If A <> aStart Then P�idejR�me�ek
    MousePointer = vbDefault
End Sub

Private Function JeVM��ku(X, Y) As Boolean
    ' Pokud sou�et �tverc� nad ob�ma odv�snami (X a Y)
    ' je men�� ne� �tverec nad p�eponou (polom�rem)
    ' nebo shodn�,
    ' pak je bod (X, Y) uvnit� kruhu.
    ' A p�id�me pro jistotu odrazn� body.
    JeVM��ku = ((X - 7) ^ 2 + (Y - 7) ^ 2 <= 7 ^ 2) _
    Or obrVzorOkraje.Point(X, Y) = vbBlack
    
End Function

Private Sub NamapujM��ek()
    Dim X As Long, Y As Long
    For X = 0 To 14
        For Y = 0 To 14
            If JeVM��ku(X, Y) Then
                MapaM��ku(X, Y) = Abs((7 ^ 2 - ((X - 7) ^ 2 + (Y - 7) ^ 2))) ^ (1 / 2)
            Else
                MapaM��ku(X, Y) = MimoM��ek
            End If
        Next Y
    Next X
End Sub

Private Sub NamapujSty�n�Body()
    Dim X As Long, Y As Long
    Dim pam���ka As Long, pamV��ka As Long, i As Long
    Umo�n�no = False
    MousePointer = vbHourglass
    obrDr�ha(0).Visible = False
    obrDr�ha(1).Visible = False
    obrTer�n.Visible = False
    obrSt�ny.Visible = True
    obrSt�ny.MousePointer = vbHourglass
    obrSt�ny.DrawMode = vbCopyPen
    obrSt�ny.Cls
    DoEvents
    ReDim MapaSty�n�chBod�(UBound(Mapa, 1) - 14, UBound(Mapa, 2) - 14, 0)
    Stav = "Po��t�m sty�n� body � (Stop: Escape)"
    MousePointer = vbHourglass
    pam���ka = UBound(Mapa, 1) - 14
    pamV��ka = UBound(Mapa, 2) - 14
    For Y = 0 To pamV��ka
        For X = 0 To pam���ka
            SadaSty�n�chBod� X, Y
            'obrSt�ny.PSet (X + 7, Y + 7)
            'On Error Resume Next
            Dim R As Long, G As Long, B As Long, Prost�edn�Bod As Long
            G = MapaSty�n�chBod�(X, Y, 0).X * 15
            B = MapaSty�n�chBod�(X, Y, 0).Y * 15
            'On Error Resume Next
            R = (MapaSty�n�chBod�(X, Y, 1).X + MapaSty�n�chBod�(X, Y, 1).Y) * 7.5
            obrSt�ny.PSet (X + 7, Y + 7), RGB(R, G, B)
            'On Error GoTo 0
            If (X + 1) Mod 64 = 0 Then DoEvents
            If P�eru�itProces Then
                Exit Sub
            End If
        Next
        DoEvents
        'obrTer�n.Line (X + 7, 7)-(X + 7, UBound(Mapa, 2) - 14 + 1 + 7)
    Next
End Sub


Private Sub Vyst�nuj()
    If P�eru�itProces Then
        Exit Sub
    End If
    Dim X, Y, St�nX, St�nY, V��kaSt�nu
    obrSt�ny.Cls
    obrSt�ny.BackColor = vbWhite
    Stav = "Kresl�m st�ny � (Stop: Escape)"
    obrDr�ha(0).Visible = False
    obrDr�ha(1).Visible = False
    obrTer�n.Visible = False
    obrSt�ny.Visible = True
    DoEvents
    obrSt�ny.MousePointer = vbHourglass
    For Y = 0 To obrTer�n.ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 0 To obrTer�n.ScaleWidth - 2
            If X = 0 Or Y = 0 Then
                V��kaSt�nu = opRozm�ry.M��ka
            Else
                V��kaSt�nu = Mapa(X, Y) + (Not nabSyt�St�ny.Checked) * 2
            End If
            If X + 2 < UBound(Mapa, 1) Then
                If Mapa(X + 1, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 2, Y + 1) < Mapa(X, Y) Then
                    St�nX = X + 2
                    St�nY = Y + 1
                Else
                    St�nX = X + 1
                    St�nY = Y + 1
                End If
            Else
                St�nX = X + 1
                St�nY = Y + 1
            End If
            Do Until Mapa(Int(St�nX), Int(St�nY)) >= V��kaSt�nu
                'Stop
                If obrSt�ny.Point(St�nX, St�nY) _
                <> �ed� Then
                    Debug.Assert obrSt�ny.Point(St�nX, St�nY) = &HFFFFFF
                    'obrSt�ny.PSet (St�nX, St�nY), �ed�
                End If
                obrSt�ny.Line (St�nX, St�nY)-(St�nX + 1, St�nY + 1.5), �ed�
                'obrSt�ny.PSet (St�nX, St�nY), �ed�
                St�nX = St�nX + 1
                St�nY = St�nY + 1 / 2
                V��kaSt�nu = V��kaSt�nu - 2
                'Debug.Assert Not St�nX = 320
                If St�nX >= obrTer�n.ScaleWidth - 1 _
                Or St�nY >= obrTer�n.ScaleHeight - 1 _
                Then Exit Do
            Loop
            'To je pomal�!
            'obrSt�ny.Line (X + 1, Y + 1)-(St�nX, St�nY), �ed�
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
    Next Y
    Stav = "Retu�uji st�ny � (Stop: Escape)"
    For Y = 1 To obrDr�ha(1).ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 1 To obrDr�ha(1).ScaleWidth - 2
            ' Retu�e p�esahuj�c�ch st�n�
            If Mapa(X, Y - 1) < Mapa(X, Y) - 1 _
            And obrSt�ny.Point(X, Y) <> vbWhite _
            And (obrSt�ny.Point(X, Y + 1) = vbWhite _
             Or (obrSt�ny.Point(X - 1, Y - 1) = vbWhite _
              And Mapa(X, Y) = Mapa(X - 1, Y - 1) _
             And Mapa(X, Y) > Mapa(X + 1, Y + 1))) _
            And Y < UBound(Mapa, 2) Then
                'Debug.Assert False
                obrSt�ny.PSet (X, Y), vbWhite
            End If
            ' Retu�e mezer mezi rohy
            If obrSt�ny.Point(X + 1, Y) <> vbWhite _
            And obrSt�ny.Point(X, Y + 1) <> vbWhite Then
                If Mapa(X, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 1, Y) = Mapa(X, Y) Then
                    obrSt�ny.PSet (X, Y), �ed�
                End If
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
    Next Y
    ' Vyb�lit r�me�ek
    obrSt�ny.Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    Refresh
    Stav = "Aplikuji st�ny � (Stop: Escape)"
    obrTer�n.Picture = obrDr�ha(1).Image
    obrSt�ny.Visible = False
    obrTer�n.Visible = True
    For Y = 1 To obrTer�n.ScaleHeight - 1
        obrTer�n.MousePointer = vbHourglass
        For X = 1 To obrTer�n.ScaleWidth - 1
            If obrSt�ny.Point(X, Y) <> vbWhite Then
                obrTer�n.PSet (X, Y), _
                RGB(�erven�(obrTer�n.Point(X, Y)) / 2, _
                    Zelen�(obrTer�n.Point(X, Y)) / 2, _
                    Modr�(obrTer�n.Point(X, Y)) / 2)
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
    Next Y
p�eru�eno:
'    obrTer�n.DrawMode = DrawModeConstants.vbMergePen
'    obrTer�n.PaintPicture obrSt�ny.Image, 0, 0 _
'    , , , , , , vbSrcPaint Or vbMergePaint
    'obrSt�ny.Visible = False
    'obrTer�n.MousePointer = vbCustom
    'MousePointer = vbDefault
    Stav = "P�ipraven"
End Sub

Private Sub Osv�tli()
    If P�eru�itProces Then
        P�eru�itProces = False
        GoTo p�eru�eno
    End If
    Dim X, Y
    Stav = "St�nuji �ikm� plochy a hrany � (Stop: Escape)"
    obrTer�n.Visible = True
    obrSt�ny.Visible = False
    'DoEvents
    For Y = 1 To obrTer�n.ScaleHeight - 1
        obrTer�n.MousePointer = vbHourglass
        For X = 1 To obrTer�n.ScaleWidth - 1
'            If Mapa(X - 1, Y - 1) < Mapa(X, Y) - 2 _
'            And obrSt�ny.Point(X, Y) = vbWhite Then
'                obrTer�n.PSet (X, Y), _
'                RGB(�erven�(obrTer�n.Point(X, Y)) + 30, _
'                    Zelen�(obrTer�n.Point(X, Y)) + 30, _
'                    Modr�(obrTer�n.Point(X, Y)) + 30)
'            End If
            If obrSt�ny.Point(X, Y) = vbWhite _
            And nabZv�raz�ovat.Checked Then
                Dim R As Long, G As Long, B As Long
                Dim P�id�k As Long, PX, PY
'                If Mapa(X - 1, Y) <> Mapa(X, Y) _
'                And Mapa(X, Y - 1) <> Mapa(X, Y) Then
                    If X = 1 Then
                        PX = 32
                    Else
                        PX = Mapa(X - 1, Y)
                    End If
                    If Y = 1 Then
                        PY = 32
                    Else
                        PY = Mapa(X, Y - 1)
                    End If
                    P�id�k = (Mapa(X, Y) - PX) * 32 _
                    + (Mapa(X, Y) - PY) * 16
'                Else
'                    P�id�k = (Mapa(X, Y) - Mapa(X - 1, Y - 1)) * 32
'                End If
                Const MaxP�id = 52
                If P�id�k > MaxP�id Then P�id�k = MaxP�id
                If P�id�k < -MaxP�id Then P�id�k = -MaxP�id
                R = �erven�(obrTer�n.Point(X, Y)) + P�id�k
                G = Zelen�(obrTer�n.Point(X, Y)) + P�id�k
                B = Modr�(obrTer�n.Point(X, Y)) + P�id�k
                If R < 0 Then R = 0
                If G < 0 Then G = 0
                If B < 0 Then B = 0
                On Error Resume Next
                obrTer�n.PSet (X, Y), RGB(R, G, B)
                On Error GoTo 0
            End If
        Next X
        'obrSt�ny.Refresh
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            P�eru�itProces = False
            GoTo p�eru�eno
        End If
    Next Y
p�eru�eno:
'    obrTer�n.PaintPicture obrSt�ny.Image, 0, 0, _
'    , , , , , , RasterOpConstants.vbMergePaint
    'obrSt�ny.Visible = False
    obrTer�n.MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = "P�ipraven"
    Umo�n�no = True
End Sub

Private Sub Uka�Obr�zekPodleNab�dky()
    obrSt�ny.Visible = False
    obrTer�n.Visible = nabSt�ny.Checked
    obrDr�ha(0).Visible = nabReli�f.Checked
    obrDr�ha(1).Visible = nabTextura.Checked
End Sub

Private Property Let Akce(Nosi� As TypAkce)
    Dim i As Object
    nabBo�it.Checked = (Nosi� = aBo�it)
    nabVtisknoutPrvek.Checked = (Nosi� = aPrvek)
    nabObr�zek.Checked = (Nosi� = aObr�zek)
    nabStart.Checked = (Nosi� = aStart)
    nabNas�tPrvek.Checked = (Nosi� = aNas�t)
    For Each i In imgM��ek
        i.Visible = (Nosi� = aStart)
    Next
    For Each i In imgStart
        i.Visible = (Nosi� = aStart)
    Next
    Dim Barva As Long
    Select Case Nosi�
    Case aBo�it
        Barva = vbRed
    Case aObr�zek
        Barva = vbMagenta
    Case aPrvek
        Barva = vbBlue
    Case aStart
        Barva = RGB(0, 200, 100)
        For Each i In obdPoloha
            Srovnej imgM��ek(i.Index), i
        Next
    Case Else
        Barva = �ed�
    End Select
    For Each i In obdPoloha
        i.BorderColor = Barva
        i.Visible = Not (Nosi� = aStart)
    Next
End Property

Private Property Get Akce() As TypAkce
    If nabBo�it.Checked Then
        Akce = aBo�it
    ElseIf nabObr�zek.Checked Then
        Akce = aObr�zek
    ElseIf nabVtisknoutPrvek.Checked Then
        Akce = aPrvek
    ElseIf nabStart.Checked Then
        Akce = aStart
    ElseIf nabNas�tPrvek.Checked Then
        Akce = aNas�t
    Else
        Akce = aNic
    End If
End Property

Private Function Hol�Jm�no(Soub As String) As String
    If Soub = "" Then Exit Function
    If Mid(Soub, Len(Soub) - 3, 1) = "." Then
        Hol�Jm�no = Left(Soub, Len(Soub) - 4)
    Else
        Hol�Jm�no = Soub
    End If
End Function

Private Sub P�idejR�me�ek()
    Dim i
    Dim V��kaNebety�n� 'takov� vej�ka, aby se vod n�
                       'ur�it� vodrazil
    'Tahle d�l� moc dlouhej st�n (ale d� se zkr�tit):
    V��kaNebety�n� = 1000
    'Tak rad�i tuhle (rad�i ne, proto�e by se vod n�
                     'nejsp� nevodrazil):
    'V��kaNebety�n� = 64
    MousePointer = vbHourglass
    Refresh
    For i = 0 To opRozm�ry.���ka + 2 - 1
        Mapa(i, 0) = V��kaNebety�n�
        Mapa(i, opRozm�ry.V��ka + 2 - 1) = V��kaNebety�n�
    Next
    For i = 0 To opRozm�ry.V��ka + 2 - 1
        Mapa(0, i) = V��kaNebety�n�
        Mapa(opRozm�ry.���ka + 2 - 1, i) = V��kaNebety�n�
    Next
    'Vyzna�it okraj na reli�fu
    obrDr�ha(0).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    'Vyzna�it okraj i na textu�e (nevim, jak by to jin�� �lo)
    '                            (kv�li st�n�m a map�)
    obrDr�ha(1).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    DoEvents
    MousePointer = vbDefault
End Sub

Private Sub Sma�R�me�ek()
    Dim i, ���ka, V��ka
    ���ka = UBound(Mapa, 1)
    V��ka = UBound(Mapa, 2)
    MousePointer = vbHourglass
    Refresh
    For i = 0 To ���ka
        Mapa(i, V��ka) = 0
    Next
    For i = 0 To V��ka
        Mapa(���ka, i) = 0
    Next
    obrDr�ha(0).Line (0, 0)-(���ka, V��ka), opStupn�.BarvaPodleV��ky(0), B
    obrDr�ha(1).Line (0, 0)-(���ka, V��ka), vbWhite, B
    DoEvents
    MousePointer = vbDefault
End Sub

Private Property Let Umo�n�no(Nosi� As Boolean)
    Dim Kontrolka As Control
    For Each Kontrolka In Me.Controls
        Select Case Kontrolka.Name
        Case "CD", "CDObr", "obdPoloha", "nabUlo�itJenGrafiku", _
        "nabSep0", "nabSep1", "nabSep2", _
        "nabSep3", "nabSep4", "nabSep5", "nabSep6", "nabSep7", _
        "obrTer�n", "obrSt�ny", "UXYZ", _
        "imgM��ek", "imgStart"
            'nic
        Case Else
            Kontrolka.Enabled = Nosi�
        End Select
    Next
End Property

Private Property Get Umo�n�no() As Boolean
    Umo�n�no = nabSoubor.Enabled
End Property
