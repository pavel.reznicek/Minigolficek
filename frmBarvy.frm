VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H008080FF&
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ForeColor       =   &H00004080&
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Click()
    Dim LeftColor, MidColor, Msg, RightColor    ' Declare variables.
    AutoRedraw = -1 ' Turn on AutoRedraw.
    Height = 3 * 1440   ' Set height to 3 inches.
    Width = 5 * 1440    ' Set width to 5 inches.
    BackColor = RGB(0, 0, &HFF) ' Set background to blue.
    ForeColor = RGB(&HFF, 0, 0) ' Set foreground to red.
    Line (0, 0)-(Width / 3, Height), , BF   ' Red box.
    ForeColor = RGB(&HFF, &HFF, &HFF) ' Set foreground to white.
    Line (Width / 3, 0)-((Width / 3) * 2, Height), , BF
    LeftColor = Point(0, 0) ' Find color of left box,
    MidColor = Point(Width / 2, Height / 2) ' middle box, and
    RightColor = Point(Width, Height)   ' right box.
    Msg = "The color number for the red box on the left side of "
    Msg = Msg & "the form is " & Hex(LeftColor) & ". The "
    Msg = Msg & "color of the white box in the center is "
    Msg = Msg & Hex(MidColor) & ". The color of the blue "
    Msg = Msg & "box on the right is " & Hex(RightColor) & "."
    MsgBox Msg  ' Display message.
End Sub

