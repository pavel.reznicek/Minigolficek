VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmNast 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nastaven� Minigolf��ku"
   ClientHeight    =   4935
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   6135
   Icon            =   "frmNast.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   329
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   409
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab tbsOptions 
      Height          =   4215
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   7435
      _Version        =   393216
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Hra"
      TabPicture(0)   =   "frmNast.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "picOptions(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Editor drah"
      TabPicture(1)   =   "frmNast.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picOptions(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Barvy"
      TabPicture(2)   =   "frmNast.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "picOptions(2)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&St�nov�n�"
      TabPicture(3)   =   "frmNast.frx":0060
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "Frame1"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "fraBarvySt�n�"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).ControlCount=   2
      Begin VB.Frame fraBarvySt�n� 
         Caption         =   "Barvy st�n�"
         Height          =   2415
         Left            =   3120
         TabIndex        =   59
         Top             =   480
         Width           =   2535
         Begin VB.CommandButton tlBarvaOsv 
            Height          =   375
            Left            =   1920
            Style           =   1  'Graphical
            TabIndex        =   63
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton tlBarvaSS 
            Height          =   375
            Left            =   1920
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   360
            Width           =   375
         End
         Begin VB.Label lblBarvaOsv 
            Caption         =   "Barva osv�tlen�:"
            Height          =   255
            Left            =   240
            TabIndex        =   61
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label lblBarvaSS 
            Caption         =   "Barva syt�ch st�n�:"
            Height          =   255
            Left            =   240
            TabIndex        =   60
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Obecn�"
         Height          =   2415
         Left            =   240
         TabIndex        =   54
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chbObtahovat 
            Caption         =   "Obtahovat osv�tlen� hrany"
            Height          =   375
            Left            =   240
            TabIndex        =   58
            Top             =   1800
            Width           =   1935
         End
         Begin VB.CheckBox chbSyt�St�ny 
            Caption         =   "Syt� st�ny na �ikm�ch ploch�ch"
            Height          =   375
            Left            =   240
            TabIndex        =   57
            Top             =   360
            Width           =   1935
         End
         Begin VB.CheckBox chbZv�r�ik 
            Caption         =   "Zv�raz�ovat �ikm� plochy a hrany"
            Height          =   375
            Left            =   240
            TabIndex        =   56
            Top             =   840
            Width           =   2055
         End
         Begin VB.CheckBox chbZv�rSyt 
            Caption         =   "Zv�raz�ovat pod syt�mi st�ny"
            Height          =   375
            Left            =   240
            TabIndex        =   55
            Top             =   1320
            Width           =   1935
         End
      End
      Begin VB.PictureBox picOptions 
         BorderStyle     =   0  'None
         Height          =   3780
         Index           =   1
         Left            =   -74880
         ScaleHeight     =   3780
         ScaleWidth      =   5685
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   380
         Width           =   5685
         Begin VB.Frame fraM��ka 
            Caption         =   "M��ka (zvl᚝)"
            Height          =   1575
            Index           =   0
            Left            =   120
            TabIndex        =   49
            Top             =   120
            Width           =   2415
            Begin VB.CheckBox chbZarovn�vat 
               Caption         =   "Zarovn�vat ke m��ce"
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   52
               Top             =   960
               Width           =   1935
            End
            Begin VB.CheckBox chbSt�edit 
               Caption         =   "St�edit do m��ky"
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   51
               Top             =   1200
               Width           =   1935
            End
            Begin Minigolf��ek.�oup� �ou�M� 
               Height          =   285
               Index           =   0
               Left            =   240
               TabIndex        =   50
               Top             =   240
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   503
               Max             =   128
               ���kaTxt        =   36
               N�pis           =   "���ka:"
            End
            Begin Minigolf��ek.�oup� �ouDM� 
               Height          =   285
               Index           =   0
               Left            =   240
               TabIndex        =   53
               Top             =   600
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   503
               Max             =   128
               ���kaTxt        =   36
               N�pis           =   "D�lka:"
            End
         End
         Begin VB.Frame fraStavPov 
            Caption         =   "Stavebn� povolen� (spole�n�)"
            Height          =   1455
            Left            =   120
            TabIndex        =   46
            Top             =   1800
            Width           =   2415
            Begin VB.CheckBox chbStavPovGra 
               Caption         =   "na grafiku"
               Height          =   255
               Left            =   240
               TabIndex        =   48
               Top             =   840
               Width           =   1815
            End
            Begin VB.CheckBox chbStavPovRel 
               Caption         =   "na reli�f"
               Height          =   255
               Left            =   240
               TabIndex        =   47
               Top             =   1080
               Width           =   1695
            End
         End
         Begin VB.Frame fraTrB 
            Caption         =   "Transparentn� barva (editor)"
            Height          =   1455
            Left            =   2640
            TabIndex        =   41
            Top             =   1800
            Width           =   2895
            Begin VB.CheckBox chbTrBGra 
               Caption         =   "na grafiku"
               Height          =   255
               Left            =   240
               TabIndex        =   44
               Top             =   840
               Width           =   2175
            End
            Begin VB.CheckBox chbTrBRel 
               Caption         =   "na reli�f"
               Height          =   255
               Left            =   240
               TabIndex        =   43
               Top             =   1080
               Width           =   2175
            End
            Begin VB.CommandButton tlTrB 
               Height          =   255
               Left            =   240
               Style           =   1  'Graphical
               TabIndex        =   42
               Top             =   240
               Width           =   2415
            End
            Begin VB.Label lblTrB 
               Caption         =   "Pou��vat transparentn� barvu:"
               Height          =   255
               Left            =   240
               TabIndex        =   45
               Top             =   600
               Width           =   2175
            End
         End
         Begin VB.CheckBox chbVyrovn�vat 
            Caption         =   "P�i bo�en� vyrovn�vat se z�kladem (editor)"
            Height          =   375
            Left            =   2880
            TabIndex        =   40
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Frame Frame2 
            Caption         =   "Tyto volby plat� pro:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   238
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   2640
            TabIndex        =   37
            Top             =   120
            Width           =   2895
            Begin VB.OptionButton opEd�iSb 
               Caption         =   "Editor drah"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   39
               Top             =   240
               Value           =   -1  'True
               Width           =   1215
            End
            Begin VB.OptionButton opEd�iSb 
               Caption         =   "Sb�rku prvk�"
               Height          =   255
               Index           =   1
               Left            =   1440
               TabIndex        =   38
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame fraM��ka 
            Caption         =   "M��ka (zvl᚝)"
            Height          =   1575
            Index           =   1
            Left            =   0
            TabIndex        =   32
            Top             =   0
            Width           =   2415
            Begin VB.CheckBox chbSt�edit 
               Caption         =   "St�edit do m��ky"
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   34
               Top             =   1200
               Width           =   1935
            End
            Begin VB.CheckBox chbZarovn�vat 
               Caption         =   "Zarovn�vat ke m��ce"
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   33
               Top             =   960
               Width           =   1935
            End
            Begin Minigolf��ek.�oup� �ou�M� 
               Height          =   285
               Index           =   1
               Left            =   240
               TabIndex        =   35
               Top             =   240
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   503
               Max             =   128
               ���kaTxt        =   36
               N�pis           =   "���ka:"
            End
            Begin Minigolf��ek.�oup� �ouDM� 
               Height          =   285
               Index           =   1
               Left            =   240
               TabIndex        =   36
               Top             =   600
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   503
               Max             =   128
               ���kaTxt        =   36
               N�pis           =   "D�lka:"
            End
         End
      End
      Begin VB.PictureBox picOptions 
         BorderStyle     =   0  'None
         Height          =   3780
         Index           =   2
         Left            =   -74880
         ScaleHeight     =   3780
         ScaleWidth      =   5685
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   380
         Width           =   5685
         Begin VB.Frame fraBarvyED 
            Caption         =   "Barvy v editoru drah"
            Height          =   3465
            Left            =   120
            TabIndex        =   26
            Top             =   120
            Width           =   2535
            Begin VB.ListBox lstBED 
               Height          =   2205
               Left            =   240
               TabIndex        =   29
               ToolTipText     =   "Seznam barev v editoru drah"
               Top             =   840
               Width           =   1575
            End
            Begin VB.CommandButton tlBED 
               Height          =   2175
               Left            =   1920
               Style           =   1  'Graphical
               TabIndex        =   28
               ToolTipText     =   "Vybran� barva"
               Top             =   840
               Width           =   375
            End
            Begin VB.CommandButton tlNaplBED 
               Caption         =   "Napl�"
               Height          =   285
               Left            =   1680
               TabIndex        =   27
               ToolTipText     =   "Napln� seznam barvami v zadan�m po�tu"
               Top             =   360
               Width           =   615
            End
            Begin Minigolf��ek.�oup� �ouBED 
               Height          =   285
               Left            =   240
               TabIndex        =   30
               Top             =   360
               Width           =   1335
               _ExtentX        =   2355
               _ExtentY        =   503
               Max             =   64
               ���kaTxt        =   36
               N�pis           =   "Po�et:"
            End
         End
         Begin VB.Frame fraBarvyEP 
            Caption         =   "Barvy v editoru prvk�"
            Height          =   3465
            Left            =   3000
            TabIndex        =   21
            Top             =   120
            Width           =   2535
            Begin VB.ListBox lstBEP 
               Height          =   2205
               Left            =   240
               TabIndex        =   24
               ToolTipText     =   "Seznam barev v editoru prvk�"
               Top             =   840
               Width           =   1575
            End
            Begin VB.CommandButton tlBEP 
               Height          =   2175
               Left            =   1920
               Style           =   1  'Graphical
               TabIndex        =   23
               ToolTipText     =   "Vybran� barva"
               Top             =   840
               Width           =   375
            End
            Begin VB.CommandButton tlNaplBEP 
               Caption         =   "Napl�"
               Height          =   285
               Left            =   1680
               TabIndex        =   22
               ToolTipText     =   "Napln� seznam barvami v zadan�m po�tu"
               Top             =   360
               Width           =   615
            End
            Begin Minigolf��ek.�oup� �ouBEP 
               Height          =   285
               Left            =   240
               TabIndex        =   25
               Top             =   360
               Width           =   1335
               _ExtentX        =   2355
               _ExtentY        =   503
               Max             =   64
               ���kaTxt        =   36
               N�pis           =   "Po�et:"
            End
         End
      End
      Begin VB.PictureBox picOptions 
         BorderStyle     =   0  'None
         Height          =   3780
         Index           =   0
         Left            =   -74880
         ScaleHeight     =   3780
         ScaleWidth      =   5685
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   380
         Width           =   5685
         Begin VB.Frame fra�ipka 
            Caption         =   "�ipka"
            Height          =   1935
            Left            =   2880
            TabIndex        =   14
            Top             =   120
            Width           =   2655
            Begin VB.CommandButton tlBarvaH� 
               Height          =   375
               Left            =   2040
               Style           =   1  'Graphical
               TabIndex        =   16
               Top             =   1320
               Width           =   375
            End
            Begin VB.CommandButton tlBarvaT� 
               Height          =   375
               Left            =   2040
               Style           =   1  'Graphical
               TabIndex        =   15
               Top             =   840
               Width           =   375
            End
            Begin Minigolf��ek.�oup� �ou�� 
               Height          =   285
               Left            =   240
               TabIndex        =   17
               Top             =   360
               Width           =   2175
               _ExtentX        =   3836
               _ExtentY        =   503
               Max             =   32
               ���kaTxt        =   36
               N�pis           =   "���ka:"
            End
            Begin VB.Label lblBH� 
               Caption         =   "Barva hlavi�ky:"
               Height          =   255
               Left            =   240
               TabIndex        =   19
               Top             =   1380
               Width           =   1455
            End
            Begin VB.Label lblBT� 
               Caption         =   "Barva t�la:"
               Height          =   255
               Left            =   240
               TabIndex        =   18
               Top             =   900
               Width           =   1455
            End
         End
         Begin VB.Frame fraM��itJako 
            Caption         =   "M��it jako �"
            Height          =   1215
            Left            =   120
            TabIndex        =   11
            Top             =   120
            Width           =   2535
            Begin VB.OptionButton opM��itJako 
               Caption         =   "� hol�"
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   13
               Top             =   360
               Value           =   -1  'True
               Width           =   1575
            End
            Begin VB.OptionButton opM��itJako 
               Caption         =   "� prakem"
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   12
               Top             =   720
               Width           =   1575
            End
         End
         Begin VB.CheckBox chbZvuk 
            Caption         =   "Zvukov� efekty"
            Height          =   255
            Left            =   3000
            TabIndex        =   10
            Top             =   2880
            Width           =   2655
         End
         Begin VB.CheckBox chbUkotvit 
            Caption         =   "Ukotvit m��ek p�i startu"
            Height          =   255
            Left            =   3000
            TabIndex        =   9
            Top             =   2400
            Width           =   2535
         End
         Begin VB.Frame fraZpKr 
            Caption         =   "Zp�sob kreslen�"
            Height          =   1815
            Left            =   120
            TabIndex        =   5
            Top             =   1680
            Width           =   2535
            Begin VB.OptionButton opZpKres 
               Caption         =   "smy�kou"
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   8
               Top             =   360
               Value           =   -1  'True
               Width           =   1575
            End
            Begin VB.OptionButton opZpKres 
               Caption         =   "�asova�em"
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   7
               Top             =   1320
               Width           =   1575
            End
            Begin Minigolf��ek.�oup� �ouRychlost 
               Height          =   285
               Left            =   240
               TabIndex        =   6
               Top             =   720
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   503
               Max             =   32
               ���kaTxt        =   36
               N�pis           =   "Rychlost smy�ky:"
            End
         End
      End
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   120
      Top             =   4440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Uplatnit"
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   4455
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Pry�!"
      Height          =   375
      Left            =   3720
      TabIndex        =   1
      Top             =   4455
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2490
      TabIndex        =   0
      Top             =   4455
      Width           =   1095
   End
End
Attribute VB_Name = "frmNast"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Na�tiHodnoty()
    ObnovHodZRegistru
    'na��st stavy vlastnost�:
    
    'hern� nastaven�
    chbZvuk = -Zvuk
    chbUkotvit = -Ukotvit
    �ou�� = ���ka�ipky
    �ouRychlost = RychlostZobrazen�
    opM��itJako(1) = M��itPrakem
    tlBarvaH�.BackColor = BarvaH�
    tlBarvaT�.BackColor = BarvaT�
    opZpKres(1) = �asova�
    'editor a sb�rka
    'opEd�iSb(0) = Not ZdaSb�rka
    Na�tiNastED
    chbSyt�St�ny = -Syt�St�ny
    chbZv�r�ik = -Zv�raz�ovat
    chbZv�rSyt = -ZvPodSSt
    chbStavPovGra.Enabled = Povolen�Stavebn�hoPovolen�
    chbStavPovRel.Enabled = Povolen�Stavebn�hoPovolen�
    tlTrB.BackColor = TrB
    chbTrBGra = -TrBGra
    chbTrBRel = -TrBRel
    chbStavPovGra = -Stavebn�Povolen�(dzTextura)
    chbStavPovRel = -Stavebn�Povolen�(dzReli�f)
    �ouBED = frmEditorDrah.opStupn�.Po�etNadzBarev
    �ouBEP = frmEditorPrvk�.opStupn�.Po�etNadzBarev
    Napl�BED
    Napl�BEP
    tlBarvaSS.BackColor = BarvaSS
    tlBarvaOsv.BackColor = BarvaOs
End Sub

Private Sub Napl�BED()
    Dim I As Integer, txtI As String
    lstBED.Clear
    For I = frmEditorDrah.opStupn�.Po�etNadzBarev To -1 Step -1
        Select Case I
        Case -1
            txtI = "c�le"
        Case 0
            txtI = "podzem�"
        Case Else
            txtI = CStr(I)
        End Select
        lstBED.AddItem "Barva " & txtI
    Next
    lstBED.ListIndex = 0
End Sub

Private Sub Napl�BEP()
    Dim I As Integer, txtI As String
    lstBEP.Clear
    For I = frmEditorPrvk�.opStupn�.Po�etNadzBarev To -1 Step -1
        Select Case I
        Case -1
            txtI = "c�le"
        Case 0
            txtI = "podzem�"
        Case Else
            txtI = CStr(I)
        End Select
        lstBEP.AddItem "Barva " & txtI
    Next
    lstBEP.ListIndex = 0
End Sub

Private Sub Zapi�Hodnoty()
    'Hern� nastaven�
    Zvuk = chbZvuk
    Ukotvit = chbUkotvit
    ���ka�ipky = �ou��
    �ou�� = �ou��
    RychlostZobrazen� = �ouRychlost
    �ouRychlost = �ouRychlost
    M��itPrakem = opM��itJako(1)
    BarvaH� = tlBarvaH�.BackColor
    BarvaT� = tlBarvaT�.BackColor
    �asova� = opZpKres(1)
    'Editor drah a sb�rka t. p.
    If Not ZdaSb�rka Then
        Zapi�NastED
    Else
        Zapi�NastSTP
    End If
    Syt�St�ny = -chbSyt�St�ny
    Zv�raz�ovat = -chbZv�r�ik
    ZvPodSSt = -chbZv�rSyt
    ObtHrany = -chbObtahovat
    'TrB = tlTrB.BackColor 'zbyte�n�; u� tam je.
    TrBGra = -chbTrBGra
    TrBRel = -chbTrBRel
    Stavebn�Povolen�(dzTextura) = -chbStavPovGra
    Stavebn�Povolen�(dzReli�f) = -chbStavPovRel
    �ouBED = �ouBED
    �ouBEP = �ouBEP
    tlNaplBED_Click
    tlNaplBEP_Click
    BarvaSS = tlBarvaSS.BackColor
    BarvaOs = tlBarvaOsv.BackColor
End Sub

Private Sub Na�tiNastED()
    �ou�M�(0).Hodnota = frmEditorDrah.opRozm�ry.M��kaX
    �ouDM�(0).Hodnota = frmEditorDrah.opRozm�ry.M��kaY
    chbSt�edit(0) = -frmEditorDrah.St�edit
    chbZarovn�vat(0) = -frmEditorDrah.Zarovn�vat
    fraM��ka(0).Visible = True
    fraM��ka(1).Visible = False
    fraTrB.Visible = True
    chbVyrovn�vat.Visible = True
End Sub

Private Sub Zapi�NastED()
    frmEditorDrah.opRozm�ry.NastavM��ku �ou�M�(0).Hodnota, �ouDM�(0).Hodnota
    frmEditorDrah.St�edit = -chbSt�edit(0)
    frmEditorDrah.Zarovn�vat = -chbZarovn�vat(0)
End Sub

Private Sub Na�tiNastSTP()
    �ou�M�(1) = frmSb�rka.opRozm�ry.M��kaX
    �ouDM�(1) = frmSb�rka.opRozm�ry.M��kaY
    chbSt�edit(1) = -frmSb�rka.St�edit
    chbZarovn�vat(1) = -frmSb�rka.Zarovn�vat
    Srovnej fraM��ka(1), fraM��ka(0)
    fraM��ka(0).Visible = False
    fraM��ka(1).Visible = True
    fraTrB.Visible = False
    chbVyrovn�vat.Visible = False
End Sub

Private Sub Zapi�NastSTP()
    frmSb�rka.opRozm�ry.NastavM��ku �ou�M�(1), �ouDM�(1)
    frmSb�rka.St�edit = -chbSt�edit(1)
    frmSb�rka.Zarovn�vat = -chbZarovn�vat(1)
End Sub

Public Property Let Karta(Nosi� As Variant)
    'tbsOptions.Tabs(Nosi�).Selected = True
    tbsOptions.Tab = Nosi�
End Property

Public Property Get Karta() As Variant
    'Karta = tbsOptions.SelectedItem.Key
    Karta = tbsOptions.Tab
End Property

Private Sub cmdApply_Click()
    'MsgBox "Place code here to set options w/o closing dialog!"
    Zapi�Hodnoty
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    'MsgBox "Place code here to set options and close dialog!"
    Zapi�Hodnoty
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim I As Integer
    'handle ctrl+tab to move to the next tab
    If Shift = vbCtrlMask And KeyCode = vbKeyTab Then
        I = tbsOptions.TabIndex
        If I = tbsOptions.Tabs Then
            'last tab so we need to wrap to tab 1
            tbsOptions.Tab = 1
        Else
            'increment the tab
            tbsOptions.Tab = I + 1
        End If
    End If
End Sub

Private Sub Form_Load()
    'center the form
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    opZpKres_Click 0
    tlBED.Height = lstBED.Height
    tlBEP.Height = lstBEP.Height
    Na�tiHodnoty
End Sub

Private Sub lstBED_Click()
    tlBED.BackColor = frmEditorDrah.opStupn�.BarvaPatra(Obr�cen�Index(lstBED.ListIndex, lstBED))
End Sub

Private Sub lstBEP_Click()
    tlBEP.BackColor = frmEditorPrvk�.opStupn�.BarvaPatra(Obr�cen�Index(lstBEP.ListIndex, lstBEP))
End Sub

Function Obr�cen�Index(Index As Integer, lst As ListBox)
    Obr�cen�Index = lst.ListCount - Index - 2
End Function

Private Sub opEd�iSb_Click(Index As Integer)
    Static Pov�ce As Boolean
    If Index = 0 Then
        If Pov�ce Then Zapi�NastSTP
        Na�tiNastED
    Else
        If Pov�ce Then Zapi�NastED
        Na�tiNastSTP
    End If
    If Not Pov�ce Then
        Pov�ce = True
    End If
End Sub

Private Sub opZpKres_Click(Index As Integer)
'    �ouRychlost.Enabled = Not CBool(Index)
End Sub

Private Sub tbsOptions_Click(Index As Integer)
    
    Dim I As Integer '��ta�
    'show and enable the selected tab's controls
    'and hide and disable all others
            
    For I = 0 To tbsOptions.Tabs - 2
        If I = tbsOptions.Tab Then
            'picOptions(I).Left = 16
            picOptions(I).Visible = True
            picOptions(I).Enabled = True
        Else
            picOptions(I).Visible = False
            picOptions(I).Enabled = False
        End If
    Next
    
End Sub

Public Property Let ZdaSb�rka(Nosi� As Boolean)
    opEd�iSb(1) = Nosi�
End Property

Public Property Get ZdaSb�rka() As Boolean
    ZdaSb�rka = opEd�iSb(1)
End Property

Private Sub tlBarvaOsv_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaOsv.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaOsv.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBarvaSS_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaSS.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaSS.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBarvaOS_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaOsv.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaOsv.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBarvaT�_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaT�.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaT�.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBarvaH�_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaH�.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaH�.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBED_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBED.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBED.BackColor = CD.Color
    frmEditorDrah.opStupn�.BarvaPatra(Obr�cen�Index(lstBED.ListIndex, lstBED)) = CD.Color
    frmEditorDrah.opStupn�.Nabarvi
vypadni:
End Sub

Private Sub tlBEP_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBEP.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBEP.BackColor = CD.Color
    frmEditorPrvk�.opStupn�.BarvaPatra(Obr�cen�Index(lstBEP.ListIndex, lstBEP)) = CD.Color
    frmEditorDrah.opStupn�.Nabarvi
vypadni:
End Sub

Private Sub tlNaplBED_Click()
    frmEditorDrah.opStupn�.Po�etNadzBarev = �ouBED
    frmEditorDrah.opStupn�.Nabarvi
    Napl�BED
End Sub

Private Sub tlNaplBEP_Click()
    frmEditorPrvk�.opStupn�.Po�etNadzBarev = �ouBEP
    frmEditorPrvk�.opStupn�.Nabarvi
    Napl�BEP
End Sub

Private Sub tlTrB_Click()
    
    frmTransparent.Show 1, Me
    tlTrB.BackColor = TrB
'    CD.Flags = cdlCCRGBInit
'    CD.Color = tlTrB.BackColor
'    On Error GoTo vypadni
'    CD.ShowColor
'    tlTrB.BackColor = CD.Color
'vypadni:
End Sub
