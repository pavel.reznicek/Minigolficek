VERSION 5.00
Begin VB.UserControl Knofl�k 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   BackStyle       =   0  'Transparent
   ClientHeight    =   990
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2910
   EditAtDesignTime=   -1  'True
   KeyPreview      =   -1  'True
   MaskColor       =   &H00FFFFFF&
   MaskPicture     =   "Knofl�k.ctx":0000
   PropertyPages   =   "Knofl�k.ctx":00AA
   ScaleHeight     =   990
   ScaleWidth      =   2910
   ToolboxBitmap   =   "Knofl�k.ctx":00C0
   Begin VB.PictureBox obrRozm�ry 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   720
      ScaleHeight     =   735
      ScaleWidth      =   1335
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "Knofl�k"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Public Event Click()
'Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Public Event DblClick()
Dim pamStisk As IPictureDisp
Dim pamObr As IPictureDisp
'
'Private Sub UserControl_DblClick()
'    RaiseEvent DblClick
'End Sub
'
'Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If Not Enabled Then DoEvents: Exit Sub
'    RaiseEvent MouseDown(Button, Shift, X, Y)
'    DoEvents
'End Sub
'
'Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If Not Enabled Then DoEvents: Exit Sub
'    RaiseEvent MouseMove(Button, Shift, X, Y)
'    DoEvents
'End Sub
'
'Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If Not Enabled Then DoEvents: Exit Sub
'    RaiseEvent MouseUp(Button, Shift, X, Y)
'    If Button = vbLeftButton Then
'        If 0 <= X And X < ScaleWidth _
'        And 0 <= Y And Y < ScaleHeight Then
'            RaiseEvent Click
'        End If
'    End If
'    DoEvents
'End Sub

Public Property Let Obr�zek(Nosi� As IPictureDisp)
    Set pamObr = Nosi�
    Picture = Nosi�
    PropertyChanged "Obr�zek"
End Property

Public Property Set Obr�zek(Nosi� As IPictureDisp)
    Set pamObr = Nosi�
    Picture = Nosi�
    PropertyChanged "Obr�zek"
End Property

Public Property Get Obr�zek() As IPictureDisp
    Set Obr�zek = pamObr
End Property

Public Property Let Maska(Nosi� As IPictureDisp)
    obrRozm�ry.Picture = Nosi�
    UserControl.MaskPicture = obrRozm�ry.Picture
    UserControl_Resize
    PropertyChanged "Maska"
End Property

Public Property Set Maska(Nosi� As IPictureDisp)
    obrRozm�ry.Picture = Nosi�
    UserControl.MaskPicture = obrRozm�ry.Picture
    UserControl_Resize
    PropertyChanged "Maska"
End Property

Public Property Get Maska() As IPictureDisp
    Set Maska = MaskPicture
End Property

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Private Sub UserControl_Resize()
    Static vypadni As Boolean
    If vypadni Then Exit Sub
    vypadni = True
    If UserControl.Width <> obrRozm�ry.Width Or UserControl.Height <> obrRozm�ry.Height Then
        Size obrRozm�ry.Width, obrRozm�ry.Height
    End If
    vypadni = False
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Maska = .ReadProperty("Maska", LoadPicture())
        Obr�zek = .ReadProperty("Obr�zek", LoadPicture())
        Enabled = .ReadProperty("Enabled", True)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Maska", Maska, LoadPicture()
        .WriteProperty "Obr�zek", Obr�zek, LoadPicture()
        .WriteProperty "Enabled", Enabled, True
    End With
End Sub
