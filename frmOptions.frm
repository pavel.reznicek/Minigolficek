VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNast 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nastaven� Minigolf��ku"
   ClientHeight    =   4920
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   6150
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   328
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   285
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraM��ka 
         Caption         =   "M��ka (zvl᚝)"
         Height          =   1575
         Index           =   1
         Left            =   0
         TabIndex        =   46
         Top             =   0
         Width           =   2415
         Begin VB.CheckBox chbZarovn�vat 
            Caption         =   "Zarovn�vat ke m��ce"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   49
            Top             =   960
            Width           =   1935
         End
         Begin VB.CheckBox chbSt�edit 
            Caption         =   "St�edit do m��ky"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   48
            Top             =   1200
            Width           =   1935
         End
         Begin Minigolf��ek.�oup� �ou�M� 
            Height          =   285
            Index           =   1
            Left            =   240
            TabIndex        =   47
            Top             =   240
            Width           =   1935
            _ExtentX        =   2778
            _ExtentY        =   503
            Max             =   128
            ���kaTxt        =   36
            N�pis           =   "���ka:"
         End
         Begin Minigolf��ek.�oup� �ouDM� 
            Height          =   285
            Index           =   1
            Left            =   240
            TabIndex        =   50
            Top             =   600
            Width           =   1935
            _ExtentX        =   2778
            _ExtentY        =   503
            Max             =   128
            ���kaTxt        =   36
            N�pis           =   "D�lka:"
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Tyto volby plat� pro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2640
         TabIndex        =   43
         Top             =   120
         Width           =   2895
         Begin VB.OptionButton opEd�iSb 
            Caption         =   "Sb�rku prvk�"
            Height          =   255
            Index           =   1
            Left            =   1440
            TabIndex        =   45
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton opEd�iSb 
            Caption         =   "Editor drah"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   44
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "St�nov�n� (spole�n�)"
         Height          =   1815
         Left            =   120
         TabIndex        =   39
         Top             =   1800
         Width           =   2415
         Begin VB.CheckBox chbSyt�St�ny 
            Caption         =   "Syt� st�ny na �ikm�ch ploch�ch"
            Height          =   375
            Left            =   240
            TabIndex        =   40
            Top             =   360
            Width           =   1935
         End
         Begin VB.CheckBox chbZv�r�ik 
            Caption         =   "Zv�raz�ovat �ikm� plochy a hrany"
            Height          =   375
            Left            =   240
            TabIndex        =   41
            Top             =   840
            Width           =   2055
         End
         Begin VB.CheckBox chbZv�rSyt 
            Caption         =   "Zv�raz�ovat pod syt�mi st�ny"
            Height          =   375
            Left            =   240
            TabIndex        =   42
            Top             =   1320
            Width           =   1935
         End
      End
      Begin VB.CheckBox chbVyrovn�vat 
         Caption         =   "P�i bo�en� vyrovn�vat se z�kladem"
         Height          =   255
         Left            =   2640
         TabIndex        =   38
         Top             =   3360
         Width           =   2775
      End
      Begin VB.Frame fraTrB 
         Caption         =   "Transparentn� barva (editor)"
         Height          =   1455
         Left            =   2640
         TabIndex        =   33
         Top             =   1800
         Width           =   2895
         Begin VB.CommandButton tlTrB 
            Height          =   255
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   240
            Width           =   2415
         End
         Begin VB.CheckBox chbTrBRel 
            Caption         =   "na reli�f"
            Height          =   255
            Left            =   240
            TabIndex        =   36
            Top             =   1080
            Width           =   2175
         End
         Begin VB.CheckBox chbTrBGra 
            Caption         =   "na grafiku"
            Height          =   255
            Left            =   240
            TabIndex        =   35
            Top             =   840
            Width           =   2175
         End
         Begin VB.Label lblTrB 
            Caption         =   "Pou��vat transparentn� barvu:"
            Height          =   255
            Left            =   240
            TabIndex        =   34
            Top             =   600
            Width           =   2175
         End
      End
      Begin VB.Frame fraStavPov 
         Caption         =   "Stavebn� povolen� (spole�n�)"
         Height          =   855
         Left            =   2640
         TabIndex        =   30
         Top             =   840
         Width           =   2895
         Begin VB.CheckBox chbStavPovRel 
            Caption         =   "na reli�f"
            Height          =   255
            Left            =   240
            TabIndex        =   32
            Top             =   480
            Width           =   2415
         End
         Begin VB.CheckBox chbStavPovGra 
            Caption         =   "na grafiku"
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame fraM��ka 
         Caption         =   "M��ka (zvl᚝)"
         Height          =   1575
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   120
         Width           =   2415
         Begin Minigolf��ek.�oup� �ou�M� 
            Height          =   285
            Index           =   0
            Left            =   240
            TabIndex        =   28
            Top             =   240
            Width           =   1935
            _ExtentX        =   2778
            _ExtentY        =   503
            Max             =   128
            ���kaTxt        =   36
            N�pis           =   "���ka:"
         End
         Begin VB.CheckBox chbSt�edit 
            Caption         =   "St�edit do m��ky"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   27
            Top             =   1200
            Width           =   1935
         End
         Begin VB.CheckBox chbZarovn�vat 
            Caption         =   "Zarovn�vat ke m��ce"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   26
            Top             =   960
            Width           =   1935
         End
         Begin Minigolf��ek.�oup� �ouDM� 
            Height          =   285
            Index           =   0
            Left            =   240
            TabIndex        =   29
            Top             =   600
            Width           =   1935
            _ExtentX        =   2778
            _ExtentY        =   503
            Max             =   128
            ���kaTxt        =   36
            N�pis           =   "D�lka:"
         End
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   285
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   1785
         Left            =   1545
         TabIndex        =   9
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   0
      Left            =   240
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraZpKr 
         Caption         =   "Zp�sob kreslen�"
         Height          =   1815
         Left            =   120
         TabIndex        =   19
         Top             =   1680
         Width           =   2535
         Begin Minigolf��ek.�oup� �ouRychlost 
            Height          =   285
            Left            =   240
            TabIndex        =   22
            Top             =   720
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   503
            Max             =   32
            ���kaTxt        =   36
            N�pis           =   "Rychlost smy�ky:"
         End
         Begin VB.OptionButton opZpKres 
            Caption         =   "�asova�em"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   21
            Top             =   1320
            Width           =   1575
         End
         Begin VB.OptionButton opZpKres 
            Caption         =   "smy�kou"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   20
            Top             =   360
            Value           =   -1  'True
            Width           =   1575
         End
      End
      Begin VB.CheckBox chbUkotvit 
         Caption         =   "Ukotvit m��ek p�i startu"
         Height          =   255
         Left            =   3000
         TabIndex        =   18
         Top             =   2400
         Width           =   2535
      End
      Begin VB.CheckBox chbZvuk 
         Caption         =   "Zvukov� efekty"
         Height          =   255
         Left            =   3000
         TabIndex        =   17
         Top             =   2880
         Width           =   2655
      End
      Begin VB.Frame fraM��itJako 
         Caption         =   "M��it jako �"
         Height          =   1215
         Left            =   120
         TabIndex        =   14
         Top             =   120
         Width           =   2535
         Begin VB.OptionButton opM��itJako 
            Caption         =   "� prakem"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   16
            Top             =   720
            Width           =   1575
         End
         Begin VB.OptionButton opM��itJako 
            Caption         =   "� hol�"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   15
            Top             =   360
            Value           =   -1  'True
            Width           =   1575
         End
      End
      Begin VB.Frame fra�ipka 
         Caption         =   "�ipka"
         Height          =   1935
         Left            =   2880
         TabIndex        =   4
         Top             =   120
         Width           =   2655
         Begin VB.CommandButton tlBarvaT� 
            Height          =   375
            Left            =   2040
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton tlBarvaH� 
            Height          =   375
            Left            =   2040
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   1320
            Width           =   375
         End
         Begin Minigolf��ek.�oup� �ou�� 
            Height          =   285
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   2175
            _ExtentX        =   3836
            _ExtentY        =   503
            Max             =   32
            ���kaTxt        =   36
            N�pis           =   "���ka:"
         End
         Begin VB.Label lblBT� 
            Caption         =   "Barva t�la:"
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   900
            Width           =   1455
         End
         Begin VB.Label lblBH� 
            Caption         =   "Barva hlavi�ky:"
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   1380
            Width           =   1455
         End
      End
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   120
      Top             =   4440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   240
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   10
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Uplatnit"
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   4455
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Pry�!"
      Height          =   375
      Left            =   3720
      TabIndex        =   1
      Top             =   4455
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2490
      TabIndex        =   0
      Top             =   4455
      Width           =   1095
   End
   Begin MSComctlLib.TabStrip tbsOptions 
      Height          =   4215
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   7435
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   3
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Hra"
            Key             =   "Hra"
            Object.ToolTipText     =   "Nastaven� hern�ho prost�ed�"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Editor &drah"
            Key             =   "Editor drah"
            Object.ToolTipText     =   "Nastaven� Editoru drah a okna se sb�rkou ter�nn�ch prvk�"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Barvy"
            Key             =   "Barvy"
            Object.ToolTipText     =   "Nastaven� barev pou��van�ch v editorech"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmNast"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Na�tiHodnoty()
    ObnovHodZRegistru
    'na��st stavy vlastnost�:
    
    'hern� nastaven�
    chbZvuk = -Zvuk
    chbUkotvit = -Ukotvit
    �ou�� = ���ka�ipky
    �ouRychlost = RychlostZobrazen�
    opM��itJako(1) = M��itPrakem
    tlBarvaH�.BackColor = BarvaH�
    tlBarvaT�.BackColor = BarvaT�
    opZpKres(1) = �asova�
    'editor a sb�rka
    'opEd�iSb(0) = Not ZdaSb�rka
    Na�tiNastED
    chbSyt�St�ny = -Syt�St�ny
    chbZv�r�ik = -Zv�raz�ovat
    chbZv�rSyt = -ZvPodSSt
    chbStavPovGra.Enabled = Povolen�Stavebn�hoPovolen�
    chbStavPovRel.Enabled = Povolen�Stavebn�hoPovolen�
    tlTrB.BackColor = TrB
    chbTrBGra = -TrBGra
    chbTrBRel = -TrBRel
    chbStavPovGra = -Stavebn�Povolen�(dzTextura)
    chbStavPovRel = -Stavebn�Povolen�(dzReli�f)
End Sub

Private Sub Zapi�Hodnoty()
    'Hern� nastaven�
    Zvuk = chbZvuk
    Ukotvit = chbUkotvit
    ���ka�ipky = �ou��
    �ou�� = �ou��
    RychlostZobrazen� = �ouRychlost
    �ouRychlost = �ouRychlost
    M��itPrakem = opM��itJako(1)
    BarvaH� = tlBarvaH�.BackColor
    BarvaT� = tlBarvaT�.BackColor
    �asova� = opZpKres(1)
    'Editor drah a sb�rka t. p.
    If Not ZdaSb�rka Then
        Zapi�NastED
    Else
        Zapi�NastSTP
    End If
    Syt�St�ny = -chbSyt�St�ny
    Zv�raz�ovat = -chbZv�r�ik
    ZvPodSSt = -chbZv�rSyt
    'TrB = tlTrB.BackColor 'zbyte�n�; u� tam je.
    TrBGra = -chbTrBGra
    TrBRel = -chbTrBRel
    Stavebn�Povolen�(dzTextura) = -chbStavPovGra
    Stavebn�Povolen�(dzReli�f) = -chbStavPovRel
End Sub

Private Sub Na�tiNastED()
    �ou�M�(0) = frmEditorDrah.opRozm�ry.M��kaX
    �ouDM�(0) = frmEditorDrah.opRozm�ry.M��kaY
    chbSt�edit(0) = -frmEditorDrah.St�edit
    chbZarovn�vat(0) = -frmEditorDrah.Zarovn�vat
    fraM��ka(0).Visible = True
    fraM��ka(1).Visible = False
    fraTrB.Visible = True
    chbVyrovn�vat.Visible = True
End Sub

Private Sub Zapi�NastED()
    frmEditorDrah.opRozm�ry.NastavM��ku �ou�M�(0), �ouDM�(0)
    frmEditorDrah.St�edit = -chbSt�edit(0)
    frmEditorDrah.Zarovn�vat = -chbZarovn�vat(0)
End Sub

Private Sub Na�tiNastSTP()
    �ou�M�(1) = frmSb�rka.opRozm�ry.M��kaX
    �ouDM�(1) = frmSb�rka.opRozm�ry.M��kaY
    chbSt�edit(1) = -frmSb�rka.St�edit
    chbZarovn�vat(1) = -frmSb�rka.Zarovn�vat
    Srovnej fraM��ka(1), fraM��ka(0)
    fraM��ka(0).Visible = False
    fraM��ka(1).Visible = True
    fraTrB.Visible = False
    chbVyrovn�vat.Visible = False
End Sub

Private Sub Zapi�NastSTP()
    frmSb�rka.opRozm�ry.NastavM��ku �ou�M�(1), �ouDM�(1)
    frmSb�rka.St�edit = -chbSt�edit(1)
    frmSb�rka.Zarovn�vat = -chbZarovn�vat(1)
End Sub

Public Property Let Karta(Nosi� As Variant)
    tbsOptions.Tabs(Nosi�).Selected = True
End Property

Public Property Get Karta() As Variant
    Karta = tbsOptions.SelectedItem.Key
End Property

Private Sub cmdApply_Click()
    'MsgBox "Place code here to set options w/o closing dialog!"
    Zapi�Hodnoty
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    'MsgBox "Place code here to set options and close dialog!"
    Zapi�Hodnoty
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    'handle ctrl+tab to move to the next tab
    If Shift = vbCtrlMask And KeyCode = vbKeyTab Then
        i = tbsOptions.SelectedItem.Index
        If i = tbsOptions.Tabs.Count Then
            'last tab so we need to wrap to tab 1
            Set tbsOptions.SelectedItem = tbsOptions.Tabs(1)
        Else
            'increment the tab
            Set tbsOptions.SelectedItem = tbsOptions.Tabs(i + 1)
        End If
    End If
End Sub

Private Sub Form_Load()
    'center the form
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    opZpKres_Click 0
    
    Na�tiHodnoty
End Sub

Private Sub opEd�iSb_Click(Index As Integer)
    Static Pov�ce As Boolean
    If Index = 0 Then
        If Pov�ce Then Zapi�NastSTP
        Na�tiNastED
    Else
        If Pov�ce Then Zapi�NastED
        Na�tiNastSTP
    End If
    If Not Pov�ce Then
        Pov�ce = True
    End If
End Sub

Private Sub opZpKres_Click(Index As Integer)
'    �ouRychlost.Enabled = Not CBool(Index)
End Sub

Private Sub tbsOptions_Click()
    
    Dim i As ITab, j As Integer '��ta� a upraven� ��ta�
    'show and enable the selected tab's controls
    'and hide and disable all others
            
    For Each i In tbsOptions.Tabs
        j = i.Index - 1
        If j = tbsOptions.SelectedItem.Index - 1 Then
            picOptions(j).Left = 16
            picOptions(j).Visible = True
            picOptions(j).Enabled = True
        Else
            picOptions(j).Visible = False
            picOptions(j).Enabled = False
        End If
    Next
    
End Sub

Public Property Let ZdaSb�rka(Nosi� As Boolean)
    opEd�iSb(1) = Nosi�
End Property

Public Property Get ZdaSb�rka() As Boolean
    ZdaSb�rka = opEd�iSb(1)
End Property

Private Sub tlBarvaT�_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaT�.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaT�.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlBarvaH�_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = tlBarvaH�.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    tlBarvaH�.BackColor = CD.Color
vypadni:
End Sub

Private Sub tlTrB_Click()
    
    frmTransparent.Show 1, Me
    tlTrB.BackColor = TrB
'    CD.Flags = cdlCCRGBInit
'    CD.Color = tlTrB.BackColor
'    On Error GoTo vypadni
'    CD.ShowColor
'    tlTrB.BackColor = CD.Color
'vypadni:
End Sub
