VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Z�sobn�k"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Obsahuje sd�lenou texturu a sd�len� reli�f."
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarReli�f As Variant 'local copy
Private mvarTextura As Variant 'local copy
'To fire this event, use RaiseEvent with the following syntax:
'RaiseEvent Zm�na[(arg1, arg2, ... , argn)]
Public Event Zm�na()
'local variable(s) to hold property value(s)
Private mvar���ka As Integer 'local copy
Private mvarV��ka As Integer 'local copy

Public Property Let V��ka(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.V��ka = 5
    mvarV��ka = vData
    ReDim Preserve mvarReli�f(mvar���ka, mvarV��ka)
    UplatniZm�ny
End Property


Public Property Get V��ka() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.V��ka
    If IsArray(mvarReli�f) Then
        mvarV��ka = UBound(mvarReli�f, 2) + 1
    Else
        mvarV��ka = 0
    End If
    V��ka = mvarV��ka
End Property



Public Property Let ���ka(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.���ka = 5
    Dim Nov�Rel As Variant
    ReDim Nov�Rel(vData, V��ka)
    Dim X%, Y%
    On Error Resume Next
    For X = 0 To vData - 1
        For Y = 0 To mvarV��ka - 1
            Nov�Rel(X, Y) = mvarReli�f(X, Y)
        Next
    Next
    On Error GoTo 0
    mvarReli�f = Nov�Rel
    mvar���ka = vData
    UplatniZm�ny
End Property


Public Property Get ���ka() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.���ka
    If IsArray(mvarReli�f) Then
        mvar���ka = UBound(mvarReli�f, 1) + 1
    Else
        mvar���ka = 0
    End If
    ���ka = mvar���ka
End Property

Public Sub Rozm��(ByVal r���ka As Integer, ByVal rV��ka As Integer)
    Dim Nov�Rel As Variant
    ReDim Nov�Rel(r���ka - 1, rV��ka - 1)
    Dim X&, Y&
    If IsEmpty(mvarReli�f) Then
        Nov�Rel(0, 0) = 0
        mvarReli�f = Nov�Rel
        GoTo vypadni
    End If
    On Error Resume Next
    For X = 0 To r���ka - 1
        For Y = 0 To rV��ka - 1
            Nov�Rel(X, Y) = mvarReli�f(X, Y)
        Next
    Next
    On Error GoTo 0
vypadni:
    mvarReli�f = Nov�Rel
    mvar���ka = r���ka
    mvarV��ka = rV��ka
    UplatniZm�ny
End Sub



Public Property Let Textura(ByVal vData As IPictureDisp)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Textura = 5
    Set mvarTextura = vData
    'Clipboard.Clear
    'Clipboard.SetData mvarTextura
    UplatniZm�ny True
End Property


Public Property Set Textura(ByVal vData As IPictureDisp)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Textura = Form1
    Set mvarTextura = vData
    'Clipboard.Clear
    'Clipboard.SetData mvarTextura
    UplatniZm�ny True
End Property


Public Property Get Textura() As IPictureDisp
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Textura
    If IsObject(mvarTextura) Then
        Set Textura = mvarTextura
    End If
End Property



Public Property Let Reli�f(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Reli�f = 5
    If IsArray(vData) Then
        mvarReli�f = vData
        UplatniZm�ny
    End If
End Property


Public Property Get Reli�f() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Reli�f
    If Not IsArray(mvarReli�f) Then
        ReDim mvarReli�f(0, 0)
    End If
    Reli�f = mvarReli�f()
End Property

Public Property Let Bod(ByVal X%, ByVal Y%, ByVal Nosi� As Single)
    If IsArray(mvarReli�f) Then
        mvarReli�f(X, Y) = Nosi�
        If X >= 0 And X < ���ka _
        And Y >= 0 And Y < V��ka Then
            mvarReli�f(X, Y) = Nosi�
            frmSb�rka.obrPamRel.PSet (X, Y), frmEditorDrah.opStupn�.BarvaPodleV��ky(Nosi�)
        End If
    End If
End Property

Public Property Get Bod(ByVal X%, ByVal Y%) As Single
Attribute Bod.VB_UserMemId = 0
    If IsArray(mvarReli�f) Then
        If X >= 0 And X < ���ka _
        And Y >= 0 And Y < V��ka Then
            Bod = mvarReli�f(X, Y)
        End If
    End If
End Property

Public Sub UplatniZm�ny(Optional JenTexturu As Boolean)
    Dim Obd As Shape
    frmSb�rka.obrPamObr.Width = ���ka
    frmSb�rka.obrPamObr.Height = V��ka
    If Not mvarTextura = Empty Then
        frmSb�rka.obrPamObr.Picture = LoadPicture()
        frmSb�rka.obrPamObr.PaintPicture mvarTextura, 0, 0
    End If
    frmSb�rka.obrPamRel.Width = ���ka
    frmSb�rka.obrPamRel.Height = V��ka
    frmSb�rka.Vyst�e�Z�sobn�k
'    For Each Obd In frmSb�rka.obdPoloha
'        Obd.Width = ���ka
'        Obd.Height = V��ka
'    Next
    frmEditorDrah.obrPamObr.Width = ���ka
    frmEditorDrah.obrPamObr.Height = V��ka
    If Not mvarTextura = Empty Then
        frmEditorDrah.obrPamObr.Picture = LoadPicture()
        frmEditorDrah.obrPamObr.PaintPicture mvarTextura, 0, 0
    End If
    If frmEditorDrah.Akce = aPrvek Then
        frmEditorDrah.opRozmV�b.Nastav ���ka, V��ka
    End If
    'frmEditorPrvk�.opRozm�ry.Nastav ���ka, V��ka
    If Not JenTexturu Then
        NakresliRel
        If Not frmP�edvolby.tvP�edvolby.SelectedItem Is Nothing Then
            If frmP�edvolby.tvP�edvolby.SelectedItem.FullPath = "reli�f ze z�sobn�ku" Then frmP�edvolby.Na�tiReli�f
        End If
    End If
End Sub

Sub NakresliRel()
    Dim X%, Y%, �%, V%
    � = ���ka
    v = V��ka
    On Error Resume Next
    For X = 0 To �
        For Y = 0 To v
            frmSb�rka.obrPamRel.PSet (X, Y), frmEditorDrah.opStupn�.BarvaPodleV��ky(Bod(X, Y))
        Next
    Next
End Sub
