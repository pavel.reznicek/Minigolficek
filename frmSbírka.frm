VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSb�rka 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sb�rka ter�nn�ch prvk�"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   4710
   Icon            =   "frmSb�rka.frx":0000
   MaxButton       =   0   'False
   ScaleHeight     =   326
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   314
   Begin VB.PictureBox obrSt�ny 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   9
      Top             =   3360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox obrZ�sobn�k 
      BackColor       =   &H8000000C&
      Height          =   1695
      Left            =   1200
      ScaleHeight     =   109
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   61
      TabIndex        =   5
      ToolTipText     =   "Z�sobn�k"
      Top             =   1320
      Width           =   975
      Begin VB.PictureBox obrPamRel 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   480
         Left            =   0
         ScaleHeight     =   32
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   32
         TabIndex        =   7
         ToolTipText     =   "Reli�f v z�sobn�ku"
         Top             =   720
         Width           =   480
      End
      Begin VB.PictureBox obrPamObr 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   480
         Left            =   0
         ScaleHeight     =   32
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   32
         TabIndex        =   6
         ToolTipText     =   "Textura v z�sobn�ku"
         Top             =   0
         Width           =   480
      End
   End
   Begin Minigolf��ek.UkazatelXYZ UXYZ 
      Height          =   735
      Left            =   3000
      TabIndex        =   4
      Top             =   3885
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1296
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin Minigolf��ek.Rozm�ry opRozm�ry 
      Height          =   525
      Left            =   3000
      TabIndex        =   3
      Top             =   3360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   953
      Max���ka        =   600
      MaxV��ka        =   480
      ���ka           =   320
      V��ka           =   320
      Enabled         =   0   'False
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   2
      Left            =   120
      MouseIcon       =   "frmSb�rka.frx":0442
      Negotiate       =   -1  'True
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   2
      Top             =   2040
      Visible         =   0   'False
      Width           =   855
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H00808080&
         BorderStyle     =   6  'Inside Solid
         DrawMode        =   6  'Mask Pen Not
         Height          =   480
         Index           =   2
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin MSComDlg.CommonDialog CD 
      Left            =   2400
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".mig"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   1
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   1
      Top             =   1080
      Width           =   855
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H000000FF&
         BorderStyle     =   6  'Inside Solid
         DrawMode        =   6  'Mask Pen Not
         Height          =   480
         Index           =   1
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin VB.PictureBox obrDr�ha 
      AutoRedraw      =   -1  'True
      Height          =   855
      Index           =   0
      Left            =   120
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   53
      TabIndex        =   0
      Top             =   120
      Width           =   855
      Begin VB.Shape obdPoloha 
         BorderColor     =   &H000000FF&
         BorderStyle     =   6  'Inside Solid
         DrawMode        =   6  'Mask Pen Not
         Height          =   480
         Index           =   0
         Left            =   15
         Top             =   15
         Width           =   480
      End
   End
   Begin MSComDlg.CommonDialog CDObr 
      Left            =   2400
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   ".mig"
      DialogTitle     =   "Otev��t"
      Filter          =   "Dr�hy minigolfu (*.glf)|*.glf"
   End
   Begin Minigolf��ek.Rozm�ry opRozmV�b 
      Height          =   525
      Left            =   1200
      TabIndex        =   8
      Top             =   3360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   953
      TextX           =   "�. v�b.:"
      TextY           =   "D. v�b.:"
      Max���ka        =   64
      MaxV��ka        =   64
      ���ka           =   64
      V��ka           =   64
      M��kaX         =   1
      M��kaY         =   1
   End
   Begin MSComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   276
      Left            =   0
      TabIndex        =   10
      Top             =   4620
      Width           =   4704
      _ExtentX        =   8308
      _ExtentY        =   476
      SimpleText      =   "P�ipraven"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6245
            MinWidth        =   2117
            Text            =   "P�ipraven"
            TextSave        =   "P�ipraven"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmSb�rka.frx":0594
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmSb�rka.frx":0AE8
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmSb�rka.frx":103C
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   476
            MinWidth        =   339
            Picture         =   "frmSb�rka.frx":1590
         EndProperty
      EndProperty
   End
   Begin VB.Menu nabSoubor 
      Caption         =   "&Soubor"
      Begin VB.Menu nabOtev��t 
         Caption         =   "&Otev��t sb�rku"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabSep0 
         Caption         =   "-"
      End
      Begin VB.Menu nabMinigolf��ek 
         Caption         =   "=> &Minigolf��ek"
         Shortcut        =   {F2}
      End
      Begin VB.Menu nabEditorDrah 
         Caption         =   "=> Editor &drah"
         Shortcut        =   {F3}
      End
      Begin VB.Menu nabEditorPrvk� 
         Caption         =   "=> Editor ter�nn�ch &prvk�"
         Shortcut        =   {F4}
      End
      Begin VB.Menu nabSep9 
         Caption         =   "-"
      End
      Begin VB.Menu nabZav��t 
         Caption         =   "&Zav��t"
      End
      Begin VB.Menu nabKonec 
         Caption         =   "&Konec"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu nab�pravy 
      Caption         =   "�&pravy"
      Begin VB.Menu nabUpravit 
         Caption         =   "&Upravit obsah z�sobn�ku"
         Shortcut        =   ^{F4}
      End
      Begin VB.Menu nabSep7 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu nabSep6 
         Caption         =   "-"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu nabZobrazit 
      Caption         =   "&Zobrazit"
      Begin VB.Menu nabTextura 
         Caption         =   "- &texuru"
         Checked         =   -1  'True
         Shortcut        =   ^X
      End
      Begin VB.Menu nabReli�f 
         Caption         =   "- &reli�f"
         Shortcut        =   ^R
      End
      Begin VB.Menu nabSt�ny 
         Caption         =   "- texturu se &st�ny"
         Shortcut        =   ^S
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
      End
      Begin VB.Menu nabVykreslitSt�ny 
         Caption         =   "&Vykreslit st�ny"
         Shortcut        =   {F9}
      End
   End
   Begin VB.Menu nabNastaven� 
      Caption         =   "&Nastaven�"
      Begin VB.Menu nabSouhNast 
         Caption         =   "Souhrn &nastaven�"
         Shortcut        =   ^N
      End
      Begin VB.Menu nabSep8 
         Caption         =   "-"
      End
      Begin VB.Menu nabM��ka 
         Caption         =   "&Zarovn�vat ke m��ce"
         Checked         =   -1  'True
         Shortcut        =   ^Z
      End
      Begin VB.Menu nabSt�edit 
         Caption         =   "St�&edit do m��ky"
         Shortcut        =   ^E
      End
      Begin VB.Menu nabRozte� 
         Caption         =   "Rozte� &m��ky�"
         Shortcut        =   ^M
      End
      Begin VB.Menu nabSep5 
         Caption         =   "-"
      End
      Begin VB.Menu nabStavPov 
         Caption         =   "&Stavebn� povolen�"
         Begin VB.Menu nabStPGra 
            Caption         =   "na &grafiku"
            Checked         =   -1  'True
            Shortcut        =   {F11}
         End
         Begin VB.Menu nabStPRel 
            Caption         =   "na &reli�f"
            Checked         =   -1  'True
            Shortcut        =   {F12}
         End
      End
      Begin VB.Menu nabSep4 
         Caption         =   "-"
      End
      Begin VB.Menu nabTransparent 
         Caption         =   "&Transparentn� barva �"
         Shortcut        =   ^T
      End
      Begin VB.Menu nabPou��vatTrB 
         Caption         =   "&Pou��vat transparentn� barvu"
         Begin VB.Menu nabTrBTex 
            Caption         =   "pro &grafiku"
            Shortcut        =   +{F11}
         End
         Begin VB.Menu nabTrBRel 
            Caption         =   "pro &reli�f"
            Shortcut        =   +{F12}
         End
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmSb�rka"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim Mapa() As Single
Dim MapaPrvku() As Single
Dim MapaSty�n�chBod�() As XYByte
Public P�eru�itProces As Boolean
Public opStupn� As Stupn�
Public TrB As OLE_COLOR 'transparentn� barva

Dim P�vAkce As TypAkce

Const �ed� = 8421504 'RGB(128, 128, 128)
Const P�ipraven = "P�ipraven"

Private Enum TypAkce
    aBo�it = -1
    aNic = 0
    aPrvek = 1
    aObr�zek = 2
    aStart = 3
    aNas�t = 4
End Enum


Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        'KeyAscii = 0
        If MsgBox("P�eru�it prob�haj�c� proces (pokud n�jak� prob�h�)?", vbQuestion Or vbYesNo) = vbYes Then
            P�eru�itProces = True
        End If
    End If
End Sub

Private Sub Form_Load()
    DefTPP
'    TrB = DejNastaven�("Barvy", "Transparentn�", vbWhite)
'    nabTrBTex.Checked = DejNastaven�("Nastaven�", "Transparentn� textura", False)
'    nabTrBRel.Checked = DejNastaven�("Nastaven�", "Transparentn� reli�f", False)
'    nabM��ka.Checked = DejNastaven�("Nastaven�", "Zarovn�vat na sb�rce", True)
'    St�edit = St�edit
'    opRozm�ry.M��kaX = DejNastaven�("Nastaven�", "M��ka ve sb�rce X", 32)
'    opRozm�ry.M��kaY = DejNastaven�("Nastaven�", "M��ka ve sb�rce Y", 32)
    ObnovHodZRegistru
    obrDr�ha(0).Visible = False
    obrDr�ha(0).BackColor = opStupn�.BarvaPodleV��ky(0)
    obrPamRel.BackColor = opStupn�.BarvaPodleV��ky(0)
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Width = opRozm�ry.M��kaX
        Obd.Height = opRozm�ry.M��kaY
    Next Obd
    Dim Img As Control
    Nam��
    ReDim Mapa(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1)
    ReDim MapaPrvku(31, 31)
    Me.KeyPreview = True
    Set frmV�b�r.opStupn� = opStupn�
End Sub

Private Sub Form_Resize()
    If WindowState <> vbNormal Then Exit Sub
'    opStupn�.Left = 0
'    opStupn�.Height = ScaleHeight _
'    - opRozm�ry.Height - UXYZ.Height - sbrStav.Height
'    opStupn�.Top = ScaleHeight - opStupn�.Height - sbrStav.Height
    opRozm�ry.Move 0, 0
    opRozmV�b.Move 0, Spodek(opRozm�ry)
    UXYZ.Move 0, Spodek(opRozmV�b)
    obrZ�sobn�k.Move 0, Spodek(UXYZ), opRozm�ry.Width, ScaleHeight - Spodek(UXYZ) - sbrStav.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not frmMinigolf��ek.Visible And Not (frmEditorDrah.Visible Or frmEditorPrvk�.Visible) Then
        End
    Else
        Cancel = True
        Me.Hide
    End If
End Sub

Private Sub nabEditorDrah_Click()
    frmEditorDrah.Show
End Sub

Private Sub nabKonec_Click()
    End
End Sub

Private Sub nabMinigolf��ek_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Minigolf��ek"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\Minigolf��ek", vbNormalFocus
    frmMinigolf��ek.Show
End Sub

Private Sub nabEditorPrvk�_Click()
'    On Error GoTo neni_spu�t�nej
'    AppActivate "Editor ter�nn�ch prvk�"
'    Exit Sub
'neni_spu�t�nej:
'    Shell App.Path & "\EditorPrvk�", vbNormalFocus
    frmEditorPrvk�.Show
End Sub

Private Sub nabM��ka_Click()
    Zarovn�vat = Not Zarovn�vat
End Sub

Public Property Let Zarovn�vat(Nosi� As Boolean)
    nabM��ka.Checked = Nosi�
    Ulo�Nastaven� "Nastaven�", "Zarovn�vat na sb�rce", nabM��ka.Checked
End Property

Public Property Get Zarovn�vat() As Boolean
    Zarovn�vat = nabM��ka.Checked
End Property

Private Sub nabN�pov�da_Click()
'    frmN�pov�da.Show
    On Error GoTo Neni_spu�t�n�_n�pov�da
    AppActivate "N�pov�da�k�Minigolf��ku"
    Exit Sub
Neni_spu�t�n�_n�pov�da:
    Shell "winhelp " & App.Path & "\Minigolficek.hlp"
End Sub

Private Sub nabOtev��t_Click()
    DoEvents
    Umo�n�no = False
    CD.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    CD.DialogTitle = "Otev��t sb�rku prvk�"
    CD.InitDir = App.Path & "\Prvky\" & Hol�Jm�no(CD.FileTitle)
    CD.Filter = "Sb�rky ter�nn�ch prvk� (*.stp)|*.stp"
    On Error GoTo vypadni
    CD.ShowOpen
    DoEvents
    Otev�i CD.FileName
vypadni:
    Umo�n�no = True
End Sub

Public Sub Otev�i(Soubor As String)
    Umo�n�no = False
    Me.Show
    DoEvents
    With CDObr
    .Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
    .DialogTitle = "Otev��t texturu sb�rky prvk� " & Hol�Jm�no(Dir(Soubor))
    .InitDir = App.Path & "\Prvky\" & Hol�Jm�no(Dir(Soubor))
    .Filter = "Textury sb�rek ter�nn�ch prvk� (*.bmp)|*.bmp"
    .FileName = Hol�Jm�no(Dir(Soubor)) & ".bmp"
    End With
    On Error GoTo vypadni
    CDObr.ShowOpen
    DoEvents
    On Error GoTo chyba
    Stav = "Otev�r�m sb�rku �"
    Dim Jm�noTextury As String, Jm�noSt�n� As String
    Jm�noTextury = CDObr.FileName
    Dim ���ka As Integer, V��ka As Integer
    Dim Rozte�X As Byte, Rozte�Y As Byte
    Dim Start As XY
    Close
    Open Soubor For Binary As #1
    Get #1, , ���ka
    Get #1, , V��ka
    If ���ka > 5000 Or V��ka > 5000 _
    Or ���ka < 0 Or V��ka < 0 Then
        Close
        MsgBox "�patn� form�t souboru!", vbCritical, "Chyba"
        GoTo vypadni
    End If
    Get #1, , Rozte�X
    Get #1, , Rozte�Y
    If Rozte�Y = 0 Then Rozte�Y = Rozte�X
    ReDim Mapa(���ka + 2 - 1, V��ka + 2 - 1)
    Get #1, , Mapa()
    Close
    opRozm�ry.Nastav ���ka, V��ka
    opRozm�ry.NastavM��ku Rozte�X, Rozte�Y
    obrDr�ha(1).Picture = LoadPicture(Jm�noTextury)
    Caption = "Sb�rka ter�nn�ch prvk� " & Hol�Jm�no(Dir(Soubor))
'    UplatniMapu
vypadni:
    On Error GoTo 0
    Umo�n�no = True
    Stav = P�ipraven
    If Not Me.Visible Then Me.Show
    Beep
    Me.SetFocus
    Exit Sub
chyba:
    MsgBox "Chyba p�i otev�r�n�!", vbCritical, "Chyba"
    GoTo vypadni
End Sub
'
'Private Sub nabRozte�_Click()
'    Dim Rozte�%, �etRozte�$
'    On Error Resume Next
'    �etRozte� = InputBox("Zadejte rozte� m��ky v bodech:", "Rozte� m��ky", opRozm�ry.M��kaX)
'    If �etRozte� = "" Then Exit Sub
'    Rozte� = Val(�etRozte�)
'    If Rozte� < 1 Then Rozte� = 1:   MsgBox "Rozte� m��ky nem��e b�t men�� ne� 1 bod - zv�t�uji na 1.", vbInformation
'    If Rozte� > 64 Then Rozte� = 64: MsgBox "Rozte� m��ky nem��e b�t v�t�� ne� 64 bod� - zmen�uji na 64.", vbInformation
'    opRozm�ry.NastavM��ku Rozte�, Rozte�
'End Sub

Private Sub nabSouhNast_Click()
    frmNast.Karta = 1
    frmNast.Show 1, Me
End Sub

Private Sub nabStPGra_Click()
    Stavebn�Povolen�(dzTextura) = Not nabStPGra.Checked
End Sub

Private Sub nabStPRel_Click()
    Stavebn�Povolen�(dzReli�f) = Not nabStPRel.Checked
End Sub

Private Sub nabSt�edit_Click()
    St�edit = Not St�edit
End Sub

Private Sub nabTransparent_Click()
    frmTransparent.Show 1, Me
End Sub

Private Sub nabTrBRel_Click()
    nabTrBRel.Checked = Not nabTrBRel.Checked
    frmEditorDrah.nabTrBRel.Checked = nabTrBRel.Checked
    Ulo�Nastaven� "Nastaven�", "Transparentn� reli�f", nabTrBRel.Checked
End Sub

Private Sub nabTrBTex_Click()
    nabTrBTex.Checked = Not nabTrBTex.Checked
    frmEditorDrah.nabTrBTex.Checked = nabTrBTex.Checked
    Ulo�Nastaven� "Nastaven�", "Transparentn� textura", nabTrBTex.Checked
End Sub

Private Sub nabReli�f_Click()
    nabTextura.Checked = False
    nabSt�ny.Checked = False
    nabReli�f.Checked = True
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = True
    obrDr�ha(dzStTex).Visible = False
End Sub

Private Sub nabTextura_Click()
    nabTextura.Checked = True
    nabReli�f.Checked = False
    nabSt�ny.Checked = False
    obrDr�ha(1).Visible = True
    obrDr�ha(0).Visible = False
    obrDr�ha(dzStTex).Visible = False
End Sub

Private Sub nabSt�ny_Click()
    nabSt�ny.Checked = True
    nabTextura.Checked = False
    nabReli�f.Checked = False
    obrDr�ha(1).Visible = False
    obrDr�ha(0).Visible = False
    obrDr�ha(dzStTex).Visible = True
End Sub

Private Sub nabUpravit_Click()
    frmEditorPrvk�.Otev�iZeZ�sobn�ku
    frmEditorPrvk�.Show
End Sub

Private Sub nabVykreslitSt�ny_Click()
    Umo�n�no = False
    MousePointer = vbHourglass
    P�eru�itProces = False
    DoEvents
    nabTextura.Checked = False
    nabReli�f.Checked = False
    nabSt�ny.Checked = True
    P�idejR�me�ek
    Vyst�nuj
    Osv�tli
    P�idejR�me�ek
    Uka�Obr�zekPodleNab�dky
    Umo�n�no = True
    MousePointer = vbDefault
End Sub

Private Sub obrDr�ha_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
End Sub

Private Sub obrDr�ha_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    Hejbni X, Y
    UXYZ.NastavXYZ X, Y, Mapa(X, Y)
End Sub

Private Sub obrDr�ha_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 1 Or Y > obrDr�ha(Index).ScaleHeight - 2 _
    Or X < 1 Or X > obrDr�ha(Index).ScaleWidth - 2 Then Exit Sub
    Stavba Index
    DoEvents
End Sub

Private Sub Hejbni(X As Single, Y As Single)
    Dim Obd As Shape
    For Each Obd In obdPoloha
        If St�edit Then
            Obd.Move _
            X + ((-X + 1) Mod opRozm�ry.M��kaX - (Obd.Width - opRozm�ry.M��kaX) / 2) * -nabM��ka.Checked + Obd.Width / 2 * (Not nabM��ka.Checked), _
            Y + ((-Y + 1) Mod opRozm�ry.M��kaY - (Obd.Height - opRozm�ry.M��kaY) / 2) * -nabM��ka.Checked + Obd.Height / 2 * (Not nabM��ka.Checked)
        Else
            Obd.Move _
            X + ((-X + 1) Mod opRozm�ry.M��kaX) * -nabM��ka.Checked, _
            Y + ((-Y + 1) Mod opRozm�ry.M��kaY) * -nabM��ka.Checked
        End If
    Next
End Sub

Private Sub obrZ�sobn�k_Resize()
    Vyst�e�Z�sobn�k
End Sub

Public Sub Vyst�e�Z�sobn�k()
    With obrZ�sobn�k
    obrPamObr.Move (.ScaleWidth - obrPamObr.Width) / 2, (.ScaleHeight / 2 - obrPamObr.Height) / 2
    obrPamRel.Move (.ScaleWidth - obrPamRel.Width) / 2, (.ScaleHeight * 3 / 2 - obrPamRel.Height) / 2
    End With
End Sub

Private Sub opRozm�ry_Ru�n�Zm�na()
    Dim BylP�ipravenej As Boolean
    BylP�ipravenej = (Stav = P�ipraven)
    If BylP�ipravenej Then Stav = "M�n�m rozm�ry sb�rky �"
    Nam��
    O��zniMapu
    'UplatniMapu
    If BylP�ipravenej Then Stav = P�ipraven
End Sub

Private Sub opRozm�ry_Nastaven�()
    Dim BylP�ipravenej As Boolean
    BylP�ipravenej = (Stav = P�ipraven)
    If BylP�ipravenej Then Stav = "M�n�m rozm�ry sb�rky �"
    Nam��
    'O��zniMapu
    UplatniMapu
    If BylP�ipravenej Then Stav = P�ipraven
End Sub

Private Sub opRozm�ry_Zm�naM��ky()
    Dim I As Shape, Rozte�X%, Rozte�Y%
    Rozte�X = opRozm�ry.M��kaX
    Rozte�Y = opRozm�ry.M��kaY
    If Rozte�Y = 0 Then Rozte�Y = Rozte�X
    For Each I In obdPoloha
        Rozm�� I, Rozte�X, Rozte�Y
    Next
    opRozmV�b.Nastav Rozte�X, Rozte�Y
    Ulo�Nastaven� "Nastaven�", "M��ka ve sb�rce X", Rozte�X
    Ulo�Nastaven� "Nastaven�", "M��ka ve sb�rce Y", Rozte�Y
'    Debug.Assert Not Rozte�X <> 32 Or Not Rozte�Y <> 32
End Sub

Private Property Let Stav(Nosi� As String)
    sbrStav.Panels(1).Text = Nosi�
End Property

Private Property Get Stav() As String
    Stav = sbrStav.Panels(1).Text
End Property

Private Sub Nam��()
    With obrDr�ha(0)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrDr�ha(1)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrSt�ny
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    With obrDr�ha(dzStTex)
    .Move opStupn�.Width, 0, .Width - .ScaleWidth _
    + opRozm�ry.���ka + 2, _
    .Height - .ScaleHeight _
    + opRozm�ry.V��ka + 2
    End With
    Width = Width - ScaleWidth * TPPX + (obrDr�ha(0).Width + opStupn�.Width) * TPPX
    Dim Minim_Y, Je_pod_min_Y
    Minim_Y = obrDr�ha(0).Height - obrDr�ha(0).ScaleHeight + 320 ' jako minim�ln� vej�ka opStupn�
    Je_pod_min_Y = obrDr�ha(0).Height <= Minim_Y
    Height = Height - ScaleHeight * TPPY + (sbrStav.Height + IIf(Je_pod_min_Y, Minim_Y, obrDr�ha(0).Height)) * TPPY
    DoEvents
    Static Pov�ce As Boolean
    If Not Pov�ce Then
        Pov�ce = Not Pov�ce
        Exit Sub
    End If
End Sub

Private Sub O��zniMapu()
    Dim ���ka As Long, V��ka As Long
    Dim P�v� As Long, P�vV As Long
    On Error Resume Next
    P�v� = UBound(Mapa, 1)
    P�vV = UBound(Mapa, 2)
    On Error GoTo 0
    ���ka = opRozm�ry.���ka + 2
    V��ka = opRozm�ry.V��ka + 2
    Sma�R�me�ek
    Screen.MousePointer = vbHourglass
'    MousePointer = vbHourglass
    ReDim Nov�Mapa(���ka - 1, V��ka - 1)
    Dim X As Long, Y As Long
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            On Error Resume Next
            Nov�Mapa(X, Y) = Mapa(X, Y)
        Next Y
    Next X
    'DoEvents
'    MousePointer = vbHourglass
    ReDim Mapa(���ka - 1, V��ka - 1)
    For X = 0 To ���ka - 1
        For Y = 0 To V��ka - 1
            'On Error Resume Next
            Mapa(X, Y) = Nov�Mapa(X, Y)
        Next Y
    Next X
    P�idejR�me�ek
    UplatniMapu P�v�, P�vV
    Screen.MousePointer = vbDefault
End Sub

Private Sub UplatniMapu(Optional Za�X As Long, Optional Za�Y As Long)
    obrDr�ha(dzReli�f).Enabled = False
    obrDr�ha(dzReli�f).Visible = True
    obrDr�ha(dzStTex).Visible = False
    obrDr�ha(dzTextura).Visible = False
    Umo�n�no = False
    MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, pamV��ka As Long, pam���ka As Long
    pam���ka = UBound(Mapa, 1)
    pamV��ka = UBound(Mapa, 2)
    For Y = Za�X To pamV��ka
        For X = Za�Y To pam���ka
            ' Stupn� u� to kontrolujou
            SetPixel obrDr�ha(dzReli�f).hdc, X, Y, opStupn�.BarvaPodleV��ky(Mapa(X, Y))
            'obrDr�ha(dzReli�f).PSet (X, Y), opStupn�.BarvaPodleV��ky(Mapa(X, Y))
        Next
        obrDr�ha(dzReli�f).Refresh
        Stav = "Kresl�m reli�f: ��dek " & Y & " (Stop: Escape)"
        DoEvents
        If P�eru�itProces Then P�eru�itProces = False: Exit For
    Next
    obrDr�ha(dzReli�f).Enabled = True
    Uka�Obr�zekPodleNab�dky
    Umo�n�no = True
    MousePointer = vbDefault
End Sub

Private Sub Stavba(Index As Integer)
    MousePointer = vbHourglass
    Refresh
    Dim X As Long, Y As Long, Levobok As Long, Vr�ek As Long
    Dim V��ka As Long, ���ka As Long
    frmEditorDrah.Akce = aPrvek
    With obdPoloha(Index)
        Levobok = .Left
        Vr�ek = .Top
        ���ka = .Width
        V��ka = .Height
        'If Stavebn�Povolen�(dzReli�f) Then
        Z�sobn�k.Rozm�� ���ka, V��ka
        'If Stavebn�Povolen�(dzTextura) Then
        Me.obrPamObr.PaintPicture obrDr�ha(dzTextura).Image, 0, 0, , , .Left, .Top, .Width, .Height
'        ReDim Sd�len�MapaPrvku(.Width - 1, .Height - 1)
    End With
    If Stavebn�Povolen�(dzReli�f) Then
        On Error Resume Next
        For Y = Vr�ek To Vr�ek + V��ka - 1
            For X = Levobok To Levobok + ���ka - 1
                Z�sobn�k(X - Levobok, Y - Vr�ek) = Mapa(X, Y)
            Next X
        Next Y
    End If
    If Stavebn�Povolen�(dzTextura) Then
        Z�sobn�k.Textura = Me.obrPamObr.Image
    End If
'    frmEditorDrah.Na�tiSd�lenouMapuPrvku
    MousePointer = vbDefault
End Sub

Private Sub Vyst�nuj()
    If P�eru�itProces Then
        Exit Sub
    End If
    Dim X, Y, St�nX, St�nY, V��kaSt�nu
    obrSt�ny.Cls
    obrSt�ny.BackColor = vbWhite
    obrDr�ha(dzReli�f).Visible = False
    obrDr�ha(dzTextura).Visible = False
    obrDr�ha(dzStTex).Visible = False
    obrSt�ny.Visible = True
    DoEvents
    obrSt�ny.MousePointer = vbHourglass
    For Y = 0 To obrDr�ha(dzStTex).ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 0 To obrDr�ha(dzStTex).ScaleWidth - 2
            If X = 0 Or Y = 0 Then
                V��kaSt�nu = opRozm�ry.M��kaX
            Else
                V��kaSt�nu = Mapa(X, Y) + (Not frmEditorDrah.nabSyt�St�ny.Checked) * 2
            End If
            If X + 2 < UBound(Mapa, 1) Then
                If Mapa(X + 1, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 2, Y + 1) < Mapa(X, Y) Then
                    St�nX = X + 2
                    St�nY = Y + 1
                Else
                    St�nX = X + 1
                    St�nY = Y + 1
                End If
            Else
                St�nX = X + 1
                St�nY = Y + 1
            End If
            Do Until Mapa(Int(St�nX), Int(St�nY)) >= V��kaSt�nu
                'Stop
                If obrSt�ny.Point(St�nX, St�nY) _
                <> �ed� Then
                    Debug.Assert obrSt�ny.Point(St�nX, St�nY) = &HFFFFFF
                    'obrSt�ny.PSet (St�nX, St�nY), �ed�
                End If
                obrSt�ny.Line (St�nX, St�nY)-(St�nX + 1, St�nY + 1.5), �ed�
                'obrSt�ny.PSet (St�nX, St�nY), �ed�
                St�nX = St�nX + 1
                St�nY = St�nY + 1 / 2
                V��kaSt�nu = V��kaSt�nu - 2
                'Debug.Assert Not St�nX = 320
                If St�nX >= obrDr�ha(dzStTex).ScaleWidth - 1 _
                Or St�nY >= obrDr�ha(dzStTex).ScaleHeight - 1 _
                Then Exit Do
            Loop
            'To je pomal�!
            'obrSt�ny.Line (X + 1, Y + 1)-(St�nX, St�nY), �ed�
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Kresl�m st�ny � " & Y & " (Stop: Escape)"
    Next Y
    For Y = 1 To obrDr�ha(dzTextura).ScaleHeight - 2
        obrSt�ny.MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzTextura).ScaleWidth - 2
            ' Retu�e p�esahuj�c�ch st�n�
            If Mapa(X, Y - 1) < Mapa(X, Y) - 1 _
            And obrSt�ny.Point(X, Y) <> vbWhite _
            And (obrSt�ny.Point(X, Y + 1) = vbWhite _
             And obrSt�ny.Point(X + 1, Y + 1) = vbWhite _
             Or (obrSt�ny.Point(X - 1, Y - 1) = vbWhite _
              And Mapa(X, Y) = Mapa(X - 1, Y - 1) _
             And Mapa(X, Y) > Mapa(X + 1, Y + 1))) _
            And Y < UBound(Mapa, 2) Then
                'Debug.Assert False
                SetPixel obrSt�ny.hdc, X, Y, vbWhite
                'obrSt�ny.PSet (X, Y), vbWhite
            End If
            ' Retu�e mezer mezi rohy
            If obrSt�ny.Point(X + 1, Y) <> vbWhite _
            And obrSt�ny.Point(X, Y + 1) <> vbWhite Then
                If Mapa(X, Y + 1) = Mapa(X, Y) _
                And Mapa(X + 1, Y) = Mapa(X, Y) Then
                    obrSt�ny.PSet (X, Y), �ed�
                End If
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Retu�uji st�ny � �. " & Y & " (Stop: Escape)"
    Next Y
    ' Vyb�lit r�me�ek
    obrSt�ny.Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    Refresh
    obrDr�ha(dzStTex).Picture = obrDr�ha(dzTextura).Image
    obrSt�ny.Visible = False
    obrDr�ha(dzStTex).Visible = True
    Dim Max&, R&, G&, B&
    Max = �erven�(BarvaSS)
    If Zelen�(BarvaSS) > Max Then Max = Zelen�(BarvaSS)
    If Modr�(BarvaSS) > Max Then Max = Modr�(BarvaSS)
    Max = Max + 64
    For Y = 1 To obrDr�ha(dzStTex).ScaleHeight - 1
        obrDr�ha(dzStTex).MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzStTex).ScaleWidth - 1
            If obrSt�ny.Point(X, Y) <> vbWhite Then
                R = �erven�(obrDr�ha(dzStTex).Point(X, Y)) + �erven�(BarvaSS) - Max
                G = Zelen�(obrDr�ha(dzStTex).Point(X, Y)) + Zelen�(BarvaSS) - Max
                B = Modr�(obrDr�ha(dzStTex).Point(X, Y)) + Modr�(BarvaSS) - Max
                If R < 0 Then R = 0
                If G < 0 Then G = 0
                If B < 0 Then B = 0
                SetPixel obrDr�ha(dzStTex).hdc, X, Y, _
                RGB(R, G, B)
            End If
            If ObtHrany Then
                If X < obrDr�ha(dzStTex).ScaleWidth - 2 And Y < obrDr�ha(dzStTex).ScaleHeight - 2 Then
                    If (Mapa(X + 1, Y) > Mapa(X, Y) + 2 Or Mapa(X, Y + 1) > Mapa(X, Y) + 2) Then
                        SetPixel obrDr�ha(dzStTex).hdc, X, Y, BarvaOs
                    End If
                End If
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            GoTo p�eru�eno
        End If
        Stav = "Aplikuji st�ny � �. " & Y & " (Stop: Escape)"
    Next Y
p�eru�eno:
    Stav = P�ipraven
End Sub

Private Sub Osv�tli()
    If Not Zv�raz�ovat Then GoTo p�eru�eno
    If P�eru�itProces Then
        P�eru�itProces = False
        GoTo p�eru�eno
    End If
    Dim X, Y
    obrDr�ha(dzStTex).Visible = True
    obrSt�ny.Visible = False
    For Y = 1 To obrDr�ha(dzStTex).ScaleHeight - 1
        obrDr�ha(dzStTex).MousePointer = vbHourglass
        For X = 1 To obrDr�ha(dzStTex).ScaleWidth - 1
            If (obrSt�ny.Point(X, Y) = vbWhite _
            Or frmEditorDrah.nabZvPodSt�ny.Checked) _
            And frmEditorDrah.nabZv�raz�ovat.Checked Then
                Dim R As Long, G As Long, B As Long
                Dim P�id�k As Long, PX, PY
                If X = 1 Then
                    PX = Mapa(X, Y)
                Else
                    PX = Mapa(X - 1, Y)
                End If
                If Y = 1 Then
                    PY = Mapa(X, Y)
                Else
                    PY = Mapa(X, Y - 1)
                End If
                P�id�k = (Mapa(X, Y) - PX) * 64 _
                + (Mapa(X, Y) - PY) * 32
                Dim MaxP�id As Integer, MaxUbr As Integer
                MaxP�id = 96
                MaxUbr = -96
                If P�id�k > MaxP�id Then P�id�k = MaxP�id
                If P�id�k < MaxUbr Then P�id�k = MaxUbr
                R = �erven�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * �erven�(BarvaOs) / 255 - 32 + �erven�(BarvaOs) / 8
                G = Zelen�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * Zelen�(BarvaOs) / 255 - 32 + Zelen�(BarvaOs) / 8
                B = Modr�(obrDr�ha(dzStTex).Point(X, Y)) + P�id�k * Modr�(BarvaOs) / 255 - 32 + Modr�(BarvaOs) / 8
                If R < 0 Then R = 0
                If G < 0 Then G = 0
                If B < 0 Then B = 0
                On Error Resume Next
                SetPixelV obrDr�ha(dzStTex).hdc, X, Y, RGB(R, G, B)
                'obrDr�ha(dzStTex).PSet (X, Y), RGB(R, G, B)
                On Error GoTo 0
            End If
        Next X
        If Y Mod 8 = 0 Then DoEvents
        If P�eru�itProces Then
            P�eru�itProces = False
            GoTo p�eru�eno
        End If
    Stav = "St�nuji �ikm� plochy a hrany � " & Y & " (Stop: Escape)"
    Next Y
p�eru�eno:
    obrDr�ha(dzStTex).MousePointer = vbDefault
    obrSt�ny.MousePointer = vbDefault
    MousePointer = vbDefault
    Stav = P�ipraven
    Umo�n�no = True
End Sub

Private Sub Uka�Obr�zekPodleNab�dky()
    obrSt�ny.Visible = False
    obrDr�ha(dzStTex).Visible = nabSt�ny.Checked
    obrDr�ha(0).Visible = nabReli�f.Checked
    obrDr�ha(1).Visible = nabTextura.Checked
End Sub

Private Sub P�idejR�me�ek()
    Dim I
    Dim V��kaNebety�n� 'takov� vej�ka, aby se vod n�
                       'ur�it� vodrazil
    'Tahle d�l� moc dlouhej st�n (ale d� se zkr�tit):
    V��kaNebety�n� = 1000
    'Tak rad�i tuhle (rad�i ne, proto�e by se vod n�
                     'nejsp� nevodrazil):
    'V��kaNebety�n� = 64
    MousePointer = vbHourglass
    Refresh
    For I = 0 To opRozm�ry.���ka + 2 - 1
        Mapa(I, 0) = V��kaNebety�n�
        Mapa(I, opRozm�ry.V��ka + 2 - 1) = V��kaNebety�n�
    Next
    For I = 0 To opRozm�ry.V��ka + 2 - 1
        Mapa(0, I) = V��kaNebety�n�
        Mapa(opRozm�ry.���ka + 2 - 1, I) = V��kaNebety�n�
    Next
    'Vyzna�it okraj na reli�fu
    obrDr�ha(0).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    'Vyzna�it okraj i na textu�e (nevim, jak by to jin�� �lo)
    '                            (kv�li st�n�m a map�)
    obrDr�ha(1).Line (0, 0)-(opRozm�ry.���ka + 2 - 1, opRozm�ry.V��ka + 2 - 1), &HFFFFFF, B
    DoEvents
    MousePointer = vbDefault
End Sub

Private Sub Sma�R�me�ek()
    Dim I, ���ka, V��ka
    ���ka = UBound(Mapa, 1)
    V��ka = UBound(Mapa, 2)
    MousePointer = vbHourglass
    Refresh
    For I = 0 To ���ka
        Mapa(I, V��ka) = 0
    Next
    For I = 0 To V��ka
        Mapa(���ka, I) = 0
    Next
    obrDr�ha(0).Line (0, 0)-(���ka, V��ka), opStupn�.BarvaPodleV��ky(0), B
    obrDr�ha(1).Line (0, 0)-(���ka, V��ka), vbWhite, B
    DoEvents
    MousePointer = vbDefault
End Sub

Private Property Let Umo�n�no(Nosi� As Boolean)
    Dim Kontrolka As Control
    For Each Kontrolka In Me.Controls
        Select Case Kontrolka.Name
        Case "CD", "CDObr", _
        "opRozm�ry", "obrSt�ny", "UXYZ"
            'nic
        Case "obdPoloha"
            Kontrolka.Visible = Nosi�
        Case Else
            If Left(Kontrolka.Name, 6) <> "nabSep" Then Kontrolka.Enabled = Nosi�
        End Select
    Next
End Property

Private Property Get Umo�n�no() As Boolean
    Umo�n�no = nabSoubor.Enabled
End Property

Private Sub opRozmV�b_Nastaven�()
    Dim Obd As Shape
    For Each Obd In obdPoloha
        Obd.Width = opRozmV�b.���ka
        Obd.Height = opRozmV�b.V��ka
    Next
End Sub

Private Sub opRozmV�b_Ru�n�Zm�na()
    opRozmV�b_Nastaven�
End Sub

Public Property Let St�edit(Nosi� As Boolean)
    nabSt�edit.Checked = Nosi�
    Ulo�Nastaven� "Nastaven�", "St�edit na sb�rce", Nosi�
End Property

Public Property Get St�edit() As Boolean
    St�edit = DejNastaven�("Nastaven�", "St�edit na sb�rce", True)
End Property

Private Sub sbrStav_PanelClick(ByVal Panel As msComctlLib.Panel)
    Select Case Panel.Index
    Case 2
        If Panel.Enabled Then Stavebn�Povolen�(dzTextura) = Not Stavebn�Povolen�(dzTextura)
    Case 3
        If Panel.Enabled Then Stavebn�Povolen�(dzReli�f) = Not Stavebn�Povolen�(dzReli�f)
    Case 4
        TrBGra = Not TrBGra
    Case 5
        TrBRel = Not TrBRel
    End Select
End Sub


