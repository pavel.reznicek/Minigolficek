VERSION 5.00
Begin VB.UserControl Knofl�k 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   BackStyle       =   0  'Transparent
   ClientHeight    =   990
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2910
   EditAtDesignTime=   -1  'True
   KeyPreview      =   -1  'True
   MaskColor       =   &H00FFFFFF&
   MaskPicture     =   "Knofl��ek.ctx":0000
   PropertyPages   =   "Knofl��ek.ctx":00AA
   ScaleHeight     =   990
   ScaleWidth      =   2910
   ToolboxBitmap   =   "Knofl��ek.ctx":00C0
   Begin VB.PictureBox obrRozm�ry 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   720
      ScaleHeight     =   735
      ScaleWidth      =   1335
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "Knofl�k"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event Click()
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event DblClick()
Dim Zma�knuto As Boolean
Dim pamStisk As IPictureDisp
Dim pamTl As IPictureDisp

Private Sub UserControl_DblClick()
    Picture = Stisknut�Tla��tko
    Refresh
    Picture = Tla��tko
    RaiseEvent DblClick
End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
    Case vbKeyReturn
        Picture = Stisknut�Tla��tko
    Case vbKeySpace
        Picture = Stisknut�Tla��tko
    Case Else
    End Select
    DoEvents
End Sub

Private Sub UserControl_KeyUp(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
    Case vbKeyReturn
        Picture = Tla��tko
        RaiseEvent Click
    Case vbKeySpace
        Picture = Tla��tko
        RaiseEvent Click
    Case Else
    End Select
    DoEvents
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then DoEvents: Exit Sub
    If Button = MouseButtonConstants.vbLeftButton Then
        Picture = Stisknut�Tla��tko
    End If
    RaiseEvent MouseDown(Button, Shift, X, Y)
    DoEvents
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then DoEvents: Exit Sub
    Static MinMimo As Boolean
    If Button = vbLeftButton Then
        If 0 <= X And X < ScaleWidth _
        And 0 <= Y And Y < ScaleHeight Or _
        obrRozm�ry.Point(X, Y) <> vbBlack Then
            If MinMimo Then
                Picture = Stisknut�Tla��tko
                MinMimo = False
            End If
        Else
            If MinMimo Then
                Picture = Tla��tko
                MinMimo = True
            End If
        End If
    End If
    RaiseEvent MouseMove(Button, Shift, X, Y)
    DoEvents
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then DoEvents: Exit Sub
    If Button = vbLeftButton Then
        Zma�knuto = False
        Picture = Tla��tko
        If 0 <= X And X < ScaleWidth _
        And 0 <= Y And Y < ScaleHeight Then
            RaiseEvent Click
        End If
    End If
    RaiseEvent MouseUp(Button, Shift, X, Y)
    DoEvents
End Sub

Public Property Let Tla��tko(Nosi� As IPictureDisp)
    Set pamTl = Nosi�
    Picture = Nosi�
    PropertyChanged "Tla��tko"
End Property

Public Property Set Tla��tko(Nosi� As IPictureDisp)
    Set pamTl = Nosi�
    Picture = Nosi�
    PropertyChanged "Tla��tko"
End Property

Public Property Get Tla��tko() As IPictureDisp
    Set Tla��tko = pamTl
End Property

Public Property Let Stisknut�Tla��tko(Nosi� As IPictureDisp)
    Set pamStisk = Nosi�
    PropertyChanged "Stisknut�Tla��tko"
End Property

Public Property Set Stisknut�Tla��tko(Nosi� As IPictureDisp)
    Set pamStisk = Nosi�
    PropertyChanged "Stisknut�Tla��tko"
End Property

Public Property Get Stisknut�Tla��tko() As IPictureDisp
    Set Stisknut�Tla��tko = pamStisk
End Property

Public Property Let Maska(Nosi� As IPictureDisp)
    obrRozm�ry.Picture = Nosi�
    UserControl.MaskPicture = obrRozm�ry.Picture
    UserControl_Resize
    PropertyChanged "Maska"
End Property

Public Property Set Maska(Nosi� As IPictureDisp)
    obrRozm�ry.Picture = Nosi�
    UserControl.MaskPicture = obrRozm�ry.Picture
    UserControl_Resize
    PropertyChanged "Maska"
End Property

Public Property Get Maska() As IPictureDisp
    Set Maska = MaskPicture
End Property

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Private Sub UserControl_Resize()
    Static Vypadni As Boolean
    If Vypadni Then Exit Sub
    Vypadni = True
    If UserControl.Width <> obrRozm�ry.Width Or UserControl.Height <> obrRozm�ry.Height Then
        Size obrRozm�ry.Width, obrRozm�ry.Height
    End If
    Vypadni = False
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Maska = .ReadProperty("Maska", LoadPicture(App.Path & "\maska knofl�ku.bmp"))
        Tla��tko = .ReadProperty("Tla��tko", LoadPicture(App.Path & "\knofl�k.bmp"))
        Stisknut�Tla��tko = .ReadProperty("Stisknut�Tla��tko", LoadPicture(App.Path & "\knofl�k.bmp"))
        Enabled = .ReadProperty("Enabled", True)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Maska", Maska, LoadPicture(App.Path & "\maska knofl�ku.bmp")
        .WriteProperty "Tla��tko", Tla��tko, LoadPicture(App.Path & "\knofl�k.bmp")
        .WriteProperty "Stisknut�Tla��tko", Stisknut�Tla��tko, LoadPicture(App.Path & "\knofl�k.bmp")
        .WriteProperty "Enabled", Enabled, True
    End With
End Sub
