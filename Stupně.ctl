VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl Stupn� 
   Alignable       =   -1  'True
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3015
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3480
   ScaleHeight     =   201
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   232
   Begin MSComDlg.CommonDialog CD 
      Left            =   2760
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin ComCtl2.UpDown udStupe� 
      Height          =   240
      Left            =   2040
      TabIndex        =   3
      ToolTipText     =   "Setiny"
      Top             =   2400
      Width           =   180
      _ExtentX        =   344
      _ExtentY        =   423
      _Version        =   327681
      OrigLeft        =   96
      OrigTop         =   160
      OrigRight       =   109
      OrigBottom      =   177
      Max             =   15000
      Min             =   -5000
      Wrap            =   -1  'True
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtStupe� 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   960
      MultiLine       =   -1  'True
      TabIndex        =   2
      Text            =   "Stupn�.ctx":0000
      Top             =   2400
      Width           =   852
   End
   Begin VB.PictureBox obrStupn� 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   480
      MousePointer    =   7  'Size N S
      ScaleHeight     =   113
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   113
      TabIndex        =   0
      Top             =   480
      Width           =   1695
      Begin VB.Line Ln 
         BorderColor     =   &H00000000&
         DrawMode        =   6  'Mask Pen Not
         X1              =   16
         X2              =   80
         Y1              =   70
         Y2              =   70
      End
      Begin VB.Line LnC�l 
         BorderColor     =   &H00FF00FF&
         X1              =   16
         X2              =   80
         Y1              =   88
         Y2              =   88
      End
   End
   Begin ComCtl2.UpDown ud1 
      Height          =   240
      Left            =   1800
      TabIndex        =   4
      ToolTipText     =   "Jednotky"
      Top             =   2400
      Width           =   180
      _ExtentX        =   344
      _ExtentY        =   423
      _Version        =   327681
      OrigLeft        =   170
      OrigTop         =   200
      OrigRight       =   186
      OrigBottom      =   220
      Increment       =   100
      Max             =   15000
      Min             =   -5000
      Wrap            =   -1  'True
      Enabled         =   -1  'True
   End
   Begin VB.Label lblTer�n 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "&Ter�n:"
      Height          =   195
      Left            =   360
      TabIndex        =   1
      Top             =   2400
      Width           =   465
   End
   Begin VB.Menu nabNast 
      Caption         =   "<popup>"
      Begin VB.Menu nabNastaven� 
         Caption         =   "&Nastaven� barev�"
      End
      Begin VB.Menu nabSep1 
         Caption         =   "-"
      End
      Begin VB.Menu nabP�ebarvit 
         Caption         =   "&P�ebarvit reli�f"
      End
      Begin VB.Menu nabPe�liv� 
         Caption         =   "Pe�liv� &odst�ny"
      End
      Begin VB.Menu nabSep2 
         Caption         =   "-"
      End
      Begin VB.Menu nabVynulovat 
         Caption         =   "&Vynulovat"
      End
      Begin VB.Menu nabSep3 
         Caption         =   "-"
      End
      Begin VB.Menu nabZav��t 
         Caption         =   "Pry� (Escape)"
      End
   End
End
Attribute VB_Name = "Stupn�"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "PropPageWizardRun" ,"Yes"
Option Explicit

Dim pamPo�etBarev As Integer
Const P�esnost = 100
Public Event Change()
Public Event ColorChange()
Public Event ColorSelect()
'Dim PoleBarev() As Long
Dim InicBarvy(-1 To 5) As Long
Public Precisn�Barvy As Boolean

Private Sub DefInicBarvy()
    InicBarvy(-1) = vbMagenta
    InicBarvy(0) = RGB(128, 128, 20)
    InicBarvy(1) = vbWhite
    InicBarvy(2) = vbBlack
    InicBarvy(3) = vbGreen
    InicBarvy(4) = vbYellow
    InicBarvy(5) = vbBlue
End Sub

Public Property Let Width(Nosi� As Single)
    Extender.Width = Nosi�
End Property

Public Property Get Width() As Single
    Width = Extender.Width
End Property

Public Property Let Height(Nosi� As Single)
    Extender.Height = Nosi�
End Property

Public Property Get Height() As Single
    Height = Extender.Height
End Property

Public Property Let Left(Nosi� As Single)
    Extender.Left = Nosi�
End Property

Public Property Get Left() As Single
    Left = Extender.Left
End Property

Public Property Let Top(Nosi� As Single)
    Extender.Top = Nosi�
End Property

Public Property Get Top() As Single
    Top = Extender.Top
End Property

Public Property Let Po�etNadzBarev(Nosi� As Integer)
    Ulo�Nastaven� Extender.Parent.Name & "\Barvy", "Po�et", Nosi�
    'ReDim Preserve PoleBarev(-1 To Nosi�)
    Nabarvi
End Property

Public Property Get Po�etNadzBarev() As Integer
    On Error Resume Next
    Po�etNadzBarev = DejNastaven�(Extender.Parent.Name & "\Barvy", "Po�et", 5)
End Property

Public Property Let BarvaPatra(ByVal Patro As Integer, Nosi� As OLE_COLOR)
    'If BarvaPatra(Patro) = Nosi� Then Exit Property
'    PoleBarev(Patro) = Nosi�
    Ulo�Nastaven� UserControl.Extender.Parent.Name & "\Barvy", "Barva" & Patro, Nosi�
    PropertyChanged "Barva"
    Nabarvi
    RaiseEvent ColorChange
End Property

Public Property Get BarvaPatra(ByVal Patro As Integer) As OLE_COLOR
'    On Error Resume Next
    Dim IniBarva As Long
'    DefInicBarvy
    If Patro <= 5 And Patro >= -1 Then
        IniBarva = InicBarvy(Patro)
    End If
    BarvaPatra = DejNastaven�(UserControl.Extender.Parent.Name & "\Barvy", "Barva" & Patro, IniBarva)
    'Debug.Assert Not (IniBarva = 0 And Patro <= 5 And Patro >= -1)
    'Debug.Assert Not BarvaPatra = 0
    'BarvaPatra = PoleBarev(Patro)
End Property

Public Property Let V��ka(Nosi� As Single)
    udStupe� = Nosi� * P�esnost
    PropertyChanged "V��ka"
End Property

Public Property Get V��ka() As Single
    V��ka = udStupe� / P�esnost
End Property

Public Function Barva() As OLE_COLOR
    If V��ka = V��kaC�le Then
        Barva = BarvaPatra(-1)
    Else
        If Not Precisn�Barvy Then
            Barva = obrStupn�.Point(0, -V��ka * P�esnost - (obrStupn�.ScaleHeight - V��ka * P�esnost) / V��kaVzorku)
        Else
            Barva = BarvaPodleV��ky(V��ka)
        End If
    End If
End Function

Public Function BarvaPodleV��ky(TaV��ka As Single) As OLE_COLOR
    Dim Rozd�&, RozdZ&, RozdM&, �&, Z&, M&, Zb&
    Dim D�l&, Za�B&, KonB&, Za�&, Kon&
    
    If TaV��ka = V��kaC�le Then
        BarvaPodleV��ky = BarvaPatra(-1)
    Else
        If Precisn�Barvy Then
            D�l = Max / (Po�etNadzBarev - 1)
            If TaV��ka < 0 Then
                D�l = Abs(Min)
                Za� = -1
                Kon = 0
            Else
                Za� = TaV��ka \ D�l + 1
                Kon = TaV��ka \ D�l + 2
            End If
            Za�B = BarvaPatra(Za�)
            KonB = BarvaPatra(Kon)
            Zb = (TaV��ka) Mod D�l
            If Zb = 0 Then
                Rozd� = (�erven�(KonB) - �erven�(Za�B))
                RozdZ = (�erven�(KonB) - �erven�(Za�B))
                RozdM = (�erven�(KonB) - �erven�(Za�B))
            Else
                Rozd� = (�erven�(KonB) - �erven�(Za�B))
                RozdZ = (Zelen�(KonB) - Zelen�(Za�B))
                RozdM = (Modr�(KonB) - Modr�(Za�B))
            End If
            � = �erven�(Za�B) + Rozd� * Zb / D�l
            Z = Zelen�(Za�B) + RozdZ * Zb / D�l
            M = Modr�(Za�B) + RozdM * Zb / D�l
            BarvaPodleV��ky = RGB(�, Z, M)
        Else
            BarvaPodleV��ky = obrStupn�.Point(0, -TaV��ka * P�esnost - (obrStupn�.ScaleHeight - TaV��ka * P�esnost) / CLng(V��kaVzorku))
        End If
    End If
End Function

Public Property Let Min(Nosi� As Long)
    udStupe�.Min = Nosi� * P�esnost
    ud1.Min = Nosi� * P�esnost
    obrStupn�.ScaleHeight = udStupe�.Max - udStupe�.Min
    obrStupn�.ScaleTop = -obrStupn�.ScaleHeight - udStupe�.Min
    Nabarvi
    Vyzna�Hodnotu
End Property

Public Property Get Min() As Long
    Min = udStupe�.Min / P�esnost
End Property

Public Property Let Max(Nosi� As Long)
    udStupe�.Max = Nosi� * P�esnost
    ud1.Max = Nosi� * P�esnost
    obrStupn�.ScaleHeight = udStupe�.Max - udStupe�.Min
    obrStupn�.ScaleTop = -obrStupn�.ScaleHeight - udStupe�.Min
    PropertyChanged "Max"
    Nabarvi
    Vyzna�Hodnotu
End Property

Public Property Get Max() As Long
    Max = udStupe�.Max / P�esnost
End Property

Public Property Let V��kaVzorku(Nosi� As Single)
    UserControl.Height = UserControl.Height - ScaleHeight * TPPY + (Nosi� + txtStupe�.Height) * TPPY
    PropertyChanged "V��kaVzorku"
End Property

Public Property Get V��kaVzorku() As Single
    V��kaVzorku = obrStupn�.Height
End Property

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    Dim Ctl As Control
    For Each Ctl In UserControl.Controls
        If Not TypeOf Ctl Is Line _
        And Not TypeOf Ctl Is CommonDialog _
        And Not TypeOf Ctl Is Menu Then
            Ctl.Enabled = Nosi�
        End If
    Next
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Public Sub Nabarvi()
    'If Not Ambient.UserMode Then Exit Sub
    Dim I As Double, j As Integer
    Dim �D�l As Double, ZD�l As Double, MD�l As Double
    Dim Sou�� As Double, Sou�Z As Double, Sou�M As Double
    Dim Za��#, Za�Z#, Za�M#
    Dim Krok
    Krok = CDec(obrStupn�.ScaleHeight / V��kaVzorku)
    
    �D�l = 255 / (obrStupn�.ScaleHeight)
    ZD�l = 255 / (obrStupn�.ScaleHeight)
    MD�l = 255 / (obrStupn�.ScaleHeight)
    Dim Spodek&, Vr�ek&, D�lka&, PosV��ka&, NegV��ka&
    Vr�ek = obrStupn�.ScaleTop
    Spodek = obrStupn�.ScaleTop + obrStupn�.ScaleHeight - Krok
    D�lka = Spodek - Vr�ek
    PosV��ka = -Vr�ek + Krok
    NegV��ka = -Spodek
'    On Error Resume Next
    �k�la NegV��ka, 0, BarvaPatra(0), BarvaPatra(1)
    Dim L As Integer, Nejv As Integer
    Nejv = Po�etNadzBarev
    For L = 1 To Po�etNadzBarev
        Call �k�la(PosV��ka * (L - 1) / (Nejv - 1), PosV��ka * L / (Nejv - 1), BarvaPatra(L), BarvaPatra(L + 1))
    Next
    '�k�la NegV��ka, 0, BarvaPodzem�, Barva1
    '�k�la 0, PosV��ka / 4, BarvaPatra(1), BarvaPatra(2)
    '�k�la PosV��ka / 4, PosV��ka / 2, BarvaPatra(2), BarvaPatra(3)
    '�k�la PosV��ka / 2, PosV��ka * 3 / 4, BarvaPatra(3), BarvaPatra(4)
    '�k�la PosV��ka * 3 / 4, PosV��ka, BarvaPatra(4), BarvaPatra(5)
    
    ' Pro c�l rad�i zvl�tn� ��rku (aby to neru�ilo podzem�)
    With LnC�l
        .X1 = 0
        .X2 = obrStupn�.ScaleWidth
        .Y1 = Spodek
        .Y2 = Spodek
        .BorderColor = BarvaPatra(-1)
    End With
End Sub

Private Sub �k�la(Za� As Long, Kon As Long, Za�Barva As Long, KonBarva As Long)
    Dim I As Double, j As Integer
    Dim �D�l As Double, ZD�l As Double, MD�l As Double
    Dim Sou�� As Double, Sou�Z As Double, Sou�M As Double
    Dim Za��#, Za�Z#, Za�M#, Krok#
    
    �D�l = (�erven�(KonBarva) - �erven�(Za�Barva)) / CDbl(Kon - Za�)
    ZD�l = (Zelen�(KonBarva) - Zelen�(Za�Barva)) / CDbl(Kon - Za�)
    MD�l = (Modr�(KonBarva) - Modr�(Za�Barva)) / CDbl(Kon - Za�)
    Za�� = �erven�(Za�Barva)
    Za�Z = Zelen�(Za�Barva)
    Za�M = Modr�(Za�Barva)
    
    Krok = (obrStupn�.ScaleHeight - 1) / V��kaVzorku
    
    For I = 0 To Kon - Za� Step Krok * Sgn(Kon - Za�)
    
            Sou�� = Za�� + �D�l * (I)
            Sou�Z = Za�Z + ZD�l * (I)
            Sou�M = Za�M + MD�l * (I)
    
        If Sou�� < 0 Then Sou�� = 0
        If Sou�Z < 0 Then Sou�Z = 0
        If Sou�M < 0 Then Sou�M = 0
        obrStupn�.Line (0, -(Za� + I))-(obrStupn�.ScaleWidth, -(Za� + I)), _
        RGB(Sou��, Sou�Z, Sou�M)
    Next
End Sub
'
'Private Sub nabBarvaC�le_Click()
'    CD.DialogTitle = "Vybrat barvu c�le"
'    CD.Color = BarvaPatra(-1)
'    CD.Flags = cdlCCRGBInit
'    On Error GoTo vypadni
'    CD.ShowColor
'    On Error GoTo 0
'    BarvaPatra(-1) = CD.Color
'    RaiseEvent ColorSelect
'vypadni:
'End Sub
'
'Private Sub nabBarvaPodzem�_Click()
'    CD.DialogTitle = "Vybrat barvu podzem�"
'    CD.Color = BarvaPatra(0)
'    CD.Flags = cdlCCRGBInit
'    On Error GoTo vypadni
'    CD.ShowColor
'    On Error GoTo 0
'    BarvaPatra(0) = CD.Color
'    RaiseEvent ColorSelect
'vypadni:
'End Sub

Private Sub nabNastaven�_Click()
    frmNast.Karta = 2
    frmNast.Show 0, Parent
End Sub

Private Sub nabPe�liv�_Click()
    Precisn�Barvy = Not Precisn�Barvy
    nabPe�liv�.Checked = Precisn�Barvy
End Sub

Private Sub nabP�ebarvit_Click()
    RaiseColorSelect
End Sub

Private Sub nabVynulovat_Click()
    udStupe� = 0
End Sub

Private Sub obrStupn�_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 93 Then UserControl.PopupMenu nabNast
End Sub

Private Sub obrStupn�_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Then Exit Sub
    With obrStupn�
    If Y < .ScaleTop Then
        udStupe� = -.ScaleTop
    ElseIf Y > .ScaleTop + .ScaleHeight Then
        udStupe� = -(.ScaleTop + .ScaleHeight)
    Else
        udStupe� = -Y + Y Mod P�esnost
    End If
    End With
End Sub

Private Sub obrStupn�_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Then Exit Sub
    With obrStupn�
    If Y < .ScaleTop Then
        udStupe� = -.ScaleTop
    ElseIf Y > .ScaleTop + .ScaleHeight Then
        udStupe� = -(.ScaleTop + .ScaleHeight)
    Else
        udStupe� = -Y + Y Mod P�esnost
    End If
    End With
End Sub

Private Sub obrStupn�_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    With obrStupn�
        Select Case Button
        Case vbLeftButton
            If Y < .ScaleTop Then
                udStupe� = -.ScaleTop
            ElseIf Y > .ScaleTop + .ScaleHeight Then
                udStupe� = -(.ScaleTop + .ScaleHeight)
            Else
                udStupe� = -Y + Y Mod P�esnost
            End If
        Case vbRightButton
            PopupMenu nabNast
        Case Else
        End Select
    End With
End Sub

Private Sub txtStupe�_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0") To Asc("9"), 8, Asc("-"), Asc("."), Asc(",")
        'nech tak
    Case 13
        'potvr� hodnotu
        If Val(txtStupe�) * P�esnost < udStupe�.Min Then
            udStupe� = udStupe�.Min
        ElseIf Val(txtStupe�) * P�esnost > udStupe�.Max Then
            udStupe� = udStupe�.Max
        Else
            'ud1 = Val(txtStupe�) * P�esnost
            udStupe� = HodS��rkou(txtStupe�) * P�esnost
        End If
        KeyAscii = 0
    Case Else
        'nic nepi�
        KeyAscii = 0
    End Select
End Sub

Private Sub ud1_Change()
    If udStupe� <> ud1 Then udStupe� = ud1
End Sub

Private Sub udStupe�_Change()
    Vyzna�Hodnotu
    If udStupe� <> ud1 Then ud1 = udStupe�
    'If udStupe� - 1 <> Val(txtStupe�) * P�esnost Then udStupe� = Val(txtStupe�) * P�esnost + 1
    txtStupe� = udStupe� / P�esnost
    RaiseEvent Change
End Sub

Private Sub Vyzna�Hodnotu()
    With obrStupn�
    .AutoRedraw = False
    .Refresh
    .DrawMode = DrawModeConstants.vbInvert
    Ln.X1 = 0
    Ln.Y1 = -udStupe� - (.ScaleHeight - udStupe�) / V��kaVzorku
    Ln.X2 = .ScaleWidth
    Ln.Y2 = -udStupe� - (.ScaleHeight - udStupe�) / V��kaVzorku
    .DrawMode = DrawModeConstants.vbCopyPen
    .AutoRedraw = True
    End With
End Sub

Private Sub UserControl_Initialize()
    DefTPP
    DefInicBarvy
    ud1.Increment = P�esnost
    BackColor = txtStupe�.BackColor
'    Na�tiBarvy
    Vyzna�Hodnotu
End Sub
'
'Private Sub Na�tiBarvy()
'    Dim I As Integer
'    Po�etNadzBarev = Po�etNadzBarev
'    On Error GoTo vypadni
'    For I = -1 To Po�etNadzBarev
'        BarvaPatra(I) = DejNastaven�(Extender.Parent.Name & "\Barvy", "Barva" & I, vbBlack)
'    Next
'vypadni:
'End Sub

Private Sub UserControl_InitProperties()
    DefInicBarvy
'    Na�tiBarvy
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        'Zm�na = .ReadProperty("Zm�na", 0.1)
        'Po�etNadzBarev = .ReadProperty("Po�etNadzBarev", 5)
        DefInicBarvy
'        Na�tiBarvy
        V��ka = .ReadProperty("V��ka", 0)
        V��kaVzorku = .ReadProperty("V��kaVzorku", obrStupn�.Height)
        N�pis = .ReadProperty("N�pis", lblTer�n)
        Min = .ReadProperty("Min", V��kaC�le)
        Max = .ReadProperty("Max", 128)
        Enabled = .ReadProperty("Enabled", True)
    End With
End Sub

Private Sub UserControl_Resize()
    'On Error Resume Next
    obrStupn�.Move 0, 0, ScaleWidth, ScaleHeight - txtStupe�.Height
    txtStupe�.Move lblTer�n.Width + 4, obrStupn�.Height, ScaleWidth - udStupe�.Width - ud1.Width - (lblTer�n.Width + 4)
    lblTer�n.Move 2, txtStupe�.Top + txtStupe�.Height / 2 - lblTer�n.Height / 2
    ud1.Move txtStupe�.Left + txtStupe�.Width, txtStupe�.Top
    udStupe�.Move ud1.Left + ud1.Width, txtStupe�.Top
    obrStupn�.Scale (0, -udStupe�.Max)-(obrStupn�.ScaleWidth, -udStupe�.Min)
    Nabarvi
    Vyzna�Hodnotu
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        DefInicBarvy
        .WriteProperty "Po�etNadzBarev", Po�etNadzBarev, 5
        .WriteProperty "Zm�na", Zm�na, 0.1
        .WriteProperty "V��ka", V��ka, 0
        .WriteProperty "V��kaVzorku", V��kaVzorku, obrStupn�.Height
        .WriteProperty "N�pis", N�pis, lblTer�n
        .WriteProperty "Min", Min, V��kaC�le
        .WriteProperty "Max", Max, 128
        .WriteProperty "Enabled", Enabled, True
    End With
End Sub

Public Property Let N�pis(Nosi� As String)
    lblTer�n = Nosi�
    PropertyChanged "N�pis"
End Property

Public Property Get N�pis() As String
    N�pis = lblTer�n
End Property
'
'Public Property Let P�esnost(Nosi� As Long)
'    pamP�esnost = Nosi�
'    PropertyChanged "P�esnost"
'End Property
'
'Public Property Get P�esnost() As Long
'    P�esnost = pamP�esnost
'End Property

Public Property Let Zm�na(Nosi� As Single)
    udStupe�.Increment = Nosi� * P�esnost
    PropertyChanged "Zm�na"
End Property

Public Property Get Zm�na() As Single
    Zm�na = udStupe�.Increment / P�esnost
End Property

Public Sub RaiseColorSelect()
    RaiseEvent ColorSelect
End Sub
