VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmN�pov�da 
   Caption         =   "N�pov�da k Minigolf��ku"
   ClientHeight    =   3195
   ClientLeft      =   6945
   ClientTop       =   570
   ClientWidth     =   4680
   Icon            =   "frmN�pov�da.frx":0000
   KeyPreview      =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   Begin RichTextLib.RichTextBox rtbN�pov�da 
      Height          =   3135
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   5530
      _Version        =   393217
      BorderStyle     =   0
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      RightMargin     =   4800
      TextRTF         =   $"frmN�pov�da.frx":030A
   End
End
Attribute VB_Name = "frmN�pov�da"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    On Error GoTo chyba
    rtbN�pov�da.FileName = App.Path & "\N�pov�da.rtf"
    Exit Sub
chyba:
    rtbN�pov�da.TextRTF = "{\rtf1\ansi\deff0\deftab720{\fonttbl{\f0\fnil MS Sans Serif;}{\f1\froman\fcharset2{\*\fname Symbol;}MT Symbol;}{\f2\fswiss\fcharset238{\*\fname MS Sans Serif;}MS Shell Dlg;}{\f3\fswiss\fcharset238{\*\fname MS Sans Serif;}MS Shell Dlg;}}" _
    & "{\colortbl\red0\green0\blue0;}" _
    & "\deflang1029\pard\plain\f3\fs17 Nemohu na\'e8\'edst n\'e1pov\'ecdu. Soubor \'84N\'e1pov\'ecda.rtf\'93 se z\'f8ejm\'ec pou\'9e\'edv\'e1 nebo chyb\'ed!" _
    & "\par }"
'    rtbN�pov�da.Text = "Nemohu na��st n�pov�du. Soubor ""N�pov�da.rtf"" se z�ejm� pou��v� nebo chyb�!"
End Sub

Private Sub Form_Resize()
    rtbN�pov�da.Move 0, 0, ScaleWidth, ScaleHeight
    rtbN�pov�da.RightMargin = ScaleWidth - 400
End Sub

