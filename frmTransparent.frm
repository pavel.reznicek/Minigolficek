VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTransparent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "V�b�r transparentn� barvy"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   5175
   ClientWidth     =   5475
   ControlBox      =   0   'False
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   5475
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CD 
      Left            =   1080
      Top             =   2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton tlZru�it 
      Cancel          =   -1  'True
      Caption         =   "&Zru�it"
      Height          =   375
      Left            =   1560
      TabIndex        =   7
      Top             =   2760
      Width           =   855
   End
   Begin VB.CommandButton tlOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   2760
      Width           =   855
   End
   Begin VB.PictureBox obrNov� 
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      ScaleHeight     =   795
      ScaleWidth      =   795
      TabIndex        =   4
      Top             =   1440
      Width           =   855
   End
   Begin VB.PictureBox obrNast 
      BackColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   120
      ScaleHeight     =   795
      ScaleWidth      =   795
      TabIndex        =   3
      Top             =   240
      Width           =   855
   End
   Begin Minigolf��ek.UkazatelXYZ UXYZNov� 
      Height          =   855
      Left            =   1080
      TabIndex        =   2
      Top             =   1440
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1508
      TextX           =   "�erven�: "
      TextY           =   "Zelen�: "
      TextZ           =   "Modr�: "
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin Minigolf��ek.UkazatelXYZ UXYZNast 
      Height          =   855
      Left            =   1080
      TabIndex        =   1
      Top             =   240
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1508
      TextX           =   "�erven�: "
      TextY           =   "Zelen�: "
      TextZ           =   "Modr�: "
      HodnotaX        =   "0"
      HodnotaY        =   "0"
      HodnotaZ        =   "0"
   End
   Begin VB.PictureBox obrV�b�r 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   2955
      Left            =   2760
      MousePointer    =   2  'Cross
      ScaleHeight     =   2895
      ScaleWidth      =   1995
      TabIndex        =   0
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Nov� transparentn� barva"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1200
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "St�vaj�c� transparentn� barva"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   2535
   End
End
Attribute VB_Name = "frmTransparent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim My�Dole As Boolean

Private Sub Form_Load()
    obrV�b�r.Move obrV�b�r.Left, obrV�b�r.Top, _
    ScaleWidth - obrV�b�r.Left, ScaleHeight - obrV�b�r.Top
    obrV�b�r.Picture = frmEditorDrah.obrPamObr.Image
    obrNast.BackColor = TrB
    UXYZNast.HodnotaX = �erven�(obrNast.BackColor)
    UXYZNast.HodnotaY = Zelen�(obrNast.BackColor)
    UXYZNast.HodnotaZ = Modr�(obrNast.BackColor)
    obrNov�.BackColor = TrB
    UXYZNov�.HodnotaX = �erven�(obrNast.BackColor)
    UXYZNov�.HodnotaY = Zelen�(obrNast.BackColor)
    UXYZNov�.HodnotaZ = Modr�(obrNast.BackColor)
    Width = Width - ScaleWidth + Pravobok(obrV�b�r) + obrNast.Left
    If Left + Width > Screen.Width Then Left = 0
    If Left + Width > Screen.Width Then
        Width = Screen.Width
    End If
    Height = Height - ScaleHeight + obrV�b�r.Top _
    + obrV�b�r.Height
    If Top + Height > Screen.Height Then Top = 0
    If Top + Height > Screen.Width Then
        Height = Screen.Width
    End If
End Sub

Private Sub Form_Resize()
    If ScaleWidth < obrV�b�r.Left + obrV�b�r.Width Then
        Width = Width - ScaleWidth + obrV�b�r.Left _
        + obrV�b�r.Width
    End If
    If ScaleHeight < tlOK.Top + tlOK.Height + 6 * 15 Then
        Height = Height - ScaleHeight + tlOK.Top + tlOK.Height + 8 * 15
    End If
End Sub

Private Sub obrNast_Click()
    obrNov�.BackColor = obrNast.BackColor
    With UXYZNov�
        .HodnotaX = �erven�(obrNov�.BackColor)
        .HodnotaY = Zelen�(obrNov�.BackColor)
        .HodnotaZ = Modr�(obrNov�.BackColor)
    End With
End Sub

Private Sub obrNov�_Click()
    CD.Flags = cdlCCRGBInit
    CD.Color = obrNov�.BackColor
    On Error GoTo vypadni
    CD.ShowColor
    On Error GoTo 0
    obrNov�.BackColor = CD.Color
    With UXYZNov�
        .HodnotaX = �erven�(obrNov�.BackColor)
        .HodnotaY = Zelen�(obrNov�.BackColor)
        .HodnotaZ = Modr�(obrNov�.BackColor)
    End With
vypadni:
    On Error GoTo 0
End Sub

Private Sub obrV�b�r_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    My�Dole = True
    obrNov�.BackColor = obrV�b�r.Point(X, Y)
    With UXYZNov�
        .HodnotaX = �erven�(obrNov�.BackColor)
        .HodnotaY = Zelen�(obrNov�.BackColor)
        .HodnotaZ = Modr�(obrNov�.BackColor)
    End With
End Sub

Private Sub obrV�b�r_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    If My�Dole Then
        obrNov�.BackColor = obrV�b�r.Point(X, Y)
        With UXYZNov�
            .HodnotaX = �erven�(obrNov�.BackColor)
            .HodnotaY = Zelen�(obrNov�.BackColor)
            .HodnotaZ = Modr�(obrNov�.BackColor)
        End With
    End If
End Sub

Private Sub obrV�b�r_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    If My�Dole Then
        obrNov�.BackColor = obrV�b�r.Point(X, Y)
        With UXYZNov�
            .HodnotaX = �erven�(obrNov�.BackColor)
            .HodnotaY = Zelen�(obrNov�.BackColor)
            .HodnotaZ = Modr�(obrNov�.BackColor)
        End With
    End If
    My�Dole = False
End Sub

Private Sub tlOK_Click()
    TrB = obrNov�.BackColor
    Ulo�Nastaven� "Barvy", "Transparentn�", obrNov�.BackColor
    Unload Me
End Sub

Private Sub tlZru�it_Click()
    Unload Me
End Sub
